//
//  APIHandler.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import Alamofire

internal class MyServerTrustPolicyManager: ServerTrustPolicyManager {
    // In order to trust all self-signed https certifications.
    open override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        return ServerTrustPolicy.disableEvaluation
    }
}

class APIHandler {
    
    static let sharedInstance : APIHandler = APIHandler()
    
    static let manager:SessionManager = {
        let configuration = URLSessionConfiguration.default
        let manager = SessionManager()
        return manager
        
        
//        let serverTrustPolicies: [String: ServerTrustPolicy] = [
//            "api.sdoky.com": .disableEvaluation
//        ]
//        let configuration = URLSessionConfiguration.default
//        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
//
//        Alamofire.SessionManager(
//            configuration: URLSessionConfiguration.default,
//            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
//        )
//
//        Alamofire.SessionManager.init(configuration: configuration, delegate: Alamofire.SessionDelegate.init(), serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
    
    private init() {
    }
    
    func getHttpManager() -> SessionManager {
        let sessionConfig = URLSessionConfiguration.default
       // sessionConfig.timeoutIntervalForRequest = timeout
       // sessionConfig.timeoutIntervalForResource = timeout

        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        sessionConfig.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders

        // Use customize policy
        let trustPolicies = MyServerTrustPolicyManager(policies: [:])

        let manager = Alamofire.SessionManager(configuration: sessionConfig, delegate: SessionDelegate(), serverTrustPolicyManager: trustPolicies)
        return manager
    }
    
    func setByPass() {
        let delegate: SessionDelegate = APIHandler.manager.delegate
        delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = APIHandler.manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            return (disposition, credential)
        }
    }
    
    func performGETRequest(_ apiString:NSString,isCachingNeeded:Bool?=false,params:NSDictionary,success:@escaping (_ response: AnyObject)->Void,failure:@escaping (_ error: NSError, _ response: AnyObject)->Void)->() {
        
        let formattedApiUrlAsString = apiString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let paramDict = params as! [String:Any]
        //let device = realm.objects(DeviceToken.self).first
        //let token = "Bearer " + (device ?? DeviceToken()).accessToken
        let token = "Bearer " + AppUtility.sharedInstance.getToken()
        self.setByPass()
        let requestAL = Alamofire.request(formattedApiUrlAsString!, method: .get, parameters: paramDict, headers: [
            "Authorization":token,
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json; odata=verbose"
        ])
        
        requestAL.validate(contentType: ["application/json"])
            .responseJSON { (response) in
                self.responseHandler(response, success: success, failure: failure)
        }
    }
    
    func performPOSTRequest(_ apiString:NSString,params:[String: AnyObject],success:@escaping (_ response: AnyObject)->Void,failure:@escaping (_ error: NSError, _ response: AnyObject)->Void)->() {
        
        let formattedApiUrlAsString = apiString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var token = ""
        token = "Bearer " + AppUtility.sharedInstance.getToken()
        //let device = realm.objects(DeviceToken.self).first
        // let token = "Bearer " + (device ?? DeviceToken()).accessToken
        
        
        
        
        let alamofireRequest = Alamofire.request(formattedApiUrlAsString!, method: .post, parameters: params,encoding:JSONEncoding.default, headers: [
            "Authorization":token,
            "Content-Type": "application/json",
            "Accept": "application/json" //Optional
        ])
        
        
        
        
        
        alamofireRequest.validate(contentType: ["application/json"])
            .responseJSON { (response) in
                self.responseHandler(response, success: success, failure: failure)
        }
    }
    
    func downloadPdf(docId: Int, uniqueName: String, isFolder: Bool, completionHandler:@escaping(String, Bool)->()){
        
        var downloadUrl = ""
        var params : [String:Int] = [:]
        if isFolder == true {
            downloadUrl = BASE_URL + getFolderPdfAPI
            params = ["folder_id":docId]
        }else {
            downloadUrl = BASE_URL + getDocPdfAPI
            params = ["doc_id":docId]
        }
        
        let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
        
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]//.appendingPathComponent("SDoky", isDirectory: true)
            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        print(downloadUrl)
        print(docId)
        print(uniqueName)
        
        let token = "Bearer " + AppUtility.sharedInstance.getToken()
        
        Alamofire.download(downloadUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: ["Authorization":token,
                                                                                                                              "Content-Type": "application/x-www-form-urlencoded",
                                                                                                                              "Accept": "*/*" ], to: destinationPath)
            .downloadProgress { progress in
                
        }
        .responseData { response in
            print("response: \(response)")
            switch response.result{
            case .success:
                if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                    completionHandler(filePath, true)
                }
                break
            case .failure:
                completionHandler("", false)
                break
            }
            
        }
    }
    
    
    func downloadSMapPdf(sMapId: Int, uniqueName: String, isFolder: Bool, completionHandler:@escaping(String, Bool)->()){
           
           var downloadUrl = ""
           var params : [String:Int] = [:]
           downloadUrl = BASE_URL + getSMapPdfAPI
            params = ["smap_id":sMapId]
           
           let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
           
               let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]//.appendingPathComponent("SDoky", isDirectory: true)
               let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
               return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
           }
           
           let token = "Bearer " + AppUtility.sharedInstance.getToken()
           
           Alamofire.download(downloadUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: ["Authorization":token,
                                                                                                                                 "Content-Type": "application/x-www-form-urlencoded",
                                                                                                                                 "Accept": "*/*" ], to: destinationPath)
               .downloadProgress { progress in
                   
           }
           .responseData { response in
               print("response: \(response)")
               switch response.result{
               case .success:
                   if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                       completionHandler(filePath, true)
                   }
                   break
               case .failure:
                   completionHandler("", false)
                   break
               }
               
           }
       }
    
    
    
    
    
    
    func responseHandler(_ response:DataResponse<Any>,
                         success:(_ response: AnyObject)->Void,
                         failure:(_ error: NSError, _ response: AnyObject)->Void) {
        switch response.result {
        case .success:
            print("Validation Successful")
            //            if(response.response?.statusCode == 401) {
            //
            //                let error = NSError(domain: "", code: 401, userInfo: [NSLocalizedDescriptionKey:"Token Expired"])
            //
            //                failure(error,response.result.value as AnyObject)
            //
            //            }else{
            
            success(response.result.value! as AnyObject)
            
            //}
            
            
        case .failure(let error):
            
            
            let nserror = error as NSError
            
            if let targetStatusCode = (response.response as AnyObject).statusCode {
                
                if targetStatusCode == 401 {
                    let e = NSError(domain: "custom", code: 401, userInfo: [:])
                    failure(e, response.result.value as AnyObject)
                }
                
                
            }
            else if(nserror.code == -1009) {
                let error = NSError(domain: "", code: 200, userInfo: [NSLocalizedDescriptionKey:"Your mobile device must be connected to the Internet with Wi-Fi or cellular network in order to use the app."])
                failure(error, response.result.value as AnyObject)
            }
            else{
                failure(error as NSError, response.result.value as AnyObject)
            }
        }
    }
    
}

