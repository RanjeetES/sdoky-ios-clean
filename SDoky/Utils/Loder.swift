//
//  Loder.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

class Loader: NSObject {
    
    static let sharedInstance : Loader = Loader()
    private override init() {
        
    }
    
    private let activityIndicator = UIActivityIndicatorView()
    private func setupLoader() {
        removeLoader()
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        activityIndicator.color = AppUtility.sharedInstance.hexStringToUIColor(hex: "0E80E4")
    }
    
    func showLoader() {
        
        setupLoader()
        
        let appDel = UIApplication.shared.delegate
        let holdingView = appDel?.window??.rootViewController!.view!
        
        DispatchQueue.main.async {
//           let alert = UIAlertController(title: nil, message: "", preferredStyle: .alert)
//
//            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//            loadingIndicator.hidesWhenStopped = true
//            loadingIndicator.style = UIActivityIndicatorView.Style.gray
//            loadingIndicator.startAnimating();
//
//            alert.view.addSubview(loadingIndicator)
//
            
            //present(alert, animated: true, completion: nil)
            
//            self.activityIndicator.center = holdingView!.center
//            self.activityIndicator.startAnimating()
//            holdingView!.addSubview(self.activityIndicator)
//            UIApplication.shared.beginIgnoringInteractionEvents()

            
        }
    
    }
    
    func removeLoader(){
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
}
