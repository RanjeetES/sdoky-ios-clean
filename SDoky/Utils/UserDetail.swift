////
////  UserDetail.swift
////  SDoky
////
////  Created by Ranjeet Sah on 26/04/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import Foundation
//import UIKit
//import ObjectMapper
////import ObjectMapper_Realm
////import RealmSwift
//
//class UserDetailResponse: Mappable {
//    var userRegistered: Bool?
//    var userDetailData: UserDetailData?
//    var error: String?
//    var message: String?
//    var statusCode: Int?
//    
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        userRegistered    <- map["user_registered"]
//        error    <- map["error"]
//        message         <- map["message"]
//        statusCode      <- map["status_code"]
//        statusCode      <- map["access_token"]
//        userDetailData <- map ["data"]
//    }
//    
//}
