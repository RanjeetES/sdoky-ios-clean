//
//  AppUtiltiy.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import Alamofire
import CoreData


struct SelectModel {
    var id = -1
    var title = "Select Item"
}

class AppUtility: NSObject {
    
    static let sharedInstance : AppUtility = AppUtility()
    
    private override init() {
        
    }
    
    private let userDefaults = UserDefaults.standard
    
    func setIsLoggedIn(loggedInFlag: Bool) {
        userDefaults.set(loggedInFlag, forKey: IS_LOGGED_IN_KEY)
        synchronizeUserDefaults()
    }
    
    func setIsLogedInBefore(isLogedInBefore: Bool) {
        userDefaults.set(isLogedInBefore, forKey: IS_LOGGED_IN_BEFORE)
        synchronizeUserDefaults()
    }
    
    func setHasLocalData(hasLocalData: Bool) {
        userDefaults.set(hasLocalData, forKey: HAS_LOCAL_DATA)
        synchronizeUserDefaults()
    }
    
    func setHasLocalSMap(hasLocalSMap: Bool) {
        userDefaults.set(hasLocalSMap, forKey: HAS_LOCAL_SMAP)
        synchronizeUserDefaults()
    }
    
    func setToken(token: String) {
        userDefaults.set(token, forKey: TOKEN_KEY)
        synchronizeUserDefaults()
    }
    
    func setPlayerId(playerId: String) {
        userDefaults.set(playerId, forKey: PLAYER_KEY)
        synchronizeUserDefaults()
    }
    
    func setCustomerId(customerId: Int) {
        userDefaults.set(customerId, forKey: CUSTOMER_ID_KEY)
        synchronizeUserDefaults()
    }
    
    func setCustomerEmail(customerEmail: String) {
        userDefaults.set(customerEmail, forKey: CUSTOMER_EMAIL)
        synchronizeUserDefaults()
    }
    
    func setLangugeSelected(id: Int) {
        userDefaults.set(id, forKey: LANGUAGE_SELECTED)
    }
    
    func getIsLoggedIn() -> Bool {
        
        return userDefaults.bool(forKey: IS_LOGGED_IN_KEY)
    }
    
    func getIsLogedInBefore() -> Bool {
        
        return userDefaults.bool(forKey: IS_LOGGED_IN_BEFORE)
    }
    
    func getToken() -> String {
        
        if let token = userDefaults.string(forKey: TOKEN_KEY) {
            return token
        }
        return ""
    }
    
    func getPlayerId() -> String {
        
        if let playerId = userDefaults.string(forKey: PLAYER_KEY) {
            return playerId
        }
        return ""
    }
    
    func getCustomerId() -> Int {
        return userDefaults.integer(forKey: CUSTOMER_ID_KEY)
    }
    
    func getLanguageSelected() -> Int {
        return userDefaults.integer(forKey: LANGUAGE_SELECTED)
    }
    
    func getCustomerEmail() -> String {
        if let email = userDefaults.string(forKey: CUSTOMER_EMAIL) {
            return email
        }
        return ""
    }
    
    private func synchronizeUserDefaults() {
        userDefaults.synchronize()
    }
    
    func setIsRememberMe(rememberMeFlag: Bool) {
        userDefaults.set(rememberMeFlag, forKey: IS_REMEMBER_ME_CLICKED)
        synchronizeUserDefaults()
    }
    
    func getIsRememberMe() -> Bool {
        
        return userDefaults.bool(forKey: IS_REMEMBER_ME_CLICKED)
    }
    
    
    func getHasLocalData() -> Bool {
        
        return userDefaults.bool(forKey: HAS_LOCAL_DATA)
    }
    
    
    func getHasLocalSMap() -> Bool {
        
        return userDefaults.bool(forKey: HAS_LOCAL_SMAP)
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    func passwordHash(from email: String, password: String) -> String {
        let salt = "x4vV8bGgqqmQwgCoyXFQj+(o.nUNQhVP7ND"
        return "\(password).\(email).\(salt)".sha256()
    }
    
    func getContext() -> NSManagedObjectContext {
        
        //        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        //        return context
        
        let appDelegate: AppDelegate
        if Thread.current.isMainThread {
            appDelegate = UIApplication.shared.delegate as! AppDelegate
        } else {
            appDelegate = DispatchQueue.main.sync {
                return UIApplication.shared.delegate as! AppDelegate
            }
        }
        return appDelegate.persistentContainer.viewContext
        
    }
    
    func localToSMap(sMapLocal: SMapLocal) -> SMap {
           
           let data = sMapLocal
           var occurences : [Occurrence] = []
           var oldOccurences : [Occurrence] = []
           
           for occurence in data.occurences! {
               
               let occurenceToShow = occurence as! OccurrenceLocal
               
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId ), docRefIdEdited: Int(occurenceToShow.docRefIdEdited ))
               occurences.append(occurenceModel)
               
           }
           
           for occurence in data.oldOccurences! {
               
               let occurenceToShow = occurence as! OccurrenceLocal
               
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId ), docRefIdEdited: Int(occurenceToShow.docRefIdEdited ))
               oldOccurences.append(occurenceModel)
               
           }
           
        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: data.isOmy)
           
           return localSMapToShow
           
       }
    
    func syncSMap(data: String) {
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        let postData = data.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/smap-occurrence-sync")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                //self.reloadTable()
                //           }
                
//                if let e = error as? NSError {
//                    if e.code == 401 {
//                        
//                    }
//                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                do {
                    let deletedOccurencesArray : [DeletedOccurence] = try AppUtility.sharedInstance.getContext().fetch(DeletedOccurence.fetchRequest())
                    for d in deletedOccurencesArray {
                        AppUtility.sharedInstance.getContext().delete(d)
                    }
                    
                    let deletedSMapArray : [DeletedSMap] = try AppUtility.sharedInstance.getContext().fetch(DeletedSMap.fetchRequest())
                    for d in deletedSMapArray {
                        AppUtility.sharedInstance.getContext().delete(d)
                    }
                }catch {
                    print("error")
                }
            }
        })
        
        dataTask.resume()
    }
    
    func getAndSyncSMaps() {
        var sMapResponseData :  [SMap] = []
        do {
            var localSMap : [SMap] = []
            let sMapLocal:[SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
            
            for sMap in sMapLocal {
                
                var occurences : [Occurrence] = []
                var oldOccurences : [Occurrence] = []
                
                for occurence in sMap.occurences! {
                    
                    let occurenceToShow = occurence as! OccurrenceLocal
                    
                    let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
                    occurences.append(occurenceModel)
                    
                }
                
                for occurence in sMap.oldOccurences! {
                    
                    let occurenceToShow = occurence as! OccurrenceLocal
                    
                    let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
                    oldOccurences.append(occurenceModel)
                    
                }
                
                
                
                let localSMapToShow = SMap(smapId: Int(sMap.smapId), isData: sMap.isData, title: sMap.title ?? "", firstEntry: sMap.firstEntry ?? "", secondEntry: sMap.secondEntry ?? "", userId: Int(sMap.userId), status: sMap.status, createdAt: sMap.createdAt ?? "", updatedAt: sMap.updatedAt ?? "", deletedAt: sMap.deletedAt ?? "", occurrences: occurences, offlineAction: sMap.offlineAction ?? "", isOmy: sMap.isOmy )
                
                localSMap.append(localSMapToShow)
            }
            sMapResponseData = localSMap
            
        }catch{
            print("Error Fetching")
        }
        
        if AppUtility.sharedInstance.getHasLocalSMap() == true {
            var deletedOccurencesInt : [Int] = []
            var deletedSMapsInt : [Int] = []
            do {
               
                let deletedOccurences: [DeletedOccurence] = try AppUtility.sharedInstance.getContext().fetch(DeletedOccurence.fetchRequest())
                for doc in deletedOccurences {
                    deletedOccurencesInt.append(Int(doc.occurenceId))
                }
                let deletedSMaps: [DeletedSMap] = try AppUtility.sharedInstance.getContext().fetch(DeletedSMap.fetchRequest())
                for folder in deletedSMaps {
                    deletedSMapsInt.append(Int(folder.sMapId))
                }
            }catch {
                print("Error fetching")
            }
            
            let params = sMapResponseData.toJSONString()!.addingPercentEncoding(withAllowedCharacters: .symbols)//?.replacingOccurrences(of: "\\\"", with: "\"")
            
            let d = params?.replacingOccurrences(of: "\\", with: "")
            
            
            let x = "smaps_occurrences={\"smaps_occurrences\":\(params!),\"deleted_smaps_ids\":\(deletedSMapsInt),\"deleted_occurrences_ids\":\(deletedOccurencesInt)}"
            
            self.syncSMap(data: x)
            
        }
        
    }
    
    func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
        if dateString == "" {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = desFormat
        return dateFormatter.string(from: date!)
    }
    
}
