//
//  Extension.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import Alamofire
import CryptoSwift
import CommonCrypto

var vSpinner : UIView?


extension Date {
    func stringDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    func stringDateSDoky() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter.string(from: date)
    }
    
    func stringDateSDoky1() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'"
        return dateFormatter.string(from: date)
    }
    
    func convertToSDokyDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter.string(from: self)
    }
    
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}


extension UITextView {
    func rangeFromTextRange(textRange:UITextRange) -> NSRange {
        let location:Int = self.offset(from: self.beginningOfDocument, to: textRange.start)
        let length:Int = self.offset(from: textRange.start, to: textRange.end)
        return NSMakeRange(location, length)
    }
    
    func convertRange(_ range: NSRange) -> UITextRange? {
      let beginning = beginningOfDocument
      if let start = position(from: beginning, offset: range.location), let end = position(from: start, offset: range.length) {
        let resultRange = textRange(from: start, to: end)
        return resultRange
      } else {
        return nil
      }
    }

    func frame(ofRange range: NSRange) -> [CGRect]? {
      if let textRange = convertRange(range) {
        let rects = selectionRects(for: textRange)
        return rects.map { $0.rect }
      } else {
        return nil
      }
    }
}
class Utility {
    
    static let sharedInstance = Utility()
    
    private init() {
        
    }
    
}
extension UIViewController {
    
    func checkExpiry() {
        do {
            let userInfoList : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
            if userInfoList.count > 0 {
                if userInfoList.first?.subscription?.expiresAt?.getDaysFromStringDate() ?? 0 < 0 {
                    let pricingVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                    pricingVC.isFromAccount = false
                    pricingVC.isFromNotification = false
                    self.show(pricingVC, sender: self)
                }
            }else {
                let subscriptionCheckVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "SubscriptionCheckVC") as! SubscriptionCheckVC
                self.show(subscriptionCheckVC, sender: self)
            }
        }catch {
            print("Could not fetch")
        }
    }
    
    
    func checkUnAuthorized() {
        AppUtility.sharedInstance.setToken(token: "")
       AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: false)
       AppUtility.sharedInstance.setCustomerId(customerId: 0)
       AppUtility.sharedInstance.setCustomerEmail(customerEmail: "")
       let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
       
       self.show(loginVC, sender: self)
    }
    
    func showSpinner(onView : UIView) {
        
        var secondsRemaining = 30
        let spinnerView = UIView.init(frame: onView.bounds)
        //spinnerView.backgroundColor = hexStringToUIColor(hex: "0E80E4")
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        
        ai.hidesWhenStopped = true
        ai.style = .whiteLarge
        ai.color = AppUtility.sharedInstance.hexStringToUIColor(hex: "0E80E4")
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
            
        }
        
        vSpinner = spinnerView
        
        
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    func chnageStatusBarBackground() {
        
        let statusBarFrame = UIApplication.shared.statusBarFrame
        let statusBarView = UIView(frame: statusBarFrame)
        self.view.addSubview(statusBarView)
        statusBarView.backgroundColor = .green
    }
    
    func showLoader()  {
        
        //let view = UIView()
        //view.frame = self.view.frame
        //view.backgroundColor = UIColor.white
        //view.alpha = 0.7
        
        let spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = hexStringToUIColor(hex: "0E80E4")
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        //        self.view.mask = view
        //        for view in self.view.subviews {
        //            view.isUserInteractionEnabled = false
        //        }
    }
    
    func hideLoader() {
        
        self.view.mask = nil
        for view in self.view.subviews {
            view.isUserInteractionEnabled = true
        }
    }
    
    //    func customizeNavigationBar() {
    //
    //        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(self.openLeftMenu) )
    //        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    //    }
    
    
    //    @objc func openLeftMenu() {
    //        self.slideMenuController()?.openLeft()
    //    }
    
    func showAlert() {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: "Something went wrong. Please try again later", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }
    }
    
    func showAlertWithMessage(message: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }
    }
    
    func showAlertWithMessageTitleAlert(message: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }
    }
    
    
    func showAlertWithMessageWithTitle(title: String, message: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //    func showPopup(_ controller: UIViewController, sourceView: UIView) {
    //        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    //        presentationController.sourceView = sourceView
    //        presentationController.sourceRect = sourceView.bounds
    //        presentationController.permittedArrowDirections = [.up ,.down]
    //        self.present(controller, animated: true)
    //    }
    
    func removeErrorViews() {
        
        for eachView in view.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
                
            }
        }
    }
    
    func showPopup(_ controller: UIViewController, sourceView: UIView) {
        
        
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        var rect = sourceView.frame
        rect.size.width = 1
        presentationController.popoverLayoutMargins = UIEdgeInsets(top: 20, left: rect.origin.x + 2, bottom: 0, right: 0)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = .init(rawValue: 0)
        self.present(controller, animated: true)
    }
    
    func showPopupForTable(_ controller: UIViewController, sourceView: UIView) {
        
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = .init(rawValue: 0)
        self.present(controller, animated: true)
    }
    
    func endEditing() {
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    func showErrorLabel(message: String, button: UIButton) {
        
        button.layer.borderWidth = 1.0
        button.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        button.layer.cornerRadius = 5.0
        button.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: button.bottomAnchor, multiplier: 0.5),
            button.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneErrorLabel(message: String, button: UIButton) {
        button.layer.borderWidth = 1.0
        button.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        button.layer.cornerRadius = 5.0
        button.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    
}

extension UIImage {
    
    public convenience init?(bounds: CGRect, colors: [UIColor], orientation: GradientOrientation = .horizontal) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors.map({ $0.cgColor })
        
        if orientation == .horizontal {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5);
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5);
        }
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    func setTitleImage() -> UIImageView {
        let image: UIImage = UIImage(named: "logo_title.png")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        return imageView
    }
    
    func imageFrame()->CGRect{
      let imageViewSize = self.frame.size
      guard let imageSize = self.image?.size else{return CGRect.zero}
      let imageRatio = imageSize.width / imageSize.height
      let imageViewRatio = imageViewSize.width / imageViewSize.height
      if imageRatio < imageViewRatio {
         let scaleFactor = imageViewSize.height / imageSize.height
         let width = imageSize.width * scaleFactor
         let topLeftX = (imageViewSize.width - width) * 0.5
         return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
      }else{
         let scalFactor = imageViewSize.width / imageSize.width
         let height = imageSize.height * scalFactor
         let topLeftY = (imageViewSize.height - height) * 0.5
         return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
      }
    }
}

class AnswerView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners(corners: [.topLeft,.topRight,.bottomLeft], radius: 16)
    }
}

class QuestionView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners(corners: [.topLeft,.topRight,.bottomRight], radius: 16)
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
        
    // Find a subview corresponding to the className parameter, recursively.
    func subviewWithClassName(_ className: String) -> UIView? {
        
        if NSStringFromClass(type(of: self)) == className {
            return self
        } else {
            for subview in subviews {
                return subview.subviewWithClassName(className)
            }
        }
        return nil
    }
    
    func swizzlePerformAction() {
        swizzleMethod(#selector(canPerformAction), withSelector: #selector(swizzledCanPerformAction))
    }
    
    private func swizzleMethod(_ currentSelector: Selector, withSelector newSelector: Selector) {
        if let currentMethod = self.instanceMethod(for: currentSelector),
            let newMethod = self.instanceMethod(for:newSelector) {
            let newImplementation = method_getImplementation(newMethod)
            method_setImplementation(currentMethod, newImplementation)
        } else {
            print("Could not find originalSelector")
        }
    }
    
    private func instanceMethod(for selector: Selector) -> Method? {
        let classType = type(of: self)
        return class_getInstanceMethod(classType, selector)
    }
    
    @objc private func swizzledCanPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
}

extension UITableView {
    
    func scroll(to: Position, animated: Bool) {
        let sections = numberOfSections
        let rows = numberOfRows(inSection: numberOfSections - 1)
        switch to {
        case .top:
            if rows > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.scrollToRow(at: indexPath, at: .top, animated: animated)
            }
            break
        case .bottom:
            if rows > 0 {
                let indexPath = IndexPath(row: rows - 1, section: sections - 1)
                self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
            break
        }
    }
    
    enum Position {
        case top
        case bottom
    }
}

@IBDesignable
class InfoView: UIView {
    
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        layer.cornerRadius = 6
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1
    }
    
    
}

@IBDesignable class RoundButton : UIButton{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        refreshCorners(value: cornerRadius)
    }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
}

extension UITabBar {
    
    static let height: CGFloat = 60.0

    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            sizeThatFits.height = UITabBar.height + window.safeAreaInsets.bottom
        } else {
            sizeThatFits.height = UITabBar.height
        }
        return sizeThatFits
    }
    
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = 60.0 // adjust your size here
//        return sizeThatFits
//    }
    
    func tabsVisiblty(_ isVisiblty: Bool = true){
        if isVisiblty {
            self.isHidden = false
            self.layer.zPosition = 0
        } else {
            self.isHidden = true
            self.layer.zPosition = -1
        }
    }
}

//extension StringProtocol {
//    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
//        range(of: string, options: options)?.lowerBound
//    }
//    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
//        range(of: string, options: options)?.upperBound
//    }
//    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
//        var indices: [Index] = []
//        var startIndex = self.startIndex
//        while startIndex < endIndex,
//            let range = self[startIndex...]
//                .range(of: string, options: options) {
//                indices.append(range.lowerBound)
//                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
//                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
//        }
//        return indices
//    }
//    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
//        var result: [Range<Index>] = []
//        var startIndex = self.startIndex
//        while startIndex < endIndex,
//            let range = self[startIndex...]
//                .range(of: string, options: options) {
//                result.append(range)
//                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
//                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
//        }
//        return result
//    }
//}

extension String {
    
    func hmac(algorithm: CryptoAlgorithm, key: String) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        let keyStr = key.cString(using: String.Encoding.utf8)
        let keyLen = Int(key.lengthOfBytes(using: String.Encoding.utf8))
        
        CCHmac(algorithm.HMACAlgorithm, keyStr!, keyLen, str!, strLen, result)
        
        let digest = stringFromResult(result: result, length: digestLen)
        
        result.deallocate()
        
        return digest
    }
    
    private func stringFromResult(result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash).lowercased()
    }
    
    func indicesOf(string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex

        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }

        return indices
    }
    
    func changeDateFormat() -> String {
        if self != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: self)
            dateFormatter.dateFormat = "d.MM.y"
            if let goodDate = dateFormatter.string(from: d!) as? String {
                return goodDate
            }
            
        }else {
            return ""
        }
        
    }
    
    func getDaysFromStringDate() -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: self)
        if let d = date {
            let calendar = Calendar.current

            let components = calendar.dateComponents([.day], from: Date(), to: d)

            return (components.day ?? 0)
        }
        return 0
        
        
    }
    
    func getDateFromStringDate() -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: self)
        if let d = date {
           return d
        }
        return Date()
        
    }
    
    func getDateFromSdokyStringDate() -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: self)
        if let d = date {
           return d
        }
        return Date()
        
    }

    
    var htmlAttributedString: NSAttributedString? {

        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            
        //let attributedString =  NSAttributedString(data: self.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue),options: options,documentAttributes: nil))
        do {
        let attributedString = try NSAttributedString(data: self.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: options, documentAttributes: nil)
                
        return attributedString
        }catch {
            print("Error")
        }
       return nil
    }
    
    func changeDateFormatString() -> String {
        if self != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: self)
            dateFormatter.dateFormat = "dd MMMM yyyy"
            if let goodDate = dateFormatter.string(from: d!) as? String {
                return goodDate
            }
            
        }else {
            return ""
        }
        
    }
    
    func trunc(length: Int, trailing: String = "") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
    
    func sMapText(occurence: Occurrence) -> String {
        let titleWithout  = occurence.title ?? ""
        return self.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\(titleWithout)</a>", options: [.literal]) 
    }
    
    func hashtags() -> [String]
    {
        if let regex = try? NSRegularExpression(pattern: "#[a-z\\s0-9]+,", options: .caseInsensitive)
        {
            let string = self as NSString

            return regex.matches(in: self, options: [], range: NSRange(location: 0, length: string.length)).map {
                string.substring(with: $0.range)
            }
        }

        return []
    }
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
    
    func capturedGroups(withRegex pattern: String) -> [String] {
        var results = [String]()

        var regex: NSRegularExpression
        do {
            regex = try NSRegularExpression(pattern: pattern, options: [])
        } catch {
            return results
        }
        let matches = regex.matches(in: self, options: [], range: NSRange(location:0, length: self.count))

        guard let match = matches.first else { return results }

        let lastRangeIndex = match.numberOfRanges - 1
        guard lastRangeIndex >= 0 else { return results }

        for i in 1...lastRangeIndex {
            let capturedGroupIndex = match.range(at: i)
            let matchedString = (self as NSString).substring(with: capturedGroupIndex)
            results.append(matchedString)
        }

        return results
    }
    
    func matches(for regex: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
        
    func countInstances(of stringToFind: String) -> (Int, [Range<String.Index>]) {
        var stringToSearch = self
        var count = 0
        var ranges : [Range<String.Index>] = []
        while let foundRange = stringToSearch.range(of: stringToFind, options: .diacriticInsensitive) {
            ranges.append(foundRange)
            stringToSearch = stringToSearch.replacingCharacters(in: foundRange, with: "")
            count += 1
        }
        return (count, ranges)
    }
}

extension Array where Element : Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
}


extension UISwitch {

    func set(width: CGFloat, height: CGFloat) {

        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51

        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth

        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}

extension NSRange {
    private init(string: String, lowerBound: String.Index, upperBound: String.Index) {
        let utf16 = string.utf16

        let lowerBound = lowerBound.samePosition(in: utf16)
        let location = utf16.distance(from: utf16.startIndex, to: lowerBound!)
        let length = utf16.distance(from: lowerBound!, to: upperBound.samePosition(in: utf16)!)

        self.init(location: location, length: length)
    }

    public init(range: Range<String.Index>, in string: String) {
        self.init(string: string, lowerBound: range.lowerBound, upperBound: range.upperBound)
    }

    public init(range: ClosedRange<String.Index>, in string: String) {
        self.init(string: string, lowerBound: range.lowerBound, upperBound: range.upperBound)
    }
}


extension UITextField {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

extension UINavigationController {

    func backToViewController(viewController: Swift.AnyClass) {

            for element in viewControllers {
                if element.isKind(of: viewController) {
                    self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
