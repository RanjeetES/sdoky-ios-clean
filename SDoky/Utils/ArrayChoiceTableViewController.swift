//
//  ArrayChoiceTableViewController.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/26/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class ArrayChoiceTableViewController<Element> : UITableViewController {
    
    typealias SelectionHandler = (Element) -> Void
    
    private let values : [Element]
    private let onSelect : SelectionHandler?
    var isFromData = false
    
    init(_ values : [Element], onSelect : SelectionHandler? = nil) {
        self.values = values
        self.onSelect = onSelect
        super.init(style: .plain)
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.separatorStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        //cell.textLabel?.text = (values[indexPath.row] as! SelectModel).title
        
        if isFromData == true {
            cell.textLabel?.attributedText = NSAttributedString(string: (values[indexPath.row] as! SelectModel).title, attributes: tableOptionsattributes)
            cell.textLabel?.textAlignment = .left
            //cell.textLabel!.addLeftImage()
            
            
        }else {
            cell.textLabel?.attributedText = NSAttributedString(string: (values[indexPath.row] as! SelectModel).title, attributes: languageTextattributes)
            cell.textLabel?.textAlignment = .center
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true)
        onSelect?(values[indexPath.row])
    }
    
}
