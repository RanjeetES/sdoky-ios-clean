//
//  DataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/10/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer
import Alamofire
import CoreData
import IQKeyboardManagerSwift
import Speech
import Lottie



class MyTapGestureSMap: UITapGestureRecognizer {
    var tapTag = Int()
}

class SMapTableViewHeaderView : UITableViewHeaderFooterView {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDocNumber: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    var isOpen: Bool = false
    var numberOfRows: Int = 0
    var sectiontTag : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}


class SMapTableFooterView : UITableViewHeaderFooterView {
    
    @IBOutlet weak var btnAddFolder: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
class SMapTableViewCell :  UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    
    
}

class SMapSearchTableHeaderView : UITableViewHeaderFooterView {
    @IBOutlet weak var lblDocumentCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class SMapSearchDataTableViewCell :  UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
}

class SMapVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AddNewFolderVCDelegate, HomeAPIDelegate, RenameVCDelegate, MoveVCDelegate, UITextFieldDelegate, SFSpeechRecognizerDelegate, UIPopoverPresentationControllerDelegate, SMapAPIDelegate {
    
    
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnMic: UIButton!
    //@IBOutlet weak var btnMydata: UIButton!
    //@IBOutlet weak var btnInternet: UIButton!
    @IBOutlet weak var tvData: UITableView!
    @IBOutlet weak var btnAddNewFolder: UIButton!
    //@IBOutlet weak var vbtnData :  UIView!
    //@IBOutlet weak var vbtnInternet :  UIView!
    @IBOutlet weak var btnSearchIcon: UIButton!
    @IBOutlet weak var btnMicAnimated: AnimationView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBOutlet weak var btnSMapData: UIButton!
    @IBOutlet weak var btnSMapSomeone: UIButton!
    
    var openSectionSet : Set<Int> = []
    var selectedSection = 0
    var sMapDeleted = false
    var fileDeleted = false
    var detectionTimer:Timer?
    
    
    var folderResponseData: [FolderResponseData] = []
    var folderDocResponseData: [FolderDocResponseData] = []
    var sMapResponseData: [SMap] = []
    var dataForTableView : [String : [SMap]] = [:]
    var sectionHeaderTitle = ["Data SMap", "Person SMap"]
    var folders : [Folder] = []
    var unfilteredSMapLocal : [SMapLocal] = []
    var sMapLocal : [SMapLocal] = []
    var unfilteredDocs : [Document] = []
    var docs : [Document] = []
    var unfilteredOccurencesLocal : [OccurrenceLocal] = []
    var occurencesLocal : [OccurrenceLocal] = []
    var folderId = 0
    var myData = true
    var isAfterSync = false
    var deletedFolders : [Int] = []
    var deletedFiles : [Int] = []
    var searchDataResponse :  [InternetsearchResponseData] = []
    
    var selectModelList: [SelectModel] = []
    
    var selectedVehicleType = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    private let refreshControl = UIRefreshControl()
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    //Search
    @IBOutlet weak var searchTableView : UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Commented
        //        btnMicAnimated.contentMode = .scaleAspectFit
        //
        //        btnMicAnimated.loopMode = .loop
        //
        //        btnMicAnimated.animationSpeed = 0.5
        //Commented
        
        
        let headerNib = UINib.init(nibName: "ScanTableViewHeaderView", bundle: nil)
        tvData.register(headerNib,forHeaderFooterViewReuseIdentifier: "sectionHeader")
        
        //        let footerNib = UINib.init(nibName: "DataTableFooterView", bundle: nil)
        //        tvData.register(footerNib,forHeaderFooterViewReuseIdentifier: "sectionFooter")
        
        let searchHeaderNib = UINib.init(nibName: "SearchTableHeaderView", bundle: nil)
        self.searchTableView.register(searchHeaderNib, forHeaderFooterViewReuseIdentifier: "searchHeader")
        
        
        self.navigationController?.navigationBar.isHidden = true
        tvData.isHidden = false
        tvData.delegate = self
        tvData.dataSource = self
        //tvData.tableFooterView = UIView()
        //tvData.contentInsetAdjustmentBehavior = .never
        self.tvData.contentInset = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0)
        
        self.endEditing()
        self.tfSearch.delegate = self
        self.tfSearch.addTarget(self, action: #selector(self.textIsChanging(_:)), for: .editingChanged)

        btnAddNewFolder.layer.borderWidth = 1.0
        btnAddNewFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnAddNewFolder.layer.cornerRadius = 5.0
        
        btnSMapData.layer.borderWidth = 1.0
        btnSMapData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSMapData.layer.cornerRadius = 5.0
        btnSMapSomeone.layer.borderWidth = 1.0
        btnSMapSomeone.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSMapSomeone.layer.cornerRadius = 5.0
        
        /*btnMydata.layer.borderWidth = 4.0
         btnMydata.layer.masksToBounds = false
         btnMydata.layer.cornerRadius = btnMydata.frame.height/2
         btnMydata.layer.borderColor = UIColor.white.cgColor
         btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
         
         vbtnData.layer.borderWidth = 2.0
         vbtnData.layer.masksToBounds = false
         vbtnData.layer.cornerRadius = vbtnData.frame.height/2
         vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
         
         btnInternet.layer.borderWidth = 4.0
         btnInternet.layer.masksToBounds = false
         btnInternet.layer.cornerRadius = btnInternet.frame.height/2
         btnInternet.layer.borderColor = UIColor.white.cgColor
         btnInternet.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
         self.btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
         self.btnInternet.backgroundColor = UIColor.white
         
         vbtnInternet.layer.borderWidth = 2.0
         vbtnInternet.layer.masksToBounds = false
         vbtnInternet.layer.cornerRadius = vbtnInternet.frame.height/2
         vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
         */
        self.tvData.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(getNewData(_:)), for: .valueChanged)
        
        refreshControl.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        
        //Search TableView
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        self.searchTableView.tableFooterView = UIView()
        
        //        //Speech
        //
        //        let lang = AppUtility.sharedInstance.getLanguageSelected()
        //        switch lang {
        //
        //        case 0:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        //        case 1:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr-FR"))
        //        case 2:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "de-DE"))
        //        case 3:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
        //        case 4:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ru-RU"))
        //        case 5:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ja-JP"))
        //        case 6:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "zh-TW"))
        //        default:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        //
        //        }
        //
        //        speechRecognizer?.delegate = self
        //
        //
        //        btnMic.isEnabled = false
        //        speechRecognizer!.delegate = self
        //        SFSpeechRecognizer.requestAuthorization { (authStatus) in
        //
        //            var isButtonEnabled = false
        //
        //            switch authStatus {
        //            case .authorized:
        //                isButtonEnabled = true
        //
        //            case .denied:
        //                isButtonEnabled = false
        //                print("User denied access to speech recognition")
        //
        //            case .restricted:
        //                isButtonEnabled = false
        //                print("Speech recognition restricted on this device")
        //
        //            case .notDetermined:
        //                isButtonEnabled = false
        //                print("Speech recognition not yet authorized")
        //            @unknown default:
        //                print("Nothing")
        //            }
        //
        //            OperationQueue.main.addOperation() {
        //                self.btnMic.isEnabled = isButtonEnabled
        //            }
        //        }
        
        
    }
    
    @objc private func getNewData(_ sender: Any) {
        

        self.getLocalFolderDocs()

        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        // IQKeyboardManager.shared.enable = true
        print("disappear")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        self.checkExpiry()
        self.getLocalFolderDocs()
        self.tabBarController?.tabBar.isHidden = false
        self.setToNormalState()
        loader.isHidden = true
        
        //Speech
        
        let lang = AppUtility.sharedInstance.getLanguageSelected()
        switch lang {
            
        case 0:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        case 1:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr-FR"))
        case 2:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "de-DE"))
        case 3:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
        case 4:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ru-RU"))
        case 5:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ja-JP"))
        case 6:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "zh-TW"))
        default:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
            
        }
        
        speechRecognizer?.delegate = self
        
        
        btnMic.isEnabled = false
        speechRecognizer!.delegate = self
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            @unknown default:
                print("Nothing")
            }
            
            OperationQueue.main.addOperation() {
                self.btnMic.isEnabled = isButtonEnabled
            }
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        IQKeyboardManager.shared.enable = false
        //        self.tabBarController?.tabBar.isHidden = false
        //        self.getLocalFolderDocs()
        self.tabBarController?.tabBar.isHidden = false
        if AppUtility.sharedInstance.isConnectedToInternet() {
            //            let homeAPI = HomeAPI()
            //            homeAPI.delegate = self
            //            //homeAPI.getFolders()
            //            homeAPI.getFolderDocs()
            //            self.showSpinner(onView: self.view)
        }else {
            // self.getLocalFolderDocs()
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tvData {
            return self.dataForTableView.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tvData {
            
            
            
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "sectionHeader") as? SMapTableViewHeaderView
            let sData = self.dataForTableView[sectionHeaderTitle[section]]
            view?.lblName.text = sectionHeaderTitle[section]
            
            
            if sData?.count ?? 0 > 1 {
                view?.lblDocNumber.text = "\(sData?.count ?? 0) maps"
            }else {
                view?.lblDocNumber.text = "\(sData?.count ?? 0) map"
            }
            
            view?.numberOfRows = sData?.count ?? 0
            view?.btnArrow.tag = section
            view?.btnMenu.tag = section
            view?.btnArrow.addTarget(self, action: #selector(sectionArrowTapped(_:)), for: .touchUpInside)
            view?.btnMenu.addTarget(self, action: #selector(sectionMenuTapped(_:)), for: .touchUpInside)
            
            view?.isUserInteractionEnabled = true
            let tap = MyTapGestureSMap(target: self, action: #selector(sectionTapped(_:)))
            view?.addGestureRecognizer(tap)
            tap.tapTag = section
            
            //            view.transform = CGAffineTransform(translationX: 0, y: 0)
            //
            //
            //
            //            UIView.animate(withDuration: 12, delay: 3, options: [.allowUserInteraction], animations: { () -> Void in
            //                view.transform = CGAffineTransform(translationX: 0, y: 900)
            //            }, completion: nil)
            //
            
            
            if false {
                view?.ivIcon.image = UIImage(named: "help")
                view?.btnMenu.isHidden = true
            }else {
                if self.openSectionSet.contains(section) {
                    view?.ivIcon.image = UIImage(named: "folder-open")
                    
                }else {
                    view?.ivIcon.image = UIImage(named: "folder-close")
                }
                view?.btnMenu.isHidden = true
            }
            
            if self.openSectionSet.contains(section) {
                view?.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
                view?.isOpen = true
                
            }else {
                view?.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
                view?.isOpen = false
            }
            
            
            return view
        }else {
            var count = 0
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "searchHeader") as? SearchTableHeaderView
            if myData == true {
                count = self.sMapLocal.count
            }else {
                
                count = self.searchDataResponse.count
                if count == 0 {
                    view?.isHidden = true
                }else {
                    view?.isHidden = false
                }
            }
            
            view?.lblDocumentCount.text = "\(count) results found"
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if tableView == self.tvData {
            
            if (section == self.folderDocResponseData.count - 1){
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                    "sectionFooter") as? SMapTableFooterView
                view?.btnAddFolder.layer.borderWidth = 1.0
                view?.btnAddFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
                view?.btnAddFolder.layer.cornerRadius = 5.0
                view?.btnAddFolder.addTarget(self, action: #selector(self.addNewFolder(_:)), for: .touchUpInside)
                
                return view
            }else {
                return UIView()
            }
        }else {
            return UIView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if tableView == self.tvData {
            if (section == self.folderDocResponseData.count - 1){
                return CGFloat.init(60.0)
            }else {
                return CGFloat.init(0.0)
            }
        }else{
            return CGFloat.init(0.0)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tvData {
            return CGFloat.init(34.0)
        }else {
            return CGFloat.init(16.0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tvData {
            
            return CGFloat.init(40.0)
        }else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tvData {
            
            if self.openSectionSet.contains(section) {
                return self.dataForTableView[sectionHeaderTitle[section]]?.count ?? 0
            }else {
                return 0
            }
        }else {
            if myData == true {
                return self.sMapLocal.count
            }else {
                return self.searchDataResponse.count
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tvData {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "smap_data_cell", for: indexPath) as! SMapTableViewCell
            let rData = self.dataForTableView[sectionHeaderTitle[indexPath.section]]
            
            
            if (rData?[indexPath.row].title ?? "").count > 16 {
                cell.lblName.text = "\((rData?[indexPath.row].title ?? "").trunc(length: 16))..."
            }else {
                cell.lblName.text = rData?[indexPath.row].title ?? ""
            }
            
            let date = self.changeDateFormat(date: (rData?[indexPath.row].updatedAt)!)
            cell.lblDate.text = date
            //cell.ivIcon.image = UIImage(named: "doc")
            
            cell.btnMenu.tag = rData?[indexPath.row].smapId ?? 0
            cell.btnMenu.addTarget(self, action: #selector(rowMenuTapped(_:)), for: .touchUpInside)
            
//            if rData.docs![indexPath.row].adminPushed == true {
//                cell.btnMenu.isHidden = true
//                cell.ivIcon.image = UIImage(named: "file-green")
//            }else {
//                cell.btnMenu.isHidden = false
//                cell.ivIcon.image = UIImage(named: "doc")
//            }
            
            
            return cell
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "smap_search_cell", for: indexPath) as! SMapSearchDataTableViewCell
            var title = ""
            var desc = ""
            
            if myData == true {
                
                title = self.sMapLocal[indexPath.row].title ?? ""
                desc = (self.sMapLocal[indexPath.row].firstEntry ?? "").trunc(length: 80)
            }else {
                
                title = self.searchDataResponse[indexPath.row].title ?? ""
                desc = (self.searchDataResponse[indexPath.row].snippet ?? "").trunc(length: 120)
            }
            
            let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
            
            let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
            
            let attributedString1 = NSMutableAttributedString(string :desc, attributes:attrs1)
            
            
            let attributedString2 = NSMutableAttributedString(string: "... Read more", attributes:attrs2)
            
            attributedString1.append(attributedString2)
            // self.lblText.attributedText = attributedString1
            
            
            
            cell.lblName.text = title
            
            cell.lblData.attributedText = attributedString1
            
            
            
            //cell.lblData.text = self.docs[indexPath.row].desc == "" ? "Empty description" :   "\(self.docs[indexPath.row].desc ?? "") Read more"
            return cell
        }
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if tableView == self.tvData {
    //            return ""
    //        }else {
    //            return "\(self.docs.count) results found"
    //        }
    //    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tvData {
            
            
            let rData = self.dataForTableView[sectionHeaderTitle[indexPath.section]]
                        
            if rData?[indexPath.row].isData == true {
                
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewSMapDataVC") as! ViewSMapDataVC
                vc.sMap = rData?[indexPath.row]
                
                self.show(vc, sender: self)
                
            }else {
                
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewSMapPersonVC") as! ViewSMapPersonVC
                vc.sMap = rData?[indexPath.row]
                
                self.show(vc, sender: self)
                
            }
        }else {
            
            if myData == true {
                
                let data = self.sMapLocal[indexPath.row]
                var occurences : [Occurrence] = []
                var oldOccurences : [Occurrence] = []
                    
                    for occurence in data.occurences! {
                        
                        let occurenceToShow = occurence as! OccurrenceLocal
                        
                        let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
                        occurences.append(occurenceModel)
                        
                    }
                
                for occurence in data.oldOccurences! {
                    
                    let occurenceToShow = occurence as! OccurrenceLocal
                    
                   let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
                    oldOccurences.append(occurenceModel)
                    
                }
                    
                let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: data.isOmy)
                    
                
                if data.isData == true {
                    
                    let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewSMapDataVC") as! ViewSMapDataVC
                    vc.sMap = localSMapToShow
                    self.show(vc, sender: self)
                    
                }else {
                    
                    let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewSMapPersonVC") as! ViewSMapPersonVC
                    vc.sMap = localSMapToShow
                    self.show(vc, sender: self)
                    
                }
                
            }else {
                
                let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WebPopOverController") as! WebPopOverController
                // vc.modalPresentationStyle = .popover
                //vc.preferredContentSize = CGSize(width: self.view.frame.width - 40, height: self.view.frame.height)
                vc.url = self.searchDataResponse[indexPath.row].link
                vc.folders = self.folderDocResponseData
                vc.searchTitle = self.searchDataResponse[indexPath.row].title
                
                
                
                //                let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: vc)
                //                var rect = self.view.frame
                //                rect.size.width = 1
                //                presentationController.popoverLayoutMargins = UIEdgeInsets(top: 0, left: rect.origin.x + 1, bottom: 0, right: 0)
                //                presentationController.sourceView = self.view
                //                presentationController.sourceRect = self.view.frame
                //                       presentationController.permittedArrowDirections = .init(rawValue: 0)
                self.show(vc, sender: self)
                
            }
            
            
            
        }
        
    }
    
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 12)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnSMapDataAction(_ sender: Any) {
        
        let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "SMapDataVC") as! SMapDataVC
        self.show(vc, sender: self)
        
    }
    
    @IBAction func btnSMapPersonAction(_ sender: Any) {
        
        let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "SMapPersonVC") as! SMapPersonVC
        self.show(vc, sender: self)
        
        
    }
    
    @IBAction func btnAddNewFolderAction(_ sender: Any) {
        
        self.showAddNewFolder()
        
    }
    
    @objc func addNewFolder(_ sender: UIButton) {
        self.showAddNewFolder()
    }
    
    func showAddNewFolder() {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "AddNewFolderVC") as! AddNewFolderVC
        viewController.delegate = self
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 250.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func showRenameFolder(currentTitle: String, folderId: Int) {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "RenameFolderVC") as! RenameFolderVC
        viewController.delegate = self
        viewController.currentTitle = currentTitle
        viewController.folderId = folderId
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 250.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func didAddFolder() {
        
        //        let homeAPI = HomeAPI()
        //        homeAPI.delegate = self
        //        homeAPI.getFolders()
        //        self.showSpinner(onView: self.view)
        //        self.viewDidAppear(false)
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
    }
    
    func didRenameFolder() {
        
        //        let homeAPI = HomeAPI()
        //        homeAPI.delegate = self
        //        homeAPI.getFolders()
        //        self.showSpinner(onView: self.view)
        //        self.viewDidAppear(false)
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
    }
    
    func didMoveFile() {
        //        self.viewDidAppear(false)
        //
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
        
    }
    
    //    func didWriteNewFile() {
    //           self.tabBarController?.tabBar.isHidden = false
    //           self.getLocalFolderDocs()
    //       }
    
    
    
    func didGetFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let folderResponse:FolderResponse = Mapper<FolderResponse>().map(JSON: json) {
                    
                    self.folderResponseData = folderResponse.data!
                    //self.reloadTable()
                }
            }
            
            
        }
        
    }
    
    func didFailGetFolderError(_ error: NSError, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        print(error.debugDescription)
    }
    
    func didGetSMapsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        self.refreshControl.endRefreshing()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let sMapResponse:SMapResponse = Mapper<SMapResponse>().map(JSON: json) {
                    self.sMapResponseData = sMapResponse.data!
                    
                    do {
                        self.sMapLocal = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if self.sMapLocal.count > 0 {
                        for sMap in sMapLocal {
                            AppUtility.sharedInstance.getContext().delete(sMap)
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }
                    
//                    do {
//                        self.occurencesLocal = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
//                        
//                    }catch{
//                        print("Error Fetching")
//                    }
//                    
//                    if self.occurencesLocal.count > 0 {
//                        for occurence in occurencesLocal {
//                            AppUtility.sharedInstance.getContext().delete(occurence)
//                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        }
//                        
//                    }
                    //                    self.docs = []
                    //
                    //                    do {
                    //                        let deletedFiles : [DeletedFiles] = try AppUtility.sharedInstance.getContext().fetch(DeletedFiles.fetchRequest())
                    //                        if self.deletedFiles.count > 0 {
                    //                            for file in deletedFiles {
                    //                                AppUtility.sharedInstance.getContext().delete(file)
                    //                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    //                            }
                    //
                    //                        }
                    //
                    //                    }catch{
                    //                        print("Error Fetching")
                    //                    }
                    //
                    //                    self.deletedFiles = []
                    //
                    //                    do {
                    //                        let deletedFolders : [DeletedFolder] = try AppUtility.sharedInstance.getContext().fetch(DeletedFolder.fetchRequest())
                    //                        if deletedFolders.count > 0 {
                    //                            for folder in deletedFolders {
                    //                                AppUtility.sharedInstance.getContext().delete(folder)
                    //                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    //                            }
                    //
                    //                        }
                    //
                    //                    }catch{
                    //                        print("Error Fetching")
                    //                    }
                    //
                    //                    self.deletedFolders = []
                    
                    
                    
                    for sMap in sMapResponseData {
                        let sMapToSave = SMapLocal(context: AppUtility.sharedInstance.getContext())
                        sMapToSave.smapId = Int16(sMap.smapId ?? -1)
                        sMapToSave.isData = sMap.isData ?? true
                        sMapToSave.title = sMap.title ?? ""
                        sMapToSave.oldTitle = sMap.title ?? ""
                        sMapToSave.firstEntry = (sMap.firstEntry ?? "").replacingOccurrences(of: "\n{4,10}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "") ?? ""
                        sMapToSave.secondEntry = (sMap.secondEntry ?? "").replacingOccurrences(of: "\n{4,10}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
                            ?? ""
                        sMapToSave.oldFirstEntry = (sMap.firstEntry ?? "").replacingOccurrences(of: "\n{4,10}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "") ?? ""
                        sMapToSave.oldSecondEntry = (sMap.secondEntry ?? "").replacingOccurrences(of: "\n{4,10}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
                            ?? ""
                        sMapToSave.status = sMap.status ?? true
                        sMapToSave.userId = Int16(sMap.userId ?? -1)
                        sMapToSave.createdAt = sMap.createdAt ?? ""
                        sMapToSave.updatedAt = sMap.updatedAt ?? ""
                        sMapToSave.isOmy = sMap.isOmy ?? false
                        sMapToSave.offlineAction = ""
                        var occurences = NSMutableSet()
                        for occurence in sMap.occurrences! {
                            let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
                            occurenceToSave.occurenceId = Int16(occurence.occurenceId ?? 0)
                            occurenceToSave.smapId = Int16(occurence.smapId ?? 0)
                            occurenceToSave.title = occurence.title ?? ""
                            occurenceToSave.isFirst = occurence.isFirst ?? true
                            occurenceToSave.desc = occurence.description ?? ""
                            occurenceToSave.updatedAt = occurence.updatedAt ?? ""
                            occurenceToSave.offlineAction = ""
                            occurenceToSave.createdAt = occurence.createdAt ?? ""
                            occurenceToSave.newTitle = occurence.title ?? ""
                            occurenceToSave.newDesc = occurence.description ?? ""
                            occurenceToSave.tempTitle = occurence.tempTitle ?? ""
                            occurenceToSave.docRefId = Int16(occurence.docRefId ?? 0)
                            occurenceToSave.newDocRefId = Int16(occurence.docRefId ?? 0)
                            occurenceToSave.docRefIdEdited = Int16(occurence.docRefIdEdited ?? 0)
                            occurences.add(occurenceToSave)
                        }
                        sMapToSave.occurences = occurences
                        sMapToSave.oldOccurences = occurences
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        sMap.oldOccurrences = sMap.occurrences
                    }
                    self.sMapResponseData = sMapResponse.data!
                    let data = self.sMapResponseData.filter({$0.isData == true}).sorted { $0.title!.lowercased() < $1.title!.lowercased() }
                    let person = self.sMapResponseData.filter({$0.isData == false}).sorted { $0.title!.lowercased() < $1.title!.lowercased() }
                    self.dataForTableView.updateValue(data, forKey: sectionHeaderTitle[0])
                    self.dataForTableView.updateValue(person, forKey: sectionHeaderTitle[1])
                    if isAfterSync == true {
                        self.isAfterSync = false
                        //self.searchTableView.reloadData()
                    }else {
                        self.reloadTable()
                    }
                    
                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                    
                    //                    if self.fileDeleted == true {
                    //                        self.fileDeleted = false
                    //                        self.showConfirmation(message: "Folder deleted successfully.")
                    //                    }
                    //
                    //                    if self.folderDeleted == true {
                    //                        self.folderDeleted = false
                    //                        self.showConfirmation(message: "Doc deleted successfully.")
                    //                    }
                }
            }
            
            
        }
        
    }
    
    func didFailGetSMapsError(_ error: NSError, resultStatus: Bool) {
        self.refreshControl.endRefreshing()
        self.removeSpinner()
        print(error.debugDescription)
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    func didDeleteFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                //self.sMapDeleted = true
                self.viewDidAppear(false)
                //                let alert = UIAlertController(title: "Alert", message: "Folder deleted successfully.", preferredStyle: UIAlertController.Style.alert)
                //
                //                // add the actions (buttons)
                //                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                //                    action in
                //                    self.didAddFolder()
                //                }))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func didFailDeleteFolderError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        
        print(error.debugDescription)
    }
    
    func changeDateFormat(date: String) -> String {
        if date != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "d.MM.y"
            let goodDate = dateFormatter.string(from: d!)
            return goodDate
        }else {
            return ""
        }
        
    }
    
    @objc func sectionArrowTapped(_ sender: UIButton){
        
        let section = self.tvData.headerView(forSection: sender.tag) as! SMapTableViewHeaderView as! SMapTableViewHeaderView
        
        if section.isOpen == false {
            section.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
            self.openSectionSet.removeAll()
            self.openSectionSet.insert(sender.tag)
            
        }else {
            section.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
            self.openSectionSet.remove(sender.tag)
        }
        self.reloadTable()
        
    }
    
    
    @objc func sectionTapped(_ sender: MyTapGestureSMap){
        
        let s = sender.tapTag
        let section = self.tvData.headerView(forSection: s) as! SMapTableViewHeaderView
        
        //        section.backgroundView?.backgroundColor = .lightGray
        //
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //           section.backgroundView?.backgroundColor = .clear
        //        }
        
        
        if section.isOpen == false {
            section.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
            self.openSectionSet.removeAll()
            self.openSectionSet.insert(s)
            
        }else {
            section.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
            self.openSectionSet.remove(s)
        }
        self.reloadTable()
        
    }
    
    @objc func sectionMenuTapped(_ sender: UIButton){
        self.selectModelList.removeAll()
        //self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
        //self.selectModelList.append(SelectModel(id: 1, title: RENAME))
        //self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
        //self.selectModelList.append(SelectModel(id: 3, title: DELETE))
        
        if folderDocResponseData[sender.tag].docs?.count == 0 {
            
            self.selectModelList.removeAll()
            self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
            self.selectModelList.append(SelectModel(id: 1, title: RENAME))
            //self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
            self.selectModelList.append(SelectModel(id: 3, title: DELETE))
            
        }else {
            
            self.selectModelList.removeAll()
            self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
            self.selectModelList.append(SelectModel(id: 1, title: RENAME))
            self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
            //self.selectModelList.append(SelectModel(id: 3, title: DELETE))
            
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            let folder = self.folderDocResponseData[sender.tag]
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            switch model.title {
            case NEW_DATA:
                self.createNewData(folder: folder)
            case RENAME:
                self.renameFolder(currentTitle: folder.title ?? "", folderId: folder.folderId ?? 0)
            case DOWNLOAD_AS_PDF:
                self.downloadFolderDocs(folderName: folder.title ?? "", folderId: folder.folderId ?? 0)
            case DELETE:
                print("No Action")
            default:
                print("No Action")
            }
            
        }
        controller.preferredContentSize = CGSize(width: 180.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopupForTable(controller, sourceView: sender as UIView)
    }
    
    @objc func rowMenuTapped(_ sender: UIButton){
        self.selectModelList.removeAll()
        
        if let sMap = self.sMapResponseData.filter({$0.smapId == sender.tag}).first {
            
            if sMap.isOmy == true {
                self.selectModelList.append(SelectModel(id: 1, title: EDIT))
                self.selectModelList.append(SelectModel(id: 2, title: SAVE_AS_PDF))
                self.selectModelList.append(SelectModel(id: 3, title: DELETE))
                
            }else {
                self.selectModelList.append(SelectModel(id: 1, title: EDIT))
                self.selectModelList.append(SelectModel(id: 3, title: DELETE))
                
            }
            
        }
        
        
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
                    
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            let sMapId = sender.tag
            if let sMap = self.sMapResponseData.filter({$0.smapId == sMapId}).first {
                
                switch model.title {
                           case EDIT:
                               self.editSMap(sMap: sMap)
                           case SAVE_AS_PDF:
                            self.downloadSMap(sMap: sMap)
                           case DELETE:
                            self.deleteSMap(sMap: sMap)
                           default:
                               print("No action")
                           }
                
            }
            
           
            
        }
        controller.preferredContentSize = CGSize(width: 180.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopupForTable(controller, sourceView: sender as UIView)
    }
    
    
    func moveDocument(docId: Int) {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "MoveVC") as! MoveVC
        viewController.delegate = self
        viewController.folderId = folderId
        viewController.docId = docId
        viewController.folders = self.folderDocResponseData
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 260.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    
    func createNewData(folder: FolderDocResponseData) {
        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
        vc.folders = self.folderDocResponseData
        vc.folder = folder
        vc.isFromInternetSearch = false
        vc.isFromScan = false
        vc.data = ""
        vc.searchTitle = ""
        self.show(vc, sender: self)
        //self.tabBarController?.selectedIndex = 2
    }
    
    func renameFolder(currentTitle: String, folderId: Int) {
        self.showRenameFolder(currentTitle: currentTitle, folderId: folderId)
    }
    
    func downloadFolderDocs(folderName: String, folderId: Int) {
        //        for doc in docs {
        //            self.downloadDoc(docTitle: doc.title ?? "", docId: doc.docId ?? 0)
        //        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadPdf(docId: folderId, uniqueName: folderName, isFolder: true) { (message, status) in
                self.showAlertWithMessageTitleAlert(message: "Pdf saved in SDoky folder")
            }
        } else {
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
    }
    
    func downloadSMap(sMap: SMap) {
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadSMapPdf(sMapId: sMap.smapId ?? -1, uniqueName: sMap.title ?? "Unnamed", isFolder: false) { (message, status) in
                       self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
                   }
        }else{
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
        
    }
    
    func deleteSMap(sMap: SMap) {
        
        let alert = UIAlertController(title: "Confirm to delete SMap?", message: "Are you sure to delete sMap \(sMap.title ?? "")?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
                do {
                    var localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    
                    if let sMapToDelete = localSMaps.filter({Int($0.smapId) == sMap.smapId}).first {
                        sMapToDelete.offlineAction = OFFLINE_DELETE
                        AppUtility.sharedInstance.getContext().delete(sMapToDelete)
                        let deletedSMap = DeletedSMap(context: AppUtility.sharedInstance.getContext())
                        deletedSMap.sMapId = Int16(sMap.smapId ?? -1)
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        self.sMapDeleted = true
                        self.getLocalFolderDocs()
                    }
                    
                    
                }catch {
                    print("Error fetching")
                }
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // Doc
    func editSMap(sMap: SMap) {
        
        if sMap.isData == true {
            let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapDataVC") as! EditSMapDataVC
            vc.sMap = sMap
            vc.firstEdit = false
            self.show(vc, sender: self)
        }else {
            let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapPersonVC") as! EditSMapPersonVC
            vc.sMap = sMap
            vc.firstEdit = false
            self.show(vc, sender: self)
        }
       
        
    }
    
    func deleteDoc(docId: Int, folderId: Int, docTitle: String) {
        
        let alert = UIAlertController(title: "Confirm to delete document?", message: "Are you sure to delete file " + docTitle + "?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.deleteDoc(params: ["doc_id":docId])
                //self.showSpinner(onView: self.view)
            }else {
                do {
                    var localFolders : [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                    let folder = localFolders.filter({Int($0.folderId) == folderId}).first!
                    folder.offlineAction = OFFLINE_EDIT
                    
                    var docs = NSMutableSet()
                    for doc in folder.docs! {
                        docs.add(doc)
                        let d = doc as! Document
                        if Int(d.docId) == docId {
                            d.offlineAction = OFFLINE_DELETE
                            docs.remove(d)
                        }
                    }
                    folder.docs = docs
                    let deletedDoc = DeletedFiles(context: AppUtility.sharedInstance.getContext())
                    deletedDoc.fileId = Int16(docId)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    self.fileDeleted = true
                    self.getLocalFolderDocs()
                    
                    //self.showConfirmation(message: "Doc deleted successfully.")
                    
                }catch {
                    print("Error fetching")
                }
                
            }
            
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func didDeleteDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                self.fileDeleted = true
                self.viewDidAppear(false)
                //                let alert = UIAlertController(title: "Alert", message: "Doc deleted successfully.", preferredStyle: UIAlertController.Style.alert)
                //
                //                // add the actions (buttons)
                //                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                //                    action in
                //                    // self.didAddFolder()
                //                    self.viewDidAppear(false)
                //                }))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func didFailDeleteDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    func reloadTable() {
        
        if true {
            
            //var array = self.folderDocResponseData.filter({$0.title == "Help"})
            //        for (index, item) in array.enumerated() {
            //            if index != 0 {
            //                array.remove(at: index)
            //            }
            //        }
            
//            self.folderDocResponseData.remove(at: 0)
//            self.sMapResponseData = sMapResponseData.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
//
//            array.append(contentsOf: self.folderDocResponseData)
//
//            self.folderDocResponseData = array
//
//            for folder in self.folderDocResponseData {
//                folder.docs = folder.docs?.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
//            }
            
            
            do {
                
                self.unfilteredOccurencesLocal = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                self.unfilteredSMapLocal = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
            }catch {
                print("Error fetching")
            }
            
            DispatchQueue.main.async {
                self.tvData.reloadData()
                //self.searchTableView.reloadData()
                if self.fileDeleted == true {
                    self.fileDeleted = false
                    self.showConfirmation(message: "Doc deleted successfully.")
                }
                
                if self.sMapDeleted == true {
                    self.sMapDeleted = false
                    self.showConfirmation(message: "SMap deleted successfully.")
                }
            }
        }else {
            do {
                
                self.unfilteredOccurencesLocal = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                self.unfilteredSMapLocal = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                //self.docs = self.docs.sorted{ $0.title!.lowercased() < $1.title!.lowercased() }
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
            }catch {
                print("Error fetching")
            }
        }
        
        
    }
    
    func getLocalFolderDocs() {
        
        //        do {
        //            self.docs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
        //
        //        }catch{
        //            print("Error fetching")
        //        }
        
        do {
            var localSMap : [SMap] = []
            sMapLocal = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
            
            for sMap in sMapLocal {
                
                var occurences : [Occurrence] = []
                var oldOccurences : [Occurrence] = []
                
                for occurence in sMap.occurences! {
                    
                    let occurenceToShow = occurence as! OccurrenceLocal
                    
                    let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId ), docRefIdEdited: Int(occurenceToShow.docRefIdEdited ))
                    occurences.append(occurenceModel)
                    
                }
                for occurence in sMap.oldOccurences! {

                    let occurenceToShow = occurence as! OccurrenceLocal

                    let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId ), docRefIdEdited: Int(occurenceToShow.docRefIdEdited ))
                    oldOccurences.append(occurenceModel)

                }
                
                let localSMapToShow = SMap(smapId: Int(sMap.smapId), isData: sMap.isData, title: sMap.title ?? "", firstEntry: sMap.firstEntry ?? "", secondEntry: sMap.secondEntry ?? "", userId: Int(sMap.userId), status: sMap.status, createdAt: sMap.createdAt ?? "", updatedAt: sMap.updatedAt ?? "", deletedAt: sMap.deletedAt ?? "", occurrences: occurences, offlineAction: sMap.offlineAction ?? "", isOmy: sMap.isOmy )
                
                localSMap.append(localSMapToShow)
            }
            self.sMapResponseData = localSMap
            let data = self.sMapResponseData.filter({$0.isData == true}).sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            let person = self.sMapResponseData.filter({$0.isData == false}).sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            self.dataForTableView.updateValue(data, forKey: sectionHeaderTitle[0])
            self.dataForTableView.updateValue(person, forKey: sectionHeaderTitle[1])
            //self.reloadTable()
            
        }catch{
            print("Error Fetching")
        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() {
            
            //                        let homeAPI = HomeAPI()
            //                        homeAPI.delegate = self
            //                        homeAPI.getFolderDocs()
            //            DispatchQueue.main.async {
            //                self.showSpinner(onView: self.view)
            //            }
            //self.showSpinner(onView: self.view)
            //
            //let homeAPI = HomeAPI()
            //homeAPI.delegate = self
            //            if self.folderDocResponseData.count > 0 {
            //                let params = Mapper().toJSONString(self.folderDocResponseData)?.replacingOccurrences(of: "\\\"", with: "\"")
            //                let d = params?.replacingOccurrences(of: "\\", with: "")
            //                 print(d)
            //                 self.callPostmanType(data: "folders_docs="+d!)
            //            }else {
            //                let homeAPI = HomeAPI()
            //                homeAPI.delegate = self
            //                homeAPI.getFolderDocs()
            //                DispatchQueue.main.async {
            //                    self.showSpinner(onView: self.view)
            //                }
            //
            //            }
            
            if AppUtility.sharedInstance.getHasLocalSMap() == true {
                self.reloadTable()
                //let params = Mapper().toJSONString(self.folderDocResponseData)?.replacingOccurrences(of: "\\\"", with: "\"")
                
                do {
                    self.deletedFiles = []
                    self.deletedFolders = []
                    let deletedDocs: [DeletedOccurence] = try AppUtility.sharedInstance.getContext().fetch(DeletedOccurence.fetchRequest())
                    for doc in deletedDocs {
                        self.deletedFiles.append(Int(doc.occurenceId))
                    }
                    let deletedFolders: [DeletedSMap] = try AppUtility.sharedInstance.getContext().fetch(DeletedSMap.fetchRequest())
                    for folder in deletedFolders {
                        self.deletedFolders.append(Int(folder.sMapId))
                    }
                }catch {
                    print("Error fetching")
                }
                
                //let params = self.folderDocResponseData.toJSONString()!.addingPercentEncoding(withAllowedCharacters: .symbols)//?.replacingOccurrences(of: "\\\"", with: "\"")
//                var occurences:[Occurrence] = []
//                for occurence in occurencesLocal {
//                    
//                    let occurenceToShow = occurence
//                    
//                    let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
//                    occurences.append(occurenceModel)
//                    
//                }
                
                let params = self.sMapResponseData.toJSONString()!.addingPercentEncoding(withAllowedCharacters: .symbols)//?.replacingOccurrences(of: "\\\"", with: "\"")
                
                let d = params?.replacingOccurrences(of: "\\", with: "")
                let paramsToSend = "folders_docs={\"folders_docs\":" + (params ?? "") + ",\"deleted_docs_ids\":\"[],\"deleted_folders_ids\":[]}"
                
                
                //let o = "occurences={\"occurences\":\(params!),\"deleted_occurences_ids\":\(self.deletedFiles)}"
                
                
                let x = "smaps_occurrences={\"smaps_occurrences\":\(params!),\"deleted_smaps_ids\":\(self.deletedFolders),\"deleted_occurrences_ids\":\(self.deletedFiles)}"
                
                print(x)
                //Commented
                self.syncSMap(data: x)
                
            }else {
                
                let smapAPI = SMapAPI()
                smapAPI.delegate = self
                smapAPI.getSMaps()
                DispatchQueue.main.async {
                    self.showSpinner(onView: self.view)
                }
                
            }
            
            
            //let jsonData = try! JSONSerialization.data(withJSONObject: d, options: [])
            
            //var jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            //let params = Mapper().toJSONArray(self.folderDocResponseData)
            
            
            
            //            let postData = "[{\"folder_id\":678,\"title\":\"Help\",\"user_id\":4,\"status\":false,\"docs\":[{\"user_id\":0,\"status\":false,\"description\":\"This is the admin pushed documnet.\",\"title\":\"Admin -2\",\"admin_pushed\":true,\"doc_id\":335,\"updated_at\":\"2020-08-27T12:05:05.000Z\",\"source\":\"admin\",\"folder_id\":0},{\"updated_at\":\"2020-08-26T14:03:35.000Z\",\"folder_id\":0,\"doc_id\":334,\"title\":\"Test1\",\"user_id\":0,\"status\":false,\"description\":\"The test is the testing team function.\",\"source\":\"admin\",\"admin_pushed\":true}],\"updated_at\":\"2020-08-21T15:24:33.000Z\"},{\"folder_id\":673,\"docs\":[{\"title\":\"rrrrrrrr\",\"updated_at\":\"2020-08-27T15:20:09.514Z\",\"status\":true,\"user_id\":4,\"admin_pushed\":false,\"description\":\"ttt\",\"folder_id\":673,\"source\":\"personal\",\"doc_id\":381}],\"user_id\":4,\"status\":false,\"title\":\"a\",\"updated_at\":\"2020-08-26T20:07:41.000Z\"},{\"folder_id\":677,\"docs\":[],\"updated_at\":\"2020-08-27T03:27:48.000Z\",\"user_id\":4,\"status\":false,\"title\":\"hhh\"},{\"folder_id\":676,\"updated_at\":\"2020-08-27T03:27:37.000Z\",\"docs\":[],\"user_id\":4,\"title\":\"jjj\",\"status\":false},{\"user_id\":4,\"title\":\"kol\",\"folder_id\":679,\"updated_at\":\"2020-08-27T03:31:15.000Z\",\"docs\":[{\"source\":\"personal\",\"status\":true,\"folder_id\":679,\"doc_id\":380,\"user_id\":4,\"updated_at\":\"2020-08-27T15:18:39.990Z\",\"description\":\"ttt\",\"title\":\"rrrrrrrr\",\"admin_pushed\":false}],\"status\":false},{\"status\":false,\"folder_id\":671,\"user_id\":4,\"title\":\"My folder\",\"docs\":[{\"source\":\"new\",\"updated_at\":\"2020-08-27T04:07:41.000Z\",\"status\":false,\"title\":\"new\",\"description\":\"wow\",\"folder_id\":671,\"user_id\":4,\"doc_id\":379,\"admin_pushed\":false}],\"updated_at\":\"2020-08-26T19:28:13.000Z\"},{\"updated_at\":\"2020-08-26T19:50:39.000Z\",\"title\":\"wow\",\"docs\":[],\"status\":false,\"user_id\":4,\"folder_id\":672}]"
            //
            //            let convertedStr = postData.replacingOccurrences(of: "\"", with: "", options: .literal, range: nil)
            //            print(convertedStr)
            
            //            let data = params.data(using: .utf8)!
            //            do {
            //                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            //                {
            //                   print(jsonArray) // use the json here
            //                } else {
            //                    print("bad json")
            //                }
            //            } catch let error as NSError {
            //                print(error)
            //            }
            //?.replacingOccurrences(of: "/", with: "")
            
            //         homeAPI.syncFolder(params: ["folders_docs": params])
            
            //
            //
            // prepare json data
            
            
            //            let j = [["title":"new","user_id":24,"created_at":"2020-08-13T14:42:54.000Z","updated_at":"2020-08-13T14:42:54.000Z","docs":[["title":"doc","user_id": 24,"folder_id": 765,"description": "description","source": "source","created_at":"2020-08-13T14:42:54.000Z","updated_at":"2020-08-13T14:42:54.000Z"],["title": "doc 2","user_id": 24,"folder_id":765,"description": "description","source": "source","created_at":"2020-08-13T14:42:54.000Z","updated_at": "2020-08-13T14:42:54.000Z"]]]]
            //            var s = ""
            //            do {
            //                let jsonData = try JSONSerialization.data(withJSONObject: j, options: .prettyPrinted)
            //                s = jsonData.base64EncodedString()
            //
            //            }catch{
            //                print("hello")
            //            }
            
            
            
            //            let body: [String: Any] = ["folders_docs": "[]" ]
            //
            //            //let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            //            //let j:[String:Any] = [:]
            //            // create post request
            //            //let url = URL(string: BASE_URL + syncFolderAPI)!
            //            let token = "Bearer " + AppUtility.sharedInstance.getToken()
            //            let url = URL(string: "https://api.sdoky.com/api/v1/folder-doc-sync")!
            //            var request = URLRequest(url: url)
            //            request.httpMethod = "POST"
            //
            //            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            //            request.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
            //            request.setValue("application/json", forHTTPHeaderField:"Accept")
            //
            //
            //            // insert json data to the request
            //            let bodyData = try? JSONSerialization.data(
            //                withJSONObject: body,
            //                options: []
            //            )
            //            request.httpBody = body.percentEncoded()
            //
            //
            //            let headers = [
            //                "Authorization":token,
            //                "Content-Type": "application/x-www-form-urlencoded",
            //                "Accept": "application/json"
            //            ]
            //
            
            
            
            //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            //            let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //                guard let data = data,
            //                    let response = response as? HTTPURLResponse,
            //                    error == nil else {                                              // check for fundamental networking error
            //                        print("error", error ?? "Unknown error")
            //
            //                        return
            //                }
            //
            //                guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
            //                    print("statusCode should be 2xx, but is \(response.statusCode)")
            //                    print("response = \(response)")
            //
            //                    return
            //                }
            //
            //                let responseString = String(data: data, encoding: .utf8)
            //                print("responseString = \(responseString)")
            //            }
            
            //task.resume()
            //            self.call()
            
            
            //            let homeAPI = HomeAPI()
            //            homeAPI.delegate = self
            //            homeAPI.syncFolder(params: ["folders_docs":postData])
            
            //            APIHandler.sharedInstance.getHttpManager().request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers).responseData { (data) in
            //                print(data.debugDescription)
            //            }
            
            
            //            let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //                guard let data = data, error == nil else {
            //                    print(error?.localizedDescription ?? "No data")
            //                    let homeAPI = HomeAPI()
            //                    homeAPI.delegate = self
            //                    homeAPI.getFolderDocs()
            //                    //self.showSpinner(onView: self.view)
            //                    return
            //                }
            //                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            //                if let responseJSON = responseJSON as? [String: Any] {
            //                    print(responseJSON)
            //                    let homeAPI = HomeAPI()
            //                    homeAPI.delegate = self
            //                    homeAPI.getFolderDocs()
            //                    //self.showSpinner(onView: self.view)
            //                }
            //            }
            //
            //            task.resume()
            
            //     let homeAPI = HomeAPI()
            //      homeAPI.delegate = self
            //homeAPI.syncFolder(params: ["folders_docs":[:]])
        }else {
            self.reloadTable()
            
        }
    }
    
    func showConfirmation(message: String) {
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            // self.didAddFolder()
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func call() {
        
        
        let JSONObject: [String : AnyObject] = ["folders_docs" : "[]"] as [String : AnyObject]
        
        
        
        if JSONSerialization.isValidJSONObject(JSONObject) {
            var request: NSMutableURLRequest = NSMutableURLRequest()
            let url = "https://api.sdoky.com/api/v1/folder-doc-sync"
            
            var err: NSError?
            
            request.url = NSURL(string: url) as URL?
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let token = "Bearer " + AppUtility.sharedInstance.getToken()
            
            request.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: JSONObject, options: JSONSerialization.WritingOptions())
                
                print(JSONObject)
                
                
            } catch {
                print("bad things happened")
            }
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) {(response, data, error) -> Void in
                
                let httpResponse = response as! HTTPURLResponse
                if error != nil {
                    
                    print("error")
                    
                    if let e = error as? NSError {
                        if e.code == 401 {
                            self.checkUnAuthorized()
                        }
                    }
                    
                }else {
                    print(response)
                    
                    
                }
                
            }
        }
    }
    
    
    func callPostmanType(data: String) {
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        //let postData = NSMutableData(data: data.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        let postData = data.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/folder-doc-sync")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                self.reloadTable()
                
                if let e = error as? NSError {
                    if e.code == 401 {
                        self.checkUnAuthorized()
                    }
                }
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                let sMapAPI = SMapAPI()
                sMapAPI.delegate = self
                sMapAPI.getSMaps()
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    
    
    func syncOccurences(data: String) {
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        //let postData = NSMutableData(data: data.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        let postData = data.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/occurrence-sync")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                self.reloadTable()
                
                if let e = error as? NSError {
                    if e.code == 401 {
                        self.checkUnAuthorized()
                    }
                }
                //           }
            } else {
                
                let params = self.sMapResponseData.toJSONString()!.addingPercentEncoding(withAllowedCharacters: .symbols)//?.replacingOccurrences(of: "\\\"", with: "\"")
                               
                               
                
                               
                               //let x = """folders_docs={"folders_docs":\(params!),\"deleted_docs_ids\":\(self.deletedFiles),\"deleted_folders_ids\":\(self.deletedFolders)}"""
                               
                               
//                               let s = "smaps={\"smaps\":\(params!),\"deleted_smaps_ids\":\(self.deletedFolders)}"
//
//                                self.syncSMap(data: s)
                               
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    func syncSMap(data: String) {
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        //let postData = NSMutableData(data: data.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        let postData = data.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/smap-occurrence-sync")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                self.reloadTable()
                if let e = error as? NSError {
                    if e.code == 401 {
                        self.checkUnAuthorized()
                    }
                }
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                
                do {
                    let deletedOccurencesArray : [DeletedOccurence] = try AppUtility.sharedInstance.getContext().fetch(DeletedOccurence.fetchRequest())
                    for d in deletedOccurencesArray {
                        AppUtility.sharedInstance.getContext().delete(d)
                    }
                    
                    let deletedSMapArray : [DeletedSMap] = try AppUtility.sharedInstance.getContext().fetch(DeletedSMap.fetchRequest())
                    for d in deletedSMapArray {
                        AppUtility.sharedInstance.getContext().delete(d)
                    }
                }catch {
                    print("error")
                }
                
                let sMapAPI = SMapAPI()
                sMapAPI.delegate = self
                sMapAPI.getSMaps()
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    // Search Logic
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let searchText  = tfSearch.text ?? ""
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            tvData.isHidden = true
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                
                //                        self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                //                            self.searchTableView.reloadData()
                
            }else {
                //                let homeAPI = HomeAPI()
                //                homeAPI.delegate = self
                //                homeAPI.getInternetSearchData(params: ["query":searchText])
                if AppUtility.sharedInstance.isConnectedToInternet() {
                    //                    self.searchDataResponse = []
                    //                    self.searchTableView.reloadData()
                    self.showSpinner(onView: self.view)
                    self.callPostmanTypeSearch(q: "query=" + searchText)
                }else {
                    self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                }
                
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        //self.endEditing()
        tfSearch.resignFirstResponder()
        
        return true
    }
    
    
    @objc func textIsChanging(_ textField: UITextField) {
        
        
        let searchText  = tfSearch.text!
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            tvData.isHidden = true
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                self.searchDataResponse = []
                self.sMapLocal = self.unfilteredSMapLocal.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.firstEntry!.lowercased().contains(searchText.lowercased()) || $0.secondEntry!.lowercased().contains(searchText.lowercased())})
                
                
            }else {
                self.sMapLocal = []
                //                let homeAPI = HomeAPI()
                //                homeAPI.delegate = self
                //                homeAPI.getInternetSearchData(params: ["query":searchText])
                // self.callPostmanTypeSearch(data: "query=" + searchText)
            }
        }
        else{
            self.sMapLocal = []
            self.searchDataResponse = []
            self.tvData.isHidden = false
            self.searchTableView.isHidden = true
            
        }
        self.searchTableView.reloadData()
        
        //return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//
//        let searchText  = tfSearch.text! + string
//
//        if searchText.count > 0 {
//            searchTableView.isHidden = false
//            tvData.isHidden = true
//            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
//            if myData == true {
//                self.searchDataResponse = []
//                self.sMapLocal = self.unfilteredSMapLocal.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.firstEntry!.lowercased().contains(searchText.lowercased()) || $0.secondEntry!.lowercased().contains(searchText.lowercased())})
//
//
//            }else {
//                self.sMapLocal = []
//                //                let homeAPI = HomeAPI()
//                //                homeAPI.delegate = self
//                //                homeAPI.getInternetSearchData(params: ["query":searchText])
//                // self.callPostmanTypeSearch(data: "query=" + searchText)
//            }
//        }
//        else{
//            self.sMapLocal = []
//            self.searchDataResponse = []
//
//        }
//        self.searchTableView.reloadData()
//
//        return true
//    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        //self.setToNormalState()
        self.sMapLocal = []
        self.searchTableView.isHidden = true
        self.tvData.isHidden = false
        self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
        self.endEditing()
        self.tfSearch.text = ""
    }
    
    func setToNormalState() {
        //        self.docs = []
        //        self.searchTableView.isHidden = true
        //        self.tvData.isHidden = false
        //        self.endEditing()
        
        if myData == false {
            
        }else {
            
            self.docs = []
            self.searchTableView.isHidden = true
            self.tvData.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
            self.endEditing()
            self.tfSearch.text = ""
            
        }
    }
    
    
    @IBAction func microphoneTapped(_ sender: Any) {
        
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            audioEngine.inputNode.removeTap(onBus: 0)
            //btnMic.isEnabled = false
            
            btnMic.setImage(UIImage(named: "mic"), for: .normal)
            //Commented
            //btnMicAnimated.stop()
            //Commented
            //  setToNormalState()
        } else {
            startRecording()
            btnMic.setImage(UIImage(named: "mic"), for: .normal)
            //Commented
            //btnMicAnimated.play()
            //Commented
        }
        
    }
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: [.notifyOthersOnDeactivation])
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode as? AVAudioInputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        var isFinal = false
        
        if let timer = self.detectionTimer, timer.isValid {
            if isFinal {
                print("final")
                self.detectionTimer?.invalidate()
            }
        } else {
            self.detectionTimer = Timer.scheduledTimer(withTimeInterval: 7, repeats: false, block: { (timer) in
                isFinal = true
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.btnMic.isEnabled = true
                
                self.btnMic.setImage(UIImage(named: "mic"), for: .normal)
                //Commented
                //self.btnMicAnimated.stop()
                //Commented
                timer.invalidate()
            })
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask =  speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            
            
            if result != nil {
                
                self.tfSearch.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
                
                let searchText  = self.tfSearch.text ?? ""
                if self.myData == true {
                }else {
                    //self.tfSearch.becomeFirstResponder()
                }
                
                if true {
                    self.searchTableView.isHidden = false
                    self.tvData.isHidden = true
                    if self.myData == true {
                        self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                        self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
                        self.searchTableView.reloadData()
                    }else {
                        if AppUtility.sharedInstance.isConnectedToInternet() {
                            
                            //self.removeSpinner()
                            //self.showSpinner(onView: self.view)
                            self.loader.isHidden = false
                            //                            self.searchDataResponse = []
                            //                            self.searchTableView.reloadData()
                            self.callPostmanTypeSearch(q: "query=" + searchText)
                        }else {
                            self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                        }
                        
                    }
                }
                else{
                    self.docs = []
                    
                }
                
                if error != nil || isFinal {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    
                    self.btnMic.isEnabled = true
                    
                    self.btnMic.setImage(UIImage(named: "mic"), for: .normal)
                    //Commented
                    //self.btnMicAnimated.stop()
                    //Commented
                }
            }
            
            
            
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        //tfSearch.text = ""
        
    }
    
    //    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
    //        if available {
    //            btnMic.isEnabled = true
    //        } else {
    //            btnMic.isEnabled = false
    //        }
    //    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            btnMic.isEnabled = true
        } else {
            btnMic.isEnabled = false
        }
    }
    
    
    
    
}

extension SMapVC {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    func didGetInternetResultsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        
        
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                    self.tvData.isHidden = true
                    self.searchTableView.isHidden = false
                    self.searchDataResponse = internetResponse.data!
                    self.searchTableView.reloadData()
                    
                    
                }
            }
            
            
        }
        
        
    }
    
    func didFailWithInternetResultsError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    @objc func yourFuncHere3() {
        self.removeSpinner()
    }
    
    
    func callPostmanTypeSearch(q: String) {
        
        
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        
        
        
        
        let postData = NSMutableData(data: q.data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/internet-search")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        request.setValue("\(postData.length)", forHTTPHeaderField: "Content-Length")
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        let session = URLSession(configuration: sessionConfig)
        
        //let session = URLSession.shared
        
        //self.showSpinner(onView: self.view)
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                
                if let e = error as? NSError {
                    if e.code == -1001 {
                        //self.callPostmanTypeSearch(q: q)
                    }
                    var message = ""
                    switch e.code {
                    case 401:
                        self.checkUnAuthorized()
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 402:
                        message = "Your credits are exhausted. You need to buy credits."
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 403:
                        message = "Your provided API key seems to be invalid."
                    case 404:
                        message = "The API endpoint you tried doesn't exist."
                    case 405:
                        message = "The request method you are trying isn't supported on this endpoint."
                    case 422:
                        message = "A validation error occured for some of your query params or form data."
                    case 429:
                        message = "A rare error that tells you that you are making API calls too quickly."
                    case 503:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    case 500:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    default:
                        message = "Something went wrong. Please try again."
                    }
                    
                    
                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                //self.searchDataResponse = []
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                //AppUtility.sharedInstance.setHasLocalData(hasLocalData: false)
                //self.reloadTable()
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                print(httpResponse)
                
                if httpResponse?.statusCode == 500 {
                    
                    let alert = UIAlertController(title: "Alert", message: "Server error. Please try again.", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    
                    
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }else {
                    
                    
                    do {
                        let resultDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                        
                        if let json = resultDict as? [String: Any] {
                            if json["status"] as! Bool == false {
                                
                                var message = ""
                                
                                if let code = json["status_code"] as? Int {
                                    
                                    switch code {
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 402:
                                        message = "Your credits are exhausted. You need to buy credits."
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 403:
                                        message = "Your provided API key seems to be invalid."
                                    case 404:
                                        message = "The API endpoint you tried doesn't exist."
                                    case 405:
                                        message = "The request method you are trying isn't supported on this endpoint."
                                    case 422:
                                        message = "A validation error occured for some of your query params or form data."
                                    case 429:
                                        message = "A rare error that tells you that you are making API calls too quickly."
                                    case 503:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    case 500:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    default:
                                        message = "Something went wrong. Please try again."
                                    }
                                    
                                    
                                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                                    
                                    // add the actions (buttons)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                                        action in
                                        
                                        //self.callPostmanType(data: q)
                                        
                                    }))
                                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                                    
                                    // show the alert
                                    DispatchQueue.main.async {
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    
                                }
                                
                                
                                
                                
                            }else {
                                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                                    
                                    self.searchDataResponse = internetResponse.data!
                                    DispatchQueue.main.async {
                                        self.searchTableView.reloadData()
                                    }
                                    
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                        
                    } catch {
                        NSLog("ERROR \(error.localizedDescription)")
                        
                    }
                }
                
                
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    
}
     
  
