//
//  WriteVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class WriteVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfType : UITextField!
    @IBOutlet weak var tfData : UITextView!
    @IBOutlet weak var btnSelectFolder : UIButton!
    
    var folder: FolderDocResponseData?
    var folders: [FolderDocResponseData]?
    var localFolders: [Folder]?
    var id = 0
    var source = ""
    var isFromInternetSearch : Bool = false
    var isFromScan = false
    var data: String?
    var searchTitle: String?
    var isFromOccurence = false
    
    var sMap: SMap?
    var occurenceId: Int?
    var isFromData = false
    var occurenceTitle = ""
    var descToEdit: String?
    
    var selectModelList: [SelectModel] = []
    
    var selectedFolder = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSelectFolder.layer.cornerRadius = 5.0
        btnSelectFolder.layer.borderWidth = 1.0
        btnSelectFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        tfData.layer.cornerRadius = 5.0
        tfData.layer.borderWidth = 1.0
        tfData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        self.tfTitle.attributedPlaceholder = NSAttributedString(string: "Type data title", attributes: attributes)
        self.tfType.attributedPlaceholder = NSAttributedString(string: "Type data source", attributes: attributes)
        
        tfData.delegate = self
        tfTitle.delegate = self
        tfType.delegate = self
        
        self.endEditing()
        if let folderData = folder {
            //self.btnSelectFolder.setAttributedTitle(NSAttributedString(string: folderData.title!, attributes: textFieldattributes), for: .normal)
            self.btnSelectFolder.setTitle(folderData.title, for: .normal)
            self.selectedFolder = SelectModel(id: folderData.folderId!, title: folderData.title!)
            
        }else {
            self.btnSelectFolder.setTitle("Choose folder to save", for: .normal)
        }
        
        if isFromInternetSearch == true {
            self.tfData.text = data ?? ""
            self.source = "Internet"
            self.tfType.text = ""
            self.tfTitle.text = searchTitle ?? ""
        } else if isFromScan == true {
            self.tfData.text = data ?? ""
            self.source = "Scan"
            self.tfType.text = ""
            self.tfTitle.text = ""
        }
        if isFromOccurence == true {
            self.tfTitle.text = self.occurenceTitle
        }
        
        if descToEdit != nil {
            self.tfData.text = descToEdit ?? ""
            self.tfTitle.text = searchTitle ?? ""
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.selectedFolder = SelectModel(id: -1, title: "NO FOLDER")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    @IBAction func btnSelectFolderAction(_ sender: UIButton) {
        
        self.selectModelList.removeAll()
        
        if let folders = self.folders {
            for folder in folders {
                self.selectModelList.append(SelectModel(id: folder.folderId!, title: folder.title!))
            }
            
            //var array = self.selectModelList.filter({$0.title == "Help"})
            //            self.selectModelList.remove(at: 0)
            //            self.selectModelList = selectModelList.sorted { $0.title.lowercased() < $1.title.lowercased() }
            //
            //            array.append(contentsOf: self.selectModelList)
            //
            //            self.selectModelList = array
            
            
            
            
            
            
            
            var array = self.selectModelList.filter({$0.title == "Help"})
            var oneArray = NSMutableArray()
            for (index, item) in array.enumerated() {
                if index == 0 {
                    oneArray.add(item)
                }
            }
            self.selectModelList = self.selectModelList.filter({$0.title != "Help"})
            self.selectModelList = selectModelList.sorted { $0.title.lowercased() < $1.title.lowercased() }
            
            oneArray.addObjects(from: self.selectModelList)
            
            self.selectModelList = oneArray as! [SelectModel]
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedFolder.id = model.id
            self.selectedFolder.title = model.title
            self.btnSelectFolder.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopup(controller, sourceView: sender as UIView)
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfType)
        self.removePhoneErrorLabel(message: "", texrField: btnSelectFolder)
        
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
            //        }else
            //            if tfType.text == "" {
            //            self.showError(message: "Required field", texrField: self.tfType)
        }else if self.selectedFolder.id == -1 {
            self.showErrorLabel(message: "Required field", texrField: self.btnSelectFolder)
        }else{
            self.source = self.tfType.text ?? ""
            if self.source == "" {
                if isFromInternetSearch == true {
                    self.source = "Internet"
                }else if isFromScan == true {
                    self.source = "Scan"
                }else {
                    self.source = "Personal"
                }
                
            }
            let params = ["title": self.tfTitle.text ?? "",
                          "folder_id": self.selectedFolder.id,
                          "description": self.tfData.text ?? "" ,
                          "source": self.source ] as [String : Any]
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.createDoc(params: params)
                DispatchQueue.main.async {
                    self.showSpinner(onView: self.view)
                }
                
                
            }else {
                
                do {
                    let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                    var docsCopy = docs
                    docsCopy.sort { (first, second) -> Bool in
                        first.docId < second.docId
                    }
                    if docsCopy.count > 0 {
                        self.id = Int(docsCopy.last!.docId) + 1
                    }
                    
                    let existDoc = docs.filter({$0.title == self.tfTitle.text})
                    if existDoc.count > 0 {
                        self.showError(message: "User doc already exists", texrField: self.tfTitle)
                        return
                    }
                    
                }catch{
                    print("Error Fetching")
                }
                
                do {
                    self.localFolders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                }catch {
                    print("Fetch Error")
                }
                
                if var folder = (self.localFolders!.filter{ $0.folderId == self.selectedFolder.id}).first {
                    let docToSave = Document(context: AppUtility.sharedInstance.getContext())
                    docToSave.id = Int16(self.id)
                    docToSave.title = self.tfTitle.text ?? ""
                    docToSave.folderId = Int16(self.selectedFolder.id)
                    docToSave.docId = Int16(self.id)
                    docToSave.desc = self.tfData.text ?? ""
                    docToSave.source = self.source
                    docToSave.status = true
                    docToSave.adminPushed = false
                    docToSave.updatedAt = Date().stringDateSDoky()
                    docToSave.createdAt = Date().stringDateSDoky()
                    docToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
                    docToSave.offlineAction = OFFLINE_ADD
                    docToSave.folderName = folder.title ?? ""
                    var docs = NSMutableSet()
                    for doc in folder.docs! {
                        docs.add(doc)
                    }
                    docs.add(docToSave)
                    folder.docs = docs
                    folder.offlineAction = OFFLINE_EDIT
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                    self.clear()
                    
                    
                    if isFromOccurence == true {
                        
                        do {
                            let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                            if let sMapToEdit = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                                sMapToEdit.updatedAt = Date().stringDateSDoky()
                                sMapToEdit.offlineAction = OFFLINE_EDIT
                                var occurences = NSMutableSet()
                                for occurence in sMapToEdit.occurences! {
                                    let o = occurence as! OccurrenceLocal
                                    if Int(o.occurenceId) == self.occurenceId {
                                        o.docRefId = Int16(self.id)
                                        o.offlineAction = OFFLINE_EDIT
                                        o.updatedAt = Date().stringDateSDoky()
                                    }
                                    occurences.add(o)
                                }
                                
                                sMapToEdit.occurences = occurences
                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                                if isFromData == true {
                                    self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                                }else {
                                    self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                                }
                            }
                            
                        }catch {
                            print("Fetch Error")
                        }
                        
                    }else {
                        if self.folder != nil {
                            self.navigationController?.popViewController(animated: false)
                            //                        self.folder = nil
                        }else {
                            if isFromInternetSearch == true || isFromScan == true {
                                self.showHome()
                            }else {
                                self.tabBarController?.selectedIndex = 0
                                (self.tabBarController?.viewControllers![0] as! DataVC).setToNormalState()
                            }
                            
                        }
                    }
                    
                    //self.showAlertWithMessageTitleAlert(message: "Document saved successfully.")
                }
                
                
                
                
                //                do {
                //
                //                    let docToSave = Document(context: AppUtility.sharedInstance.getContext())
                //                    docToSave.id = Int16(self.id)
                //                    docToSave.title = self.tfTitle.text ?? ""
                //                    docToSave.folderId = Int16(self.selectedFolder.id)
                //                    docToSave.docId = Int16(self.id)
                //                    docToSave.desc = self.tfData.text ?? ""
                //                    docToSave.source = self.tfType.text ?? ""
                //                    docToSave.status = true
                //                    docToSave.adminPushed = false
                //                    docToSave.updatedAt = Date().stringDateSDoky()
                //                    docToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
                //                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                //                    self.navigationController?.popViewController(animated: true)
                //
                //                }catch{
                //                    print("Error Saving")
                //                }
                
                
                //               do {
                //                    self.localFolders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                //
                //                }catch{
                //                    print("Error Fetching")
                //                }
                //
                //                if self.localFolders!.count > 0 {
                //                    for folder in localFolders! {
                //                      AppUtility.sharedInstance.getContext().delete(folder)
                //                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                //                    }
                //
                //               }
                //
                //
                //                for folder in self.localFolders! {
                //
                //                    var folderToSave = Folder(context: AppUtility.sharedInstance.getContext())
                //                    if folder.folderId == self.selectedFolder.id {
                //
                //                        folderToSave.id = Int16(folder.folderId )
                //                        folderToSave.title = folder.title ?? ""
                //                        folderToSave.folderId = Int16(folder.folderId )
                //                        folderToSave.status = folder.status
                //                        folderToSave.userId = Int16(folder.userId )
                //                        folderToSave.updatedAt = folder.updatedAt ?? ""
                //                        let docToSave = Document(context: AppUtility.sharedInstance.getContext())
                //                        docToSave.id = Int16(0)
                //                        docToSave.title = self.tfTitle.text ?? ""
                //                        docToSave.folderId = Int16(self.selectedFolder.id)
                //                        docToSave.docId = Int16(0)
                //                        docToSave.desc = self.tfData.text ?? ""
                //                        docToSave.source = self.tfType.text ?? ""
                //                        docToSave.status = true
                //                        docToSave.adminPushed = false
                //                        let dateFormatter = DateFormatter()
                ////                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                ////                        let d = dateFormatter.date(from: Date())
                //                        docToSave.updatedAt = "2020-08-26T14:03:55.000Z"
                //                        docToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
                //                        folder.docs?.adding(docToSave)
                //                        folderToSave.docs = folder.docs
                //
                //                    }else {
                //                        folderToSave = folder
                //                    }
                //
                //                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                //                }
                
            }
            
            
            
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        if tfTitle.text != "" || tfType.text != "" || tfData.text != ""  {
            showSaveAlert()
        }else {
            self.clear()
//            if folder != nil {
//                self.navigationController?.popViewController(animated: false)
//            }else {
//
//                if isFromInternetSearch == true || isFromScan == true {
//                    //self.showHome()
//                    //self.navigationController?.popToRootViewController(animated: false)
//                    self.navigationController?.backToViewController(viewController: UITabBarController.self)
//                }else {
//                    self.tabBarController?.selectedIndex = 0
//                }
//            }
            self.navigateProper()
            
            
        }
    }
    
    func navigateProper() {
        
        if isFromOccurence == true {
            
            if descToEdit != nil {
                self.navigationController?.popViewController(animated: false)
            }else {
                if isFromData == true {
                    self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                }else {
                    self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                }
            }
            
        }else {
            if folder != nil {
                self.navigationController?.popViewController(animated: false)
            }else {
                if isFromInternetSearch == true || isFromScan == true {
                    //self.showHome()
                    //self.navigationController?.popToRootViewController(animated: false)
                    self.navigationController?.backToViewController(viewController: UITabBarController.self)
                }else {
                    self.tabBarController?.selectedIndex = 0
                }
                
            }
        }
        
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if tfTitle.text != "" || tfType.text != "" || tfData.text != ""  {
            showSaveAlert()
        }else {
            
            
            self.clear()
            
//            if isFromOccurence == true {
//                if isFromData == true {
//                    self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
//                }else {
//                    self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
//                }
//            }else {
//                if folder != nil {
//                    self.navigationController?.popViewController(animated: false)
//                }else {
//                    if isFromInternetSearch == true || isFromScan == true {
//                        //self.showHome()
//                        //self.navigationController?.popToRootViewController(animated: false)
//                        self.navigationController?.backToViewController(viewController: UITabBarController.self)
//                    }else {
//                        self.tabBarController?.selectedIndex = 0
//                    }
//
//                }
//            }
            self.navigateProper()
            
            
        }
    }
    
    func showHome() {
        let tabBarController = UITabBarController()
        let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
        let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
        let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
        let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
        tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        
        self.show(tabBarController, sender: self)
    }
    
    func showSaveAlert() {
        
        let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your edit process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            if self.folder != nil {
                self.navigationController?.popViewController(animated: false)
            }else {
                
//                if self.isFromInternetSearch == true || self.isFromScan == true {
//                    //self.showHome()
//                    //self.navigationController?.popToRootViewController(animated: false)
//                    self.navigationController?.backToViewController(viewController: UITabBarController.self)
//
//                }else {
//                    self.tabBarController?.selectedIndex = 0
//                    self.clear()
//                }
                self.clear()
                self.navigateProper()
                
            }
            //self.clear()
            //self.tabBarController?.selectedIndex = 0
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func didCreateDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        DispatchQueue.main.async {
            self.removeSpinner()
            if let json = resultDict as? [String: Any] {
                if json["status"] as! Bool == false {
                    
                    self.showError(message: json["message"] as! String, texrField: self.tfTitle)
                }else {
                    self.clear()
                    if self.folder != nil {
                        self.navigationController?.popViewController(animated: false)
                    }else {
                        
                        self.tabBarController?.selectedIndex = 0
                    }
                    
                }
            }
            
        }
        
    }
    
    func didFailWiCreateDocError(_ error: NSError, resultStatus: Bool) {
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        print(error.debugDescription)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        //        for constraint in view.constraints {
        //            if constraint.identifier == "fpConstraint" {
        //                constraint.constant = 40.0
        //            }
        //        }
        
        for constraint in view.constraints {
            
            if constraint.identifier == texrField.placeholder {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            
            if constraint.identifier == texrField.placeholder {
                constraint.constant = 16.0
            }
        }
    }
    
    func showErrorLabel(message: String, texrField: UIButton) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneErrorLabel(message: String, texrField: UIButton) {
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
}

extension WriteVC : HomeAPIDelegate {
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        //self.view.window!.windowLevel = UIWindow.Level.statusBar
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationBar.isHidden = false
        //        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: -24, width: self.view.bounds.width, height: 44)
        
        
        
        //self.navigationController?.navigationBar.alpha = 0.2
        //let bounds = self.navigationController!.navigationBar.bounds
        //self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: UIApplication.shared.statusBarFrame.height)
        
        //self.navigationController?.navigationBar.sizeThatFits(CGSize(width: UIScreen.main.bounds.width, height: 90))
        
        
        
        
        //self.clear()
        
        //if AppUtility.sharedInstance.isConnectedToInternet() {
        if false {
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.getFolderDocs()
            DispatchQueue.main.async {
                self.showSpinner(onView: self.view)
            }
        }else {
            do {
                
                let folders : [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                var folderDocs : [FolderDocResponseData] = []
                for folder in folders {
                    let folderToShow = FolderDocResponseData(folderId: Int(folder.folderId), title: folder.title ?? "", userId: Int(folder.userId), status: folder.status, updatedAt: folder.updatedAt ?? "")
                    folderDocs.append(folderToShow)
                }
                self.folders = folderDocs
                
            }catch{
                print("Error Fetching")
            }
            
        }
        
        
        
    }
    
    func didGetFolderDocsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let folderDocResponse:FolderDocResponse = Mapper<FolderDocResponse>().map(JSON: json) {
                    
                    self.folders = folderDocResponse.data!
                }
            }
            
            
        }
        
    }
    
    func didFailGetFolderDocsError(_ error: NSError, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        print(error.debugDescription)
    }
    
    func clear() {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfType)
        self.removePhoneErrorLabel(message: "", texrField: btnSelectFolder)
        self.tfData.text = ""
        self.tfType.text = ""
        self.tfTitle.text = ""
        if let folderData = folder {
            //self.btnSelectFolder.setAttributedTitle(NSAttributedString(string: folderData.title!, attributes: textFieldattributes), for: .normal)
            self.btnSelectFolder.setTitle(folderData.title, for: .normal)
            self.selectedFolder = SelectModel(id: folderData.folderId!, title: folderData.title!)
            
        }else {
            self.btnSelectFolder.setTitle("Choose folder to save", for: .normal)
        }
        
    }
}



