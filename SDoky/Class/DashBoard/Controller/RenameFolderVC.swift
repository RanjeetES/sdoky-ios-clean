//
//  RenameFolderVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

protocol RenameVCDelegate {
    func didRenameFolder()
}


class RenameFolderVC: UIViewController, HomeAPIDelegate {
    
    @IBOutlet weak var btnRenameFolder : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfNewFolderName : UITextField!
    
    var delegate:RenameVCDelegate?
    var currentTitle: String?
    var folderId: Int?
    var localFolders : [Folder] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btnRenameFolder.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        
        tfNewFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "folder-open", hasError: false, view: self.view, message: "")
        
        self.tfNewFolderName.attributedText = NSAttributedString(string: self.currentTitle ?? "", attributes: textFieldattributes)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    @IBAction func btnRenameAction(_ sender: Any) {
        self.removeErrorViews()
        
        if tfNewFolderName.text == "" {
            tfNewFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: "Required field")
        }else {
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.updateFolder(params: ["title": self.tfNewFolderName.text ?? self.currentTitle ?? "", "folder_id": self.folderId ?? 0])
                
            }else {
                
                do {
                    self.localFolders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                }catch {
                    print("Fetch Error")
                }
                
                let existFolder = self.localFolders.filter({$0.title == self.tfNewFolderName.text})
                if existFolder.count > 0 {
                     tfNewFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: "Folder with same name already exists")
                    return
                }
                
                if var folder = self.localFolders.filter({Int($0.folderId) == self.folderId}).first {
                    folder.title = self.tfNewFolderName.text ?? self.currentTitle ?? ""
                    folder.offlineAction = OFFLINE_EDIT
                }
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                self.delegate?.didRenameFolder()
                self.dismiss(animated: true, completion: nil)
                
            }
            
            
        }
    }
    
    func didUpdateFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                tfNewFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: json["message"] as! String)
            }else {
                
                self.delegate?.didRenameFolder()
                self.dismiss(animated: true, completion: nil)
                
            }
        }
        
    }
    
    
    func didFailUpdateFolderError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
