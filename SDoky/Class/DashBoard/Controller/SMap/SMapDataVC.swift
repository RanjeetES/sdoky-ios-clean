//
//  WriteVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class SMapDataVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, SMapAPIDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfBody : UITextView!
    @IBOutlet weak var tfConclusion : UITextView!
    
    var id : Int = 1
    var sMapId : Int = 1
    var localSMaps : [SMapLocal] = []
    var sMap: SMap?
    //let hashtagRegex = "(([#])((([a-zA-Z\-'"\s])\w+){1,5})([,]))"
    //let hashtagRegex = "#[-_a-zA-Z\\s0-9]+,"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        tfBody.layer.cornerRadius = 5.0
        tfBody.layer.borderWidth = 1.0
        tfBody.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        tfBody.delegate = self
        
        tfConclusion.layer.cornerRadius = 5.0
        tfConclusion.layer.borderWidth = 1.0
        tfConclusion.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        tfConclusion.delegate = self
        
        
        tfTitle.delegate = self
        
        self.endEditing()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfBody)
        self.removePhoneError(message: "", texrField: tfConclusion)
        
        let bodyTextToSave = self.tfBody.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression]).lowercased()
        let conclusionTextToSave = self.tfConclusion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression]).lowercased()
        
        let bodyTextToSaveFinal = self.tfBody.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        let conclusionTextToSaveFinal = self.tfConclusion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else if tfBody.text == "" && tfConclusion.text == "" {
            self.showError(message: "Any one field is mandatory other than title", texrField: self.tfBody)
            self.showError(message: "", texrField: self.tfConclusion)
//        }else if tfConclusion.text == "" {
//            self.showError(message: "Required field", texrField: self.tfConclusion)
        }else{
            
            do {
                let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                var occurencesCopy = occurences
                occurencesCopy.sort { (first, second) -> Bool in
                    first.occurenceId < second.occurenceId
                }
                if occurencesCopy.count > 0 {
                    self.id = Int(occurencesCopy.last!.occurenceId) + 1
                }
                
            }catch{
                print("Error Fetching")
            }
            
            var occurences = NSMutableSet()
            //let m = self.tfBody.text.matches(for: hashtagRegex)
            //let mC = self.tfConclusion.text.matches(for: hashtagRegex)
            let m = bodyTextToSave.matches(for: hashtagRegex)
            let mC = conclusionTextToSave.matches(for: hashtagRegex)
            
            var mCopy = bodyTextToSave.matches(for: hashtagRegex)
            mCopy.append(contentsOf: mC)
            
            if mCopy.unique.count != m.unique.count + mC.unique.count {
                self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            
            
            if m.count > m.unique.count {
                self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            if mC.count > mC.unique.count {
                self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            for h in m {
                occurences.add(self.createOccurences(title: h, type: true))
            }
            
            for h in mC {
                occurences.add(self.createOccurences(title: h, type: false))
            }
            
            do {
                self.localSMaps = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                var sMapsCopy = self.localSMaps
                sMapsCopy.sort { (first, second) -> Bool in
                    first.smapId < second.smapId
                }
                if sMapsCopy.count > 0 {
                    self.sMapId = Int(sMapsCopy.last!.smapId) + 1
                }
                
            }catch {
                print("Fetch Error")
            }
            
            let sMapToSave = SMapLocal(context: AppUtility.sharedInstance.getContext())
            sMapToSave.smapId = Int16(self.sMapId)
            sMapToSave.isData = true
            sMapToSave.title = self.tfTitle.text ?? ""
            sMapToSave.oldTitle = self.tfTitle.text ?? ""
            sMapToSave.firstEntry = bodyTextToSaveFinal
            sMapToSave.secondEntry = conclusionTextToSaveFinal
            sMapToSave.oldFirstEntry = bodyTextToSaveFinal
            sMapToSave.oldSecondEntry = conclusionTextToSaveFinal
            sMapToSave.status = true
            sMapToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
            sMapToSave.createdAt = Date().stringDateSDoky()
            sMapToSave.updatedAt = Date().stringDateSDoky()
            sMapToSave.offlineAction = OFFLINE_ADD
            sMapToSave.isOmy = false
//            for occurence in occurences {
//                occurences.add(occurence)
//            }
            sMapToSave.occurences = occurences
            sMapToSave.oldOccurences = occurences
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            //AppUtility.sharedInstance.getAndSyncSMaps()
            self.clear()
            //self.sMap = self.localToSMap(sMapLocal: sMapToSave)
            self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapToSave)
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let sMapAPi = SMapAPI()
                sMapAPi.delegate = self
                let params = ["is_data":true,"title":self.tfTitle.text ?? "", "first_entry":bodyTextToSaveFinal,"second_entry":conclusionTextToSaveFinal] as [String : Any]
                sMapAPi.createSMap(params: params)
            }else {
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapDataVC") as! EditSMapDataVC
                vc.sMap = self.sMap
                vc.firstEdit = true
                self.show(vc, sender: self)
            }
            
            
            
            
        }
        
    }
    
    func didCreateSMapSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapDataVC") as! EditSMapDataVC
                vc.sMap = self.sMap
                vc.firstEdit = true
                self.show(vc, sender: self)
                
            }else {
                if let response:SMapResponse = Mapper<SMapResponse>().map(JSON: json) {
                }
            }
        }
        
    }
    
    func didFailWithCreateSMapError(_ error: NSError, resultStatus: Bool) {
        print("error")
        let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapDataVC") as! EditSMapDataVC
        vc.sMap = self.sMap
        vc.firstEdit = true
        self.show(vc, sender: self)
    }
    
    
    func localToSMap(sMapLocal: SMapLocal) -> SMap {
        
        let data = sMapLocal
       var occurences : [Occurrence] = []
        var oldOccurences : [Occurrence] = []
        
        for occurence in data.occurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            occurences.append(occurenceModel)
            
        }
        
        for occurence in data.oldOccurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            oldOccurences.append(occurenceModel)
            
        }
        
        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: false)
        
        return localSMapToShow
        
    }
    
    
    
    func createOccurences(title: String, type: Bool) -> OccurrenceLocal {
            
            do {
                let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                var occurencesCopy = occurences
                occurencesCopy.sort { (first, second) -> Bool in
                    first.occurenceId < second.occurenceId
                }
                if occurencesCopy.count > 0 {
                    self.id = Int(occurencesCopy.last!.occurenceId) + 1
                }
                
            }catch{
                print("Error Fetching")
            }
            
            let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
            occurenceToSave.occurenceId = Int16(self.id)
            occurenceToSave.smapId = Int16(self.sMapId)
            occurenceToSave.title = title
            occurenceToSave.isFirst = type
            occurenceToSave.desc = nil
            occurenceToSave.updatedAt = Date().stringDateSDoky()
            occurenceToSave.offlineAction = OFFLINE_ADD
            occurenceToSave.createdAt = Date().stringDateSDoky()
            occurenceToSave.newTitle = title
            occurenceToSave.newDesc = nil
            occurenceToSave.tempTitle = nil
            occurenceToSave.docRefId = Int16(0)
            occurenceToSave.newDocRefId = Int16(0)
            occurenceToSave.docRefIdEdited = Int16(0)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        return occurenceToSave
            
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        if tfTitle.text != "" || tfBody.text != "" || tfConclusion.text != ""  {
            showSaveAlert()
        }else {
            self.clear()
            self.navigationController?.popViewController(animated: false)
            
        }
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if tfTitle.text != "" || tfBody.text != "" || tfConclusion.text != ""  {
            showSaveAlert()
        }else {
            self.clear()
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func showSaveAlert() {
        
        let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your edit process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            self.navigationController?.popViewController(animated: false)
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "entity" {
                constraint.constant = 40.0
            }
        }
    }
    
    func showError(message: String, texrField: UITextView) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "body" {
                 constraint.constant = 40.0
             }
            if constraint.identifier == "conclusion" {
               constraint.constant = 40.0
             }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "entity" {
                constraint.constant = 16.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextView) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "body" {
                 constraint.constant = 16.0
             }
            if constraint.identifier == "conclusion" {
               constraint.constant = 16.0
             }
        }
    }
    
    func showErrorLabel(message: String, texrField: UIButton) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneErrorLabel(message: String, texrField: UIButton) {
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
}

extension SMapDataVC : HomeAPIDelegate {
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.navigationController?.navigationBar.isHidden = true
        
    }
        
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
    }
    
    func clear() {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.tfBody.text = ""
        self.tfConclusion.text = ""
        self.tfTitle.text = ""
        
    }
}



