//
//  AddNewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditOccurenceTitleVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    
    
    var sMap: SMap?
    var occurenceId: Int?
    var isFromData: Bool?
    var occurenceTitle : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        tfTitle.delegate = self

        let occurence = sMap?.occurrences?.filter{$0.occurenceId == self.occurenceId}.first
        
        let trimmedTitle = occurence?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
        self.tfTitle.text = trimmedTitle
        self.occurenceTitle = trimmedTitle ?? ""
        self.endEditing()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
           IQKeyboardManager.shared.enable = false
           IQKeyboardManager.shared.enableAutoToolbar = false
       }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
       
       override func viewDidAppear(_ animated: Bool) {
           IQKeyboardManager.shared.enable = true
           IQKeyboardManager.shared.enableAutoToolbar = true
           
       }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        
        
        
        
        
        
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else  if tfTitle.text?.count ?? 0 > 25 {
            self.showError(message: "Title cannot be more than 25 characters", texrField: self.tfTitle)
        }else{
            
            do {
                var oldTitle = ""
                var newTitle = ""
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                let occurences: [OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                                    
                               
                if self.tfTitle.text != self.occurenceTitle {
                    let existOccurence = self.sMap!.occurrences!.filter({$0.title?.lowercased() == "#\(tfTitle.text?.lowercased() ?? ""),"})
                                   if existOccurence.count > 0 {
                                         self.showError(message: "Occurrence title already exists", texrField: self.tfTitle)
                                       return
                                   }
                }
               
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    var occurences = NSMutableSet()
                    for occurence in sMapToEdit.occurences! {
                        let o = occurence as! OccurrenceLocal
                        if Int(o.occurenceId) == self.occurenceId {
                            oldTitle = o.title ?? ""
                            newTitle = "#\(tfTitle.text ?? ""),"
                            if tfTitle.text != "" {
                                o.newTitle = o.title
                                o.title = newTitle
                            }else {
                                o.title = oldTitle
                            }
                            //o.title = self.tfTitle.text ?? o.title ?? ""
                            //o.newDesc = o.desc ?? ""
                            o.newDesc = o.desc
                            o.offlineAction = OFFLINE_EDIT
                            o.updatedAt = Date().stringDateSDoky()
                        }
                        occurences.add(o)
                    }
                    
                    sMapToEdit.occurences = occurences
                    //sMapToEdit.oldFirstEntry = sMapToEdit.firstEntry
                    //sMapToEdit.oldSecondEntry = sMapToEdit.secondEntry
                    sMapToEdit.firstEntry = sMapToEdit.firstEntry?.replacingOccurrences(of: oldTitle, with: newTitle)
                    sMapToEdit.secondEntry = sMapToEdit.secondEntry?.replacingOccurrences(of: oldTitle, with: newTitle)
                    
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                    //AppUtility.sharedInstance.getAndSyncSMaps()
                    //self.navigationController?.popViewController(animated: false)
                    //self.navigationController?.popViewController(animated: false)
                    //AppUtility.sharedInstance.getAndSyncSMaps()
                    if isFromData == true {
                        self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                    }else {
                        self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                    }
                    
                    //self.clear()
                }
                
            }catch {
                print("Fetch Error")
            }
        }
        
        
        
        
        
        
        
//        if tfTitle.text == "" {
//            self.showError(message: "Required field", texrField: self.tfTitle)
//        }else  if tfTitle.text?.count ?? 0 > 25 {
//                   self.showError(message: "Title cannot be more than 25 characters", texrField: self.tfTitle)
//               }else{
//
//            do {
//                var oldTitle = ""
//                var newTitle = ""
//                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
//
//                let occurences: [OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
//
//
//
//                let existOccurence = self.sMap!.occurrences!.filter({$0.title == "#\(tfTitle.text ?? ""),"})
//               if existOccurence.count > 0 {
//                     self.showError(message: "Occurrence title already exists", texrField: self.tfTitle)
//                   return
//               }
//
//                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
//                    sMapToEdit.updatedAt = Date().stringDateSDoky()
//                    sMapToEdit.offlineAction = OFFLINE_EDIT
//                    var occurences = NSMutableSet()
//                    for occurence in sMapToEdit.occurences! {
//                        let o = occurence as! OccurrenceLocal
//                        if Int(o.occurenceId) == self.occurenceId {
//                            oldTitle = o.title ?? ""
//                            newTitle = "#\(tfTitle.text ?? ""),"
//                            if tfTitle.text != "" {
//                                o.title = newTitle
//                            }else {
//                                o.title = oldTitle
//                            }
//                            o.offlineAction = OFFLINE_EDIT
//                            o.updatedAt = Date().stringDateSDoky()
//                        }
//                        occurences.add(o)
//                    }
//                    sMapToEdit.occurences = occurences
//                    sMapToEdit.firstEntry = sMapToEdit.firstEntry?.replacingOccurrences(of: oldTitle, with: newTitle)
//                    sMapToEdit.secondEntry = sMapToEdit.secondEntry?.replacingOccurrences(of: oldTitle, with: newTitle)
//                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
//                    AppUtility.sharedInstance.getAndSyncSMaps()
////                    if isFromData == true {
////                        self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
////                    }else {
////                        self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
////                    }
//                    self.navigationController?.popViewController(animated: false)
//
//                }
//
//            }catch {
//                print("Fetch Error")
//            }
//        }
        
    }
    
    func showError(message: String, texrField: UITextField) {
           
           texrField.layer.borderWidth = 1.0
           texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
           texrField.layer.cornerRadius = 5.0
           texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
           
           let x: CGFloat = 0
           let y: CGFloat = 0
           let height: CGFloat = 24.0
           
           let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
           
           let constraints = [
               label.heightAnchor.constraint(equalToConstant: height),
               label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
               texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
               texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
           ]
           
           label.text = message
           label.font = UIFont(name: "Montserrat-Light", size: 11)
           label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
           label.translatesAutoresizingMaskIntoConstraints = false
           
           view.addSubview(label)
           view.layoutIfNeeded()
           
           NSLayoutConstraint.activate(constraints)
           
           for constraint in view.constraints {
               
               if constraint.identifier == "entity" {
                   constraint.constant = 40.0
               }
           }
       }
       
       func removePhoneError(message: String, texrField: UITextField) {
           
           texrField.layer.borderWidth = 1.0
           texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
           texrField.layer.cornerRadius = 5.0
           texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
           
           for constraint in view.constraints {
               
               if constraint.identifier == "entity" {
                   constraint.constant = 16.0
               }
           }
       }
    
//    func localToSMap(sMapLocal: SMapLocal) -> SMap {
//
//        let data = sMapLocal
//        var occurences : [Occurrence] = []
//
//            for occurence in data.occurences! {
//
//                let occurenceToShow = occurence as! OccurrenceLocal
//
//                let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
//                occurences.append(occurenceModel)
//
//            }
//
//        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: data.isOmy)
//
//        return localSMapToShow
//
//    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func clear() {
        self.tfTitle.text = ""
        
    }
    
}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
