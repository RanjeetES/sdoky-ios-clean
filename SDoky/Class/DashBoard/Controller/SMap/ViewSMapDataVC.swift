//
//  ViewDocVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/22/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

import UIKit
import ObjectMapper
import CoreData
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer

class ViewSMapDataVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var tfBody : UITextView!
    @IBOutlet weak var tfConclusion : UITextView!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnDownload : UIButton!
    
    var sMap: SMap?
    var body = ""
    var conclusion = ""
    var bodyAttributed: NSMutableAttributedString?
    var conclusionAttributed: NSMutableAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.checkExpiry()
        
         do{
                    let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
            }
            }catch {
                print("Error")
            }
        
//        self.tfTitle.text = self.sMap?.title ?? ""
//
//        if (self.sMap?.title ?? "").count > 16 {
//            self.tfTitle.text = "\((self.sMap?.title ?? "").trunc(length: 16))..."
//        }else {
//            self.tfTitle.text = self.sMap?.title ?? ""
//        }
        
        self.lblTitle.text = self.sMap?.title ?? ""
        
//        if (self.sMap?.title ?? "").count > 16 {
//            self.lblTitle.text = "\((self.sMap?.title ?? "").trunc(length: 16))..."
//        }else {
//            self.lblTitle.text = self.sMap?.title ?? ""
//        }
        
        
        
        self.body = (self.sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
        self.conclusion = (self.sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
        
        //self.trimString()
        bodyAttributed = NSMutableAttributedString(string: self.body)
        conclusionAttributed = NSMutableAttributedString(string: self.conclusion)
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        
        bodyAttributed!.addAttributes(attrs1, range: NSRange(location: 0, length: self.body.count))
        conclusionAttributed!.addAttributes(attrs1, range: NSRange(location: 0, length: self.conclusion.count))
        
        if let occurences = sMap?.occurrences {
            let os = occurences.sorted { (o1, o2) -> Bool in
                o1.title?.count ?? 0 < o2.title?.count ?? 0
            }
            for occurence in os {
                //                if occurence.description != "" && occurence.description != nil {
                //                    self.cleanStr(occurence: occurence)
                //                }else {
                //                    self.cleanStrWithNoDesc(occurence: occurence)
                //                }
                self.cleanStr(occurence: occurence)
                
            }
        }
        //self.body = self.body.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
        //self.conclusion = self.conclusion.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
        
        //        tfBody.linkTextAttributes = [.underlineStyle: 0]
        //        tfConclusion.linkTextAttributes = [.underlineStyle: 0]
        
        let htmlStringBody = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        a {text-decoration: none;}
        .color {
        color: #0e80e4;
        }
        .a-c {text-decoration: underline;}
        </style>
        </head>
        <body>
        
        \(self.body)
        </body>
        </html>
        """
        
        let htmlStringConclusion = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        a {text-decoration: none;}
        .color {
        color: #0e80e4;
        }
        .a-c {text-decoration: underline;}
        </style>
        </head>
        <body>
        
        \(self.conclusion)
        </body>
        </html>
        """
        
        //                if let htmlText = htmlStringBody.htmlAttributedString {
        //                    tfBody.attributedText = htmlText
        //                }
        //
        //
        //                if let htmlText = htmlStringConclusion.htmlAttributedString {
        //                    tfConclusion.attributedText = htmlText
        //                }
        
        //self.anotherClean()
        
        tfBody.attributedText = bodyAttributed
        tfConclusion.attributedText = conclusionAttributed
        tfBody.delegate = self
        tfConclusion.delegate = self
        
        if sMap?.isOmy == false {
            btnDownload.isHidden = true
        }else{
            btnDownload.isHidden = false
        }
        
        // let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextView(_:)))
        //    tfBody.addGestureRecognizer(tapGesture)
        //  let tapGestureC = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextViewC(_:)))
        //    tfConclusion.addGestureRecognizer(tapGestureC)
        
    }
    
    func anotherClean() {
                
        let f = self.sMap?.firstEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
        let s = self.sMap?.secondEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
        
        var a = NSMutableAttributedString(string: f ?? "")
        var b = NSMutableAttributedString(string: s ?? "")
        
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        
        a.addAttributes(attrs1, range: NSRange(location: 0, length: f?.count ?? 0))
        b.addAttributes(attrs1, range: NSRange(location: 0, length: s?.count ?? 0))
        
        
        
        //        for o in sMap!.occurrences! {
        //            if o.description != nil {
        //                if o.description != "" {
        //                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.underlineColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR),]
        //                    if o.isFirst == true {
        //                        if let r = self.sMap?.firstEntry?.range(of: o.title!) {
        //                            a.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.firstEntry ?? ""))
        //                        }
        //
        //                    }else {
        //                        if let r = self.sMap?.secondEntry?.range(of: o.title!) {
        //                            b.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.secondEntry ?? ""))
        //                        }
        //
        //                    }
        //
        //
        //
        //                }else {
        //                   let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.underlineColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR),
        //                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.init(rawValue: 1)] as [NSAttributedString.Key : Any]
        //
        //                    if o.isFirst == true {
        //                        if let r = self.sMap?.firstEntry?.range(of: o.title!) {
        //                             a.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.firstEntry ?? ""))
        //                        }
        //
        //                    }else {
        //
        //                        if let r = self.sMap?.secondEntry?.range(of: o.title!) {
        //                            b.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.secondEntry ?? ""))
        //                        }
        //
        //                    }
        //
        //                }
        //            }else {
        //                let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.underlineColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR),
        //                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.init(rawValue: 1)] as [NSAttributedString.Key : Any]
        //
        //                if o.isFirst == true {
        //                    if let r = self.sMap?.firstEntry?.range(of: o.title!) {
        //                        a.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.firstEntry ?? ""))
        //                    }
        //
        //                }else {
        //                    if let r = self.sMap?.secondEntry?.range(of: o.title!) {
        //                        b.addAttributes(attrs1, range: NSRange(range: r, in: self.sMap?.secondEntry ?? ""))
        //                    }
        //
        //
        //                }
        //
        //            }
        //        }
        tfBody.attributedText = self.bodyAttributed
        tfConclusion.attributedText = self.conclusionAttributed
        
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditClick(_ sender: Any) {
        let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapDataVC") as! EditSMapDataVC
        vc.sMap = self.sMap
        vc.firstEdit = false
        self.show(vc, sender: self)
    }
    
    @IBAction func btnDownloadAction(_ sender: Any) {
//        APIHandler.sharedInstance.downloadSMapPdf(sMapId: self.sMap?.smapId ?? -1, uniqueName: self.sMap?.title ?? "Unnamed", isFolder: false) { (message, status) in
//            self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
//        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadSMapPdf(sMapId: sMap?.smapId ?? -1, uniqueName: sMap?.title ?? "Unnamed", isFolder: false) { (message, status) in
                       self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
                   }
        }else{
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
    }
    
    private func resolveHighlightedRanges(pattern: String, text: String) -> [NSRange] {
        guard text != nil, let regex = try? NSRegularExpression(pattern: pattern, options: [.caseInsensitive]) else { return [] }
        
        let matches = regex.matches(in: text, options: [], range: NSRange(text.startIndex..<text.endIndex, in: text))
        let ranges = matches.map { $0.range }
        return ranges
    }
    
    func cleanStr(occurence : Occurrence) {
        let titleWithout  = occurence.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        let title  = occurence.title ?? ""
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), NSAttributedString.Key.link: URL(string: "link/\(occurence.occurenceId ?? 0)")!,] as [NSAttributedString.Key : Any]
        
        var attrs = [
            NSAttributedString.Key.font :  UIFont(name: "Montserrat-Light", size: 13),
            NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: "838383"),
            NSAttributedString.Key.underlineStyle : 0] as [NSAttributedString.Key : Any]
        
        var attributes : [NSAttributedString.Key : Any]?
        
        
        if occurence.docRefId != nil {
            if occurence.docRefId != 0 {
        //if occurence.description != nil {
        //    if occurence.description != "" {
                
                attributes = attrs1 as [NSAttributedString.Key : Any]
            }else {
                
                attributes = attrs
            }
        }else {
            attributes = attrs
            
            
        }
        
        
        //        if occurence.isFirst == true {
        //            if let r = self.body.range(of: titleWithout) {
        //                self.bodyAttributed!.addAttributes(attrs1, range: NSRange(range: r, in: self.body ?? ""))
        //            }
        //
        //        }else {
        //            if let r = self.conclusion.range(of: titleWithout) {
        //                self.conclusionAttributed!.addAttributes(attrs1, range: NSRange(range: r, in: self.conclusion))
        //            }
        //
        //        }
        
        if false {
            
            //let ranges = self.body.ranges(of: title, options: [], locale: nil)
            var replacementB = self.body
            var replacementC = self.conclusion
            let bCount = replacementB.indicesOf(string: title).count
            let cCount = replacementC.indicesOf(string: title).count
            
            let bCountWithout = replacementB.countInstances(of: titleWithout).0
            let cCountWithout = replacementC.countInstances(of: titleWithout).0
            
            if bCount > 0 {
                for _ in 1...bCount {
                    if let r = replacementB.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementB), with: s)
                        if let range = replacementB.range(of: title, options: .literal) {
                            replacementB = replacementB.replacingCharacters(in: range, with: "")
                        }
                        
                    }
                }
            }
            
            
            if cCount > 0 {
                for _ in 1...cCount {
                    if let r = replacementC.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementC), with: s)
                        if let range = replacementC.range(of: title, options: .literal) {
                            replacementC = replacementC.replacingCharacters(in: range, with: "")
                        }
                    }
                }
            }
            
            if bCountWithout > 0 {
                for _ in 1...bCountWithout {
                    if let r = replacementB.range(of: titleWithout) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementB), with: s)
                        if let range = replacementB.range(of: titleWithout, options: .diacriticInsensitive) {
                            replacementB = replacementB.replacingCharacters(in: range, with: "")
                        }
                        
                    }
                }
            }
            
            
            if cCountWithout > 0 {
                for _ in 1...cCountWithout {
                    if let r = replacementC.range(of: titleWithout) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementC), with: s)
                        if let range = replacementC.range(of: titleWithout, options: .diacriticInsensitive) {
                            replacementC = replacementC.replacingCharacters(in: range, with: "")
                        }
                    }
                }
            }
            
            
            
            //                if let r = self.body.range(of: title) {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attrs1)
            //                    bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: self.body), with: s)
            //                }
            
        }else {
            //let ranges = self.conclusion.ranges(of: title, options: [], locale: nil)
            
            //                if let r = self.conclusion.range(of: title) {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attrs1)
            //                    conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: self.conclusion), with: s)
            //                }
            
            let rangesAgain = self.resolveHighlightedRanges(pattern: "\\b" + titleWithout + "\\b" ,text: self.body)
            for r in rangesAgain {
                let rep = bodyAttributed?.mutableString.substring(with: r)
                let s = NSAttributedString(string: rep ?? "", attributes: attributes)
                bodyAttributed?.replaceCharacters(in: r, with: s)
            }
            
            
            let rangesAgainC = self.resolveHighlightedRanges(pattern: "\\b" + titleWithout + "\\b" ,text: self.conclusion)
            for r in rangesAgainC {
                 let rep = conclusionAttributed?.mutableString.substring(with: r)
                let s = NSAttributedString(string: rep ?? "", attributes: attributes)
                conclusionAttributed?.replaceCharacters(in: r, with: s)
            }
            
//            let rangesAgainSpace = self.resolveHighlightedRanges(pattern: titleWithout ,text: self.body)
//            for r in rangesAgainSpace {
//                                
//                let s = NSAttributedString(string: titleWithout, attributes: attributes)
//                bodyAttributed?.replaceCharacters(in: r, with: s)
//            }
//            
//            
//            let rangesAgainCSpace = self.resolveHighlightedRanges(pattern:  titleWithout ,text: self.conclusion)
//            for r in rangesAgainCSpace {
//                let s = NSAttributedString(string:  titleWithout , attributes: attributes)
//                conclusionAttributed?.replaceCharacters(in: r, with: s)
//            }
            
            let ranges = self.resolveHighlightedRanges(pattern: title ,text: self.body)
            for r in ranges {
                                
                let s = NSAttributedString(string: titleWithout, attributes: attributes)
                bodyAttributed?.replaceCharacters(in: r, with: s)
                
                
                if let range = self.body.range(of: title, options: .diacriticInsensitive) {
                    self.body = self.body.replacingOccurrences(of: "[#,]", with: "", options: [.regularExpression], range: range)
               }
                
            }
            
            let rangesC = self.resolveHighlightedRanges(pattern: title ,text: self.conclusion)
            for r in rangesC {
                let s = NSAttributedString(string: titleWithout, attributes: attributes)
                conclusionAttributed?.replaceCharacters(in: r, with: s)
                
                if let range = self.conclusion.range(of: title, options: .diacriticInsensitive) {
                     self.conclusion = self.conclusion.replacingOccurrences(of: "[#,]", with: "", options: [.regularExpression], range: range)
                }
            }
            
            
            
        }
        
        
        
        
        
        
        //        self.body = self.body.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
        //        self.conclusion = self.conclusion.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
        
        
    }
    
    func findRange(searchTerm: String, searchSentence: String) -> [Range<String.Index>] {
        var searchRange = searchSentence.startIndex..<searchSentence.endIndex
        var ranges: [Range<String.Index>] = []
        while let range = searchSentence.range(of: searchTerm, range: searchRange) {
            ranges.append(range)
            searchRange = range.upperBound..<searchRange.upperBound
        }
        print(ranges)
        
        return ranges
        //print(ranges.map { "(\(searchSentence.distance(from: searchSentence.startIndex, to: $0.lowerBound)), \(searchSentence.distance(from: searchSentence.startIndex, to: $0.upperBound)))" })
    }
    
    func trimString() {
        self.body = self.body.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        self.conclusion = self.conclusion.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
    }
    
    
    
    func cleanStrWithNoDesc(occurence: Occurrence) {
        let titleWithout  = occurence.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        let title  = occurence.title ?? ""
        
        var attrs = [
            NSAttributedString.Key.font :  UIFont(name: "Montserrat-Light", size: 13),
            NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: "838383"),
            NSAttributedString.Key.underlineStyle : 0] as [NSAttributedString.Key : Any]
        if true {
            var replacementB = self.body
            var replacementC = self.conclusion
            let bCount = replacementB.indicesOf(string: title).count
            let cCount = replacementC.indicesOf(string: title).count
            
            if bCount > 0 {
                for _ in 1...bCount {
                    if let r = replacementB.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attrs)
                        bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementB), with: s)
                        if let range = replacementB.range(of: title, options: .literal) {
                            replacementB = replacementB.replacingCharacters(in: range, with: titleWithout)
                        }
                        
                    }
                }
            }
            
            
            if cCount > 0 {
                for _ in 1...cCount {
                    if let r = replacementC.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attrs)
                        conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementC), with: s)
                        if let range = replacementC.range(of: title, options: .literal) {
                            replacementC = replacementC.replacingCharacters(in: range, with: titleWithout)
                        }
                    }
                }
            }
        }else {
            
            //                    if let r = self.conclusion.range(of: title) {
            //                        let s = NSAttributedString(string: titleWithout, attributes: attrs)
            //                        conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: self.conclusion), with: s)
            //                    }
            
            //let ranges = self.conclusion.ranges(of: title, options: [], locale: nil)
            
            
        }
        
        //        self.body = self.body.replacingOccurrences(of: occurence.title!, with: "<span class='a-c' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</span>", options: [.literal])
        //         self.conclusion = self.conclusion.replacingOccurrences(of: occurence.title!, with: "<span class='a-c' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</span>", options: [.literal])
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        let id = Int(URL.pathComponents.last ?? "0")
        
        let o = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first
       //let body = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first?.description ?? ""
        let title = o?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        let docRefId = o?.docRefId
        
        do {
                               let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                               if let doc = docs.filter({$0.docId == Int16(docRefId ?? 0)}).first {
                                    
                                    //self.showAlertWithMessageWithTitle(title: title, message: body)
                                let body = doc.desc
                                    let viewController = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewOccurenceVC") as! ViewOccurenceVC
                                           // Initialize the bottom sheet with the view controller just created
                                    viewController.occurenceTitleString = title
                                    viewController.occurenceDescString = body
                                           let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
                                           bottomSheet.trackingScrollView?.scrollsToTop = true
                                    bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.6)
                                           // Present the bottom sheet
                                           bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
                                           bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                                           present(bottomSheet, animated: true, completion: nil)
                                
                               }
                               
                               
                           }catch{
                               print("Error Fetching")
                           }
        
        
        
        
        
        return false // return true if you also want UIAlertController to pop up
    }
    
    @objc func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        let point = tapGesture.location(in: tfBody)
        if let detectedWord = getWordAtPosition(point)
        {
            
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
                //self.occurenceClick(id: o.occurenceId ?? 0, view: tfBody)
                if o.docRefId != nil && o.docRefId != 0 {
                //if o.description != nil && o.description != "" {
                    //self.occurenceClick(title: o.title ?? "", body: o.description ?? "")
                    do {
                        let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        if let doc = docs.filter({$0.docId == Int16(o.docRefId ?? 0)}).first {
                            self.occurenceClick(title: o.title ?? "", body: doc.desc ?? "")
                        }
                        
                        
                    }catch{
                        print("Error Fetching")
                    }
                }
                
            }
        }
    }
    
    @objc func tapOnTextViewC(_ tapGesture: UITapGestureRecognizer){
        let point = tapGesture.location(in: tfConclusion)
        if let detectedWord = getWordAtPositionC(point)
        {
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
                //self.occurenceClick(id: o.occurenceId ?? 0, view: tfConclusion)
                if o.docRefId != nil && o.docRefId != 0 {
                //if o.description != nil && o.description != "" {
                    //self.occurenceClick(title: o.title ?? "", body: o.description ?? "")
                    do {
                        let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        if let doc = docs.filter({$0.docId == Int16(o.docRefId ?? 0)}).first {
                            self.occurenceClick(title: o.title ?? "", body: doc.desc ?? "")
                        }
                        
                        
                    }catch{
                        print("Error Fetching")
                    }
                }
            }
        }
    }
    
    private final func getWordAtPosition(_ point: CGPoint) -> String?{
        if let textPosition = tfBody.closestPosition(to: point)
        {
            if let range = tfBody.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            {
                if (tfBody.rangeFromTextRange(textRange: range).location <= tfBody.text.indicesOf(string: tfBody.text(in: range)!).first!) {
                    return tfBody.text(in: range)
                }else {
                    return "Not found any occurences - RK - RS Bro"
                }
                
            }
        }
        return nil}
    
    private final func getWordAtPositionC(_ point: CGPoint) -> String?{
        if let textPosition = tfConclusion.closestPosition(to: point)
        {
            if let range = tfConclusion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            {
                if (tfConclusion.rangeFromTextRange(textRange: range).location <= tfConclusion.text.indicesOf(string: tfConclusion.text(in: range)!).first!) {
                    return tfConclusion.text(in: range)
                }else {
                    return "Not found any occurences - RK - RS Bro"
                }
            }
        }
        return nil}
    
    
    func occurenceClick(title: String, body: String) {
        let t = title.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
        self.showAlertWithMessageWithTitle(title: t, message: body)
        
    }
    
    
}

