//
//  SearchViewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 10/19/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class SearchViewDataVC: UIViewController {
    
    @IBOutlet weak var txtView : UITextView!
    @IBOutlet weak var btnEditAndSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDateSource : UILabel!
    @IBOutlet weak var btnBack : UIButton!
    
    
    var data : String = ""
    var searchTitle: String?
    var sMap: SMap?
    var occurenceId: Int?
    var isFromData :  Bool?
    var descToEdit: String?
    var docTitle: String?
    var docDate: String?
    var docSource: String?
    var docRefId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEditAndSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        self.txtView.text = descToEdit
        self.lblTitle.text = docTitle
        self.lblDateSource.text = "\(docDate?.changeDateFormatString() ?? "") (\(docSource ?? "")))"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    
    @IBAction func btnCancelAction( _sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEditAndSaveAction( _sender: Any) {
        
       // let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "EditDataVC") as! EditDataVC
        
       // vc.sMap = self.sMap
//        vc.occurenceId = self.occurenceId
//        vc.isFromData = self.isFromData ?? true
//        vc.isFromOccurence = true
            //vc.occurenceTitle = occurenceTitle.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
//        vc.occurenceTitle = searchTitle ?? ""
//        vc.descToEdit = descToEdit
//        vc.searchTitle = self.searchTitle
        
        
//        vc.sMap = self.sMap
//        vc.occurenceId = self.occurenceId
//        vc.isFromData = self.isFromData ?? true
//        vc.isFromOccurence = true
//        vc.docId = self.docRefId
//        self.show(vc, sender: self)
        
    
    
//        vc.sMap = self.sMap
//        vc.occurenceId = self.occurenceId
//        vc.isFromData = self.isFromData
//        vc.descToEdit = descToEdit as! String
//        self.show(vc, sender: self)
        
        
            
            do {
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    var occurences = NSMutableSet()
                    for occurence in sMapToEdit.occurences! {
                        let o = occurence as! OccurrenceLocal
                        if Int(o.occurenceId) == self.occurenceId {
                            o.docRefId = Int16(self.docRefId ?? 0)
                            o.offlineAction = OFFLINE_EDIT
                            o.updatedAt = Date().stringDateSDoky()
                        }
                        occurences.add(o)
                    }
                    
                    sMapToEdit.occurences = occurences
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                    if isFromData == true {
                        self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                    }else {
                        self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                    }
                }
                
            }catch {
                print("Fetch Error")
            }
            
        
       
    }
    
}
