//
//  DataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/10/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer
import Alamofire
import CoreData
import IQKeyboardManagerSwift



//class SearchTableHeaderView : UITableViewHeaderFooterView {
//    @IBOutlet weak var lblDocumentCount: UILabel!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//    }
//}
//
//class SearchDataTableViewCell :  UITableViewCell {
//
//    @IBOutlet weak var ivIcon: UIImageView!
//    @IBOutlet weak var lblName: UILabel!
//    @IBOutlet weak var lblData: UILabel!
//
//}

class OccurenceSearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, HomeAPIDelegate {
    
    
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnMydata: UIButton!
    @IBOutlet weak var btnInternet: UIButton!
    @IBOutlet weak var vbtnData :  UIView!
    @IBOutlet weak var vbtnInternet :  UIView!
    @IBOutlet weak var btnSearchIcon: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var folderResponseData: [FolderResponseData] = []
    var folderDocResponseData: [FolderDocResponseData] = []
    var folders : [Folder] = []
    var unfilteredDocs : [Document] = []
    var docs : [Document] = []
    var myData = true
    var searchDataResponse :  [InternetsearchResponseData] = []
    var sMap: SMap?
    var occurenceId: Int?
    var searchText: String?
    var isFromData : Bool?
    var docRefId: Int?
    
    
    //Search
    @IBOutlet weak var searchTableView : UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let searchHeaderNib = UINib.init(nibName: "SearchTableHeaderView", bundle: nil)
        self.searchTableView.register(searchHeaderNib, forHeaderFooterViewReuseIdentifier: "searchHeader")
        self.navigationController?.navigationBar.isHidden = true
        
        self.endEditing()
        self.tfSearch.delegate = self

        btnMydata.layer.borderWidth = 4.0
        btnMydata.layer.masksToBounds = false
        btnMydata.layer.cornerRadius = btnMydata.frame.height/2
        btnMydata.layer.borderColor = UIColor.white.cgColor
        btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        vbtnData.layer.borderWidth = 2.0
        vbtnData.layer.masksToBounds = false
        vbtnData.layer.cornerRadius = vbtnData.frame.height/2
        vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        btnInternet.layer.borderWidth = 4.0
        btnInternet.layer.masksToBounds = false
        btnInternet.layer.cornerRadius = btnInternet.frame.height/2
        btnInternet.layer.borderColor = UIColor.white.cgColor
        btnInternet.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnInternet.backgroundColor = UIColor.white
        
        vbtnInternet.layer.borderWidth = 2.0
        vbtnInternet.layer.masksToBounds = false
        vbtnInternet.layer.cornerRadius = vbtnInternet.frame.height/2
        vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        //Search TableView
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        self.searchTableView.tableFooterView = UIView()
        self.tfSearch.text = self.searchText ?? ""
        do {
            
            self.unfilteredDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
           // self.unfilteredDocs = self.unfilteredDocs.filter{$0.adminPushed == false}
            
        }catch {
            print("Error fetching")
        }
        
        let searchText  = tfSearch.text!
            
        if searchText.count > 0 {
            searchTableView.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                self.searchDataResponse = []
                //self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased()) || $0.folderName!.lowercased().contains(searchText.lowercased())})
                self.filter(searchText: searchText)
                
                
            }else {
                self.docs = []
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        self.searchTableView.reloadData()
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        // IQKeyboardManager.shared.enable = true
        print("disappear")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.checkExpiry()
        IQKeyboardManager.shared.enable = false
        
        self.getLocalFolderDocs()
        self.tabBarController?.tabBar.isHidden = false
        //self.setToNormalState()
        loader.isHidden = true
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
            var count = 0
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "searchHeader") as? SearchTableHeaderView
            if myData == true {
                count = self.docs.count
            }else {
                
                count = self.searchDataResponse.count
                if count == 0 {
                    view?.isHidden = true
                }else {
                    view?.isHidden = false
                }
            }
            
            view?.lblDocumentCount.text = "\(count) results found"
            return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView()
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
            return CGFloat.init(0.0)
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return CGFloat.init(16.0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
            if myData == true {
                return self.docs.count
            }else {
                return self.searchDataResponse.count
            }
                
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            
            let cell = tableView.dequeueReusableCell(withIdentifier: "occurence_search_cell", for: indexPath) as! SearchDataTableViewCell
            var title = ""
            var desc = ""
            
            if myData == true {
                
                if self.docs[indexPath.row].adminPushed == true {
                    cell.ivIcon.image = UIImage(named: "file-green")
                }else {
                    cell.ivIcon.image = UIImage(named: "doc")
                }
                title = self.docs[indexPath.row].title ?? ""
                desc = (self.docs[indexPath.row].desc ?? "").trunc(length: 80)
            }else {
                
                title = self.searchDataResponse[indexPath.row].title ?? ""
                desc = (self.searchDataResponse[indexPath.row].snippet ?? "").trunc(length: 120)
            }
            
            let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
            
            let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
            
            let attributedString1 = NSMutableAttributedString(string :desc, attributes:attrs1)
            
            
            let attributedString2 = NSMutableAttributedString(string: "... Read more", attributes:attrs2)
            
            attributedString1.append(attributedString2)
            // self.lblText.attributedText = attributedString1
            cell.lblName.text = title
            
            cell.lblData.attributedText = attributedString1
            
            return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                    
            if myData == true {
                
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "SearchViewDataVC") as! SearchViewDataVC
                vc.sMap = self.sMap
                vc.occurenceId = self.occurenceId
                vc.descToEdit = self.docs[indexPath.row].desc
                vc.docTitle = self.docs[indexPath.row].title
                vc.docDate = self.docs[indexPath.row].updatedAt
                vc.docSource = self.docs[indexPath.row].source
                vc.isFromData = self.isFromData
                vc.searchTitle = self.docs[indexPath.row].title
                vc.docRefId = Int(self.docs[indexPath.row].docId)
                
                self.show(vc, sender: self)
            }else {
                
                if self.searchDataResponse.count > 0 {
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WebPopOverController") as! WebPopOverController
                    vc.url = self.searchDataResponse[indexPath.row].link
                    vc.folders = self.folderDocResponseData
                    vc.searchTitle = self.searchDataResponse[indexPath.row].title
                    vc.isFromSMap = true
                    vc.sMap = self.sMap
                    vc.occurenceId = self.occurenceId
                    vc.isFromData = self.isFromData
                    self.show(vc, sender: self)
                }
                
                
            }
            
    }
    
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 12)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnDataAction(_ sender: Any) {
        
        self.btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnInternet.backgroundColor = UIColor.white
        self.vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        self.vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.myData = true
        //self.searchTableView.isHidden = true
        //self.searchDataResponse = []
        //self.searchTableView.reloadData()
        let searchText  = tfSearch.text!
            
        if searchText.count > 0 {
            searchTableView.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                self.searchDataResponse = []
                //self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased()) || $0.folderName!.lowercased().contains(searchText.lowercased())})
                self.filter(searchText: searchText)
                
            }else {
                self.docs = []
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        self.searchTableView.reloadData()
        
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnInternetAction(_ sender: Any) {
        self.docs = []
        self.searchTableView.reloadData()
        self.btnMydata.layer.backgroundColor = UIColor.white.cgColor
        self.btnInternet.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        self.vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.myData = false
        
        let searchText  = tfSearch.text ?? ""
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
            
                
            }else {
               
                if AppUtility.sharedInstance.isConnectedToInternet() {
                    self.showSpinner(onView: self.view)
                    self.callPostmanTypeSearch(q: "query=" + searchText)
                }else {
                    self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                }
                
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
            
        }
        //self.endEditing()
        tfSearch.resignFirstResponder()
        
    }
    

    func didGetFolderDocsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let folderDocResponse:FolderDocResponse = Mapper<FolderDocResponse>().map(JSON: json) {
                    self.folderDocResponseData = folderDocResponse.data!
                    do {
                        self.folders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if self.folders.count > 0 {
                        for folder in folders {
                            AppUtility.sharedInstance.getContext().delete(folder)
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }
                    
                    do {
                        self.docs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if self.docs.count > 0 {
                        for doc in docs {
                            AppUtility.sharedInstance.getContext().delete(doc)
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }
                    self.docs = []
                    
                    for folder in folderDocResponseData {
                        let folderToSave = Folder(context: AppUtility.sharedInstance.getContext())
                        folderToSave.id = Int16(folder.folderId ?? -1)
                        folderToSave.title = folder.title ?? ""
                        folderToSave.folderId = Int16(folder.folderId ?? -1)
                        folderToSave.status = folder.status ?? true
                        folderToSave.userId = Int16(folder.userId ?? -1)
                        folderToSave.createdAt = folder.createdAt ?? ""
                        folderToSave.updatedAt = folder.updatedAt ?? ""
                        folderToSave.offlineAction = ""
                        var docs = NSMutableSet()
                        for doc in folder.docs! {
                            let docToSave = Document(context: AppUtility.sharedInstance.getContext())
                            docToSave.id = Int16(doc.docId ?? 0)
                            docToSave.title = doc.title ?? ""
                            docToSave.folderId = Int16(doc.folderId ?? 0)
                            docToSave.docId = Int16(doc.docId ?? 0)
                            docToSave.desc = doc.description ?? ""
                            docToSave.source = doc.source ?? ""
                            docToSave.status = doc.status ?? true
                            docToSave.adminPushed = doc.adminPushed ?? false
                            docToSave.updatedAt = doc.updatedAt ?? ""
                            docToSave.userId = Int16(doc.userId ?? 0)
                            docToSave.offlineAction = ""
                            docToSave.createdAt = doc.createdAt ?? ""
                            docToSave.folderName = folder.title ?? ""
                            docs.add(docToSave)
                        }
                        folderToSave.docs = docs
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }
                    self.folderDocResponseData = folderDocResponse.data!
                    self.reloadTable()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                }
            }
            
            
        }
        
    }
    
    func didFailGetFolderDocsError(_ error: NSError, resultStatus: Bool) {
        
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    func changeDateFormat(date: String) -> String {
        if date != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "d.MM.y"
            let goodDate = dateFormatter.string(from: d!)
            return goodDate
        }else {
            return ""
        }
        
    }

    func reloadTable() {
        
        if true {
            
            var array = self.folderDocResponseData.filter({$0.title == "Help"})
            //        for (index, item) in array.enumerated() {
            //            if index != 0 {
            //                array.remove(at: index)
            //            }
            //        }
            
            self.folderDocResponseData.remove(at: 0)
            self.folderDocResponseData = folderDocResponseData.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            
            array.append(contentsOf: self.folderDocResponseData)
            
            self.folderDocResponseData = array
            
            for folder in self.folderDocResponseData {
                folder.docs = folder.docs?.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            }
            
            
            do {
                
                self.unfilteredDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
               // self.unfilteredDocs = self.unfilteredDocs.filter{$0.adminPushed == false}
                
            }catch {
                print("Error fetching")
            }
            
            
        }else {
            do {
                
                self.unfilteredDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                //self.unfilteredDocs = self.unfilteredDocs.filter{$0.adminPushed == false}
                //self.docs = self.docs.sorted{ $0.title!.lowercased() < $1.title!.lowercased() }
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
            }catch {
                print("Error fetching")
            }
        }
        
    }
    
    func getLocalFolderDocs() {
    
        do {
            var localFolderDocResponse : [FolderDocResponseData] = []
            folders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
            
            for folder in folders {
                
                var docs : [Doc] = []
                
                for doc in folder.docs! {
                    
                    let docToShow = doc as! Document
                    
                    let documentModel = Doc(docId: Int(docToShow.docId), folderId: Int(docToShow.folderId), title: docToShow.title ?? "", userId: Int(docToShow.userId), status: docToShow.status, description: docToShow.desc ?? "", updatedAt: docToShow.updatedAt ?? "", source: docToShow.source ?? "", adminPushed: docToShow.adminPushed, offlineAction: docToShow.offlineAction ?? "", createdAt: docToShow.createdAt ?? "")
                    docs.append(documentModel)
                    
                }
                let localFolder = FolderDocResponseData(folderId: Int(folder.folderId), title: folder.title ?? "", userId: Int(folder.userId), status: folder.status, updatedAt: folder.updatedAt ?? "", docs: docs, offlineAction: folder.offlineAction ?? "", createdAt: folder.createdAt ?? "")
                
                localFolderDocResponse.append(localFolder)
            }
            self.folderDocResponseData = localFolderDocResponse
            self.reloadTable()
            
        }catch{
            print("Error Fetching")
        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() {
            
            
            if AppUtility.sharedInstance.getHasLocalData() == true {}
            else {
                
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.getFolderDocs()
                DispatchQueue.main.async {
                    self.showSpinner(onView: self.view)
                }
                
            }
            
        }else {
            self.reloadTable()
            
        }
    }
    
    
    // Search Logic
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let searchText  = tfSearch.text ?? ""
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                
                //                        self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                //                            self.searchTableView.reloadData()
                
            }else {
                //                let homeAPI = HomeAPI()
                //                homeAPI.delegate = self
                //                homeAPI.getInternetSearchData(params: ["query":searchText])
                if AppUtility.sharedInstance.isConnectedToInternet() {
                    //                    self.searchDataResponse = []
                    //                    self.searchTableView.reloadData()
                    self.showSpinner(onView: self.view)
                    self.callPostmanTypeSearch(q: "query=" + searchText)
                }else {
                    self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                }
                
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        //self.endEditing()
        tfSearch.resignFirstResponder()
        
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var searchText  = tfSearch.text! + string
        
        if string == "" {
            searchText  = String(tfSearch.text! .dropLast())
        }else {
            searchText  = tfSearch.text! + string
        }
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                self.searchDataResponse = []
                //self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased()) || $0.folderName!.lowercased().contains(searchText.lowercased())})
                self.filter(searchText: searchText)
                
                
            }else {
                self.docs = []
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        self.searchTableView.reloadData()
        
        return true
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        //self.setToNormalState()
        self.docs = []
        self.searchTableView.isHidden = true
        self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
        self.endEditing()
        self.tfSearch.text = ""
    }
    
    func setToNormalState() {
        
        if myData == false {
            
        }else {
            
            self.docs = []
            self.searchTableView.isHidden = true
            self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
            self.endEditing()
            self.tfSearch.text = ""
            
        }
    }
    
    
}

extension OccurenceSearchVC {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    func didGetInternetResultsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                    self.searchTableView.isHidden = false
                    self.searchDataResponse = internetResponse.data!
                    self.searchTableView.reloadData()
                    
                    
                }
            }
            
            
        }
        
        
    }
    
    func didFailWithInternetResultsError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    
    func callPostmanTypeSearch(q: String) {
        
        
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        let postData = NSMutableData(data: q.data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/internet-search")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        request.setValue("\(postData.length)", forHTTPHeaderField: "Content-Length")
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        let session = URLSession(configuration: sessionConfig)
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                
                if let e = error as? NSError {
                    if e.code == -1001 {
                        //self.callPostmanTypeSearch(q: q)
                    }
                    var message = ""
                    switch e.code {
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 402:
                        message = "Your credits are exhausted. You need to buy credits."
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 403:
                        message = "Your provided API key seems to be invalid."
                    case 404:
                        message = "The API endpoint you tried doesn't exist."
                    case 405:
                        message = "The request method you are trying isn't supported on this endpoint."
                    case 422:
                        message = "A validation error occured for some of your query params or form data."
                    case 429:
                        message = "A rare error that tells you that you are making API calls too quickly."
                    case 503:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    case 500:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    default:
                        message = "Something went wrong. Please try again."
                    }
                    
                    
                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                //self.searchDataResponse = []
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                //AppUtility.sharedInstance.setHasLocalData(hasLocalData: false)
                //self.reloadTable()
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                print(httpResponse)
                
                if httpResponse?.statusCode == 500 {
                    
                    let alert = UIAlertController(title: "Alert", message: "Server error. Please try again.", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    
                    
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }else {
                    
                    
                    do {
                        let resultDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                        
                        if let json = resultDict as? [String: Any] {
                            if json["status"] as! Bool == false {
                                
                                var message = ""
                                
                                if let code = json["status_code"] as? Int {
                                    
                                    switch code {
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 402:
                                        message = "Your credits are exhausted. You need to buy credits."
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 403:
                                        message = "Your provided API key seems to be invalid."
                                    case 404:
                                        message = "The API endpoint you tried doesn't exist."
                                    case 405:
                                        message = "The request method you are trying isn't supported on this endpoint."
                                    case 422:
                                        message = "A validation error occured for some of your query params or form data."
                                    case 429:
                                        message = "A rare error that tells you that you are making API calls too quickly."
                                    case 503:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    case 500:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    default:
                                        message = "Something went wrong. Please try again."
                                    }
                                    
                                    
                                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                                    
                                    // add the actions (buttons)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                                        action in
                                        
                                        //self.callPostmanType(data: q)
                                        
                                    }))
                                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                                    
                                    // show the alert
                                    DispatchQueue.main.async {
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    
                                }
                                
                                
                                
                                
                            }else {
                                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                                    
                                    self.searchDataResponse = internetResponse.data!
                                    DispatchQueue.main.async {
                                        self.searchTableView.reloadData()
                                    }
                                    
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                        
                    } catch {
                        NSLog("ERROR \(error.localizedDescription)")
                        
                    }
                }
                
                
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    func filter(searchText: String) {
        
        let tilteMatched = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased())})
        
         let descMatched = self.unfilteredDocs.filter({ $0.desc!.lowercased().contains(searchText.lowercased())})
        
        let folderMatched = self.unfilteredDocs.filter({$0.folderName!.lowercased().contains(searchText.lowercased())})
        
        self.docs.removeAll()
        self.docs.append(contentsOf: tilteMatched )
        self.docs.append(contentsOf: descMatched )
        self.docs.append(contentsOf: folderMatched )
        
        self.docs = self.docs.unique
    }
    
    
}
