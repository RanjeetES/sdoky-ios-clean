//
//  ViewDocVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/22/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

import UIKit
import ObjectMapper
import CoreData
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer

class ViewSMapPersonVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var tfHobby : UITextView!
    @IBOutlet weak var tfOpinion : UITextView!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnDownload : UIButton!
    
    var sMap: SMap?
    var hobby = ""
    var opinion = ""
    var hobbyAttributed: NSMutableAttributedString?
    var opinionAttributed: NSMutableAttributedString?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
        
        do{
            let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
            if let sMapLocal = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
            }
        }catch {
            print("Error")
        }
        
        //        self.tfTitle.text = self.sMap?.title ?? ""
        //        if (self.sMap?.title ?? "").count > 16 {
        //            self.tfTitle.text = "\((self.sMap?.title ?? "").trunc(length: 16))..."
        //        }else {
        //            self.tfTitle.text = self.sMap?.title ?? ""
        //        }
        
        
        self.lblTitle.text = self.sMap?.title ?? ""
        
        //                if (self.sMap?.title ?? "").count > 16 {
        //                    self.lblTitle.text = "\((self.sMap?.title ?? "").trunc(length: 16))..."
        //                }else {
        //                    self.lblTitle.text = self.sMap?.title ?? ""
        //                }
        self.hobby = (self.sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
        self.opinion = (self.sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]).replacingOccurrences(of: "&nbsp;", with: "")
        //self.trimString()
        hobbyAttributed = NSMutableAttributedString(string: self.hobby)
        opinionAttributed = NSMutableAttributedString(string: self.opinion)
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        
        hobbyAttributed!.addAttributes(attrs1, range: NSRange(location: 0, length: self.hobby.count))
        opinionAttributed!.addAttributes(attrs1, range: NSRange(location: 0, length: self.opinion.count))
        
        
        if let occurences = sMap?.occurrences {
            
            let os = occurences.sorted { (o1, o2) -> Bool in
                o1.title?.count ?? 0 < o2.title?.count ?? 0
            }
            
            for occurence in os {
                //                if occurence.description != "" && occurence.description != nil {
                //                    self.cleanStr(occurence: occurence)
                //                }else {
                //
                //                }
                self.cleanStr(occurence: occurence)
                
            }
        }
        //self.hobby = self.hobby.replacingOccurrences(of: "#", with: "")
        //self.opinion = self.opinion.replacingOccurrences(of: ",", with: "")
        
        
        let htmlStringHobby = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        a {text-decoration: none;}
        .color {
        color: #0e80e4;
        }
        .a-c {text-decoration: underline;}
        </style>
        </head>
        <body>
        
        <p>\(self.hobby)</p>
        </body>
        </html>
        """
        
        let htmlStringOpinion = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        a {text-decoration: none;}
        .color {
        color: #0e80e4;
        }
        .a-c {text-decoration: underline;}
        </style>
        </head>
        <body>
        
        <p>\(self.opinion)</p>
        </body>
        </html>
        """
        
        //        if let htmlText = htmlStringHobby.htmlAttributedString {
        //            tfHobby.attributedText = htmlText
        //        }
        //
        //
        //        if let htmlText = htmlStringOpinion.htmlAttributedString {
        //
        //            tfOpinion.attributedText = htmlText
        //        }
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextView(_:)))
//        tfHobby.addGestureRecognizer(tapGesture)
//        let tapGestureC = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextViewC(_:)))
//        tfOpinion.addGestureRecognizer(tapGestureC)
        
        tfHobby.attributedText = hobbyAttributed
        tfOpinion.attributedText = opinionAttributed
        
        tfHobby.delegate = self
        tfOpinion.delegate = self
        
        if sMap?.isOmy == false {
            btnDownload.isHidden = true
        }else{
            btnDownload.isHidden = false
        }
        
    }
    
    func trimString() {
        self.hobby = self.hobby.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        self.opinion = self.opinion.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditClick(_ sender: Any) {
        let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapPersonVC") as! EditSMapPersonVC
        vc.sMap = self.sMap
        vc.firstEdit = false
        self.show(vc, sender: self)
    }
    
    @IBAction func btnDownloadAction(_ sender: Any) {
        //        APIHandler.sharedInstance.downloadSMapPdf(sMapId: self.sMap?.smapId ?? -1, uniqueName: self.sMap?.title ?? "Unnamed", isFolder: false) { (message, status) in
        //            self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
        //        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadSMapPdf(sMapId: sMap?.smapId ?? -1, uniqueName: sMap?.title ?? "Unnamed", isFolder: false) { (message, status) in
                self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
            }
        }else{
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
    }
    
    func cleanStr(occurence : Occurrence) {
        let titleWithout  = occurence.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        let title  = occurence.title ?? ""
        //
        //            let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
        //
        //            var attrs = [
        //            NSAttributedString.Key.font :  UIFont(name: "Montserrat-Light", size: 13),
        //            NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR),
        //            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), NSAttributedString.Key.link: URL(string: "link/\(occurence.occurenceId ?? 0)")!,] as [NSAttributedString.Key : Any]
        
//        var attrs = [
//            NSAttributedString.Key.font :  UIFont(name: "Montserrat-Light", size: 13),
//            NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR),
//            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        var attrs = [
        NSAttributedString.Key.font :  UIFont(name: "Montserrat-Light", size: 13),
        NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: "838383"),
        NSAttributedString.Key.underlineStyle : 0] as [NSAttributedString.Key : Any]
        
        var attributes : [NSAttributedString.Key : Any]?
        
        
        if occurence.docRefId != nil {
            if occurence.docRefId != 0 {
                //if occurence?.description != nil {
                //    if occurence?.description != "" {
                
                attributes = attrs1 as [NSAttributedString.Key : Any]
            }else {
                
                attributes = attrs
            }
        }else {
            attributes = attrs
            
            
        }
        
        if false {
            
            //let ranges = self.body.ranges(of: title, options: [], locale: nil)
            var replacementB = self.hobby
            var replacementC = self.opinion
            let bCount = replacementB.indicesOf(string: title).count
            let cCount = replacementC.indicesOf(string: title).count
            
            let bCountWithout = replacementB.countInstances(of: titleWithout).0
            let cCountWithout = replacementC.countInstances(of: titleWithout).0
            
            if bCount > 0 {
                for _ in 1...bCount {
                    if let r = replacementB.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        hobbyAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementB), with: s)
                        if let range = replacementB.range(of: title, options: .literal) {
                            replacementB = replacementB.replacingCharacters(in: range, with: "")
                        }
                        
                    }
                }
            }
            
            //            if bCountWithout > 0 {
            //                for _ in 1...bCountWithout {
            //                    if let r = replacementB.range(of: titleWithout) {
            //                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
            //                        bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementB), with: s)
            //                        if let range = replacementB.range(of: titleWithout, options: .diacriticInsensitive) {
            //                            replacementB = replacementB.replacingCharacters(in: range, with: "")
            //                        }
            //
            //                    }
            //                }
            //            }
            //
            if cCount > 0 {
                for _ in 1...cCount {
                    if let r = replacementC.range(of: title) {
                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
                        opinionAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementC), with: s)
                        if let range = replacementC.range(of: title, options: .literal) {
                            replacementC = replacementC.replacingCharacters(in: range, with: "")
                        }
                    }
                }
            }
            
            //            if cCountWithout > 0 {
            //                for _ in 1...cCountWithout {
            //                    if let r = replacementC.range(of: titleWithout) {
            //                        let s = NSAttributedString(string: titleWithout, attributes: attributes)
            //                        conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: replacementC), with: s)
            //                        if let range = replacementC.range(of: titleWithout, options: .diacriticInsensitive) {
            //                            replacementC = replacementC.replacingCharacters(in: range, with: "")
            //                        }
            //                    }
            //                }
            //            }
            
            
            
            //                if let r = self.body.range(of: title) {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attrs1)
            //                    bodyAttributed!.replaceCharacters(in: NSRange(range: r, in: self.body), with: s)
            //                }
            
        }else {
            //let ranges = self.conclusion.ranges(of: title, options: [], locale: nil)
            
            //                if let r = self.conclusion.range(of: title) {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attrs1)
            //                    conclusionAttributed!.replaceCharacters(in: NSRange(range: r, in: self.conclusion), with: s)
            //                }
            
            
            let rangesAgain = self.resolveHighlightedRanges(pattern: "\\b" + titleWithout + "\\b" ,text: self.hobby)
            for r in rangesAgain {
                 let rep = hobbyAttributed?.mutableString.substring(with: r)
                let s = NSAttributedString(string: rep ?? "", attributes: attributes)
                hobbyAttributed?.replaceCharacters(in: r, with: s)
            }
            
            let rangesAgainC = self.resolveHighlightedRanges(pattern: "\\b" + titleWithout + "\\b" ,text: self.opinion)
            for r in rangesAgainC {
                 let rep = opinionAttributed?.mutableString.substring(with: r)
                let s = NSAttributedString(string: rep ?? "", attributes: attributes)
                opinionAttributed?.replaceCharacters(in: r, with: s)
            }
            
            let ranges = self.resolveHighlightedRanges(pattern: title ,text: self.hobby)
            for r in ranges {
                
                let s = NSAttributedString(string: titleWithout, attributes: attributes)
                hobbyAttributed?.replaceCharacters(in: r, with: s)
                
                if let range = self.hobby.range(of: title, options: .diacriticInsensitive) {
                    self.hobby = self.hobby.replacingOccurrences(of: "[#,]", with: "", options: [.regularExpression], range: range)
                }
            }
            
            let rangesC = self.resolveHighlightedRanges(pattern: title ,text: self.opinion)
            for r in rangesC {
                let s = NSAttributedString(string: titleWithout, attributes: attributes)
                opinionAttributed?.replaceCharacters(in: r, with: s)
                
                if let range = self.opinion.range(of: title, options: .diacriticInsensitive) {
                    self.opinion = self.opinion.replacingOccurrences(of: "[#,]", with: "", options: [.regularExpression], range: range)
                }
            }
            
            
            
            //                let ranges = self.resolveHighlightedRanges(pattern: titleWithout ,text: self.hobby)
            //                for r in ranges {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attributes)
            //                    hobbyAttributed?.replaceCharacters(in: r, with: s)
            //                }
            //
            //                let rangesC = self.resolveHighlightedRanges(pattern: titleWithout ,text: self.opinion)
            //                for r in rangesC {
            //                    let s = NSAttributedString(string: titleWithout, attributes: attributes)
            //                    opinionAttributed?.replaceCharacters(in: r, with: s)
            //                }
            
        }
        
        
        
        
        
        
        //        self.body = self.body.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
        //        self.conclusion = self.conclusion.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
        
        
    }
    
    private func resolveHighlightedRanges(pattern: String, text: String) -> [NSRange] {
        guard text != nil, let regex = try? NSRegularExpression(pattern: pattern, options: [.caseInsensitive]) else { return [] }
        
        let matches = regex.matches(in: text, options: [], range: NSRange(text.startIndex..<text.endIndex, in: text))
        let ranges = matches.map { $0.range }
        return ranges
    }
    
    //    func cleanStr(occurence : Occurrence) {
    //
    //         let titleWithout  = occurence.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
    //        self.hobby = self.hobby.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
    //        self.opinion = self.opinion.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</a>", options: [.literal])
    //    }
    
    func cleanStrWithNoDesc(occurence: Occurrence) {
        let titleWithout  = occurence.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
        self.hobby = self.hobby.replacingOccurrences(of: occurence.title!, with: "<span class='a-c' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</span>", options: [.literal])
        self.opinion = self.opinion.replacingOccurrences(of: occurence.title!, with: "<span class='a-c' href='\(occurence.occurenceId ?? 0)'>\( titleWithout )</span>", options: [.literal])
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        let id = Int(URL.pathComponents.last ?? "0")
//        let body = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first?.description ?? ""
//        let title = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
//
//        //self.showAlertWithMessageWithTitle(title: title, message: body)
//        let viewController = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewOccurenceVC") as! ViewOccurenceVC
//        // Initialize the bottom sheet with the view controller just created
//        viewController.occurenceTitleString = title
//        viewController.occurenceDescString = body
//        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
//        bottomSheet.trackingScrollView?.scrollsToTop = true
//        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.6)
//        // Present the bottom sheet
//        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
//        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//        present(bottomSheet, animated: true, completion: nil)
//
//        return false // return true if you also want UIAlertController to pop up
        
        
        let id = Int(URL.pathComponents.last ?? "0")
         
         let o = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first
        //let body = self.sMap?.occurrences?.filter { $0.occurenceId == id}.first?.description ?? ""
         let title = o?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
         let docRefId = o?.docRefId
         
         do {
                                let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                                if let doc = docs.filter({$0.docId == Int16(docRefId ?? 0)}).first {
                                     
                                     //self.showAlertWithMessageWithTitle(title: title, message: body)
                                 let body = doc.desc
                                     let viewController = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewOccurenceVC") as! ViewOccurenceVC
                                            // Initialize the bottom sheet with the view controller just created
                                     viewController.occurenceTitleString = title
                                     viewController.occurenceDescString = body
                                            let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
                                            bottomSheet.trackingScrollView?.scrollsToTop = true
                                     bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.6)
                                            // Present the bottom sheet
                                            bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
                                            bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                                            present(bottomSheet, animated: true, completion: nil)
                                 
                                }
                                
                                
                            }catch{
                                print("Error Fetching")
                            }
         
         
         
         
         
         return false // return true if you also want UIAlertController to pop up
    }
    
    
    @objc func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        let point = tapGesture.location(in: tfHobby)
        if let detectedWord = getWordAtPosition(point)
        {
            
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
                //self.occurenceClick(id: o.occurenceId ?? 0, view: tfBody)
                if o.docRefId != nil && o.docRefId != 0 {
                    //if o.description != nil && o.description != "" {
                    //self.occurenceClick(title: o.title ?? "", body: o.description ?? "")
                    do {
                        let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        if let doc = docs.filter({$0.docId == Int16(o.docRefId ?? 0)}).first {
                            self.occurenceClick(title: o.title ?? "", body: doc.desc ?? "")
                        }
                        
                        
                    }catch{
                        print("Error Fetching")
                    }
                }
                
            }
        }
    }
    
    @objc func tapOnTextViewC(_ tapGesture: UITapGestureRecognizer){
        let point = tapGesture.location(in: tfOpinion)
        if let detectedWord = getWordAtPositionC(point)
        {
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
                //self.occurenceClick(id: o.occurenceId ?? 0, view: tfConclusion)
                if o.docRefId != nil && o.docRefId != 0 {
                    //if o.description != nil && o.description != "" {
                    
                    do {
                        let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        if let doc = docs.filter({$0.docId == Int16(o.docRefId ?? 0)}).first {
                            self.occurenceClick(title: o.title ?? "", body: doc.desc ?? "")
                        }
                        
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    
                    
                }
            }
        }
    }
    
    private final func getWordAtPosition(_ point: CGPoint) -> String?{
        if let textPosition = tfHobby.closestPosition(to: point)
        {
            if let range = tfHobby.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            {
                if (tfHobby.rangeFromTextRange(textRange: range).location <= tfHobby.text.indicesOf(string: tfHobby.text(in: range)!).first!) {
                    return tfHobby.text(in: range)
                }else {
                    return "Not found any occurences - RK - RS Bro"
                }
                
            }
        }
        return nil}
    
    private final func getWordAtPositionC(_ point: CGPoint) -> String?{
        if let textPosition = tfOpinion.closestPosition(to: point)
        {
            if let range = tfOpinion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            {
                if (tfOpinion.rangeFromTextRange(textRange: range).location <= tfOpinion.text.indicesOf(string: tfOpinion.text(in: range)!).first!) {
                    return tfOpinion.text(in: range)
                }else {
                    return "Not found any occurences - RK - RS Bro"
                }
            }
        }
        return nil}
    
    
    func occurenceClick(title: String, body: String) {
        let t = title.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
        //self.showAlertWithMessageWithTitle(title: t, message: body)
        
        let viewController = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewOccurenceVC") as! ViewOccurenceVC
        // Initialize the bottom sheet with the view controller just created
        viewController.occurenceTitleString = title
        viewController.occurenceDescString = body
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.6)
        // Present the bottom sheet
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
        
        
    }
}
