//
//  AddNewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class EditSMapPersonVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate, NSLayoutManagerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfHobby : UITextView!
    @IBOutlet weak var tfOpinion : UITextView!
    
    var sMap : SMap?
    var localOccurences: [OccurrenceLocal] = []
    var localSMap: [SMapLocal] = []
    var firstEdit = false
    
    var hobby = ""
    var opinion = ""
    var id : Int = 0
    var deletedOccurences: [Int] = []
//    let hashtagRegex = "#[-_a-zA-Z\\s0-9]{1,5},"
  //  let hashtagRegex = "#[-_a-zA-Z\\s0-9]+,"
    var hobbyAttributed: NSAttributedString?
    var opinionAttributed: NSAttributedString?
    
    var selectModelList: [SelectModel] = []
    
    var selectedOccurence = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    
    func setText() {
        
        let htmlStringHobby = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        .color {
        color: #0e80e4;
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        .color-no {
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        a {text-decoration: none; color:red !important;}
        a {text-decoration: none;}
        </style>
        </head>
        <body>
        
        <p>\(self.hobby)</p>
        </body>
        </html>
        """
        
        let htmlStringOpinion = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        .color {
        color: #0e80e4;
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        .color-no {
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        a {text-decoration: none; color:red !important;}
        </style>
        </head>
        <body>
        
        <p>\(self.opinion)</p>
        </body>
        </html>
        """
        
        //         if let htmlText = htmlStringHobby.htmlAttributedString {
        //
        //                   tfHobby.attributedText = htmlText
        //            self.hobbyAttributed = htmlText
        //               }
        //
        //
        //               if let htmlText = htmlStringOpinion.htmlAttributedString {
        //
        //                   tfOpinion.attributedText = htmlText
        //                self.opinionAttributed = htmlText
        //               }
        
    }
    
    func anotherClean() {
        
//        let f = sMap?.firstEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
//        let s = sMap?.secondEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
//        
        let f = self.hobby
        let s = self.opinion
        
        var a = NSMutableAttributedString(string: f ?? "")
        var b = NSMutableAttributedString(string: s ?? "")
        
        let hideAttribute = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: "F0F0F0")]
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        
        a.addAttributes(attrs1, range: NSRange(location: 0, length: f.count ?? 0))
        b.addAttributes(attrs1, range: NSRange(location: 0, length: s.count ?? 0))
        
        for o in sMap!.occurrences! {
            if o.docRefId != nil {
                if o.docRefId != 0 {
            //if occurence?.description != nil {
            //    if occurence?.description != "" {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
                    if o.isFirst == true {
                        if let r = f.lowercased().range(of: o.title!.lowercased()) {
                            a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                            let t = NSRange(range: r, in: f.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            a.addAttributes(hideAttribute, range: hashRange)
                            a.addAttributes(hideAttribute, range:  commaRange)
                        }
                    }else {
                        if let r = s.lowercased().range(of: o.title!.lowercased()) {
                            b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                            let t = NSRange(range: r, in: s.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            b.addAttributes(hideAttribute, range: hashRange)
                            b.addAttributes(hideAttribute, range:  commaRange)
                        }
                    }
                    
                    
                    
                }else {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
                    
                    if o.isFirst == true {
                        if let r = f.lowercased().range(of: o.title!.lowercased()) {
                            a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                            let t = NSRange(range: r, in: f.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            a.addAttributes(hideAttribute, range: hashRange)
                            a.addAttributes(hideAttribute, range:  commaRange)
                        }
                    }else {
                        
                        if let r = s.lowercased().range(of: o.title!.lowercased()) {
                            b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                            let t = NSRange(range: r, in: s.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            b.addAttributes(hideAttribute, range: hashRange)
                            b.addAttributes(hideAttribute, range:  commaRange)
                        }
                    }
                    
                    
                }
            }else {
                let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
                
                if o.isFirst == true {
                    if let r = f.lowercased().range(of: o.title!.lowercased()) {
                        a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                        let t = NSRange(range: r, in: f.lowercased() ?? "")
                        let hashRange = NSRange(location: t.location, length: 1)
                        let commaRange = NSRange(location: t.upperBound-1, length: 1)
                        a.addAttributes(hideAttribute, range: hashRange)
                        a.addAttributes(hideAttribute, range:  commaRange)
                    }
                    
                }else {
                    if let r = s.lowercased().range(of: o.title!.lowercased()) {
                        b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                        let t = NSRange(range: r, in: s.lowercased() ?? "")
                        let hashRange = NSRange(location: t.location, length: 1)
                        let commaRange = NSRange(location: t.upperBound-1, length: 1)
                        b.addAttributes(hideAttribute, range: hashRange)
                        b.addAttributes(hideAttribute, range:  commaRange)
                        
                    }
                    
                }
                
            }
        }
        
//        do {
//            let regex = try NSRegularExpression(pattern: "#", options: .caseInsensitive)
//            let range = NSRange(location: 0, length: (f ?? "").utf16.count)
//            for match in regex.matches(in: (f ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: range) {
//                a.addAttributes(hideAttribute, range: match.range)
//            }
//
//            let regex2 = try NSRegularExpression(pattern: ",", options: .caseInsensitive)
//            let range2 = NSRange(location: 0, length: (f ?? "").utf16.count)
//            for match in regex2.matches(in: (f ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: range2) {
//                a.addAttributes(hideAttribute, range: match.range)
//            }
//
//            let regexC = try NSRegularExpression(pattern: "#", options: .caseInsensitive)
//            let rangeC = NSRange(location: 0, length: (s ?? "").utf16.count)
//            for match in regexC.matches(in: (s ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: rangeC) {
//                b.addAttributes(hideAttribute, range: match.range)
//            }
//
//            let regexC2 = try NSRegularExpression(pattern: ",", options: .caseInsensitive)
//            let rangeC2 = NSRange(location: 0, length: (s ?? "").utf16.count)
//            for match in regexC2.matches(in: (s ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: rangeC2) {
//                b.addAttributes(hideAttribute, range: match.range)
//            }
//        } catch {
           // NSLog("Error creating regular expresion: \(error)")
        //}
        
        tfHobby.attributedText = a
        tfOpinion.attributedText = b
        self.hobbyAttributed = a
        self.opinionAttributed = b
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.checkExpiry()
        do{
            let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
            if let sMapLocal = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                //self.sMap = self.localToSMap(sMapLocal: sMapLocal)
                self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                self.hobby = (self.sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
                self.opinion = (self.sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
                tfHobby.layoutManager.delegate = self
                tfOpinion.layoutManager.delegate = self
                self.setText()
                self.anotherClean()
                tfTitle.text = sMap?.title ?? ""
                tfHobby.layoutSubviews()
                tfOpinion.layoutSubviews()
            }
        }catch {
            print("Error fetching")
            self.showAlertWithMessageWithTitle(title: "Error", message: "Couldn't fetch smap. Please try again")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        textView.typingAttributes = attrs1
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        textView.typingAttributes = attrs1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextView(_:)))
        tfHobby.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
        let tapGestureC = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextViewC(_:)))
        tapGestureC.delegate = self
        tfOpinion.addGestureRecognizer(tapGestureC)
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        
        tfHobby.layer.cornerRadius = 5.0
        tfHobby.layer.borderWidth = 1.0
        tfHobby.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        tfOpinion.layer.cornerRadius = 5.0
        tfOpinion.layer.borderWidth = 1.0
        tfOpinion.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        
        self.endEditing()
        
        tfTitle.delegate = self
        tfHobby.delegate = self
        tfOpinion.delegate = self
        
        
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 10
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tfHobby.contentOffset.y = -self.tfHobby.contentInset.top
            self.tfOpinion.contentOffset.y = -self.tfOpinion.contentInset.top
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        //self.view.window?.windowLevel = .statusBar
        
        do {
                       self.localSMap = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                       
                   }catch {
                       print("Fetch Error")
                   }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //         if tfType.text == "" {
        //            self.tfType.text = self.source ?? ""
        //        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        for o in sMap!.occurrences! {
//
//            if let r = (textView.text ?? "").range(of: o.title ?? "") {
//                let nR = NSRange(range: r, in: textView.text ?? "")
//
//                if nR.contains(range.location) {
//
//                }
//                    if range.lowerBound > nR.lowerBound && range.lowerBound < nR.upperBound {
//                        return false
//                    }
//
//                if range.lowerBound == nR.lowerBound {
//                    if range.length == 0 {
//                        return true
//                    }else {
//                        return false
//                    }
//
//                }
//
//            }
//        }
        
        
        for o in sMap!.occurrences! {
            
            if o.isFirst == true {
                if textView == self.tfHobby {
                    if let r = (textView.text ?? "").range(of: o.title ?? "") {
                        let nR = NSRange(range: r, in: textView.text ?? "")
                        
                        if nR.contains(range.location) {
                            
                        }
                            if range.lowerBound > nR.lowerBound && range.lowerBound < nR.upperBound {
                                return false
                            }
                        
                        if range.lowerBound == nR.lowerBound {
                            if range.length == 0 {
                                return true
                            }else {
                                return false
                            }
                            
                        }
                        
                    }
                }
            }else {
                if textView == self.tfOpinion {
                    if let r = (textView.text ?? "").range(of: o.title ?? "") {
                        let nR = NSRange(range: r, in: textView.text ?? "")
                        
                        if nR.contains(range.location) {
                            
                        }
                            if range.lowerBound > nR.lowerBound && range.lowerBound < nR.upperBound {
                                return false
                            }
                        
                        if range.lowerBound == nR.lowerBound {
                            if range.length == 0 {
                                return true
                            }else {
                                return false
                            }
                            
                        }
                        
                    }
                }
            }
            
            
        }
        
        
        
        return true
    }
    
    
    @IBAction func editingChanged(_ textField: UITextField){
        //        if tfType.text == "" {
        //            self.tfType.text = self.source ?? ""
        //        }
    }
    
    
   
    
    //func occurenceClick(_ sender: UITextView) {
    func occurenceClick(id: Int, view: UITextView) {
        
        self.selectModelList.removeAll()
        let occurence = self.sMap?.occurrences?.filter{$0.occurenceId == id}.first
        let occurenceTitle = occurence?.title ?? ""
        
        if occurence?.docRefId != nil {
            if occurence?.docRefId != 0 {
        //if occurence?.description != nil {
        //    if occurence?.description != "" {
                self.selectModelList.append(SelectModel(id: 1, title: "Edit data"))
            }else {
                self.selectModelList.append(SelectModel(id: 1, title: "Write data"))
            }
        }else {
            self.selectModelList.append(SelectModel(id: 1, title: "Write data"))
        }
        
        self.selectModelList.append(SelectModel(id: 2, title: "Search data"))
        self.selectModelList.append(SelectModel(id: 2, title: "Edit title"))
        self.selectModelList.append(SelectModel(id: 2, title: "Delete occurrence"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedOccurence.id = model.id
            self.selectedOccurence.title = model.title
            
            switch model.title {
            case "Edit data":
                 
                do {
                let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "EditDataVC") as! EditDataVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfHobby.text
                    sMapToEdit.secondEntry = self.tfOpinion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        //self.sMap = self.localToSMap(sMapLocal: sMapLocal)
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                vc.sMap = self.sMap
               vc.occurenceId = id
               vc.isFromData = false
               vc.isFromOccurence = true
               vc.docId = occurence?.docRefId
               self.show(vc, sender: self)
               }catch {
                   print("Error")
               }
            case "Write data":
                do {
                let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfHobby.text
                    sMapToEdit.secondEntry = self.tfOpinion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        //self.sMap = self.localToSMap(sMapLocal: sMapLocal)
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                vc.sMap = self.sMap
                vc.occurenceId = id
                vc.isFromData = false
                vc.isFromOccurence = true
                    vc.occurenceTitle = occurenceTitle.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
                self.show(vc, sender: self)
                }catch {
                    print("Error")
                }
            case "Edit title":
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceTitleVC") as! EditOccurenceTitleVC
                vc.sMap = self.sMap
                vc.occurenceId = id
                vc.isFromData = false
                self.show(vc, sender: self)
            case "Search data":
                
                do {
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "OccurenceSearchVC") as! OccurenceSearchVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfHobby.text
                    sMapToEdit.secondEntry = self.tfOpinion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        //self.sMap = self.localToSMap(sMapLocal: sMapLocal)
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                 vc.sMap = self.sMap
                    vc.occurenceId = id
                    vc.isFromData = false
                    vc.searchText = occurence?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
                    vc.docRefId = occurence?.docRefId ?? 0
                self.show(vc, sender: self)
                }catch {
                    print("Error")
                }
//                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "OccurenceSearchVC") as! OccurenceSearchVC
//                vc.sMap = self.sMap
//                vc.occurenceId = id
//                vc.isFromData = false
//                vc.searchText = occurence?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
//                self.show(vc, sender: self)
            case "Delete occurrence":
                do {
                    var trimmedTitle = ""
                    var untrimmedTitle = ""
                     var isHobby = true
                    let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        sMapToEdit.updatedAt = Date().stringDateSDoky()
                        sMapToEdit.offlineAction = OFFLINE_EDIT
                        var occurences = NSMutableSet()
                        for occur in sMapToEdit.occurences! {
                            
                            occurences.add(occur)
                            let o = occur as! OccurrenceLocal
                            if Int(o.occurenceId) == occurence!.occurenceId {
                                if o.isFirst == true {
                                    isHobby = true
                                }else {
                                    isHobby = false
                                }
                                o.offlineAction = OFFLINE_DELETE
                                untrimmedTitle = o.title ?? ""
                                trimmedTitle = o.title!.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
                                occurences.remove(o)
                            }
                            
                        }
                        sMapToEdit.occurences = occurences
                        //sMapToEdit.firstEntry = self.tfHobby.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                        //sMapToEdit.secondEntry = self.tfOpinion.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                        
                        if isHobby {
                            sMapToEdit.firstEntry = self.tfHobby.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                            sMapToEdit.secondEntry = self.tfOpinion.text
                            
                            
                        }else {
                            sMapToEdit.secondEntry = self.tfOpinion.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                            sMapToEdit.firstEntry = self.tfHobby.text
                        }
                        sMapToEdit.title = self.tfTitle.text
                        
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        sMapToEdit.firstEntry = sMapToEdit.firstEntry?.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
//                        sMapToEdit.secondEntry = sMapToEdit.secondEntry?.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                        
                        
//                        if isHobby {
//                                                    self.hobby = self.tfHobby.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
//                                                   self.opinion = self.tfOpinion.text
//                                               }else {
//                                                    self.opinion = self.tfOpinion.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
//                                                   self.hobby = self.tfHobby.text
//                                               }
//                        
                         self.deletedOccurences.append(occurence?.occurenceId ?? 0)
                        
//                        let deletedOccurence = DeletedOccurence(context: AppUtility.sharedInstance.getContext())
//                        deletedOccurence.occurenceId = Int16(occurence?.occurenceId ?? 0)
//                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                        
//                        AppUtility.sharedInstance.getAndSyncSMaps()
//                        
                        let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                        if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                            //self.sMap = self.localToSMap(sMapLocal: sMapLocal)
                            self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                        }
//                        
                        self.hobby = (self.sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
                        
                        self.opinion = (self.sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
//                        
//                        
//                        if let occurences = self.sMap?.occurrences {
//                            for occurence in occurences {
//                                self.cleanStr(occurence: occurence)
//                            }
//                        }
                        self.setText()
                        self.anotherClean()
                        self.tfTitle.text = self.sMap?.title ?? ""
                        self.tfHobby.layoutSubviews()
                        self.tfOpinion.layoutSubviews()
                        
                    }
                    
                }catch {
                    print("Fetch Error")
                    self.showAlertWithMessageWithTitle(title: "Error", message: "Couldn't fetch smap. Please try again")
                }
                
            default:
                print("No Action")
            }
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        showPopup(controller, sourceView: view as UIView)
        
    }
    
    func localToSMap(sMapLocal: SMapLocal) -> SMap {
        
        let data = sMapLocal
        var occurences : [Occurrence] = []
        var oldOccurences : [Occurrence] = []
        
        for occurence in data.occurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "" , tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            occurences.append(occurenceModel)
            
        }
        
        for occurence in data.oldOccurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "" , tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            oldOccurences.append(occurenceModel)
            
        }
        
        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: true)
        
        return localSMapToShow
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneError(message: "", texrField: tfTitle)
        let hobbyTextToSave = self.tfHobby.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        let opinionTextToSave = self.tfOpinion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else if self.validateOccurence() {
            print("error")
        }else {
            
            var occurences = NSMutableSet()
            let m = hobbyTextToSave.matches(for: hashtagRegex)
            let mC = opinionTextToSave.matches(for: hashtagRegex)
            
            var mCopy = hobbyTextToSave.matches(for: hashtagRegex)
                                  mCopy.append(contentsOf: mC)
                                  
                                  if mCopy.unique.count != m.unique.count + mC.unique.count {
                                      //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                                    self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                                      return
                                  }
            
            if m.count > m.unique.count {
                //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                return
            }
            
            if mC.count > mC.unique.count {
                //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                return
            }
            
            for h in m {
                let c = self.sMap?.occurrences?.filter{$0.title ?? "" == h}.count
                
                if c == 0 {
                    print(h)
                    
                    do {
                        let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                        var occurencesCopy = occurences
                        occurencesCopy.sort { (first, second) -> Bool in
                            first.occurenceId < second.occurenceId
                        }
                        if occurencesCopy.count > 0 {
                            self.id = Int(occurencesCopy.last!.occurenceId) + 1
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
                    occurenceToSave.occurenceId = Int16(self.id)
                    occurenceToSave.smapId = Int16(self.sMap?.smapId ?? 0)
                    occurenceToSave.title = h
                    occurenceToSave.isFirst = true
                    occurenceToSave.desc = nil
                    occurenceToSave.updatedAt = Date().stringDateSDoky()
                    occurenceToSave.offlineAction = OFFLINE_ADD
                    occurenceToSave.createdAt = Date().stringDateSDoky()
                    occurenceToSave.newTitle = h
                    occurenceToSave.newDesc = nil
                    occurenceToSave.tempTitle = nil
                    occurenceToSave.docRefId = Int16(0)
                    occurenceToSave.newDocRefId = Int16(0)
                    occurenceToSave.docRefIdEdited = Int16(0)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    occurences.add(occurenceToSave)
                    
                }
            }
            
            for h in mC {
                let c = self.sMap?.occurrences?.filter{$0.title ?? "" == h}.count
                
                if c == 0 {
                    print(c)
                    do {
                        let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                        var occurencesCopy = occurences
                        occurencesCopy.sort { (first, second) -> Bool in
                            first.occurenceId < second.occurenceId
                        }
                        if occurencesCopy.count > 0 {
                            self.id = Int(occurencesCopy.last!.occurenceId) + 1
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
                    occurenceToSave.occurenceId = Int16(self.id)
                    occurenceToSave.smapId = Int16(self.sMap?.smapId ?? 0)
                    occurenceToSave.title = h
                    occurenceToSave.isFirst = false
                    occurenceToSave.desc = nil
                    occurenceToSave.updatedAt = Date().stringDateSDoky()
                    occurenceToSave.offlineAction = OFFLINE_ADD
                    occurenceToSave.createdAt = Date().stringDateSDoky()
                    occurenceToSave.newTitle = h
                    occurenceToSave.newDesc = nil
                    occurenceToSave.tempTitle = nil
                    occurenceToSave.docRefId = Int16(0)
                    occurenceToSave.newDocRefId = Int16(0)
                    occurenceToSave.docRefIdEdited = Int16(0)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    occurences.add(occurenceToSave)
                }
            }
            
            do {
                self.localSMap = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
            }catch {
                print("Fetch Error")
            }
            
            do {
                self.localOccurences = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                
            }catch{
                print("Error Fetching")
            }
            
            if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                
                for occurence in sMapToSave.occurences! {
                    (occurence as! OccurrenceLocal).newTitle = (occurence as! OccurrenceLocal).title
                    (occurence as! OccurrenceLocal).newDesc = (occurence as! OccurrenceLocal).desc
                    (occurence as! OccurrenceLocal).newDocRefId = (occurence as! OccurrenceLocal).docRefId
                        occurences.add(occurence)
                }
                sMapToSave.occurences = occurences
                sMapToSave.oldOccurences = occurences
                sMapToSave.title = self.tfTitle.text ?? self.sMap?.title ?? ""
                sMapToSave.oldTitle = self.tfTitle.text ?? self.sMap?.title ?? ""
                sMapToSave.firstEntry = hobbyTextToSave 
                sMapToSave.secondEntry = opinionTextToSave 
                sMapToSave.oldFirstEntry = hobbyTextToSave 
                sMapToSave.oldSecondEntry = opinionTextToSave 
                if firstEdit == true {
                    sMapToSave.offlineAction = OFFLINE_ADD
                }else {
                    sMapToSave.offlineAction = OFFLINE_EDIT
                }
                sMapToSave.isOmy = true
            }
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            for d in self.deletedOccurences {
                let deletedOccurence = DeletedOccurence(context: AppUtility.sharedInstance.getContext())
                deletedOccurence.occurenceId = Int16(d)
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            }
            
            //AppUtility.sharedInstance.getAndSyncSMaps()
            //self.navigationController?.popViewController(animated: true)
            self.navigationController?.backToViewController(viewController: UITabBarController.self)
            
        }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

           return true
       }
    
    private func resolveHighlightedRanges(pattern: String, text: String) -> [NSRange] {
        guard text != nil, let regex = try? NSRegularExpression(pattern: pattern, options: []) else { return [] }
        
        let matches = regex.matches(in: text, options: [], range: NSRange(text.startIndex..<text.endIndex, in: text))
        let ranges = matches.map { $0.range }
        return ranges
    }
    
    func validateOccurence() -> Bool {
        
        var errorMessage = ""
        var duplicateOccurences : [String] = []
        for o in self.sMap!.occurrences! {
            let count = self.tfHobby.text.lowercased().indicesOf(string: o.title?.lowercased() ?? "").count
            let count1 = self.tfOpinion.text.lowercased().indicesOf(string: o.title?.lowercased() ?? "").count
            if (count + count1) > 1 {
                duplicateOccurences.append((o.title ?? "").replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: ""))
            }
        }
        
        if duplicateOccurences.count > 0 {
            if duplicateOccurences.count > 1 {
                errorMessage = errorMessage.appending("Below duplicate tags aren't allowed:\n").appending(duplicateOccurences.joined(separator: "\n"))
                self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: errorMessage)
            }else {
                errorMessage = errorMessage.appending("Below duplicate tag isn't allowed:\n").appending(duplicateOccurences.joined(separator: "\n"))
                self.showAlertWithMessageWithTitle(title: "Duplicate tag", message: errorMessage)
            }
            errorMessage = ""
            return true
        }else {
            errorMessage = ""
            return false
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        if tfTitle.text != self.sMap?.title || tfHobby.attributedText != self.hobbyAttributed || tfOpinion.attributedText != self.opinionAttributed  {
                   showSaveAlert()
               }else {
            self.deletedOccurences = []
            if let sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                           if sMapToSave.occurences!.count > 0 {
                                let x = sMapToSave.occurences?.first as? OccurrenceLocal
                                print(x?.occurenceId)
                            }
                            
                           if sMapToSave.oldOccurences!.count > 0 {
                                let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                                print(x?.occurenceId)
                            }
                            
                            for o in sMapToSave.oldOccurences! {
                                (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                                (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                                (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                                (o as! OccurrenceLocal).offlineAction = ""
                            }
                            sMapToSave.occurences = sMapToSave.oldOccurences
                            sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                            sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                sMapToSave.title = sMapToSave.oldTitle
                            //sMapToSave.offlineAction = ""
                       }
                       (UIApplication.shared.delegate as! AppDelegate).saveContext()
                       AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                   if firstEdit == true {
                    
//                    if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                                       sMapToSave.offlineAction = OFFLINE_EDIT
//                                   }
//                                   (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                                   AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                        self.navigationController?.backToViewController(viewController: UITabBarController.self)
                   }else {
                       self.navigationController?.popViewController(animated: true)
                   }
               }
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if tfTitle.text != self.sMap?.title || tfHobby.attributedText != self.hobbyAttributed || tfOpinion.attributedText != self.opinionAttributed  {
            showSaveAlert()
        }else {
            self.deletedOccurences = []
            if let sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                            if sMapToSave.occurences!.count > 0 {
                                 let x = sMapToSave.occurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                            if sMapToSave.oldOccurences!.count > 0 {
                                 let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                             for o in sMapToSave.oldOccurences! {
                                 (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                                 (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                                (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                                (o as! OccurrenceLocal).offlineAction = ""
                             }
                             sMapToSave.occurences = sMapToSave.oldOccurences
                             sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                             sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                sMapToSave.title = sMapToSave.oldTitle
                          // sMapToSave.offlineAction = ""
                       }
                       (UIApplication.shared.delegate as! AppDelegate).saveContext()
                       AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            if firstEdit == true {
                
//                if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                                   sMapToSave.offlineAction = OFFLINE_EDIT
//                               }
//                               (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                               AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                 self.navigationController?.backToViewController(viewController: UITabBarController.self)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    func showSaveAlert() {
        
        let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your write process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            self.deletedOccurences = []
            if let sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                           if sMapToSave.occurences!.count > 0 {
                                let x = sMapToSave.occurences?.first as? OccurrenceLocal
                                print(x?.occurenceId)
                            }
                            
                           if sMapToSave.oldOccurences!.count > 0 {
                                let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                                print(x?.occurenceId)
                            }
                            
                            for o in sMapToSave.oldOccurences! {
                                (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                                (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                                (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                                (o as! OccurrenceLocal).offlineAction = ""
                            }
                            sMapToSave.occurences = sMapToSave.oldOccurences
                            sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                            sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                            sMapToSave.title = sMapToSave.oldTitle
                           //sMapToSave.offlineAction = ""
                       }
                       (UIApplication.shared.delegate as! AppDelegate).saveContext()
                       AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            if self.firstEdit == true {
//                if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                                   sMapToSave.offlineAction = OFFLINE_EDIT
//                               }
//                               (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                               AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                 self.navigationController?.backToViewController(viewController: UITabBarController.self)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func cleanStr(occurence : Occurrence) {
        
        //let titleWithout  = occurence.title ?? ""
        // self.hobby = self.hobby.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\(titleWithout )</a>", options: [.literal])
        //self.opinion = self.opinion.replacingOccurrences(of: occurence.title!, with: "<a class='color' href='\(occurence.occurenceId ?? 0)'>\(titleWithout )</a>", options: [.literal])
        
        
        let titleWithout  = occurence.title?.replacingOccurrences(of: "", with: "").replacingOccurrences(of: "", with: "") ?? ""
        var styleClass = ""
        if occurence.docRefId != nil {
            if occurence.docRefId != 0 {
        //if occurence?.description != nil {
        //    if occurence?.description != "" {
                styleClass = "color"
            }else {
                styleClass = "color-no"
            }
        }else {
            styleClass = "color-no"
        }
        
        if occurence.isFirst == true {
            self.hobby = self.hobby.replacingOccurrences(of: occurence.title!, with: "<span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'> \( titleWithout ) </span>", options: [.literal], range: self.hobby.range(of: occurence.title!))
        }else {
            self.opinion = self.opinion.replacingOccurrences(of: occurence.title!, with: "<span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'> \( titleWithout ) </span>", options: [.literal], range: self.opinion.range(of: occurence.title!))
        }
        
        
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let id = Int(URL.pathComponents.last ?? "0")
        textView.tag = id ?? 0
        //self.occurenceClick(textView)
        
        return false // return true if you also want UIAlertController to pop up
    }
    
    @objc func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        //tfHobby.becomeFirstResponder()
        let point = tapGesture.location(in: tfHobby)
        if let detectedWord = getWordAtPosition(point)
        {
            
//            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
//                self.occurenceClick(id: o.occurenceId ?? 0, view: tfHobby)
//            }
            
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == true}) as? [Occurrence] {
                
                if o.count > 1 {
                    let occ = o.filter({$0.title == "#\(detectedWord),"}).first
                    if occ != nil {
                        self.occurenceClick(id: occ?.occurenceId ?? 0, view: tfHobby)
                    }else {
                        self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfHobby)
                    }
                }else {
                    self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfHobby)
                }
                
            }
            
//            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == true}).first {
//                self.occurenceClick(id: o.occurenceId ?? 0, view: tfHobby)
//            }
        }
    }
    
    @objc func tapOnTextViewC(_ tapGesture: UITapGestureRecognizer){
        //tfOpinion.becomeFirstResponder()
        let point = tapGesture.location(in: tfOpinion)
        if let detectedWord = getWordAtPositionC(point)
        {
//            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord))}).first {
//                self.occurenceClick(id: o.occurenceId ?? 0, view: tfOpinion)
//            }
            
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == false}) as? [Occurrence] {
                
                if o.count > 1 {
                    let occ = o.filter({$0.title == "#\(detectedWord),"}).first
                    
                    if occ != nil {
                        self.occurenceClick(id: occ?.occurenceId ?? 0, view: tfOpinion)
                    }else {
                        self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfOpinion)
                    }
                }else {
                    self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfOpinion)
                }
                
            }
            
//            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == false}).first {
//                self.occurenceClick(id: o.occurenceId ?? 0, view: tfOpinion)
//            }
            
        }
    }
    
    
    private final func getWordAtPosition(_ point: CGPoint) -> String?{
//        if let textPosition = tfHobby.closestPosition(to: point)
//        {
//            if let range = tfHobby.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
//            {
//                if (tfHobby.rangeFromTextRange(textRange: range).location <= tfHobby.text.indicesOf(string: tfHobby.text(in: range)!).first!) {
//                    return tfHobby.text(in: range)
//                }else {
//                    return "Not found any occurences - RK - RS Bro"
//                }
//            }
//        }
        
        if let textPosition = tfHobby.closestPosition(to: point)
        {
            
            if let range = tfHobby.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            
            {
                
                for o in sMap!.occurrences! {
                  //  if o.isFirst == true {
                    if let r = tfHobby.text.lowercased().range(of: o.title!.lowercased()) {
                        let t = NSRange(range: r, in: tfHobby.text.lowercased() ?? "")
                            let l = tfHobby.rangeFromTextRange(textRange: range)
                            
                            if t.contains(l.lowerBound) {
                                return tfHobby.text(in: range)
                                if (tfHobby.rangeFromTextRange(textRange: range).location <= tfHobby.text.indicesOf(string: tfHobby.text(in: range)!).first!) {
                                   // return tfBody.text(in: range)
                                }else {
                                    return "Not found any occurences - RK - RS Bro"
                                }
                            }
                            
                        }
                        
                   // }
                }
                                
                
                
                
            }
        }
        return nil}
    
    private final func getWordAtPositionC(_ point: CGPoint) -> String?{
//        if let textPosition = tfOpinion.closestPosition(to: point)
//        {
//            if let range = tfOpinion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
//            {
//                if (tfOpinion.rangeFromTextRange(textRange: range).location <= tfOpinion.text.indicesOf(string: tfOpinion.text(in: range)!).first!) {
//                    return tfOpinion.text(in: range)
//                }else {
//                    return "Not found any occurences - RK - RS Bro"
//                }
//            }
//        }
        
        if let textPosition = tfOpinion.closestPosition(to: point)
        {
            
            if let range = tfOpinion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
            
            {
                
                for o in sMap!.occurrences! {
                  //  if o.isFirst == true {
                    if let r = tfOpinion.text.lowercased().range(of: o.title!.lowercased()) {
                        let t = NSRange(range: r, in: tfOpinion.text.lowercased() ?? "")
                            let l = tfOpinion.rangeFromTextRange(textRange: range)
                            
                            if t.contains(l.lowerBound) {
                                return tfOpinion.text(in: range)
                                if (tfOpinion.rangeFromTextRange(textRange: range).location <= tfOpinion.text.indicesOf(string: tfOpinion.text(in: range)!).first!) {
                                   // return tfBody.text(in: range)
                                }else {
                                    return "Not found any occurences - RK - RS Bro"
                                }
                            }
                            
                        }
                        
                   // }
                }
                                
                
                
                
            }
        }
        return nil}
    
}



