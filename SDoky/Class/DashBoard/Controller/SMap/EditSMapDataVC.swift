//
//  AddNewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer

class HashtagTextView: UITextView {
    
    //let hashtagRegex = "#[-_a-zA-Z\\s0-9]+,"
    
    //let hashtagRegex = "#[-_a-zA-Z\\s0-9]{1,5},"
    
    
    private var cachedFrames: [CGRect] = []
    
    private var backgrounds: [UIView] = []
    
    //    override func viewDidLayoutSubviews() {
    //        super
    //        dispatch_async(dispatch_get_main_queue(), {
    //
    //        })
    //    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        var superRect = super.caretRect(for: position)
        guard let font = self.font else { return superRect }
        
        // "descender" is expressed as a negative value,
        // so to add its height you must subtract its value
        superRect.size.height = font.pointSize - font.descender
        return superRect
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textUpdated()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func textUpdated() {
        
        let ranges = resolveHighlightedRanges()
        let frames = ranges.compactMap { frame(ofRange: $0) }.reduce([], +)
        
        if cachedFrames != frames {
            cachedFrames = frames
            
            backgrounds.forEach { $0.removeFromSuperview() }
            backgrounds = cachedFrames.map { frame in
                let background = UIView()
                background.backgroundColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "F0F0F0")
                background.frame = CGRect(x: frame.minX+1, y: frame.minY-2, width: frame.width+1, height: frame.size.height - 3)
                //background.frame = frame
                background.layer.cornerRadius = 10
                //background.layer.borderWidth = 1.0
                //background.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "C5E0FF").cgColor
                insertSubview(background, at: 0)
                return background
            }
            
        }
    }
    
    private func configureView() {
        NotificationCenter.default.addObserver(self, selector: #selector(textUpdated), name: UITextView.textDidChangeNotification, object: self)
    }
    
    private func resolveHighlightedRanges() -> [NSRange] {
        guard text != nil, let regex = try? NSRegularExpression(pattern: hashtagRegex, options: []) else { return [] }
        
        let matches = regex.matches(in: text, options: [], range: NSRange(text.startIndex..<text.endIndex, in: text))
        let ranges = matches.map { $0.range }
        return ranges
    }
}



class EditSMapDataVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, NSLayoutManagerDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfBody : UITextView!
    @IBOutlet weak var tfConclusion : UITextView!
    
    var sMap : SMap?
    var oldSMap : SMap?
    var localOccurences: [OccurrenceLocal] = []
    var localSMap: [SMapLocal] = []
    var body = ""
    var conclusion = ""
    var id : Int = 0
    var firstEdit = false
    var deletedOccurences: [Int] = []
    
    //    let hashtagRegex = "#[-_a-zA-Z\\s0-9]{1,5},"
    // let hashtagRegex = "#[-_a-zA-Z\\s0-9]+,"
    
    var bodyAttributed: NSAttributedString?
    var conclusionAttributed: NSAttributedString?
    
    
    
    var selectModelList: [SelectModel] = []
    
    var selectedOccurence = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    
    
    func setText(body: String, conclusion: String) {
        
        let htmlStringBody = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        .color {
        color: #0e80e4;
        border: 5px solid red;
        border-radius: 5px;
        background-color: #f5f7f9;
        }
        .color-no {
        border: 5px solid red;
        border-radius: 5px;
        background-color: #f5f7f9;
        }
        a {text-decoration: none; color:red !important;}
        </style>
        </head>
        <body>
        
        <p>\(self.body)</p>
        </body>
        </html>
        """
        
        let htmlStringConclusion = """
        <html>
        <head>
        <style>
        body {
        font-family: 'Montserrat-Light';
        text-decoration  : none;
        color: #0b1223;
        font-size: 13px;
        }
        .color {
        color: #0e80e4;
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        .color-no {
        border: 1px solid #C5E0FF;
        border-radius: 16px;
        padding: 2px 9px;
        background-color: #f5f7f9;
        }
        a {text-decoration: none; color:red !important;}
        </style>
        </head>
        <body>
        
        <p>\(self.conclusion)</p>
        </body>
        </html>
        """
        
        if let htmlText = htmlStringBody.htmlAttributedString {
            
            //tfBody.attributedText = htmlText
            //self.bodyAttributed = htmlText
            
            //
            //tfBody.text = sMap?.firstEntry ?? ""
            
            //let attributes = [
            //NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "000000")] as [NSAttributedString.Key : Any]
            
            //let attributedString = NSAttributedString(string: sMap?.firstEntry ?? "", attributes: attributes)
            
            
            
            //let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)]
            
            
            //let attributedString = NSMutableAttributedString(string:sMap?.firstEntry ?? "")
            
            
            
            //            if let occurences = sMap?.occurrences {
            //                for occurence in occurences {
            //                    self.cleanStr(occurence: occurence)
            //                }
            //            }
            
            
            //tfBody.attributedText = attributedString
            
        }
        
        
        //        if let htmlText = htmlStringConclusion.htmlAttributedString {
        //            //tfConclusion.attributedText = htmlText
        //            self.conclusionAttributed = htmlText
        //        }
    }
    
    
    func anotherClean() {
        
        //let f = sMap?.firstEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])//.trimmingCharacters(in: .newlines)
        //let s = sMap?.secondEntry?.replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])//.trimmingCharacters(in: .newlines)
        
        let f = self.body
        let s = self.conclusion
        
        
        var a = NSMutableAttributedString(string: f ?? "")
        var b = NSMutableAttributedString(string: s ?? "")
        
        let hideAttribute = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: "F0F0F0")]
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        
        a.addAttributes(attrs1, range: NSRange(location: 0, length: f.count ?? 0))
        b.addAttributes(attrs1, range: NSRange(location: 0, length: s.count ?? 0))
        
        for o in sMap!.occurrences! {
            if o.docRefId != nil {
                if o.docRefId != 0 {
            //if o.description != nil {
            //    if o.description != "" {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
                    if o.isFirst == true {
                        if let r = f.lowercased().range(of: o.title!.lowercased()) {
                            a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                            let t = NSRange(range: r, in: f.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            a.addAttributes(hideAttribute, range: hashRange)
                            a.addAttributes(hideAttribute, range:  commaRange)
                        }
                        
                    }else {
                        if let r = s.lowercased().range(of: o.title!.lowercased()) {
                            b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                            let t = NSRange(range: r, in: s.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            b.addAttributes(hideAttribute, range: hashRange)
                            b.addAttributes(hideAttribute, range:  commaRange)
                        }
                        
                    }
                    
                    
                    
                }else {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
                    
                    if o.isFirst == true {
                        if let r = f.lowercased().range(of: o.title!.lowercased()) {
                            a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                            let t = NSRange(range: r, in: f.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            a.addAttributes(hideAttribute, range: hashRange)
                            a.addAttributes(hideAttribute, range:  commaRange)
                        }
                        
                    }else {
                        
                        if let r = s.lowercased().range(of: o.title!.lowercased()) {
                            b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                            let t = NSRange(range: r, in: s.lowercased() ?? "")
                            let hashRange = NSRange(location: t.location, length: 1)
                            let commaRange = NSRange(location: t.upperBound-1, length: 1)
                            b.addAttributes(hideAttribute, range: hashRange)
                            b.addAttributes(hideAttribute, range:  commaRange)
                        }
                        
                    }
                    
                }
            }else {
                let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
                
                if o.isFirst == true {
                    if let r = f.lowercased().range(of: o.title!.lowercased()) {
                        a.addAttributes(attrs1, range: NSRange(range: r, in: f.lowercased() ?? ""))
                        let t = NSRange(range: r, in: f.lowercased() ?? "")
                        let hashRange = NSRange(location: t.location, length: 1)
                        let commaRange = NSRange(location: t.upperBound-1, length: 1)
                        a.addAttributes(hideAttribute, range: hashRange)
                        a.addAttributes(hideAttribute, range:  commaRange)
                    }
                    
                }else {
                    if let r = s.lowercased().range(of: o.title!.lowercased()) {
                        b.addAttributes(attrs1, range: NSRange(range: r, in: s.lowercased() ?? ""))
                        let t = NSRange(range: r, in: s.lowercased() ?? "")
                        let hashRange = NSRange(location: t.location, length: 1)
                        let commaRange = NSRange(location: t.upperBound-1, length: 1)
                        b.addAttributes(hideAttribute, range: hashRange)
                        b.addAttributes(hideAttribute, range:  commaRange)
                    }
                    
                    
                }
                
            }
        }
        
        //        do {
        //            let regex = try NSRegularExpression(pattern: "#", options: .caseInsensitive)
        //            let range = NSRange(location: 0, length: (f ?? "").utf16.count)
        //            for match in regex.matches(in: (f ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: range) {
        //                a.addAttributes(hideAttribute, range: match.range)
        //            }
        //
        //            let regex2 = try NSRegularExpression(pattern: ",", options: .caseInsensitive)
        //            let range2 = NSRange(location: 0, length: (f ?? "").utf16.count)
        //            for match in regex2.matches(in: (f ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: range2) {
        //                a.addAttributes(hideAttribute, range: match.range)
        //            }
        //
        //            let regexC = try NSRegularExpression(pattern: "#", options: .caseInsensitive)
        //            let rangeC = NSRange(location: 0, length: (s ?? "").utf16.count)
        //            for match in regexC.matches(in: (s ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: rangeC) {
        //                b.addAttributes(hideAttribute, range: match.range)
        //            }
        //
        //            let regexC2 = try NSRegularExpression(pattern: ",", options: .caseInsensitive)
        //            let rangeC2 = NSRange(location: 0, length: (s ?? "").utf16.count)
        //            for match in regexC2.matches(in: (s ?? "").folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: rangeC2) {
        //                b.addAttributes(hideAttribute, range: match.range)
        //            }
        //        } catch {
        //            NSLog("Error creating regular expresion: \(error)")
        //        }
        
        
        tfBody.attributedText = a
        tfConclusion.attributedText = b
        self.bodyAttributed = a
        self.conclusionAttributed = b
        
    }
    
    private func resolveHighlightedRanges(pattern: String, text: String) -> [NSRange] {
        guard text != nil, let regex = try? NSRegularExpression(pattern: pattern, options: []) else { return [] }
        
        let matches = regex.matches(in: text, options: [], range: NSRange(text.startIndex..<text.endIndex, in: text))
        let ranges = matches.map { $0.range }
        return ranges
    }
    

    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.checkExpiry()
        
        do{
            let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
            if let sMapLocal = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                self.body = (sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]) //.trimmingCharacters(in: .newlines) ?? ""
                self.conclusion = (sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression]) //?.trimmingCharacters(in: .newlines) ?? ""
                
                tfBody.layoutManager.delegate = self
                tfConclusion.layoutManager.delegate = self
                self.setText(body: "", conclusion: "")
                self.anotherClean()
                tfTitle.text = sMap?.title ?? ""
                tfBody.layoutSubviews()
                tfConclusion.layoutSubviews()
                let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
                //tfBody.typingAttributes = attrs1
                //tfConclusion.typingAttributes = attrs1
                
            }
        }catch {
            print("Error fetching")
            self.showAlertWithMessageWithTitle(title: "Error", message: "Couldn't fetch smap. Please try again")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        textView.typingAttributes = attrs1
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
        textView.typingAttributes = attrs1
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tfBody.contentOffset.y = -self.tfBody.contentInset.top
            self.tfConclusion.contentOffset.y = -self.tfConclusion.contentInset.top
        }
    }
    
    
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 10
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextView(_:)))
        tapGesture.delegate = self
        tfBody.addGestureRecognizer(tapGesture)
        let tapGestureC = UITapGestureRecognizer(target: self, action: #selector(self.tapOnTextViewC(_:)))
        tapGestureC.delegate = self
        tfConclusion.addGestureRecognizer(tapGestureC)
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        
        tfBody.layer.cornerRadius = 5.0
        tfBody.layer.borderWidth = 1.0
        tfBody.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        tfConclusion.layer.cornerRadius = 5.0
        tfConclusion.layer.borderWidth = 1.0
        tfConclusion.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        
        self.endEditing()
        
        tfTitle.delegate = self
        tfBody.delegate = self
        tfConclusion.delegate = self
        
        do{
                    let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                    self.oldSMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
            }
        }catch {
            print("Error")
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        //self.view.window?.windowLevel = .statusBar
        //        self.tfBody.setContentOffset(.zero, animated: false)
        //self.tfConclusion.scrollRangeToVisible(NSMakeRange(0,0))
        
        do {
                       self.localSMap = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                       
                   }catch {
                       print("Fetch Error")
                   }
        
        
    }
    
    
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //         if tfType.text == "" {
        //            self.tfType.text = self.source ?? ""
        //        }
    }
    
    
    //func occurenceClick(_ sender: UITextView) {
    func occurenceClick(id: Int, view: UITextView) {
        self.selectModelList.removeAll()
        let occurence = self.sMap?.occurrences?.filter{$0.occurenceId == id}.first
        let occurenceTitle = occurence?.title ?? ""
        
        if occurence?.docRefId != nil {
            if occurence?.docRefId != 0 {
        //if occurence?.description != nil {
        //    if occurence?.description != "" {
                self.selectModelList.append(SelectModel(id: 1, title: "Edit data"))
            }else {
                self.selectModelList.append(SelectModel(id: 1, title: "Write data"))
            }
            
        }else {
            self.selectModelList.append(SelectModel(id: 1, title: "Write data"))
        }
        
        self.selectModelList.append(SelectModel(id: 2, title: "Search data"))
        self.selectModelList.append(SelectModel(id: 2, title: "Edit title"))
        self.selectModelList.append(SelectModel(id: 2, title: "Delete occurrence"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedOccurence.id = model.id
            self.selectedOccurence.title = model.title
            
            switch model.title {
            case "Edit data":
//                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceVC") as! EditOccurenceVC
//                vc.sMap = self.sMap
//                vc.occurenceId = id
//                vc.isFromData = true
//                self.show(vc, sender: self)
                
                
                do {
                //let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceVC") as! EditOccurenceVC
                    
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "EditDataVC") as! EditDataVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfBody.text
                    sMapToEdit.secondEntry = self.tfConclusion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                vc.sMap = self.sMap
                vc.occurenceId = id
                vc.isFromData = true
                vc.isFromOccurence = true
                vc.docId = occurence?.docRefId
                self.show(vc, sender: self)
                }catch {
                    print("Error")
                }
            case "Edit title":
//                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceTitleVC") as! EditOccurenceTitleVC
//                vc.sMap = self.sMap
//                vc.occurenceId = id
//                vc.isFromData = true
//                self.show(vc, sender: self)
                
                
                //let viewController = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "ViewOccurenceVC") as! ViewOccurenceVC
                       // Initialize the bottom sheet with the view controller just created
                
                
                
                do {
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceTitleVC") as! EditOccurenceTitleVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfBody.text
                    sMapToEdit.secondEntry = self.tfConclusion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                    
                                      
                
                vc.sMap = self.sMap
                vc.occurenceId = id
                vc.isFromData = true
                self.show(vc, sender: self)
                    
//                    let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: vc)
//                                      bottomSheet.trackingScrollView?.scrollsToTop = true
//                    bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 250.0)
//                                      // Present the bottom sheet
//                                      bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
//                                      bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//                    self.present(bottomSheet, animated: true, completion: nil)
                }catch {
                    print("Error")
                }
            case "Write data":
//                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceVC") as! EditOccurenceVC
//                vc.sMap = self.sMap
//                vc.occurenceId = id
//                vc.isFromData = true
//                self.show(vc, sender: self)
            
                do {
                //let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditOccurenceVC") as! EditOccurenceVC
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfBody.text
                    sMapToEdit.secondEntry = self.tfConclusion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                vc.sMap = self.sMap
                vc.occurenceId = id
                vc.isFromData = true
                vc.isFromOccurence = true
                    vc.occurenceTitle = occurenceTitle.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
                self.show(vc, sender: self)
                }catch {
                    print("Error")
                }
            case "Search data":
                
                
                
                do {
                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "OccurenceSearchVC") as! OccurenceSearchVC
                
                let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
                if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                    sMapToEdit.updatedAt = Date().stringDateSDoky()
                    sMapToEdit.offlineAction = OFFLINE_EDIT
                    sMapToEdit.firstEntry = self.tfBody.text
                    sMapToEdit.secondEntry = self.tfConclusion.text
                    sMapToEdit.title = self.tfTitle.text
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                    }
                    
                }
                
                 vc.sMap = self.sMap
                    vc.occurenceId = id
                    vc.isFromData = true
                    vc.searchText = occurence?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
                    vc.docRefId = occurence?.docRefId ?? 0
                self.show(vc, sender: self)
                }catch {
                    print("Error")
                }
                
                
//                let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "OccurenceSearchVC") as! OccurenceSearchVC
//                vc.sMap = self.sMap
//                vc.occurenceId = id
//                vc.isFromData = true
//                vc.searchText = occurence?.title?.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "") ?? ""
//                self.show(vc, sender: self)
            case "Delete occurrence":
                do {
                    var trimmedTitle = ""
                    var untrimmedTitle = ""
                    var isBody = true
                    let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                    if let sMapToEdit = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                        sMapToEdit.updatedAt = Date().stringDateSDoky()
                        sMapToEdit.offlineAction = OFFLINE_EDIT
                        var occurences = NSMutableSet()
                        for occur in sMapToEdit.occurences! {
                            
                            occurences.add(occur)
                            let o = occur as! OccurrenceLocal
                            if Int(o.occurenceId) == occurence!.occurenceId {
                                if o.isFirst == true {
                                    isBody = true
                                }else {
                                    isBody = false
                                }
                                o.offlineAction = OFFLINE_DELETE
                                untrimmedTitle = o.title ?? ""
                                trimmedTitle = o.title!.replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: "")
                                occurences.remove(o)
                            }
                            
                        }
                        sMapToEdit.occurences = occurences
                        if isBody {
                            sMapToEdit.firstEntry = self.tfBody.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                            sMapToEdit.secondEntry = self.tfConclusion.text
                            
                            
                        }else {
                            sMapToEdit.secondEntry = self.tfConclusion.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
                            sMapToEdit.firstEntry = self.tfBody.text
                        }
                        sMapToEdit.title = self.tfTitle.text
                        
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        
                        
//                        if isBody {
//                             self.body = self.tfBody.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
//                            self.conclusion = self.tfConclusion.text
//                        }else {
//                             self.conclusion = self.tfConclusion.text.replacingOccurrences(of: untrimmedTitle, with: trimmedTitle)
//                            self.body = self.tfBody.text
//                        }
                        
                       
                        self.deletedOccurences.append(occurence?.occurenceId ?? 0)
                        
//                        let deletedOccurence = DeletedOccurence(context: AppUtility.sharedInstance.getContext())
//                        deletedOccurence.occurenceId = Int16(occurence?.occurenceId ?? 0)
//                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                        
                        //AppUtility.sharedInstance.getAndSyncSMaps()
                        
                        let localSMapsAfterSync : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                        if let sMapLocal = localSMapsAfterSync.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                            self.sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapLocal)
                        }
                        
                        self.body = (self.sMap?.firstEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
                        
                        self.conclusion = (self.sMap?.secondEntry ?? "").replacingOccurrences(of: "\n{4,30}", with: "\n\n\n", options: [.regularExpression])
                        
//                        if let occurences = self.sMap?.occurrences {
//                            for occurence in occurences {
//                               // self.cleanStr(occurence: occurence)
//                            }
//                        }
                        self.anotherClean()
                        self.tfTitle.text = self.sMap?.title ?? ""
                        self.tfBody.layoutSubviews()
                        self.tfConclusion.layoutSubviews()
                        
                    }
                    
                }catch {
                    print("Fetch Error")
                }
                
            default:
                print("No Action")
            }
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        showPopup(controller, sourceView: view as UIView)
        
    }
    
    
    
//    func localToSMap(sMapLocal: SMapLocal) -> SMap {
//        
//        let data = sMapLocal
//        var occurences : [Occurrence] = []
//        var oldOccurences : [Occurrence] = []
//        
//        for occurence in data.occurences! {
//            
//            let occurenceToShow = occurence as! OccurrenceLocal
//            
//            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
//            occurences.append(occurenceModel)
//            
//        }
//        
//        for occurence in data.oldOccurences! {
//            
//            let occurenceToShow = occurence as! OccurrenceLocal
//            
//            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
//            oldOccurences.append(occurenceModel)
//            
//        }
//        
//        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: data.isOmy)
//        
//        return localSMapToShow
//        
//    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneError(message: "", texrField: tfTitle)
        
        let bodyTextToSave = self.tfBody.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        let conclusionTextToSave = self.tfConclusion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else if self.validateOccurence() {
            print("error")
        }else {
            var occurences = NSMutableSet()
            let m = bodyTextToSave.matches(for: hashtagRegex)
            let mC = conclusionTextToSave.matches(for: hashtagRegex)
            
            var mCopy = bodyTextToSave.matches(for: hashtagRegex)
            mCopy.append(contentsOf: mC)
            
            if mCopy.unique.count != m.unique.count + mC.unique.count {
                self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                
                return
            }
            
            if m.count > m.unique.count {
                self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                return
            }
            
            if mC.count > mC.unique.count {
                self.showAlertWithMessageWithTitle(title: "Error", message: "Duplicate tags found in smap.")
                //self.showAlertWithMessageWithTitle(title: "Error", message: "Failed to implement the changes to the SMap.")
                return
            }
            
            for h in m {
                let c = self.sMap?.occurrences?.filter{$0.title ?? "" == h}.count
                
                if c == 0 {
                    print(h)
                    
                    do {
                        let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                        var occurencesCopy = occurences
                        occurencesCopy.sort { (first, second) -> Bool in
                            first.occurenceId < second.occurenceId
                        }
                        if occurencesCopy.count > 0 {
                            self.id = Int(occurencesCopy.last!.occurenceId) + 1
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
                    occurenceToSave.occurenceId = Int16(self.id)
                    occurenceToSave.smapId = Int16(self.sMap?.smapId ?? 0)
                    occurenceToSave.title = h
                    occurenceToSave.isFirst = true
                    occurenceToSave.desc = nil
                    occurenceToSave.updatedAt = Date().stringDateSDoky()
                    occurenceToSave.offlineAction = OFFLINE_ADD
                    occurenceToSave.createdAt = Date().stringDateSDoky()
                    occurenceToSave.newTitle = title
                    occurenceToSave.newDesc = nil
                    occurenceToSave.tempTitle = nil
                    occurenceToSave.docRefId = Int16(0)
                    occurenceToSave.newDocRefId = Int16(0)
                    occurenceToSave.docRefIdEdited = Int16(0)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    occurences.add(occurenceToSave)
                    
                }
            }
            
            for h in mC {
                let c = self.sMap?.occurrences?.filter{$0.title ?? "" == h}.count
                
                if c == 0 {
                    print(c)
                    do {
                        let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                        var occurencesCopy = occurences
                        occurencesCopy.sort { (first, second) -> Bool in
                            first.occurenceId < second.occurenceId
                        }
                        if occurencesCopy.count > 0 {
                            self.id = Int(occurencesCopy.last!.occurenceId) + 1
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
                    occurenceToSave.occurenceId = Int16(self.id)
                    occurenceToSave.smapId = Int16(self.sMap?.smapId ?? 0)
                    occurenceToSave.title = h
                    occurenceToSave.isFirst = false
                    occurenceToSave.desc = nil
                    occurenceToSave.updatedAt = Date().stringDateSDoky()
                    occurenceToSave.offlineAction = OFFLINE_ADD
                    occurenceToSave.createdAt = Date().stringDateSDoky()
                    occurenceToSave.newTitle = title
                    occurenceToSave.newDesc = nil
                    occurenceToSave.tempTitle = nil
                    occurenceToSave.docRefId = Int16(0)
                    occurenceToSave.newDocRefId = Int16(0)
                    occurenceToSave.docRefIdEdited = Int16(0)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    occurences.add(occurenceToSave)
                }
            }
            
            
            
            
            do {
                self.localSMap = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                
            }catch {
                print("Fetch Error")
            }
            
            do {
                self.localOccurences = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                
            }catch{
                print("Error Fetching")
            }
            
            if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                
                
                for occurence in sMapToSave.occurences! {
                    (occurence as! OccurrenceLocal).newTitle = (occurence as! OccurrenceLocal).title
                    (occurence as! OccurrenceLocal).newDesc = (occurence as! OccurrenceLocal).desc
                    (occurence as! OccurrenceLocal).newDocRefId = (occurence as! OccurrenceLocal).docRefId
                    occurences.add(occurence)
                }
                sMapToSave.occurences = occurences
                sMapToSave.oldOccurences = occurences
                sMapToSave.title = self.tfTitle.text ?? self.sMap?.title ?? ""
                sMapToSave.oldTitle = self.tfTitle.text ?? self.sMap?.title ?? ""
                sMapToSave.firstEntry = bodyTextToSave 
                sMapToSave.secondEntry = conclusionTextToSave 
                sMapToSave.oldFirstEntry = bodyTextToSave 
                sMapToSave.oldSecondEntry = conclusionTextToSave
                if firstEdit == true {
                    sMapToSave.offlineAction = OFFLINE_ADD
                }else {
                    sMapToSave.offlineAction = OFFLINE_EDIT
                }
                
                sMapToSave.isOmy = true
            }
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            //self.navigationController?.popViewController(animated: true)

            for d in self.deletedOccurences {
                let deletedOccurence = DeletedOccurence(context: AppUtility.sharedInstance.getContext())
                deletedOccurence.occurenceId = Int16(d)
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            }
            
             //AppUtility.sharedInstance.getAndSyncSMaps()
            self.navigationController?.backToViewController(viewController: UITabBarController.self)
            
        }
        
    }
    
    
    func validateOccurence() -> Bool {
        var errorMessage = ""
        var duplicateOccurences : [String] = []
        for o in self.sMap!.occurrences! {
            let count = self.tfBody.text.lowercased().indicesOf(string: o.title?.lowercased() ?? "").count
            let count1 = self.tfConclusion.text.lowercased().indicesOf(string: o.title?.lowercased() ?? "").count
            if (count + count1) > 1 {
                duplicateOccurences.append((o.title ?? "").replacingOccurrences(of: "#", with: "").replacingOccurrences(of: ",", with: ""))
            }
        }
        
        if duplicateOccurences.count > 0 {
            if duplicateOccurences.count > 1 {
                errorMessage = errorMessage.appending("Below duplicate tags aren't allowed:\n").appending(duplicateOccurences.joined(separator: "\n"))
                self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: errorMessage)
            }else {
                errorMessage = errorMessage.appending("Below duplicate tag isn't allowed:\n").appending(duplicateOccurences.joined(separator: "\n"))
                self.showAlertWithMessageWithTitle(title: "Duplicate tag", message: errorMessage)
            }
            errorMessage = ""
            return true
        }else {
            errorMessage = ""
            return false
        }
    }
    
    
    
    
    @IBAction func btnCancelAction(_ sender: Any) {
        do {
        let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
        
        if tfTitle.text != self.sMap?.title || self.bodyAttributed != self.tfBody.attributedText || self.conclusionAttributed != self.tfConclusion.attributedText {
            showSaveAlert()
        }else {
            
            self.deletedOccurences = []
            if let sMapToSave = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                if sMapToSave.occurences!.count > 0 {
                    let x = sMapToSave.occurences?.first as? OccurrenceLocal
                    print(x?.occurenceId)
                }
                
               if sMapToSave.oldOccurences!.count > 0 {
                    let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                    print(x?.occurenceId)
                }
                
                for o in sMapToSave.oldOccurences! {
                    (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                    (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                    (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                    (o as! OccurrenceLocal).offlineAction = ""
                    
                }
                sMapToSave.occurences = sMapToSave.oldOccurences
                sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                sMapToSave.title = sMapToSave.oldTitle
                
               // if sMapToSave.oldOccurences != nil || sMapToSave.oldOccurences?.count != 0 {
                //sMapToSave.occurences = sMapToSave.oldOccurences
                //sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                //sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                
                
//                var occurences = NSMutableSet()
//                for o in sMapToSave.oldOccurences! {
//                    (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
//                    (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
//                    occurences.add(o)
//                }
//                sMapToSave.occurences = occurences
                
//                for o in sMapToSave.occurences! {
//                    (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
//                    (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
//                }
                 //   sMapToSave.oldOccurences = NSMutableSet()
                //}
                
//                for occurence in sMapToSave.occurences! {
//                    if ((occurence as! OccurrenceLocal).title != (occurence as! OccurrenceLocal).newTitle) {
//                        (occurence as! OccurrenceLocal).title = (occurence as! OccurrenceLocal).newTitle
//                    }
//
//                    if ((occurence as! OccurrenceLocal).desc != (occurence as! OccurrenceLocal).newDesc) {
//                         (occurence as! OccurrenceLocal).desc = (occurence as! OccurrenceLocal).newDesc
//                    }
//
//
//                }
                //sMapToSave.offlineAction = ""
//                if firstEdit == true {
//                    sMapToSave.offlineAction = OFFLINE_ADD
//                }else {
//                    sMapToSave.offlineAction = ""
//                }
            }
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            if firstEdit == true {
                
//                if let sMapToSave = localSMaps.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                    sMapToSave.offlineAction = OFFLINE_EDIT
//                }
                //(UIApplication.shared.delegate as! AppDelegate).saveContext()
                //AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                
                self.navigationController?.backToViewController(viewController: UITabBarController.self)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        }
        catch {
            print("err")
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        for o in sMap!.occurrences! {
            
            if o.isFirst == true {
                if textView == self.tfBody {
                    if let r = (textView.text ?? "").range(of: o.title ?? "") {
                        let nR = NSRange(range: r, in: textView.text ?? "")
                        
                        if nR.contains(range.location) {
                            
                        }
                            if range.lowerBound > nR.lowerBound && range.lowerBound < nR.upperBound {
                                return false
                            }
                        
                        if range.lowerBound == nR.lowerBound {
                            if range.length == 0 {
                                return true
                            }else {
                                return false
                            }
                            
                        }
                        
                    }
                }
            }else {
                if textView == self.tfConclusion {
                    if let r = (textView.text ?? "").range(of: o.title ?? "") {
                        let nR = NSRange(range: r, in: textView.text ?? "")
                        
                        if nR.contains(range.location) {
                            
                        }
                            if range.lowerBound > nR.lowerBound && range.lowerBound < nR.upperBound {
                                return false
                            }
                        
                        if range.lowerBound == nR.lowerBound {
                            if range.length == 0 {
                                return true
                            }else {
                                return false
                            }
                            
                        }
                        
                    }
                }
            }
            
            
        }
        
        
        return true
    }
    
    
   
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        print(range)
//        if let r = (textField.text ?? "").range(of: "#fine,") {
//            let nR = NSRange(range: r, in: textField.text ?? "")
//
//            if range == nR {
//                return false
//            }
//        }
//
//
//        return true
//    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if tfTitle.text != self.sMap?.title || self.bodyAttributed != self.tfBody.attributedText || self.conclusionAttributed != self.tfConclusion.attributedText {
            showSaveAlert()
        }else {
            self.deletedOccurences = []
            if let sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                            if sMapToSave.occurences!.count > 0 {
                                 let x = sMapToSave.occurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                            if sMapToSave.oldOccurences!.count > 0 {
                                 let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                             for o in sMapToSave.oldOccurences! {
                                 (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                                 (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                                (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                                (o as! OccurrenceLocal).offlineAction = ""
                             }
                             sMapToSave.occurences = sMapToSave.oldOccurences
                             sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                             sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                             sMapToSave.title = sMapToSave.oldTitle
                           //sMapToSave.offlineAction = ""
                       }
                       (UIApplication.shared.delegate as! AppDelegate).saveContext()
                       AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            if firstEdit == true {
//                if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                    sMapToSave.offlineAction = OFFLINE_EDIT
//                    sMapToSave.isOmy = true
//                }
//                (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                self.navigationController?.backToViewController(viewController: UITabBarController.self)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
            
            //self.showHome()
        }
    }
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    func showSaveAlert() {
        
        let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your write process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            //               if self.folder != nil {
            //                  self.navigationController?.popViewController(animated: false)
            //              }else {
            //
            //                  self.tabBarController?.selectedIndex = 0
            //
            //              }
            
            self.deletedOccurences = []
            
            if let sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
                         if sMapToSave.occurences!.count > 0 {
                                 let x = sMapToSave.occurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                            if sMapToSave.oldOccurences!.count > 0 {
                                 let x = sMapToSave.oldOccurences?.first as? OccurrenceLocal
                                 print(x?.occurenceId)
                             }
                             
                             for o in sMapToSave.oldOccurences! {
                                 (o as! OccurrenceLocal).title = (o as! OccurrenceLocal).newTitle
                                 (o as! OccurrenceLocal).desc = (o as! OccurrenceLocal).newDesc
                                (o as! OccurrenceLocal).docRefId = (o as! OccurrenceLocal).newDocRefId
                                (o as! OccurrenceLocal).offlineAction = ""
                             }
                             sMapToSave.occurences = sMapToSave.oldOccurences
                             sMapToSave.firstEntry = sMapToSave.oldFirstEntry
                             sMapToSave.secondEntry = sMapToSave.oldSecondEntry
                             sMapToSave.title = sMapToSave.oldTitle
                           // sMapToSave.offlineAction = ""
                       }
                       (UIApplication.shared.delegate as! AppDelegate).saveContext()
                       AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            
            if self.firstEdit == true {
//                if var sMapToSave = self.localSMap.filter({Int($0.smapId) == self.sMap?.smapId}).first {
//                    sMapToSave.offlineAction = OFFLINE_EDIT
//                    sMapToSave.isOmy = true
//                }
                //(UIApplication.shared.delegate as! AppDelegate).saveContext()
                //AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                self.navigationController?.backToViewController(viewController: UITabBarController.self)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
            //self.showHome()
            //self.clear()
            //self.tabBarController?.selectedIndex = 0
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHome() {
        let tabBarController = UITabBarController()
        let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
        let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
        let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
        let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
        tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
        self.show(tabBarController, sender: self)
    }
    
    
    
    func cleanStr(occurence : Occurrence) {
        
        let titleWithout  = occurence.title?.replacingOccurrences(of: "", with: "").replacingOccurrences(of: "", with: "") ?? ""
        var styleClass = ""
        if occurence.docRefId != nil {
            if occurence.docRefId != 0 {
        //if occurence?.description != nil {
        //    if occurence?.description != "" {
                styleClass = "color"
            }else {
                styleClass = "color-no"
            }
        }else {
            styleClass = "color-no"
        }
        //self.body = self.body.replacingOccurrences(of: occurence.title!, with: "<span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'>\(titleWithout )</span>", options: [.literal])
        //self.conclusion = self.conclusion.replacingOccurrences(of: occurence.title!, with: "<span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'>\(titleWithout )</span>", options: [.literal])
        
        
        if occurence.isFirst == true {
            self.body = self.body.replacingOccurrences(of: occurence.title!, with: " <span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'> \(titleWithout) </span> ", options: [.literal], range: self.body.range(of: occurence.title!))
        }else {
            self.conclusion = self.conclusion.replacingOccurrences(of: occurence.title!, with: " <span class='\(styleClass)' href='\(occurence.occurenceId ?? 0)'> \( titleWithout ) </span> ", options: [.literal], range: self.conclusion.range(of: occurence.title!))
        }
        
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let id = Int(URL.pathComponents.last ?? "0")
        textView.tag = id ?? 0
        //self.occurenceClick(textView)
        return false // return true if you also want UIAlertController to pop up
    }
    
    
    @objc func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        //tfBody.becomeFirstResponder()
        let point = tapGesture.location(in: tfBody)
        
        if let detectedWord = getWordAtPosition(point)
        {
            if var o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == true}) as? [Occurrence] {
                
                if o.count > 1 {
                    
                    o.sort { (first, second) -> Bool in
                        first.title ?? "" < second.title ?? ""
                    }
                    
                    for occ in o {
                        if occ.title ?? "" == "#\(detectedWord)," {
                            self.occurenceClick(id: occ.occurenceId ?? 0, view: tfBody)
                            return
                        }
                    }
                    let occ = o.filter({($0.title ?? "").contains(detectedWord)}).first
                    //let occ = o.filter({$0.title == "#\(detectedWord),"}).first
                    if occ != nil {
                        self.occurenceClick(id: occ?.occurenceId ?? 0, view: tfBody)
                    }else {
                        //let occ = o.filter({($0.title ?? "").contains(detectedWord)}).first
                        self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfBody)
                    }
                    
                }else {
                    self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfBody)
                }
                
            }
        }
    }
    
    @objc func tapOnTextViewC(_ tapGesture: UITapGestureRecognizer){
        //tfConclusion.becomeFirstResponder()
        let point = tapGesture.location(in: tfConclusion)
        if let detectedWord = getWordAtPositionC(point)
        {
            if let o = self.sMap!.occurrences!.filter({($0.title!.contains(detectedWord)) && $0.isFirst == false}) as? [Occurrence] {
                
                if o.count > 1 {
                    let occ = o.filter({$0.title == "#\(detectedWord),"}).first
                    if occ != nil {
                        self.occurenceClick(id: occ?.occurenceId ?? 0, view: tfConclusion)
                    }else {
                        self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfConclusion)
                    }
                }else {
                    self.occurenceClick(id: o.first?.occurenceId ?? 0, view: tfConclusion)
                }
                
            }
        }
    }
    
    
    private final func getWordAtPosition(_ point: CGPoint) -> String?{
        
        
        if let textPosition = tfBody.closestPosition(to: point)
        {
            
            if let range = tfBody.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
                
            {
                
                for o in sMap!.occurrences! {
                    //  if o.isFirst == true {
                    if let r = tfBody.text.lowercased().range(of: o.title!.lowercased()) {
                        let t = NSRange(range: r, in: tfBody.text.lowercased() ?? "")
                        let l = tfBody.rangeFromTextRange(textRange: range)
                        
                        if t.contains(l.lowerBound) {
                            return tfBody.text(in: range)
                            if (tfBody.rangeFromTextRange(textRange: range).location <= tfBody.text.indicesOf(string: tfBody.text(in: range)!).first!) {
                                // return tfBody.text(in: range)
                            }else {
                                return "Not found any occurences - RK - RS Bro"
                            }
                        }
                        
                    }
                    
                    // }
                }
                
                
                
                
            }
        }
        
        
        
        //        if let textPosition = tfBody.closestPosition(to: point)
        //        {
        //            if let range = tfBody.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
        //            {
        //                if (tfBody.rangeFromTextRange(textRange: range).location <= tfBody.text.indicesOf(string: tfBody.text(in: range)!).first!) {
        //                    return tfBody.text(in: range)
        //                }else {
        //                    return "Not found any occurences - RK - RS Bro"
        //                }
        //            }
        //        }
        return nil}
    
    private final func getWordAtPositionC(_ point: CGPoint) -> String?{
        
        
        if let textPosition = tfConclusion.closestPosition(to: point)
        {
            
            if let range = tfConclusion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
                
            {
                
                for o in sMap!.occurrences! {
                    //  if o.isFirst == false {
                    if let r = tfConclusion.text.lowercased().range(of: o.title!.lowercased()) {
                        let t = NSRange(range: r, in: tfConclusion.text.lowercased() ?? "")
                        let l = tfConclusion.rangeFromTextRange(textRange: range)
                        
                        if t.contains(l.lowerBound) {
                            return tfConclusion.text(in: range)
                            if (tfConclusion.rangeFromTextRange(textRange: range).location <= tfConclusion.text.indicesOf(string: tfConclusion.text(in: range)!).first!) {
                                // return tfBody.text(in: range)
                            }else {
                                return "Not found any occurences - RK - RS Bro"
                            }
                        }
                        
                    }
                    
                    // }
                }
                
                
                
                
            }
        }
        
        //        if let textPosition = tfConclusion.closestPosition(to: point)
        //        {
        //            if let range = tfConclusion.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: UITextDirection(rawValue: 1))
        //            {
        //                if (tfConclusion.rangeFromTextRange(textRange: range).location <= tfConclusion.text.indicesOf(string: tfConclusion.text(in: range)!).first!) {
        //                    return tfConclusion.text(in: range)
        //                }else {
        //                    return "Not found any occurences - RK - RS Bro"
        //                }
        //            }
        //        }
        return nil}
    
    func findRange(searchTerm: String, searchSentence: String) {
        var searchRange = searchSentence.startIndex..<searchSentence.endIndex
        var ranges: [Range<String.Index>] = []
        while let range = searchSentence.range(of: searchTerm, range: searchRange) {
            ranges.append(range)
            searchRange = range.upperBound..<searchRange.upperBound
        }
        print(ranges)
        //print(ranges.map { "(\(searchSentence.distance(from: searchSentence.startIndex, to: $0.lowerBound)), \(searchSentence.distance(from: searchSentence.startIndex, to: $0.upperBound)))" })
    }
}


