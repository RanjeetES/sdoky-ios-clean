//
//  ViewOccurenceVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 11/24/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class ViewOccurenceVC: UIViewController {
    
    @IBOutlet weak var occurenceTitle: UILabel!
    @IBOutlet weak var occurenceDesc: UITextView!
    
    var occurenceTitleString: String?
    var occurenceDescString: String?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.occurenceDesc.contentOffset.y = -self.occurenceDesc.contentInset.top
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.occurenceTitle.text = self.occurenceTitleString?.replacingOccurrences(of: "#", with: ",").replacingOccurrences(of: ",", with: "") ?? ""
        self.occurenceDesc.text = self.occurenceDescString ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }

}
