//
//  WriteVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class SMapPersonVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfHobby : UITextView!
    @IBOutlet weak var tfOpnion : UITextView!
    
    var id : Int = 1
    var sMapId : Int = 1
    var localSMaps : [SMapLocal] = []
    //let hashtagRegex = "#[-_a-zA-Z\\s0-9]{1,5},"
    //let hashtagRegex = "#[-_a-zA-Z\\s0-9]+,"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        tfHobby.layer.cornerRadius = 5.0
        tfHobby.layer.borderWidth = 1.0
        tfHobby.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        tfHobby.delegate = self
        
        
        tfOpnion.layer.cornerRadius = 5.0
        tfOpnion.layer.borderWidth = 1.0
        tfOpnion.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        tfOpnion.delegate = self
        
        
        tfTitle.delegate = self
        
        self.endEditing()
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfHobby)
        self.removePhoneError(message: "", texrField: tfOpnion)
        
        let hobbyTextToSave = self.tfHobby.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression]).lowercased()
        let opinionTextToSave = self.tfOpnion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression]).lowercased()
        
        let hobbyTextToSaveFinal = self.tfHobby.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        let opinionTextToSaveFinal = self.tfOpnion.text.replacingOccurrences(of: "\\s+,", with: ",", options: [.regularExpression])
        
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else if tfHobby.text == "" && tfOpnion.text == "" {
            self.showError(message: "Any one field is mandatory other than title", texrField: self.tfHobby)
            self.showError(message: "", texrField: self.tfOpnion)
//        }else if tfOpnion.text == "" {
//            self.showError(message: "Required field", texrField: self.tfOpnion)
        }else{
            
            do {
                let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                var occurencesCopy = occurences
                occurencesCopy.sort { (first, second) -> Bool in
                    first.occurenceId < second.occurenceId
                }
                if occurencesCopy.count > 0 {
                    self.id = Int(occurencesCopy.last!.occurenceId) + 1
                }
                
            }catch{
                print("Error Fetching")
            }
            
            var occurences = NSMutableSet()
            //let m = self.tfHobby.text.matches(for: hashtagRegex)
            //let mC = self.tfOpnion.text.matches(for: hashtagRegex)
            let m = hobbyTextToSave.matches(for: hashtagRegex)
            let mC = opinionTextToSave.matches(for: hashtagRegex)
            
            var mCopy = hobbyTextToSave.matches(for: hashtagRegex)
            mCopy.append(contentsOf: mC)
            
            if mCopy.unique.count != m.unique.count + mC.unique.count {
                 self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            if m.count > m.unique.count {
                 self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            if mC.count > mC.unique.count {
                 self.showAlertWithMessageWithTitle(title: "Duplicate tags", message: "Duplicate occurrences found.")
                return
            }
            
            for h in m {
                occurences.add(self.createOccurences(title: h, type: true))
            }
            
            for h in mC {
                occurences.add(self.createOccurences(title: h, type: false))
            }
            
            do {
                self.localSMaps = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                var sMapsCopy = self.localSMaps
                sMapsCopy.sort { (first, second) -> Bool in
                    first.smapId < second.smapId
                }
                if sMapsCopy.count > 0 {
                    self.sMapId = Int(sMapsCopy.last!.smapId) + 1
                }
                
            }catch {
                print("Fetch Error")
            }
            
            let sMapToSave = SMapLocal(context: AppUtility.sharedInstance.getContext())
            sMapToSave.smapId = Int16(self.sMapId)
            sMapToSave.isData = false
            sMapToSave.title = self.tfTitle.text ?? ""
            sMapToSave.oldTitle = self.tfTitle.text ?? ""
            sMapToSave.firstEntry = hobbyTextToSaveFinal
            sMapToSave.secondEntry = opinionTextToSaveFinal
            sMapToSave.oldFirstEntry = hobbyTextToSaveFinal
            sMapToSave.oldSecondEntry = opinionTextToSaveFinal
            sMapToSave.status = true
            sMapToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
            sMapToSave.createdAt = Date().stringDateSDoky()
            sMapToSave.updatedAt = Date().stringDateSDoky()
            sMapToSave.offlineAction = OFFLINE_ADD
            sMapToSave.isOmy = false
//            for occurence in occurences {
//                occurences.add(occurence)
//            }
            sMapToSave.occurences = occurences
            sMapToSave.oldOccurences = occurences
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
            //AppUtility.sharedInstance.getAndSyncSMaps()
            self.clear()
            //let sMap = self.localToSMap(sMapLocal: sMapToSave)
            let sMap = AppUtility.sharedInstance.localToSMap(sMapLocal: sMapToSave)
            
           let vc = SMAP_STORY_BOARD.instantiateViewController(withIdentifier: "EditSMapPersonVC") as! EditSMapPersonVC
           vc.sMap = sMap
           vc.firstEdit = true
           self.show(vc, sender: self)
                       
            
        }
        
    }
    
    func localToSMap(sMapLocal: SMapLocal) -> SMap {
        
        let data = sMapLocal
        var occurences : [Occurrence] = []
        var oldOccurences : [Occurrence] = []
        
        for occurence in data.occurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            occurences.append(occurenceModel)
            
        }
        
        for occurence in data.oldOccurences! {
            
            let occurenceToShow = occurence as! OccurrenceLocal
            
            let occurenceModel = Occurrence(occurenceId: Int(occurenceToShow.occurenceId), smapId: Int(occurenceToShow.smapId), title: occurenceToShow.title ?? "", isFirst: occurenceToShow.isFirst, description: occurenceToShow.desc ?? "", createdAt: occurenceToShow.createdAt ?? "", updatedAt: occurenceToShow.updatedAt ?? "", deletedAt: occurenceToShow.deletedAt ?? "", offlineAction: occurenceToShow.offlineAction ?? "", tempTitle: occurenceToShow.tempTitle ?? "", docRefId: Int(occurenceToShow.docRefId), docRefIdEdited: Int(occurenceToShow.docRefIdEdited))
            oldOccurences.append(occurenceModel)
            
        }
        
        let localSMapToShow = SMap(smapId: Int(data.smapId), isData: data.isData, title: data.title ?? "", firstEntry: data.firstEntry ?? "", secondEntry: data.secondEntry ?? "", userId: Int(data.userId), status: data.status, createdAt: data.createdAt ?? "", updatedAt: data.updatedAt ?? "", deletedAt: data.deletedAt ?? "", occurrences: occurences, offlineAction: data.offlineAction ?? "", isOmy: false)
        
        return localSMapToShow
        
    }
    
    func createOccurences(title: String, type: Bool) -> OccurrenceLocal {
            
            do {
                let occurences:[OccurrenceLocal] = try AppUtility.sharedInstance.getContext().fetch(OccurrenceLocal.fetchRequest())
                var occurencesCopy = occurences
                occurencesCopy.sort { (first, second) -> Bool in
                    first.occurenceId < second.occurenceId
                }
                if occurencesCopy.count > 0 {
                    self.id = Int(occurencesCopy.last!.occurenceId) + 1
                }
                
            }catch{
                print("Error Fetching")
            }
            
            let occurenceToSave = OccurrenceLocal(context: AppUtility.sharedInstance.getContext())
            occurenceToSave.occurenceId = Int16(self.id)
            occurenceToSave.smapId = Int16(self.sMapId ?? 0)
            occurenceToSave.title = title
            occurenceToSave.isFirst = type
            occurenceToSave.desc = nil
            occurenceToSave.updatedAt = Date().stringDateSDoky()
            occurenceToSave.offlineAction = OFFLINE_ADD
            occurenceToSave.createdAt = Date().stringDateSDoky()
            occurenceToSave.newTitle = title
            occurenceToSave.newDesc = nil
            occurenceToSave.tempTitle = nil
            occurenceToSave.docRefId = Int16(0)
            occurenceToSave.newDocRefId = Int16(0)
            occurenceToSave.docRefIdEdited = Int16(0)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        return occurenceToSave
            
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        if tfTitle.text != "" || tfHobby.text != "" || tfOpnion.text != ""  {
            showSaveAlert()
        }else {
            self.clear()
            self.navigationController?.popViewController(animated: false)
            
        }
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if tfTitle.text != "" || tfHobby.text != "" || tfOpnion.text != ""  {
            showSaveAlert()
        }else {
            self.clear()
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func showSaveAlert() {
        
        let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your edit process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            self.navigationController?.popViewController(animated: false)
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "entity" {
                constraint.constant = 40.0
            }
        }
    }
    
    func showError(message: String, texrField: UITextView) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "hobby" {
                constraint.constant = 40.0
            }
            if constraint.identifier == "opinion" {
               constraint.constant = 40.0
           }
            
            
        }
    }

    
    func removePhoneError(message: String, texrField: UITextView) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "hobby" {
                constraint.constant = 16.0
            }
           if constraint.identifier == "opinion" {
              constraint.constant = 16.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            
            if constraint.identifier == "entity" {
                constraint.constant = 16.0
            }
        }
    }
    
    func showErrorLabel(message: String, texrField: UIButton) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneErrorLabel(message: String, texrField: UIButton) {
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
}

extension SMapPersonVC : HomeAPIDelegate {
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
    }
    
    func clear() {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: tfTitle)
        self.tfHobby.text = ""
        self.tfOpnion.text = ""
        self.tfTitle.text = ""
        
    }
}



