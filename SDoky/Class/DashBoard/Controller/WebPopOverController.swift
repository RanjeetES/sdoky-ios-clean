//
//  WebPopOverController.swift
//  SDoky
//
//  Created by Ranjeet Sah on 9/8/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import WebKit

class TSWebView : WKWebView {
    
    var wkContentView: UIView? {
        return self.subviewWithClassName("WKContentView")
    }
    
    
    override convenience init(frame: CGRect, configuration: WKWebViewConfiguration) {
        self.init(frame: frame, configuration: configuration)
        swizzleResponderChainAction()
    }
    
    
    private func swizzleResponderChainAction() {
        wkContentView?.swizzlePerformAction()
    }
    
}

class WebPopOverController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var wkWebView : TSWebView!
    @IBOutlet weak var btnEditAndSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    var url: String?
    var data : String = ""
    var searchTitle: String?
    var folders: [FolderDocResponseData]?
    var isFromSMap = false
    var sMap: SMap?
    var occurenceId: Int?
    var isFromData : Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlToLoad = URLRequest(url: URL(string: url!)!)
        let blockRule = "[{ \"trigger\": { \"url-filter\": \".*\", \"resource-type\": [\"image\"] }, \"action\": { \"type\": \"block\" } }]"
        wkWebView.isUserInteractionEnabled = true
        wkWebView.navigationDelegate = self
        wkWebView.uiDelegate = self
        btnEditAndSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        
        
        WKContentRuleListStore.default()?.compileContentRuleList(forIdentifier: "ContentBlockingRules", encodedContentRuleList: blockRule, completionHandler: { (rulesList, error) in
            if let error = error {
                print(error)
                return
            }
            guard let rulesList = rulesList else { return }
            let config = self.wkWebView.configuration
            config.userContentController.add(rulesList)
            self.wkWebView.load(urlToLoad)
        })
        
        let editAndSave = UIMenuItem(title: "EDIT AND SAVE", action: #selector(copyText(_:)))
        UIMenuController.shared.menuItems?.append(editAndSave)
        UIMenuController.shared.menuItems = [editAndSave]
        
        //        UIMenuController.shared.menuItems?.removeAll()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //    override func viewDidDisappear(_ animated: Bool) {
    //        CATransaction.setDisableActions(true)
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    
    @objc func copyText(_ sender : UIMenuItem) {
        self.wkWebView.evaluateJavaScript("window.getSelection().toString()", completionHandler: { (data, error) in
            print(data)
            self.data = data as! String
            
            if self.data == "" {
                self.showAlertWithMessageWithTitle(title: "Select text", message: "Please select the text you want to edit and save")
            }else{
                if self.isFromSMap ==  false {
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                    vc.folders = self.folders
                    vc.isFromInternetSearch = true
                    vc.isFromScan = false
                    vc.data = data as! String
                    vc.searchTitle = self.searchTitle
                    self.show(vc, sender: self)
                }else {
                    
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                    vc.sMap = self.sMap
                    vc.occurenceId = self.occurenceId
                    vc.isFromData = self.isFromData ?? true
                    vc.descToEdit = data as? String
                    vc.searchTitle = self.searchTitle
                    vc.isFromOccurence = true
                    self.show(vc, sender: self)
                    
                }
                
            }
        })
        
        
        
        
        
    }
    
    //    func webViewDidStartLoad(_ : WKWebView) {
    //        self.showSpinner(onView: self.wkWebView.scrollView)
    //    }
    //
    //    func webViewDidFinishLoad(_ : WKWebView) {
    //        self.removeSpinner()
    //    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.removeSpinner()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showSpinner(onView: self.wkWebView.scrollView)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.removeSpinner()
    }
    
    
    
    
    @IBAction func btnCancelAction( _sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEditAndSaveAction( _sender: Any) {
        //        self.wkWebView.evaluateJavaScript("window.getSelection().toString()", completionHandler: { (data, error) in
        //            print(data)
        //        })
        
        
        
        
        //            let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
        //            vc.folders = self.folders
        //            vc.isFromInternetSearch = true
        //            vc.data = data
        //            vc.searchTitle = self.searchTitle
        //            self.show(vc, sender: self)
        
        
        self.wkWebView.evaluateJavaScript("window.getSelection().toString()", completionHandler: { (data, error) in
            print(data)
            self.data = data as! String
            if self.data == "" {
                self.showAlertWithMessageWithTitle(title: "Select text", message: "Please select the text you want to edit and save")
            }else {
                if self.isFromSMap == false {
                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                    vc.folders = self.folders
                    vc.isFromInternetSearch = true
                    vc.isFromScan = false
                    vc.data = data as! String
                    vc.searchTitle = self.searchTitle
                    self.show(vc, sender: self)
                }else {
                   let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                    vc.sMap = self.sMap
                    vc.occurenceId = self.occurenceId
                    vc.isFromData = self.isFromData ?? true
                    vc.descToEdit = data as? String
                    vc.searchTitle = self.searchTitle
                    vc.isFromOccurence = true
                    self.show(vc, sender: self)
                }
                
            }
        })
        //}
    }
    
    //    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    //        switch action {
    //        case #selector(copyText(_:)):
    //            return super.canPerformAction(action, withSender: sender)
    //        default:
    //            return false
    //        }
    //    }
    
    //    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    //        return false
    //    }
    
    //    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    //
    //        if action == #selector(copyText) {
    //            return true
    //        }
    //
    //        return false
    //    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("segue performed")
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        print("unwind")
    }
    
}





