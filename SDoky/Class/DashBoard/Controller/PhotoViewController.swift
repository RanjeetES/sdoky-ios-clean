//
//  PhotoViewController.swift
//  SDoky
//
//  Created by Ranjeet Sah on 9/26/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import Alamofire
import Mantis

class CropAreaView: UIView {
    override func point(inside point: CGPoint, with event:   UIEvent?) -> Bool {
        return false
    }
}

class PhotoViewController: UIViewController, UIScrollViewDelegate, CropViewControllerDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnRotateImage: UIButton!
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var ivImage: UIImageView!
    //@IBOutlet var imageViewWidth: NSLayoutConstraint!
    //@IBOutlet var imageViewHeight: NSLayoutConstraint!
   // @IBOutlet weak var scrollView: UIScrollView! //{
    //         didSet{
    //              scrollView.delegate = self
    //              scrollView.minimumZoomScale = 1.0
    //              scrollView.maximumZoomScale = 10.0
    //         }
    //    }
    
//    @IBOutlet var cropAreaView: CropAreaView!
    
    var image: UIImage?
    var urlPath: URL?
    var folders: [FolderDocResponseData]?
    var cropViewController: CropViewController?
    
    var didGetCroppedImage: ((UIImage) -> Void)?
    
//    var cropArea:CGRect{
//        get{
//            let factor = ivImage.image!.size.width/view.frame.width
//            let scale = 1/scrollView.zoomScale
//            let imageFrame = ivImage.imageFrame()
//            let x = (scrollView.contentOffset.x + cropAreaView.frame.origin.x - imageFrame.origin.x) * scale * factor
//            let y = (scrollView.contentOffset.y + cropAreaView.frame.origin.y - imageFrame.origin.y) * scale * factor
//            let width = cropAreaView.frame.size.width * scale * factor
//            let height = cropAreaView.frame.size.height * scale * factor
//            return CGRect(x: x, y: y, width: width, height: height)
//        }
//    }
    
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return ivImage
//    }
//
//
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.cropAreaView.isHidden = true
        // Do any additional setup after loading the view.
        btnCrop.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        //btnCancel.layer.borderWidth = 1.0
        //btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        if let i = image {
            self.ivImage.image = i
            //setImageToCrop(image: i)
            
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.pop()
    }
    
    @IBAction func btnRotateImageAction(_ sender: Any) {
        self.image = self.image?.rotate(radians: .pi/2)
        self.ivImage.image = self.image
        self.cropViewController?.r()
        
    }
    
    @IBAction func btnCropImageAction(_ sender: Any) {
        
        //        let croppedCGImage = ivImage.image?.cgImage?.cropping(to: cropArea)
        //        let croppedImage = UIImage(cgImage: croppedCGImage!)
        //        ivImage.image = croppedImage
        //        image = croppedImage
        
        //        let scale:CGFloat = 1/scrollView.zoomScale
        //        let x:CGFloat = scrollView.contentOffset.x * scale
        //        let y:CGFloat = scrollView.contentOffset.y * scale
        //        let width:CGFloat = scrollView.frame.size.width * scale
        //        let height:CGFloat = scrollView.frame.size.height * scale
        //        let croppedCGImage = ivImage.image?.cgImage?.cropping(to: CGRect(x: x, y: y, width: width, height: height))
        //        let croppedImage = UIImage(cgImage: croppedCGImage!)
        //        setImageToCrop(image: croppedImage)
        cropViewController?.crop()
        
        
        
    }
    
//    func setImageToCrop(image:UIImage){
//        ivImage.image = image
//        imageViewWidth.constant = image.size.width
//        imageViewHeight.constant = image.size.height
//        let scaleHeight = scrollView.frame.size.width/image.size.width
//        let scaleWidth = scrollView.frame.size.height/image.size.height
//        scrollView.minimumZoomScale = max(scaleWidth, scaleHeight)
//        scrollView.zoomScale = max(scaleWidth, scaleHeight)
//    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.pop()
    }
    
    func pop() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func crop(image:UIImage, cropRect:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(cropRect.size, false, image.scale)
        let origin = CGPoint(x: cropRect.origin.x * CGFloat(-1), y: cropRect.origin.y * CGFloat(-1))
        image.draw(at: origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return result
    }
    
    func rotateImage(image:UIImage, angle:CGFloat, flipVertical:CGFloat, flipHorizontal:CGFloat) -> UIImage? {
        let ciImage = CIImage(image: image)
        
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        filter?.setDefaults()
        
        let newAngle = angle * CGFloat(-1)
        
        var transform = CATransform3DIdentity
        transform = CATransform3DRotate(transform, CGFloat(newAngle), 0, 0, 1)
        //transform = CATransform3DRotate(transform, CGFloat(Double(flipVertical) * .pi), 0, 1, 0)
        //transform = CATransform3DRotate(transform, CGFloat(Double(flipHorizontal) * .pi), 1, 0, 0)
        
        let affineTransform = CATransform3DGetAffineTransform(transform)
        
        filter?.setValue(NSValue(cgAffineTransform: affineTransform), forKey: "inputTransform")
        
        let contex = CIContext(options: [CIContextOption.useSoftwareRenderer:true])
        
        let outputImage = filter?.outputImage
        let cgImage = contex.createCGImage(outputImage!, from: (outputImage?.extent)!)
        
        let result = UIImage(cgImage: cgImage!)
        return result
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func uploadImage(data: Data, type: String, filename: String) {
        
        DispatchQueue.main.async {
            self.showSpinner(onView: self.view)
        }
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "multipart/form-data",
            "cache-control": "no-cache"
        ]
        
        let url = URL(string: "https://api.sdoky.com/api/v1/file-to-text")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            // multipartFormData.append(data, withName: filename, mimeType: type)
            
            multipartFormData.append(data, withName: "file_url", fileName: filename, mimeType: type)
        },to: url!, method:.post,
          headers:headers,
          
          encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //SwiftSpinner.show(progress: Progress.fractionCompleted*100, title: "تحميل")
                })
                upload.responseJSON { response in
                    
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                    
                    switch response.response?.statusCode {
                        
                    case 200 :
                        print(response.data)
                        
                        do {
                            let resultDict = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: Any]
                            
                            if let json = resultDict as? [String: Any] {
                                if json["status"] as! Bool == false {
                                    
                                    
                                }else {
                                    print(json["data"] as! String)
                                    
                                    
                                    let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                                    vc.folders = self.folders
                                    vc.isFromInternetSearch = false
                                    vc.isFromScan = true
                                    vc.data = (json["data"] as! String)
                                    vc.searchTitle = ""
                                    self.show(vc, sender: self)
                                }
                                
                                
                            }
                        }catch{
                            print("Error")
                        }
                        
                        
                        
                        
                        break
                        
                    case 400 :
                        print("400")
                        
                        break
                        
                    case 401 :
                        print("401")
                        
                        
                        break
                        
                        
                    case 302 :
                        
                        
                        break
                    case 500 :
                        
                        
                        break
                        
                    default: break
                        
                        
                    }
                    
                }
                return
            case .failure( _):  DispatchQueue.main.async {
                self.removeSpinner()
            }
                break
                
            }
            
        })
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CropViewController {
            
            vc.image = image
            vc.mode = .customizable
            vc.delegate = self
            
            var config = Mantis.Config()
            config.ratioOptions = []
            config.showRotationDial = false
            config.addCustomRatio(byVerticalWidth: 1, andVerticalHeight: 1)
            config.presetFixedRatioType = .canUseMultiplePresetFixedRatio
            
            vc.config = config
            
            
            
            cropViewController = vc
            //self.cropViewController?.setB()
        }
    }
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage) {
        self.dismiss(animated: true)
               self.didGetCroppedImage?(cropped)
        
        if let u = urlPath {
            self.uploadImage(data: cropped.pngData()!, type: "image/"+u.pathExtension, filename: u.path)
        }else {
            self.uploadImage(data: cropped.pngData()!, type: "image/png", filename: "camera.png")
        }
    }
    
//    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
//
//    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: true)
    }
    
    
}
