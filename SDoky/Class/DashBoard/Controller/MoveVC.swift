//
//  MoveVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/27/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper

protocol MoveVCDelegate {
    func didMoveFile()
}

class MoveVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSelectFolder : UIButton!
    
    //var folder: FolderDocResponseData?
    var folders: [FolderDocResponseData]?
    var docId: Int?
    var adminPushed: Bool?
    var selectModelList: [SelectModel] = []
    var localDocs: [Document] = []
    var localFolders: [Folder] = []
    var folderId = 0
    var docTitle:String = ""
    var data:String = ""
    var source:String = ""
    var delegate:MoveVCDelegate?
    
    var selectedFolder = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removePhoneErrorLabel(message: "", button: btnSelectFolder)
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSelectFolder.layer.cornerRadius = 5.0
        btnSelectFolder.layer.borderWidth = 1.0
        btnSelectFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        
        if false {
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.getDoc(params: ["doc_id" : docId ?? -1])
            // self.showSpinner(onView: self.view)
        }else {
            
            do {
                let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                let doc = docs.filter({$0.docId == Int16(self.docId!)}).first!
                self.folderId = Int(doc.folderId)
                self.docTitle = doc.title ?? ""
                self.data = doc.desc ?? ""
                self.source = doc.source ?? ""
                
                self.btnSelectFolder.setTitle("Choose folder to save", for: .normal)
                //self.selectedFolder = SelectModel(id: self.folderId, title: "")
                
                
            }catch{
                print("Error Fetching")
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnSelectFolderAction(_ sender: UIButton) {
        
        self.selectModelList.removeAll()
        
        if let folders = self.folders {
            for folder in folders {
                if folder.folderId != self.folderId {
                    self.selectModelList.append(SelectModel(id: folder.folderId!, title: folder.title!))
                }
                
            }
            
            var array = self.selectModelList.filter({$0.title == "Help"})
            if array.count > 0 {
                self.selectModelList.remove(at: 0)
            }
            
            self.selectModelList = selectModelList.sorted { $0.title.lowercased() < $1.title.lowercased() }
            
            array.append(contentsOf: self.selectModelList)
            
            self.selectModelList = array
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedFolder.id = model.id
            self.selectedFolder.title = model.title
            self.btnSelectFolder.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopup(controller, sourceView: sender as UIView)
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneErrorLabel(message: "", button: btnSelectFolder)
        
        let params = ["doc_id": self.docId ?? -1,
                      "title": self.docTitle,
                      "folder_id": self.selectedFolder.id,
                      "description": self.data ,
                      "source": self.source] as [String : Any]
        
        //if AppUtility.sharedInstance.isConnectedToInternet() {
        if self.selectedFolder.id == -1 {
            self.showErrorLabel(message: "Required field", button: self.btnSelectFolder)
        }
        else {
            
            if false {
                
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.updateDoc(params: params)
                //  self.showSpinner(onView: self.view)
                
            }else {
                do {
                    self.localFolders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                }catch {
                    print("Fetch Error")
                }
                
                do {
                    self.localDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                    
                }catch{
                    print("Error Fetching")
                }
                
                
                let sourceFolder = self.localFolders.filter({$0.folderId == self.folderId}).first!
                let destinationFolder = self.localFolders.filter({$0.folderId == self.selectedFolder.id}).first!
                var docToEdit : Document?
                var newDocsToSaveToSource = NSMutableSet()
                for doc in sourceFolder.docs! {
                    newDocsToSaveToSource.add(doc)
                    let d = doc as! Document
                    if Int(d.docId) == self.docId {
                        docToEdit = d
                        newDocsToSaveToSource.remove(doc)
                    }
                    
                }
                sourceFolder.docs = newDocsToSaveToSource
                sourceFolder.offlineAction = OFFLINE_EDIT
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                
                var docToSaveToDestination = NSMutableSet()
                for doc in destinationFolder.docs! {
                    let d = doc as! Document
                    docToSaveToDestination.add(d)
                }
                docToEdit!.folderId = Int16(self.selectedFolder.id)
                docToEdit?.offlineAction = OFFLINE_EDIT
                docToEdit?.folderName = destinationFolder.title ?? ""
                docToSaveToDestination.add(docToEdit)
                destinationFolder.docs = docToSaveToDestination
                destinationFolder.offlineAction = OFFLINE_EDIT
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                
                
                
                //            if var folder = self.localFolders.filter({$0.folderId == self.folderId}).first {
                //
                //                var docs = NSMutableSet()
                //                for doc in folder.docs! {
                //
                //                    let d = doc as! Document
                //                    if Int(d.docId) == self.docId {
                //                        d.title = self.docTitle
                //                        d.folderId = Int16(self.selectedFolder.id)
                //                        d.source = self.source
                //                        d.desc = self.data
                //                    }
                //                    docs.add(doc)
                //                }
                //                folder.docs = docs
                //            }
                //            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                self.delegate?.didMoveFile()
                self.dismiss(animated: true, completion: nil)
                
                
            }
        }
        
        
        
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didUpdateDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        // self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                
                self.delegate?.didMoveFile()
                self.dismiss(animated: true, completion: nil)
                
                
            }
        }
    }
    
    func didFailUpdateDocError(_ error: NSError, resultStatus: Bool) {
        //self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func didGetDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        //self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                
                if let docResponse: DocResponse = Mapper<DocResponse>().map(JSON: json) {
                    self.docTitle = docResponse.data!.title ?? ""
                    self.source = docResponse.data!.source ?? ""
                    self.data = docResponse.data!.description ?? ""
                    
                    let folder = self.folders?.filter {$0.folderId == docResponse.data?.folderId}
                    if let folderData = folder?.first {
                        self.btnSelectFolder.setTitle("Choose folder to save", for: .normal)
                        self.selectedFolder = SelectModel(id: folderData.folderId!, title: folderData.title!)
                        self.folderId = folderData.folderId!
                    }
                }
                
            }
        }
    }
    
    func didFailGetDocError(_ error: NSError, resultStatus: Bool) {
        //self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    
    
}



                       
