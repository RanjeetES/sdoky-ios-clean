//
//  ScanVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import MobileCoreServices

class ScanVC: UIViewController, HomeAPIDelegate, UINavigationControllerDelegate {
    var folders: [FolderDocResponseData]?
    
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnOpenDoc: UIButton!
    @IBOutlet weak var btnToggleFlash: UIButton!
    
    let cameraController = CameraController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                
                try? self.cameraController.displayPreview(on: self.captureView)
            }
        }
                
        func styleCaptureButton() {
            btnCapture.layer.borderColor = UIColor.white.cgColor
            btnCapture.layer.borderWidth = 1.0
            
            
            btnCapture.layer.cornerRadius = min(btnCapture.frame.width, btnCapture.frame.height) / 2
        }
        
        styleCaptureButton()
        configureCameraController()
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }

        do {
            try device.lockForConfiguration()

            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
                btnToggleFlash.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
            }

            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
        //                       AttachmentHandler.shared.imagePickedBlock = { (image) in
        //                           print(image)
        //
        //                        self.uploadImage(image: image)
        //                       }
        //               //        AttachmentHandler.shared.videoPickedBlock = {(url) in
        //               //        /* get your compressed video url here */
        //               //        }
        //                       AttachmentHandler.shared.filePickedBlock = {(filePath) in
        //                           print(filePath)
        //                       }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    @IBAction func btnCaptureAction(_ sender: Any) {
        
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            
            
            let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            vc.image = image
            self.show(vc, sender: self)
//            let shittyVC = ShittyImageCropVC(frame: (self.navigationController?.view.frame)!, image: image, aspectWidth: 4, aspectHeight: 3)
//               self.navigationController?.present(shittyVC, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func btntoggleFlashAction(_ sender: Any) {
        
//        if cameraController.flashMode == .on {
//            cameraController.flashMode = .off
//            //btnToggleFlash.setImage(#imageLiteral(resourceName: "eye-seen"), for: .normal)
//        }
//
//        else {
//            cameraController.flashMode = .on
//            //btnToggleFlash.setImage(#imageLiteral(resourceName: "data"), for: .normal)
//        }
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }

        do {
            try device.lockForConfiguration()

            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
                btnToggleFlash.setImage(#imageLiteral(resourceName: "flash_off"), for: .normal)
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                    btnToggleFlash.setImage(#imageLiteral(resourceName: "flash"), for: .normal)
                } catch {
                    print(error)
                }
            }

            device.unlockForConfiguration()
        } catch {
            print(error)
        }
        
    }
    
    //    @IBAction func btnBackAction(_ sender: Any) {
    //
    //    }
    
    @IBAction func btnOpenDocAction(_ sender: Any) {
        
//        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
//        importMenu.delegate = self
//        importMenu.modalPresentationStyle = .formSheet
//        self.present(importMenu, animated: true, completion: nil)
//
//        let documentsPicker = UIDocumentPickerViewController(documentTypes: ["public.image", "public.jpeg", "public.png", String(kUTTypePDF), String(kUTTypeContent), String(kUTTypeRTF)], in: .open)
//        documentsPicker.delegate = self
//        documentsPicker.allowsMultipleSelection = false
//        documentsPicker.modalPresentationStyle = .fullScreen
//        self.present(documentsPicker, animated: true, completion: nil)
//
  
           
        let myActionSheet =  UIAlertController(title: "", message: "Please select an app to continue.", preferredStyle: UIAlertController.Style.actionSheet)
//        let msgAttrString = NSMutableAttributedString(string: "Please select an app to continue.", attributes: textattributesHead)
//        myActionSheet.setValue(msgAttrString, forKey: "attributedMessage")
       
        myActionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        myActionSheet.addAction(UIAlertAction(title: "Photos", style: UIAlertAction.Style.default, handler: { (ACTION :UIAlertAction!)in
                    let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.mediaTypes = ["public.image"]
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            }))
        myActionSheet.addAction(UIAlertAction(title: "Files", style: UIAlertAction.Style.default, handler: { (ACTION :UIAlertAction!)in
                let documentsPicker = UIDocumentPickerViewController(documentTypes: ["public.image", "public.jpeg", "public.png", String(kUTTypePDF), String(kUTTypeContent), String(kUTTypeRTF)], in: .open)
                documentsPicker.delegate = self
                documentsPicker.allowsMultipleSelection = false
                documentsPicker.modalPresentationStyle = .fullScreen
                self.present(documentsPicker, animated: true, completion: nil)
            }))
        
        //myActionSheet.actions[2].setValue(msgAttrString, forKey: "attributedTitle")


        self.present(myActionSheet, animated: true, completion: nil)
        
    }
    
    //    func didPostFileSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
    //        print(resultDict)
    //    }
    //
    //    func didFailWithPostFileError(_ error: NSError, resultStatus: Bool) {
    //        print(error.debugDescription)
    //    }
    //
    func uploadImage(data: Data, type: String, filename: String) {
        DispatchQueue.main.async {
            self.showSpinner(onView: self.view)
        }
            let headers = [
                       "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
                       "content-type": "multipart/form-data",
                       "cache-control": "no-cache"
                   ]
    
            let url = URL(string: "https://api.sdoky.com/api/v1/file-to-text")
    
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
               // multipartFormData.append(data, withName: filename, mimeType: type)
                 
                multipartFormData.append(data, withName: "file_url", fileName: filename, mimeType: type)
            },to: url!, method:.post,
               headers:headers,
    
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //SwiftSpinner.show(progress: Progress.fractionCompleted*100, title: "تحميل")
                       
                    })
                   
                    upload.responseJSON { response in
                       
                        print(response.request!)  // original URL request
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result)   // result of response serialization
                        //                        self.showSuccesAlert()
                        DispatchQueue.main.async {
                                               self.removeSpinner()
                                           }

                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                        }
    
                        switch response.response?.statusCode {
    
                        case 200 :
                            print(response.data)
    
                            do {
                                let resultDict = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: Any]
    
                            if let json = resultDict as? [String: Any] {
                                    if json["status"] as! Bool == false {
    
    
                                    }else {
                                        print(json["data"] as! String)
    
    
                                        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                                        vc.folders = self.folders
                                        vc.isFromInternetSearch = false
                                        vc.isFromScan = true
                                        vc.data = (json["data"] as! String)
                                        vc.searchTitle = ""
                                        self.show(vc, sender: self)
                                    }
    
    
                                }
                            }catch{
                                print("Error")
                            }
    
    
    
    
                            break
    
                        case 400 :
                            print("400")
    
                            break
    
                        case 401 :
                            print("401")
                            self.checkUnAuthorized()
    
    
                            break
    
    
                        case 302 :
    
    
                            break
                        case 500 :
    
    
                            break
    
                        default: break
    
    
                        }
    
                    }
                    return
                case .failure( _):  DispatchQueue.main.async {
                                                      self.removeSpinner()
                                                  }
                    break
                
                }
    
            })
    
    
        }
    
}



//MARK: - Ext. Delegate DocumentPicker
extension ScanVC: UIDocumentPickerDelegate, UIImagePickerControllerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard controller.documentPickerMode == .open, let url = urls.first, url.startAccessingSecurityScopedResource() else { return }
        defer {
            DispatchQueue.main.async {
                url.stopAccessingSecurityScopedResource()
            }
             }
        
        
        print(url.pathExtension)
        if(url.pathExtension == "jpeg" || url.pathExtension == "png" || url.pathExtension == "jpg") {
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            print(image)
            let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
            vc.urlPath = url
            vc.image = image
            vc.folders = folders
            self.show(vc, sender: self)
//            self.uploadImage(data: image.pngData()!, type: "image/"+url.pathExtension, filename: url.path)
        }else {
            guard let doc = UIDocument(fileURL: url) as? UIDocument else {return}
            print(doc)
            do {
                self.uploadImage(data: try Data(contentsOf: url), type: "application/"+url.pathExtension, filename: url.lastPathComponent)
            }catch {
                print("Unable to perform the task")
            }
            
        }
       
        controller.dismiss(animated: false)
    }

    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: false)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        
        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        vc.urlPath = info[.imageURL] as? URL
        vc.image = image
        vc.folders = folders
        self.show(vc, sender: self)
        picker.dismiss(animated: false, completion: nil)
    }
}




