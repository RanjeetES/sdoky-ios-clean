//
//  AddNewFolderVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

protocol AddNewFolderVCDelegate {
    func didAddFolder()
}


class AddNewFolderVC: UIViewController, HomeAPIDelegate {
    
    @IBOutlet weak var btnAddNewFolder : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfFolderName : UITextField!
    
    var delegate:AddNewFolderVCDelegate?
    var id : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btnAddNewFolder.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        
        tfFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "folder-open", hasError: false, view: self.view, message: "")
        
        self.tfFolderName.attributedPlaceholder = NSAttributedString(string: "Type folder name", attributes: attributes)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    @IBAction func btnAddNewFolderActions(_ sender: Any) {
        self.removeErrorViews()
        
        if tfFolderName.text == "" {
            tfFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: "Required field")
        }else {
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.createFolder(params: ["title":tfFolderName.text])
                
            }else {
                
                
                
                
                do {
                    let folders:[Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    var foldersCopy = folders
                    foldersCopy.sort { (first, second) -> Bool in
                        first.folderId < second.folderId
                    }
                    if foldersCopy.count > 0 {
                        self.id = Int(foldersCopy.last!.folderId) + 1
                    }
                    
                    let existFolder = folders.filter({$0.title?.lowercased() == self.tfFolderName.text?.lowercased()})
                    if existFolder.count > 0 {
                         self.tfFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: "Folder already exists")
                        return
                    }
                    
                    
                }catch{
                    print("Error Fetching")
                }
                
                do {
                    let folderToSave = Folder(context: AppUtility.sharedInstance.getContext())
                    folderToSave.folderId = Int16(self.id)
                    folderToSave.title = self.tfFolderName.text
                    folderToSave.userId = Int16(AppUtility.sharedInstance.getCustomerId())
                    folderToSave.updatedAt = Date().stringDateSDoky()
                    folderToSave.status = true
                    folderToSave.createdAt = Date().stringDateSDoky()
                    folderToSave.offlineAction = OFFLINE_ADD
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                    
                    self.delegate?.didAddFolder()
                    self.dismiss(animated: true, completion: nil)
                    
                }catch{
                    print("Error Saving")
                }
                
                
            }
            
        }
        
    }
    
    
    func didCreateFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                tfFolderName.addLeftImage(imageTintColor: hexStringToUIColor(hex: ERROR_COLOR), imageName: "folder-open", hasError: true, view: self.view, message: json["message"] as! String)
                
            }else {
                
                self.delegate?.didAddFolder()
                self.dismiss(animated: true, completion: nil)
                
            }
        }
        
    }
    
    func didFailWiCreateFolderError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
