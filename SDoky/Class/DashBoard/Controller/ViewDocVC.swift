//
//  ViewDocVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/22/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

import UIKit
import ObjectMapper
import CoreData

class ViewDocVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var tfData : UITextView!
    @IBOutlet weak var btnBack : UIButton!
    
    var docId: Int?
    var doc: Doc?
    var folderDocResponseData: [FolderDocResponseData]?
    var adminPushed: Bool?
    var isFromNotification = false
    //    var selectModelList: [SelectModel] = []
    //
    //    var selectedFolder = SelectModel() {
    //        didSet {
    //            self.view.setNeedsLayout()
    //        }
    //    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        self.endEditing()
        tfData.delegate = self
        tfTitle.delegate = self
        self.btnEdit.isHidden = true
        self.btnBack.isHidden = true
        
        if isFromNotification {
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.getDoc(params: ["doc_id" : docId ?? -1])
            DispatchQueue.main.async {
               self.showSpinner(onView: self.view)
            }
            
        }else {
            
            do {
                var docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                let doc = docs.filter({$0.docId == Int16(self.docId!)}).first!
                self.tfTitle.text = doc.title
                self.tfData.text = doc.desc
                //self.lblDate.text = doc.updatedAt?.changeDateFormat() ?? "" + "(" + doc.source ?? "" + ")"
                
                self.lblDate.text = "\(doc.updatedAt?.changeDateFormatString() ?? "") (\(doc.source ?? ""))"
                self.adminPushed = doc.adminPushed
                if adminPushed == true {
                    self.btnEdit.isHidden = true
                }else{
                    self.btnEdit.isHidden = false
                }
                self.btnBack.isHidden = false
                
                
            }catch{
                print("Error Fetching")
            }
            
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //if AppUtility.sharedInstance.isConnectedToInternet() {
        if isFromNotification {
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.getDoc(params: ["doc_id" : docId ?? -1])
    //        self.showSpinner(onView: self.view)
        }else {
            
            do {
                var docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                let doc = docs.filter({$0.docId == Int16(self.docId!)}).first!
                self.tfTitle.text = doc.title
                self.tfData.text = doc.desc
                
                //self.lblDate.text = doc.updatedAt?.changeDateFormat() ?? "" + "(" + doc.source ?? "" + ")"
                
                self.lblDate.text = "\(doc.updatedAt?.changeDateFormatString() ?? "") (\(doc.source ?? ""))"
                self.adminPushed = doc.adminPushed
                if adminPushed == true {
                    self.btnEdit.isHidden = true
                }else{
                    self.btnEdit.isHidden = false
                }
                self.btnBack.isHidden = false
                
                
            }catch{
                print("Error Fetching")
            }
            
        }
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneError(message: "", texrField: tfTitle)
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
        }else {
            let params = ["doc_id": self.docId ?? -1,
                          "title": self.tfTitle.text ?? "",
                          "folder_id": self.doc?.folderId ?? "",
                          "description": self.tfData.text ?? "" ,
                          "source": self.doc?.source ?? ""] as [String : Any]
            
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.updateDoc(params: params)
            self.showSpinner(onView: self.view)
            
        }
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.enableEditing(state: false)
    }
    
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        if isFromNotification {
            self.showHome()
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func showHome() {
        let tabBarController = UITabBarController()
                       let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                       let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                       let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                       let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                       let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                       tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                       tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                       self.show(tabBarController, sender: self)
    }
    
    @IBAction func btnEditClick(_ sender: Any) {
        // self.enableEditing(state: true)
        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "EditDataVC") as! EditDataVC
        vc.docId = docId
        vc.folders = self.folderDocResponseData
        vc.adminPushed = self.adminPushed
        self.show(vc, sender: self)
    }
    
    @IBAction func btnDownloadAction(_ sender: Any) {
//        APIHandler.sharedInstance.downloadPdf(docId: self.docId ?? -1, uniqueName: self.doc?.title ?? "Unnamed", isFolder: false) { (message, status) in
//            self.showAlertWithMessageTitleAlert(message: "Pdf saved in Sdoky folder")
//        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadPdf(docId: self.docId ?? -1, uniqueName: self.doc?.title ?? "Unnamed", isFolder: false) { (message, status) in
                self.showAlertWithMessageTitleAlert(message: "Pdf saved in SDoky folder")
            }
        } else {
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
    }
    
    func enableEditing(state: Bool) {
        
        self.tfTitle.isEnabled = state
        //      self.tfData.isEnabled = state
        self.btnCancel.isHidden = !state
        self.btnEdit.isHidden = state
        
    }
    
    func didUpdateDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    func didFailUpdateDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func didGetDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        DispatchQueue.main.async {
           self.removeSpinner()
        }
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                if let docResponse: DocResponse = Mapper<DocResponse>().map(JSON: json) {
                    self.doc = docResponse.data
                    self.tfTitle.text = docResponse.data!.title ?? ""
                    self.tfData.text = docResponse.data!.description ?? ""
                    self.lblDate.text = "\(docResponse.data!.updatedAt?.changeDateFormatString() ?? "") (\(docResponse.data!.source ?? ""))"
                    if docResponse.data?.adminPushed == true {
                        self.btnEdit.isHidden = true
                    }else{
                        self.btnEdit.isHidden = false
                    }
                    self.btnBack.isHidden = false
                    
                }
                
            }
        }
    }
    
    func didFailGetDocError(_ error: NSError, resultStatus: Bool) {
        DispatchQueue.main.async {
           self.removeSpinner()
            self.btnBack.isHidden = false
        }
        print(error.debugDescription)
        //self.showAlertWithMessage(message: "Error on loading document, please try again.")
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 8.0
            }
        }
    }
    
}
