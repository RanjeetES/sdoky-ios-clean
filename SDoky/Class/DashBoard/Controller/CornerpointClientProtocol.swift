//
//  CornerpointClientProtocol.swift
//  SDoky
//
//  Created by Ranjeet Sah on 9/27/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import Foundation

@objc protocol CornerpointClientProtocol
{
  func cornerHasChanged(_: CornerpointView)
}
