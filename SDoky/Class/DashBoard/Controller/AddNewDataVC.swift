//
//  AddNewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class AddNewDataVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfType : UITextField!
    @IBOutlet weak var tfData : UITextField!
    @IBOutlet weak var btnSelectFolder : UIButton!
    
    var folder: FolderDocResponseData?
    var folders: [FolderDocResponseData]?
    
    var selectModelList: [SelectModel] = []
    
    var selectedFolder = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSelectFolder.layer.cornerRadius = 5.0
        btnSelectFolder.layer.borderWidth = 1.0
        btnSelectFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        self.tfTitle.attributedPlaceholder = NSAttributedString(string: "Type data title", attributes: attributes)
        self.tfType.attributedPlaceholder = NSAttributedString(string: "Type data source", attributes: attributes)
        
        tfData.delegate = self
        tfTitle.delegate = self
        tfType.delegate = self
        
        self.endEditing()
        if let folderData = folder {
            //self.btnSelectFolder.setAttributedTitle(NSAttributedString(string: folderData.title!, attributes: textFieldattributes), for: .normal)
            self.btnSelectFolder.setTitle(folderData.title, for: .normal)
            self.selectedFolder = SelectModel(id: folderData.folderId!, title: folderData.title!)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnSelectFolderAction(_ sender: UIButton) {
        
        self.selectModelList.removeAll()
        
        if let folders = self.folders {
            for folder in folders {
                self.selectModelList.append(SelectModel(id: folder.folderId!, title: folder.title!))
            }
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedFolder.id = model.id
            self.selectedFolder.title = model.title
            self.btnSelectFolder.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopup(controller, sourceView: sender as UIView)
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfType)
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
//        }else  if tfType.text == "" {
//            self.showError(message: "Required field", texrField: self.tfType)
//        }
        }else {
            let params = ["title": self.tfTitle.text ?? "",
                          "folder_id": self.selectedFolder.id,
                          "description": self.tfData.text ?? "" ,
                          "source": self.tfType.text ?? ""] as [String : Any]
            
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.createDoc(params: params)
            self.showSpinner(onView: self.view)
            
        }
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didCreateDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    func didFailWiCreateDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
}


