//
//  DataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/10/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer
import Alamofire
import CoreData
import IQKeyboardManagerSwift
import Speech
import Lottie



class MyTapGesture: UITapGestureRecognizer {
    var tapTag = Int()
}

class DataTableViewHeaderView : UITableViewHeaderFooterView {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDocNumber: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    
    var isOpen: Bool = false
    var numberOfRows: Int = 0
    var sectiontTag : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}


class DataTableFooterView : UITableViewHeaderFooterView {
    
    @IBOutlet weak var btnAddFolder: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
class DataTableViewCell :  UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    
    
}

class SearchTableHeaderView : UITableViewHeaderFooterView {
    @IBOutlet weak var lblDocumentCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class SearchDataTableViewCell :  UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
}

class DataVC: UIViewController, UITableViewDelegate, UITableViewDataSource, AddNewFolderVCDelegate, HomeAPIDelegate, RenameVCDelegate, MoveVCDelegate, UITextFieldDelegate, SFSpeechRecognizerDelegate, UIPopoverPresentationControllerDelegate {
    
    
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnMic: UIButton!
    @IBOutlet weak var btnMydata: UIButton!
    @IBOutlet weak var btnInternet: UIButton!
    @IBOutlet weak var tvData: UITableView!
    @IBOutlet weak var btnAddNewFolder: UIButton!
    @IBOutlet weak var vbtnData :  UIView!
    @IBOutlet weak var vbtnInternet :  UIView!
    @IBOutlet weak var btnSearchIcon: UIButton!
    @IBOutlet weak var btnMicAnimated: AnimationView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    
    var openSectionSet : Set<Int> = []
    var selectedSection = 0
    var folderDeleted = false
    var fileDeleted = false
    var detectionTimer:Timer?
    
    
    var folderResponseData: [FolderResponseData] = []
    var folderDocResponseData: [FolderDocResponseData] = []
    var folders : [Folder] = []
    var unfilteredDocs : [Document] = []
    var docs : [Document] = []
    var folderId = 0
    var myData = true
    var isAfterSync = false
    var deletedFolders : [Int] = []
    var deletedFiles : [Int] = []
    var searchDataResponse :  [InternetsearchResponseData] = []
    
    var selectModelList: [SelectModel] = []
    
    var selectedVehicleType = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    private let refreshControl = UIRefreshControl()
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    //Search
    @IBOutlet weak var searchTableView : UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        btnMicAnimated.contentMode = .scaleAspectFit
        
        btnMicAnimated.loopMode = .loop
        
        btnMicAnimated.animationSpeed = 0.5
        
        
        let headerNib = UINib.init(nibName: "DataTableViewHeaderView", bundle: nil)
        tvData.register(headerNib,forHeaderFooterViewReuseIdentifier: "sectionHeader")
        
        let footerNib = UINib.init(nibName: "DataTableFooterView", bundle: nil)
        tvData.register(footerNib,forHeaderFooterViewReuseIdentifier: "sectionFooter")
        
        let searchHeaderNib = UINib.init(nibName: "SearchTableHeaderView", bundle: nil)
        self.searchTableView.register(searchHeaderNib, forHeaderFooterViewReuseIdentifier: "searchHeader")
        
        
        self.navigationController?.navigationBar.isHidden = true
        tvData.isHidden = false
        tvData.delegate = self
        tvData.dataSource = self
        //tvData.tableFooterView = UIView()
        //tvData.contentInsetAdjustmentBehavior = .never
        self.tvData.contentInset = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0)
        
        self.endEditing()
        self.tfSearch.delegate = self
        self.tfSearch.addTarget(self, action: #selector(self.textIsChanging(_:)), for: .editingChanged)
        btnAddNewFolder.layer.borderWidth = 1.0
        btnAddNewFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnAddNewFolder.layer.cornerRadius = 5.0
        
        
        btnMydata.layer.borderWidth = 4.0
        btnMydata.layer.masksToBounds = false
        btnMydata.layer.cornerRadius = btnMydata.frame.height/2
        btnMydata.layer.borderColor = UIColor.white.cgColor
        btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        vbtnData.layer.borderWidth = 2.0
        vbtnData.layer.masksToBounds = false
        vbtnData.layer.cornerRadius = vbtnData.frame.height/2
        vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        
        btnInternet.layer.borderWidth = 4.0
        btnInternet.layer.masksToBounds = false
        btnInternet.layer.cornerRadius = btnInternet.frame.height/2
        btnInternet.layer.borderColor = UIColor.white.cgColor
        btnInternet.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnInternet.backgroundColor = UIColor.white
        
        vbtnInternet.layer.borderWidth = 2.0
        vbtnInternet.layer.masksToBounds = false
        vbtnInternet.layer.cornerRadius = vbtnInternet.frame.height/2
        vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        self.tvData.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(getNewData(_:)), for: .valueChanged)
        
        refreshControl.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        
        //Search TableView
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        self.searchTableView.tableFooterView = UIView()
        
        //        //Speech
        //
        //        let lang = AppUtility.sharedInstance.getLanguageSelected()
        //        switch lang {
        //
        //        case 0:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        //        case 1:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr-FR"))
        //        case 2:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "de-DE"))
        //        case 3:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
        //        case 4:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ru-RU"))
        //        case 5:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ja-JP"))
        //        case 6:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "zh-TW"))
        //        default:
        //            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        //
        //        }
        //
        //        speechRecognizer?.delegate = self
        //
        //
        //        btnMic.isEnabled = false
        //        speechRecognizer!.delegate = self
        //        SFSpeechRecognizer.requestAuthorization { (authStatus) in
        //
        //            var isButtonEnabled = false
        //
        //            switch authStatus {
        //            case .authorized:
        //                isButtonEnabled = true
        //
        //            case .denied:
        //                isButtonEnabled = false
        //                print("User denied access to speech recognition")
        //
        //            case .restricted:
        //                isButtonEnabled = false
        //                print("Speech recognition restricted on this device")
        //
        //            case .notDetermined:
        //                isButtonEnabled = false
        //                print("Speech recognition not yet authorized")
        //            @unknown default:
        //                print("Nothing")
        //            }
        //
        //            OperationQueue.main.addOperation() {
        //                self.btnMic.isEnabled = isButtonEnabled
        //            }
        //        }
        
        
    }
    
    @objc private func getNewData(_ sender: Any) {
        
        let homeAPI = HomeAPI()
        homeAPI.delegate = self
        homeAPI.getFolderDocs()
        //        DispatchQueue.main.async {
        //            self.showSpinner(onView: self.view)
        //        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        // IQKeyboardManager.shared.enable = true
        print("disappear")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        
        self.getLocalFolderDocs()
        self.tabBarController?.tabBar.isHidden = false
        self.setToNormalState()
        loader.isHidden = true
        
        //Speech
        
        let lang = AppUtility.sharedInstance.getLanguageSelected()
        switch lang {
            
        case 0:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        case 1:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "fr-FR"))
        case 2:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "de-DE"))
        case 3:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
        case 4:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ru-RU"))
        case 5:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "ja-JP"))
        case 6:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "zh-TW"))
        default:
            speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
            
        }
        
        speechRecognizer?.delegate = self
        
        
        btnMic.isEnabled = false
        speechRecognizer!.delegate = self
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            @unknown default:
                print("Nothing")
            }
            
            OperationQueue.main.addOperation() {
                self.btnMic.isEnabled = isButtonEnabled
            }
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        IQKeyboardManager.shared.enable = false
        //        self.tabBarController?.tabBar.isHidden = false
        //        self.getLocalFolderDocs()
        self.tabBarController?.tabBar.isHidden = false
        if AppUtility.sharedInstance.isConnectedToInternet() {
            //            let homeAPI = HomeAPI()
            //            homeAPI.delegate = self
            //            //homeAPI.getFolders()
            //            homeAPI.getFolderDocs()
            //            self.showSpinner(onView: self.view)
        }else {
            // self.getLocalFolderDocs()
        }
        
//        do {
//            let userInfoList : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
//            if userInfoList.count > 0 {
//                if userInfoList.first?.subscription?.expiresAt?.getDaysFromStringDate() ?? 0 < 0 {
//                    let pricingVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
//                    pricingVC.isFromAccount = false
//                    pricingVC.isFromNotification = false
//                    self.show(pricingVC, sender: self)
//                }
//            }
//        }catch {
//            print("Could not fetch")
//        }
        self.checkExpiry()
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tvData {
            return folderDocResponseData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tvData {
            
            
            
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "sectionHeader") as? DataTableViewHeaderView
            
            let sData = folderDocResponseData[section]
            //view?.lblName.text = sData.title
            
            if (sData.title ?? "").count > 16 {
                   view?.lblName.text = "\((sData.title ?? "").trunc(length: 16))..."
               }else {
                   view?.lblName.text = sData.title ?? ""
               }
            let date = self.changeDateFormat(date: sData.updatedAt!)
            view?.lblDate.text = date
            if sData.docs?.count ?? 0 > 1 {
                view?.lblDocNumber.text = "\(sData.docs?.count ?? 0) docs"
            }else {
                view?.lblDocNumber.text = "\(sData.docs?.count ?? 0) doc"
            }
            
            view?.numberOfRows = sData.docs?.count ?? 0
            view?.btnArrow.tag = section
            view?.btnMenu.tag = section
            view?.btnArrow.addTarget(self, action: #selector(sectionArrowTapped(_:)), for: .touchUpInside)
            view?.btnMenu.addTarget(self, action: #selector(sectionMenuTapped(_:)), for: .touchUpInside)
            
            view?.isUserInteractionEnabled = true
            let tap = MyTapGesture(target: self, action: #selector(sectionTapped(_:)))
            view?.addGestureRecognizer(tap)
            tap.tapTag = section
            
            //            view.transform = CGAffineTransform(translationX: 0, y: 0)
            //
            //
            //
            //            UIView.animate(withDuration: 12, delay: 3, options: [.allowUserInteraction], animations: { () -> Void in
            //                view.transform = CGAffineTransform(translationX: 0, y: 900)
            //            }, completion: nil)
            //
            
            
            if sData.title == "Help" {
                view?.ivIcon.image = UIImage(named: "help")
                view?.btnMenu.isHidden = true
            }else {
                if self.openSectionSet.contains(section) {
                    view?.ivIcon.image = UIImage(named: "folder-open")
                    
                }else {
                    view?.ivIcon.image = UIImage(named: "folder-close")
                }
                view?.btnMenu.isHidden = false
            }
            
            if self.openSectionSet.contains(section) {
                view?.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
                view?.isOpen = true
                
            }else {
                view?.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
                view?.isOpen = false
            }
            
            
            return view
        }else {
            var count = 0
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "searchHeader") as? SearchTableHeaderView
            if myData == true {
                count = self.docs.count
            }else {
                
                count = self.searchDataResponse.count
                if count == 0 {
                    view?.isHidden = true
                }else {
                    view?.isHidden = false
                }
            }
            
            view?.lblDocumentCount.text = "\(count) results found"
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if tableView == self.tvData {
            
            if (section == self.folderDocResponseData.count - 1){
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                    "sectionFooter") as? DataTableFooterView
                view?.btnAddFolder.layer.borderWidth = 1.0
                view?.btnAddFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
                view?.btnAddFolder.layer.cornerRadius = 5.0
                view?.btnAddFolder.addTarget(self, action: #selector(self.addNewFolder(_:)), for: .touchUpInside)
                
                return view
            }else {
                return UIView()
            }
        }else {
            return UIView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if tableView == self.tvData {
            if (section == self.folderDocResponseData.count - 1){
                return CGFloat.init(60.0)
            }else {
                return CGFloat.init(0.0)
            }
        }else{
            return CGFloat.init(0.0)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tvData {
            return CGFloat.init(34.0)
        }else {
            return CGFloat.init(16.0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tvData {
            
            return CGFloat.init(40.0)
        }else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tvData {
            
            if self.openSectionSet.contains(section) {
                return folderDocResponseData[section].docs?.count ?? 0
            }else {
                return 0
            }
        }else {
            if myData == true {
                return self.docs.count
            }else {
                return self.searchDataResponse.count
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tvData {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "data_cell", for: indexPath) as! DataTableViewCell
            let rData = folderDocResponseData[indexPath.section]
            
            if (rData.docs![indexPath.row].title ?? "").count > 16 {
                cell.lblName.text = "\((rData.docs![indexPath.row].title ?? "").trunc(length: 16))..."
            }else {
                cell.lblName.text = rData.docs![indexPath.row].title ?? ""
            }
            let date = self.changeDateFormat(date: rData.docs![indexPath.row].updatedAt!)
            cell.lblDate.text = date
            //cell.ivIcon.image = UIImage(named: "doc")
            
            cell.btnMenu.tag = rData.docs![indexPath.row].docId!
            cell.btnMenu.addTarget(self, action: #selector(rowMenuTapped(_:)), for: .touchUpInside)
            
            if rData.docs![indexPath.row].adminPushed == true {
                cell.btnMenu.isHidden = true
                cell.ivIcon.image = UIImage(named: "file-green")
            }else {
                cell.btnMenu.isHidden = false
                cell.ivIcon.image = UIImage(named: "doc")
            }
            
            
            return cell
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "search_cell", for: indexPath) as! SearchDataTableViewCell
            var title = ""
            var desc = ""
            
            if myData == true {
                
                if self.docs[indexPath.row].adminPushed == true {
                    cell.ivIcon.image = UIImage(named: "file-green")
                }else {
                    cell.ivIcon.image = UIImage(named: "doc")
                }
                title = self.docs[indexPath.row].title ?? ""
                desc = (self.docs[indexPath.row].desc ?? "").trunc(length: 80)
            }else {
                
                title = self.searchDataResponse[indexPath.row].title ?? ""
                desc = (self.searchDataResponse[indexPath.row].snippet ?? "").trunc(length: 120)
            }
            
            let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR)]
            
            let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 13), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]
            
            let attributedString1 = NSMutableAttributedString(string :desc, attributes:attrs1)
            
            
            let attributedString2 = NSMutableAttributedString(string: "... Read more", attributes:attrs2)
            
            attributedString1.append(attributedString2)
            // self.lblText.attributedText = attributedString1
            
            
            
            cell.lblName.text = title
            
            cell.lblData.attributedText = attributedString1
            
            
            
            //cell.lblData.text = self.docs[indexPath.row].desc == "" ? "Empty description" :   "\(self.docs[indexPath.row].desc ?? "") Read more"
            return cell
        }
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if tableView == self.tvData {
    //            return ""
    //        }else {
    //            return "\(self.docs.count) results found"
    //        }
    //    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tvData {
            
            
            let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ViewDocVC") as! ViewDocVC
            vc.isFromNotification = false
            let rData = folderDocResponseData[indexPath.section]
            vc.docId = rData.docs![indexPath.row].docId!
            vc.folderDocResponseData = self.folderDocResponseData
            vc.adminPushed = rData.docs![indexPath.row].adminPushed
            self.show(vc, sender: self)
        }else {
            
            if myData == true {
                
                let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ViewDocVC") as! ViewDocVC
                vc.isFromNotification = false
                vc.docId = Int(self.docs[indexPath.row].docId)
                vc.folderDocResponseData = self.folderDocResponseData
                vc.adminPushed = self.docs[indexPath.row].adminPushed
                self.show(vc, sender: self)
            }else {
                
                let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WebPopOverController") as! WebPopOverController
                // vc.modalPresentationStyle = .popover
                //vc.preferredContentSize = CGSize(width: self.view.frame.width - 40, height: self.view.frame.height)
                vc.url = self.searchDataResponse[indexPath.row].link
                vc.folders = self.folderDocResponseData
                vc.searchTitle = self.searchDataResponse[indexPath.row].title
                vc.isFromSMap = false
                
                
                
                //                let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: vc)
                //                var rect = self.view.frame
                //                rect.size.width = 1
                //                presentationController.popoverLayoutMargins = UIEdgeInsets(top: 0, left: rect.origin.x + 1, bottom: 0, right: 0)
                //                presentationController.sourceView = self.view
                //                presentationController.sourceRect = self.view.frame
                //                       presentationController.permittedArrowDirections = .init(rawValue: 0)
                self.show(vc, sender: self)
                
            }
            
            
            
        }
        
    }
    
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 12)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnDataAction(_ sender: Any) {
        
        //        self.btnMydata.layer.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        //        btnMydata.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        //        btnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        //        self.btnInternet.layer.backgroundColor = UIColor.white.cgColor
        self.btnMydata.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.btnInternet.backgroundColor = UIColor.white
        self.vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        self.vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        //self.getLocalFolderDocs()
        self.myData = true
        self.tvData.isHidden = false
        self.searchTableView.isHidden = true
        self.searchDataResponse = []
        self.searchTableView.reloadData()
        
    }
    
    @IBAction func btnInternetAction(_ sender: Any) {
        
        //        self.btnInternet.layer.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        //        btnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        //        btnMydata.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.btnMydata.layer.backgroundColor = UIColor.white.cgColor
        self.btnInternet.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.vbtnInternet.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        self.vbtnData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.myData = false
        //        self.tvData.isHidden = true
        //        self.searchTableView.isHidden = false
        
    }
    
    @IBAction func btnAddNewFolderAction(_ sender: Any) {
        
        self.showAddNewFolder()
        
    }
    
    @objc func addNewFolder(_ sender: UIButton) {
        self.showAddNewFolder()
    }
    
    func showAddNewFolder() {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "AddNewFolderVC") as! AddNewFolderVC
        viewController.delegate = self
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 250.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func showRenameFolder(currentTitle: String, folderId: Int) {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "RenameFolderVC") as! RenameFolderVC
        viewController.delegate = self
        viewController.currentTitle = currentTitle
        viewController.folderId = folderId
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 250.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func didAddFolder() {
        
        //        let homeAPI = HomeAPI()
        //        homeAPI.delegate = self
        //        homeAPI.getFolders()
        //        self.showSpinner(onView: self.view)
        //        self.viewDidAppear(false)
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
    }
    
    func didRenameFolder() {
        
        //        let homeAPI = HomeAPI()
        //        homeAPI.delegate = self
        //        homeAPI.getFolders()
        //        self.showSpinner(onView: self.view)
        //        self.viewDidAppear(false)
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
    }
    
    func didMoveFile() {
        //        self.viewDidAppear(false)
        //
        //        self.tabBarController?.tabBar.isHidden = false
        self.getLocalFolderDocs()
        
    }
    
    //    func didWriteNewFile() {
    //           self.tabBarController?.tabBar.isHidden = false
    //           self.getLocalFolderDocs()
    //       }
    
    
    
    func didGetFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let folderResponse:FolderResponse = Mapper<FolderResponse>().map(JSON: json) {
                    
                    self.folderResponseData = folderResponse.data!
                    //self.reloadTable()
                }
            }
            
            
        }
        
    }
    
    func didFailGetFolderError(_ error: NSError, resultStatus: Bool) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        print(error.debugDescription)
    }
    
    func didGetFolderDocsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        self.refreshControl.endRefreshing()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let folderDocResponse:FolderDocResponse = Mapper<FolderDocResponse>().map(JSON: json) {
                    self.folderDocResponseData = folderDocResponse.data!
                    do {
                        self.folders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if self.folders.count > 0 {
                        for folder in folders {
                            AppUtility.sharedInstance.getContext().delete(folder)
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }
                    
                    do {
                        self.docs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if self.docs.count > 0 {
                        for doc in docs {
                            AppUtility.sharedInstance.getContext().delete(doc)
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }
                    self.docs = []
                    
                    do {
                        let deletedFiles : [DeletedFiles] = try AppUtility.sharedInstance.getContext().fetch(DeletedFiles.fetchRequest())
                        if self.deletedFiles.count > 0 {
                            for file in deletedFiles {
                                AppUtility.sharedInstance.getContext().delete(file)
                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                            }
                            
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    self.deletedFiles = []
                    
                    do {
                        let deletedFolders : [DeletedFolder] = try AppUtility.sharedInstance.getContext().fetch(DeletedFolder.fetchRequest())
                        if deletedFolders.count > 0 {
                            for folder in deletedFolders {
                                AppUtility.sharedInstance.getContext().delete(folder)
                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                            }
                            
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    self.deletedFolders = []
                    
                    
                    
                    for folder in folderDocResponseData {
                        let folderToSave = Folder(context: AppUtility.sharedInstance.getContext())
                        folderToSave.id = Int16(folder.folderId ?? -1)
                        folderToSave.title = folder.title ?? ""
                        folderToSave.folderId = Int16(folder.folderId ?? -1)
                        folderToSave.status = folder.status ?? true
                        folderToSave.userId = Int16(folder.userId ?? -1)
                        folderToSave.createdAt = folder.createdAt ?? ""
                        folderToSave.updatedAt = folder.updatedAt ?? ""
                        folderToSave.offlineAction = ""
                        var docs = NSMutableSet()
                        for doc in folder.docs! {
                            let docToSave = Document(context: AppUtility.sharedInstance.getContext())
                            docToSave.id = Int16(doc.docId ?? 0)
                            docToSave.title = doc.title ?? ""
                            docToSave.folderId = Int16(doc.folderId ?? 0)
                            docToSave.docId = Int16(doc.docId ?? 0)
                            docToSave.desc = doc.description ?? ""
                            docToSave.source = doc.source ?? ""
                            docToSave.status = doc.status ?? true
                            docToSave.adminPushed = doc.adminPushed ?? false
                            docToSave.updatedAt = doc.updatedAt ?? ""
                            docToSave.userId = Int16(doc.userId ?? 0)
                            docToSave.offlineAction = ""
                            docToSave.createdAt = doc.createdAt ?? ""
                            docToSave.folderName = folder.title ?? ""
                            docs.add(docToSave)
                        }
                        folderToSave.docs = docs
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }
                    self.folderDocResponseData = folderDocResponse.data!
                    if isAfterSync == true {
                        self.isAfterSync = false
                        //self.searchTableView.reloadData()
                    }else {
                        self.reloadTable()
                    }
                    
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                    
                    //                    if self.fileDeleted == true {
                    //                        self.fileDeleted = false
                    //                        self.showConfirmation(message: "Folder deleted successfully.")
                    //                    }
                    //
                    //                    if self.folderDeleted == true {
                    //                        self.folderDeleted = false
                    //                        self.showConfirmation(message: "Doc deleted successfully.")
                    //                    }
                }
            }
            
            
        }
        
    }
    
    func didFailGetFolderDocsError(_ error: NSError, resultStatus: Bool) {
        
        self.removeSpinner()
        print(error.debugDescription)
        
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    func didDeleteFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                self.folderDeleted = true
                self.viewDidAppear(false)
                //                let alert = UIAlertController(title: "Alert", message: "Folder deleted successfully.", preferredStyle: UIAlertController.Style.alert)
                //
                //                // add the actions (buttons)
                //                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                //                    action in
                //                    self.didAddFolder()
                //                }))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func didFailDeleteFolderError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    func changeDateFormat(date: String) -> String {
        if date != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "d.MM.y"
            let goodDate = dateFormatter.string(from: d!)
            return goodDate
        }else {
            return ""
        }
        
    }
    
    @objc func sectionArrowTapped(_ sender: UIButton){
        
        let section = self.tvData.headerView(forSection: sender.tag) as! DataTableViewHeaderView
        
        if section.isOpen == false {
            section.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
            self.openSectionSet.removeAll()
            self.openSectionSet.insert(sender.tag)
            self.openSectionSet.insert(9)
            
        }else {
            section.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
            self.openSectionSet.remove(sender.tag)
        }
        self.reloadTable()
        
    }
    
    
    @objc func sectionTapped(_ sender: MyTapGesture){
        
        let s = sender.tapTag
        let section = self.tvData.headerView(forSection: s) as! DataTableViewHeaderView
        
        //        section.backgroundView?.backgroundColor = .lightGray
        //
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //           section.backgroundView?.backgroundColor = .clear
        //        }
        
        
        if section.isOpen == false {
            section.btnArrow.setImage(UIImage(named: "arrow-up"), for: .normal)
            self.openSectionSet.removeAll()
            self.openSectionSet.insert(s)
            
        }else {
            section.btnArrow.setImage(UIImage(named: "arrow-down"), for: .normal)
            self.openSectionSet.remove(s)
        }
        self.reloadTable()
        
    }
    
    @objc func sectionMenuTapped(_ sender: UIButton){
        self.selectModelList.removeAll()
        //self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
        //self.selectModelList.append(SelectModel(id: 1, title: RENAME))
        //self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
        //self.selectModelList.append(SelectModel(id: 3, title: DELETE))
        
        if folderDocResponseData[sender.tag].docs?.count == 0 {
            
            self.selectModelList.removeAll()
            self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
            self.selectModelList.append(SelectModel(id: 1, title: RENAME))
            //self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
            self.selectModelList.append(SelectModel(id: 3, title: DELETE))
            
        }else {
            
            self.selectModelList.removeAll()
            self.selectModelList.append(SelectModel(id: 2, title: NEW_DATA))
            self.selectModelList.append(SelectModel(id: 1, title: RENAME))
            self.selectModelList.append(SelectModel(id: 3, title: DOWNLOAD_AS_PDF))
            //self.selectModelList.append(SelectModel(id: 3, title: DELETE))
            
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            let folder = self.folderDocResponseData[sender.tag]
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            switch model.title {
            case NEW_DATA:
                self.createNewData(folder: folder)
            case RENAME:
                self.renameFolder(currentTitle: folder.title ?? "", folderId: folder.folderId ?? 0)
            case DOWNLOAD_AS_PDF:
                self.downloadFolderDocs(folderName: folder.title ?? "", folderId: folder.folderId ?? 0)
            case DELETE:
                self.deleteFolder(folderId: folder.folderId ?? 0, folderTitle: folder.title ?? "")
            default:
                print("No Action")
            }
            
        }
        controller.preferredContentSize = CGSize(width: 180.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopupForTable(controller, sourceView: sender as UIView)
    }
    
    @objc func rowMenuTapped(_ sender: UIButton){
        self.selectModelList.removeAll()
        self.selectModelList.append(SelectModel(id: 2, title: EDIT))
        self.selectModelList.append(SelectModel(id: 2, title: MOVE))
        self.selectModelList.append(SelectModel(id: 1, title: SAVE_AS_PDF))
        self.selectModelList.append(SelectModel(id: 3, title: DELETE))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            // let folder = self.folderDocResponseData[sender.tag]
            
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            let docId = sender.tag
            var docTitle = ""
            var folderId = 0
            let docFolder = self.folderDocResponseData.filter({$0.docs!.contains { (doc) -> Bool in
                doc.docId == docId
                }})
            if docFolder.count > 0 {
                let filtereDoc =  docFolder.first?.docs?.filter({$0.docId == docId})
                if let doc = filtereDoc?.first {
                    docTitle = doc.title ?? ""
                    folderId = doc.folderId ?? -1
                    self.folderId = doc.folderId ?? -1
                }
            }
            
            
            switch model.title {
            case EDIT:
                self.editDoc(docId: docId)
            case SAVE_AS_PDF:
                self.downloadDoc(docTitle: docTitle, docId: docId)
            case DELETE:
                self.deleteDoc(docId: docId, folderId: folderId, docTitle: docTitle)
            case MOVE:
                self.moveDocument(docId: docId)
            default:
                print("No action")
            }
            
        }
        controller.preferredContentSize = CGSize(width: 180.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopupForTable(controller, sourceView: sender as UIView)
    }
    
    
    func moveDocument(docId: Int) {
        
        let viewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "MoveVC") as! MoveVC
        viewController.delegate = self
        viewController.folderId = folderId
        viewController.docId = docId
        viewController.folders = self.folderDocResponseData
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 260.0)
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    
    func createNewData(folder: FolderDocResponseData) {
        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
        vc.folders = self.folderDocResponseData
        vc.folder = folder
        vc.isFromInternetSearch = false
        vc.isFromScan = false
        vc.data = ""
        vc.searchTitle = ""
        self.show(vc, sender: self)
        //self.tabBarController?.selectedIndex = 2
    }
    
    func renameFolder(currentTitle: String, folderId: Int) {
        self.showRenameFolder(currentTitle: currentTitle, folderId: folderId)
    }
    
    func downloadFolderDocs(folderName: String, folderId: Int) {
        //        for doc in docs {
        //            self.downloadDoc(docTitle: doc.title ?? "", docId: doc.docId ?? 0)
        //        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadPdf(docId: folderId, uniqueName: folderName, isFolder: true) { (message, status) in
                self.showAlertWithMessageTitleAlert(message: "Pdf saved in SDoky folder")
            }
        } else {
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
    }
    
    func downloadDoc(docTitle: String, docId: Int) {
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            APIHandler.sharedInstance.downloadPdf(docId: docId, uniqueName: docTitle, isFolder: false) { (message, status) in
                self.showAlertWithMessageTitleAlert(message: "Pdf saved in SDoky folder")
            }
        }else{
            self.showAlertWithMessageWithTitle(title: "Error", message: "No internet connection")
        }
        
    }
    
    func deleteFolder(folderId: Int, folderTitle: String) {
        
        let alert = UIAlertController(title: "Confirm to delete folder?", message: "Are you sure to delete folder " + folderTitle + "?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.deleteFolder(params: ["folder_id":folderId])
                //self.showSpinner(onView: self.view)
            }else{
                do {
                    var localFolders : [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                    let folder = localFolders.filter({Int($0.folderId) == folderId}).first!
                    folder.offlineAction = OFFLINE_DELETE
                    AppUtility.sharedInstance.getContext().delete(folder)
                    let deletedFolder = DeletedFolder(context: AppUtility.sharedInstance.getContext())
                    deletedFolder.folderId = Int16(folderId)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    self.folderDeleted = true
                    self.getLocalFolderDocs()
                    //self.showConfirmation(message: "Folder deleted successfully.")
                    
                }catch {
                    print("Error fetching")
                }
                
            }
            
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // Doc
    func editDoc(docId: Int) {
        
        let vc = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "EditDataVC") as! EditDataVC
        vc.docId = docId
        vc.folders = self.folderDocResponseData
        self.show(vc, sender: self)
        
    }
    
    func deleteDoc(docId: Int, folderId: Int, docTitle: String) {
        
        let alert = UIAlertController(title: "Confirm to delete document?", message: "Are you sure to delete file " + docTitle + "?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            action in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.deleteDoc(params: ["doc_id":docId])
                //self.showSpinner(onView: self.view)
            }else {
                do {
                    var localFolders : [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                    let folder = localFolders.filter({Int($0.folderId) == folderId}).first!
                    folder.offlineAction = OFFLINE_EDIT
                    
                    var docs = NSMutableSet()
                    for doc in folder.docs! {
                        docs.add(doc)
                        let d = doc as! Document
                        if Int(d.docId) == docId {
                            d.offlineAction = OFFLINE_DELETE
                            docs.remove(d)
                        }
                    }
                    folder.docs = docs
                    let deletedDoc = DeletedFiles(context: AppUtility.sharedInstance.getContext())
                    deletedDoc.fileId = Int16(docId)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    self.fileDeleted = true
                    self.getLocalFolderDocs()
                    
                    //self.showConfirmation(message: "Doc deleted successfully.")
                    
                }catch {
                    print("Error fetching")
                }
                
            }
            
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func didDeleteDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                self.fileDeleted = true
                self.viewDidAppear(false)
                //                let alert = UIAlertController(title: "Alert", message: "Doc deleted successfully.", preferredStyle: UIAlertController.Style.alert)
                //
                //                // add the actions (buttons)
                //                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                //                    action in
                //                    // self.didAddFolder()
                //                    self.viewDidAppear(false)
                //                }))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func didFailDeleteDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    func reloadTable() {
        
        if true {
            
            var array = self.folderDocResponseData.filter({$0.title == "Help"})
            var oneArray = NSMutableArray()
                    for (index, item) in array.enumerated() {
                        if index == 0 {
                            oneArray.add(item)
                        }
                    }
            self.folderDocResponseData = self.folderDocResponseData.filter({$0.title != "Help"})
            //self.folderDocResponseData.remove(at: 0)
            self.folderDocResponseData = folderDocResponseData.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            
            //oneArray.append(contentsOf: self.folderDocResponseData)
            oneArray.addObjects(from: self.folderDocResponseData)
            
            self.folderDocResponseData = oneArray as! [FolderDocResponseData]
            
            for folder in self.folderDocResponseData {
                folder.docs = folder.docs?.sorted { $0.title!.lowercased() < $1.title!.lowercased() }
            }
            
            
            do {
                
                self.unfilteredDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                
            }catch {
                print("Error fetching")
            }
            
            DispatchQueue.main.async {
                self.removeSpinner()
                self.tvData.reloadData()
                //self.searchTableView.reloadData()
                if self.fileDeleted == true {
                    self.fileDeleted = false
                    self.showConfirmation(message: "Doc deleted successfully.")
                }
                
                if self.folderDeleted == true {
                    self.folderDeleted = false
                    self.showConfirmation(message: "Folder deleted successfully.")
                }
            }
        }else {
            do {
                
                self.unfilteredDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                //self.docs = self.docs.sorted{ $0.title!.lowercased() < $1.title!.lowercased() }
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
            }catch {
                print("Error fetching")
            }
        }
        
    }
    
    func getLocalFolderDocs() {
        
        //        do {
        //            self.docs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
        //
        //        }catch{
        //            print("Error fetching")
        //        }
        
        do {
            var localFolderDocResponse : [FolderDocResponseData] = []
            folders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
            
            for folder in folders {
                
                var docs : [Doc] = []
                
                for doc in folder.docs! {
                    
                    let docToShow = doc as! Document
                    
                    let documentModel = Doc(docId: Int(docToShow.docId), folderId: Int(docToShow.folderId), title: docToShow.title ?? "", userId: Int(docToShow.userId), status: docToShow.status, description: docToShow.desc ?? "", updatedAt: docToShow.updatedAt ?? "", source: docToShow.source ?? "", adminPushed: docToShow.adminPushed, offlineAction: docToShow.offlineAction ?? "", createdAt: docToShow.createdAt ?? "")
                    docs.append(documentModel)
                    
                }
                let localFolder = FolderDocResponseData(folderId: Int(folder.folderId), title: folder.title ?? "", userId: Int(folder.userId), status: folder.status, updatedAt: folder.updatedAt ?? "", docs: docs, offlineAction: folder.offlineAction ?? "", createdAt: folder.createdAt ?? "")
                
                localFolderDocResponse.append(localFolder)
            }
            self.folderDocResponseData = localFolderDocResponse
            //self.reloadTable()
            
        }catch{
            print("Error Fetching")
        }
        
        if AppUtility.sharedInstance.isConnectedToInternet() {
            
            //                        let homeAPI = HomeAPI()
            //                        homeAPI.delegate = self
            //                        homeAPI.getFolderDocs()
            //            DispatchQueue.main.async {
            //                self.showSpinner(onView: self.view)
            //            }
            //self.showSpinner(onView: self.view)
            //
            //let homeAPI = HomeAPI()
            //homeAPI.delegate = self
            //            if self.folderDocResponseData.count > 0 {
            //                let params = Mapper().toJSONString(self.folderDocResponseData)?.replacingOccurrences(of: "\\\"", with: "\"")
            //                let d = params?.replacingOccurrences(of: "\\", with: "")
            //                 print(d)
            //                 self.callPostmanType(data: "folders_docs="+d!)
            //            }else {
            //                let homeAPI = HomeAPI()
            //                homeAPI.delegate = self
            //                homeAPI.getFolderDocs()
            //                DispatchQueue.main.async {
            //                    self.showSpinner(onView: self.view)
            //                }
            //
            //            }
            
            if AppUtility.sharedInstance.getHasLocalData() == true {
                self.reloadTable()
                //let params = Mapper().toJSONString(self.folderDocResponseData)?.replacingOccurrences(of: "\\\"", with: "\"")
                
                do {
                    self.deletedFiles = []
                    self.deletedFolders = []
                    let deletedDocs: [DeletedFiles] = try AppUtility.sharedInstance.getContext().fetch(DeletedFiles.fetchRequest())
                    for doc in deletedDocs {
                        self.deletedFiles.append(Int(doc.fileId))
                    }
                    let deletedFolders: [DeletedFolder] = try AppUtility.sharedInstance.getContext().fetch(DeletedFolder.fetchRequest())
                    for folder in deletedFolders {
                        self.deletedFolders.append(Int(folder.folderId))
                    }
                }catch {
                    print("Error fetching")
                }
                
                let params = self.folderDocResponseData.toJSONString()!.addingPercentEncoding(withAllowedCharacters: .symbols)//?.replacingOccurrences(of: "\\\"", with: "\"")
                
                
                
                
                
                let d = params?.replacingOccurrences(of: "\\", with: "")
                let paramsToSend = "folders_docs={\"folders_docs\":" + (params ?? "") + ",\"deleted_docs_ids\":\"[],\"deleted_folders_ids\":[]}"
                
                
                //let x = """folders_docs={"folders_docs":\(params!),\"deleted_docs_ids\":\(self.deletedFiles),\"deleted_folders_ids\":\(self.deletedFolders)}"""
                
                let x = "folders_docs={\"folders_docs\":\(params!),\"deleted_docs_ids\":\(self.deletedFiles),\"deleted_folders_ids\":\(self.deletedFolders)}"
                
                
                
                print(x)
                self.callPostmanType(data: x)
                
            }else {
                
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.getFolderDocs()
                DispatchQueue.main.async {
                    self.showSpinner(onView: self.view)
                }
                
            }
            
            
            //let jsonData = try! JSONSerialization.data(withJSONObject: d, options: [])
            
            //var jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            //let params = Mapper().toJSONArray(self.folderDocResponseData)
            
            
            
            //            let postData = "[{\"folder_id\":678,\"title\":\"Help\",\"user_id\":4,\"status\":false,\"docs\":[{\"user_id\":0,\"status\":false,\"description\":\"This is the admin pushed documnet.\",\"title\":\"Admin -2\",\"admin_pushed\":true,\"doc_id\":335,\"updated_at\":\"2020-08-27T12:05:05.000Z\",\"source\":\"admin\",\"folder_id\":0},{\"updated_at\":\"2020-08-26T14:03:35.000Z\",\"folder_id\":0,\"doc_id\":334,\"title\":\"Test1\",\"user_id\":0,\"status\":false,\"description\":\"The test is the testing team function.\",\"source\":\"admin\",\"admin_pushed\":true}],\"updated_at\":\"2020-08-21T15:24:33.000Z\"},{\"folder_id\":673,\"docs\":[{\"title\":\"rrrrrrrr\",\"updated_at\":\"2020-08-27T15:20:09.514Z\",\"status\":true,\"user_id\":4,\"admin_pushed\":false,\"description\":\"ttt\",\"folder_id\":673,\"source\":\"personal\",\"doc_id\":381}],\"user_id\":4,\"status\":false,\"title\":\"a\",\"updated_at\":\"2020-08-26T20:07:41.000Z\"},{\"folder_id\":677,\"docs\":[],\"updated_at\":\"2020-08-27T03:27:48.000Z\",\"user_id\":4,\"status\":false,\"title\":\"hhh\"},{\"folder_id\":676,\"updated_at\":\"2020-08-27T03:27:37.000Z\",\"docs\":[],\"user_id\":4,\"title\":\"jjj\",\"status\":false},{\"user_id\":4,\"title\":\"kol\",\"folder_id\":679,\"updated_at\":\"2020-08-27T03:31:15.000Z\",\"docs\":[{\"source\":\"personal\",\"status\":true,\"folder_id\":679,\"doc_id\":380,\"user_id\":4,\"updated_at\":\"2020-08-27T15:18:39.990Z\",\"description\":\"ttt\",\"title\":\"rrrrrrrr\",\"admin_pushed\":false}],\"status\":false},{\"status\":false,\"folder_id\":671,\"user_id\":4,\"title\":\"My folder\",\"docs\":[{\"source\":\"new\",\"updated_at\":\"2020-08-27T04:07:41.000Z\",\"status\":false,\"title\":\"new\",\"description\":\"wow\",\"folder_id\":671,\"user_id\":4,\"doc_id\":379,\"admin_pushed\":false}],\"updated_at\":\"2020-08-26T19:28:13.000Z\"},{\"updated_at\":\"2020-08-26T19:50:39.000Z\",\"title\":\"wow\",\"docs\":[],\"status\":false,\"user_id\":4,\"folder_id\":672}]"
            //
            //            let convertedStr = postData.replacingOccurrences(of: "\"", with: "", options: .literal, range: nil)
            //            print(convertedStr)
            
            //            let data = params.data(using: .utf8)!
            //            do {
            //                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            //                {
            //                   print(jsonArray) // use the json here
            //                } else {
            //                    print("bad json")
            //                }
            //            } catch let error as NSError {
            //                print(error)
            //            }
            //?.replacingOccurrences(of: "/", with: "")
            
            //         homeAPI.syncFolder(params: ["folders_docs": params])
            
            //
            //
            // prepare json data
            
            
            //            let j = [["title":"new","user_id":24,"created_at":"2020-08-13T14:42:54.000Z","updated_at":"2020-08-13T14:42:54.000Z","docs":[["title":"doc","user_id": 24,"folder_id": 765,"description": "description","source": "source","created_at":"2020-08-13T14:42:54.000Z","updated_at":"2020-08-13T14:42:54.000Z"],["title": "doc 2","user_id": 24,"folder_id":765,"description": "description","source": "source","created_at":"2020-08-13T14:42:54.000Z","updated_at": "2020-08-13T14:42:54.000Z"]]]]
            //            var s = ""
            //            do {
            //                let jsonData = try JSONSerialization.data(withJSONObject: j, options: .prettyPrinted)
            //                s = jsonData.base64EncodedString()
            //
            //            }catch{
            //                print("hello")
            //            }
            
            
            
            //            let body: [String: Any] = ["folders_docs": "[]" ]
            //
            //            //let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            //            //let j:[String:Any] = [:]
            //            // create post request
            //            //let url = URL(string: BASE_URL + syncFolderAPI)!
            //            let token = "Bearer " + AppUtility.sharedInstance.getToken()
            //            let url = URL(string: "https://api.sdoky.com/api/v1/folder-doc-sync")!
            //            var request = URLRequest(url: url)
            //            request.httpMethod = "POST"
            //
            //            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            //            request.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
            //            request.setValue("application/json", forHTTPHeaderField:"Accept")
            //
            //
            //            // insert json data to the request
            //            let bodyData = try? JSONSerialization.data(
            //                withJSONObject: body,
            //                options: []
            //            )
            //            request.httpBody = body.percentEncoded()
            //
            //
            //            let headers = [
            //                "Authorization":token,
            //                "Content-Type": "application/x-www-form-urlencoded",
            //                "Accept": "application/json"
            //            ]
            //
            
            
            
            //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            //            let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //                guard let data = data,
            //                    let response = response as? HTTPURLResponse,
            //                    error == nil else {                                              // check for fundamental networking error
            //                        print("error", error ?? "Unknown error")
            //
            //                        return
            //                }
            //
            //                guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
            //                    print("statusCode should be 2xx, but is \(response.statusCode)")
            //                    print("response = \(response)")
            //
            //                    return
            //                }
            //
            //                let responseString = String(data: data, encoding: .utf8)
            //                print("responseString = \(responseString)")
            //            }
            
            //task.resume()
            //            self.call()
            
            
            //            let homeAPI = HomeAPI()
            //            homeAPI.delegate = self
            //            homeAPI.syncFolder(params: ["folders_docs":postData])
            
            //            APIHandler.sharedInstance.getHttpManager().request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers).responseData { (data) in
            //                print(data.debugDescription)
            //            }
            
            
            //            let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //                guard let data = data, error == nil else {
            //                    print(error?.localizedDescription ?? "No data")
            //                    let homeAPI = HomeAPI()
            //                    homeAPI.delegate = self
            //                    homeAPI.getFolderDocs()
            //                    //self.showSpinner(onView: self.view)
            //                    return
            //                }
            //                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            //                if let responseJSON = responseJSON as? [String: Any] {
            //                    print(responseJSON)
            //                    let homeAPI = HomeAPI()
            //                    homeAPI.delegate = self
            //                    homeAPI.getFolderDocs()
            //                    //self.showSpinner(onView: self.view)
            //                }
            //            }
            //
            //            task.resume()
            
            //     let homeAPI = HomeAPI()
            //      homeAPI.delegate = self
            //homeAPI.syncFolder(params: ["folders_docs":[:]])
        }else {
            self.reloadTable()
            
        }
    }
    
    func didPostSyncFolderSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                print("success")
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.getFolderDocs()
                self.showSpinner(onView: self.view)
            }
        }
        
        
    }
    
    func didFailPostSyncFolderError(_ error: NSError, resultStatus: Bool) {
        DispatchQueue.main.async {
            //  self.removeSpinner()
            
        }
        
        let homeAPI = HomeAPI()
        homeAPI.delegate = self
        homeAPI.getFolderDocs()
        self.showSpinner(onView: self.view)
        print(error.debugDescription)
        
        //        let alert = UIAlertController(title: "Sync error", message: "Error \(error.code) \(error.localizedDescription)", preferredStyle: UIAlertController.Style.alert)
        //
        //        // add the actions (buttons)
        //        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
        //            action in
        //                    let homeAPI = HomeAPI()
        //                    homeAPI.delegate = self
        //                    homeAPI.getFolderDocs()
        //                    self.showSpinner(onView: self.view)
        //        }))
        //
        //        // show the alert
        //        self.present(alert, animated: true, completion: nil)
    }
    
    func showConfirmation(message: String) {
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            // self.didAddFolder()
            self.dismiss(animated: false, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func call() {
        
        
        let JSONObject: [String : AnyObject] = ["folders_docs" : "[]"] as [String : AnyObject]
        
        
        
        if JSONSerialization.isValidJSONObject(JSONObject) {
            var request: NSMutableURLRequest = NSMutableURLRequest()
            let url = "https://api.sdoky.com/api/v1/folder-doc-sync"
            
            var err: NSError?
            
            request.url = NSURL(string: url) as URL?
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let token = "Bearer " + AppUtility.sharedInstance.getToken()
            
            request.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: JSONObject, options: JSONSerialization.WritingOptions())
                
                print(JSONObject)
                
                
            } catch {
                print("bad things happened")
            }
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) {(response, data, error) -> Void in
                
                let httpResponse = response as! HTTPURLResponse
                if error != nil {
                    
                    print("error")
                    
                    
                    if let e = error as? NSError {
                        if e.code == 401 {
                            self.checkUnAuthorized()
                        }
                    }
                    
                    
                }else {
                    print(response)
                    
                    
                }
                
            }
        }
    }
    
    
    func callPostmanType(data: String) {
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        //let postData = NSMutableData(data: data.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        let postData = data.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/folder-doc-sync")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                self.reloadTable()
                
                if let e = error as? NSError {
                    if e.code == 401 {
                        self.checkUnAuthorized()
                    }
                }
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.getFolderDocs()
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    // Search Logic
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let searchText  = tfSearch.text ?? ""
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            tvData.isHidden = true
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                
                //                        self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                //                            self.searchTableView.reloadData()
                
            }else {
                //                let homeAPI = HomeAPI()
                //                homeAPI.delegate = self
                //                homeAPI.getInternetSearchData(params: ["query":searchText])
                if AppUtility.sharedInstance.isConnectedToInternet() {
                    //                    self.searchDataResponse = []
                    //                    self.searchTableView.reloadData()
                    self.showSpinner(onView: self.view)
                    self.callPostmanTypeSearch(q: "query=" + searchText)
                }else {
                    self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                }
                
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            
        }
        //self.endEditing()
        tfSearch.resignFirstResponder()
        
        return true
    }
    
    @objc func textIsChanging(_ textField: UITextField) {
        
        
        let searchText  = tfSearch.text!
        
        if searchText.count > 0 {
            searchTableView.isHidden = false
            tvData.isHidden = true
            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
            if myData == true {
                self.searchDataResponse = []
                //self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                self.filter(searchText: searchText)
                
                
            }else {
                self.docs = []
                //                let homeAPI = HomeAPI()
                //                homeAPI.delegate = self
                //                homeAPI.getInternetSearchData(params: ["query":searchText])
                // self.callPostmanTypeSearch(data: "query=" + searchText)
            }
        }
        else{
            self.docs = []
            self.searchDataResponse = []
            self.tvData.isHidden = false
            self.searchTableView.isHidden = true
            
        }
        self.searchTableView.reloadData()
        
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//
//        let searchText  = tfSearch.text! + string
//
//        if searchText.count > 0 {
//            searchTableView.isHidden = false
//            tvData.isHidden = true
//            self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
//            if myData == true {
//                self.searchDataResponse = []
//                self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
//
//
//            }else {
//                self.docs = []
//                //                let homeAPI = HomeAPI()
//                //                homeAPI.delegate = self
//                //                homeAPI.getInternetSearchData(params: ["query":searchText])
//                // self.callPostmanTypeSearch(data: "query=" + searchText)
//            }
//        }
//        else{
//            self.docs = []
//            self.searchDataResponse = []
//
//        }
//        self.searchTableView.reloadData()
//
//        return true
//    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        //self.setToNormalState()
        self.docs = []
        self.searchTableView.isHidden = true
        self.tvData.isHidden = false
        self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
        self.endEditing()
        self.tfSearch.text = ""
    }
    
    func setToNormalState() {
        //        self.docs = []
        //        self.searchTableView.isHidden = true
        //        self.tvData.isHidden = false
        //        self.endEditing()
        
        if myData == false {
            
        }else {
            
            self.docs = []
            self.searchTableView.isHidden = true
            self.tvData.isHidden = false
            self.btnSearchIcon.setImage(UIImage(named: "search"), for: .normal)
            self.endEditing()
            self.tfSearch.text = ""
            
        }
    }
    
    
    @IBAction func microphoneTapped(_ sender: Any) {
        
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            audioEngine.inputNode.removeTap(onBus: 0)
            //btnMic.isEnabled = false
            
            btnMic.setImage(UIImage(named: "mic"), for: .normal)
            btnMicAnimated.stop()
            
            //  setToNormalState()
        } else {
            startRecording()
            btnMic.setImage(UIImage(named: "mic"), for: .normal)
            btnMicAnimated.play()
        }
        
    }
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: [.notifyOthersOnDeactivation])
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode as? AVAudioInputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        var isFinal = false
        
        if let timer = self.detectionTimer, timer.isValid {
            if isFinal {
                print("final")
                self.detectionTimer?.invalidate()
            }
        } else {
            self.detectionTimer = Timer.scheduledTimer(withTimeInterval: 7, repeats: false, block: { (timer) in
                isFinal = true
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.btnMic.isEnabled = true
                
                self.btnMic.setImage(UIImage(named: "mic"), for: .normal)
                self.btnMicAnimated.stop()
                timer.invalidate()
            })
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask =  speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            
            
            if result != nil {
                
                self.tfSearch.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
                
                let searchText  = self.tfSearch.text ?? ""
                if self.myData == true {
                }else {
                    //self.tfSearch.becomeFirstResponder()
                }
                
                if true {
                    self.searchTableView.isHidden = false
                    self.tvData.isHidden = true
                    if self.myData == true {
                        //self.docs = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased()) || $0.desc!.lowercased().contains(searchText.lowercased())})
                        self.filter(searchText: searchText)
                        self.btnSearchIcon.setImage(UIImage(named: "close"), for: .normal)
                        self.searchTableView.reloadData()
                    }else {
                        if AppUtility.sharedInstance.isConnectedToInternet() {
                            
                            //self.removeSpinner()
                            //self.showSpinner(onView: self.view)
                            self.loader.isHidden = false
                            //                            self.searchDataResponse = []
                            //                            self.searchTableView.reloadData()
                            self.callPostmanTypeSearch(q: "query=" + searchText)
                        }else {
                            self.showAlertWithMessageWithTitle(title: "Alert", message: "No internet connection")
                        }
                        
                    }
                }
                else{
                    self.docs = []
                    
                }
                
                if error != nil || isFinal {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    
                    self.btnMic.isEnabled = true
                    
                    self.btnMic.setImage(UIImage(named: "mic"), for: .normal)
                    self.btnMicAnimated.stop()
                }
            }
            
            
            
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        //tfSearch.text = ""
        
    }
    
    //    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
    //        if available {
    //            btnMic.isEnabled = true
    //        } else {
    //            btnMic.isEnabled = false
    //        }
    //    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            btnMic.isEnabled = true
        } else {
            btnMic.isEnabled = false
        }
    }
    
    
    func filter(searchText: String) {
           
           let tilteMatched = self.unfilteredDocs.filter({$0.title!.lowercased().contains(searchText.lowercased())})
           
            let descMatched = self.unfilteredDocs.filter({ $0.desc!.lowercased().contains(searchText.lowercased())})
           
           let folderMatched = self.unfilteredDocs.filter({$0.folderName!.lowercased().contains(searchText.lowercased())})
           
           self.docs.removeAll()
           self.docs.append(contentsOf: tilteMatched )
           self.docs.append(contentsOf: descMatched )
           self.docs.append(contentsOf: folderMatched )
           
           self.docs = self.docs.unique
       }
    
    
    
    
}

extension DataVC {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    func didGetInternetResultsSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        
        
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                
            }else {
                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                    self.tvData.isHidden = true
                    self.searchTableView.isHidden = false
                    self.searchDataResponse = internetResponse.data!
                    self.searchTableView.reloadData()
                    
                    
                }
            }
            
            
        }
        
        
    }
    
    func didFailWithInternetResultsError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    @objc func yourFuncHere3() {
        self.removeSpinner()
    }
    
    
    func callPostmanTypeSearch(q: String) {
        
        
        
        let headers = [
            "authorization": "Bearer \(AppUtility.sharedInstance.getToken())",
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache"
        ]
        
        
        
        
        
        let postData = NSMutableData(data: q.data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.sdoky.com/api/v1/internet-search")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        request.setValue("\(postData.length)", forHTTPHeaderField: "Content-Length")
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        let session = URLSession(configuration: sessionConfig)
        
        //let session = URLSession.shared
        
        //self.showSpinner(onView: self.view)
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                
                if let e = error as? NSError {
                    if e.code == -1001 {
                        //self.callPostmanTypeSearch(q: q)
                    }
                    var message = ""
                    switch e.code {
                    case 401:
                        self.checkUnAuthorized()
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 402:
                        message = "Your credits are exhausted. You need to buy credits."
                    case 400:
                        message = "Bad request. Our server didn't understand your request."
                    case 403:
                        message = "Your provided API key seems to be invalid."
                    case 404:
                        message = "The API endpoint you tried doesn't exist."
                    case 405:
                        message = "The request method you are trying isn't supported on this endpoint."
                    case 422:
                        message = "A validation error occured for some of your query params or form data."
                    case 429:
                        message = "A rare error that tells you that you are making API calls too quickly."
                    case 503:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    case 500:
                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                    default:
                        message = "Something went wrong. Please try again."
                    }
                    
                    
                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
                //            let homeAPI = HomeAPI()
                //           homeAPI.delegate = self
                //           homeAPI.getFolderDocs()
                //           DispatchQueue.main.async {
                //               self.showSpinner(onView: self.view)
                //self.searchDataResponse = []
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                //           }
            } else {
                let httpResponse = response as? HTTPURLResponse
                //AppUtility.sharedInstance.setHasLocalData(hasLocalData: false)
                //self.reloadTable()
                
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.removeSpinner()
                }
                
                print(httpResponse)
                
                if httpResponse?.statusCode == 500 {
                    
                    let alert = UIAlertController(title: "Alert", message: "Server error. Please try again.", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        action in
                        
                        //self.callPostmanType(data: q)
                        
                    }))
                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                    
                    // show the alert
                    
                    
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }else {
                    
                    
                    do {
                        let resultDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                        
                        if let json = resultDict as? [String: Any] {
                            if json["status"] as! Bool == false {
                                
                                var message = ""
                                
                                if let code = json["status_code"] as? Int {
                                    
                                    switch code {
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 402:
                                        message = "Your credits are exhausted. You need to buy credits."
                                    case 400:
                                        message = "Bad request. Our server didn't understand your request."
                                    case 403:
                                        message = "Your provided API key seems to be invalid."
                                    case 404:
                                        message = "The API endpoint you tried doesn't exist."
                                    case 405:
                                        message = "The request method you are trying isn't supported on this endpoint."
                                    case 422:
                                        message = "A validation error occured for some of your query params or form data."
                                    case 429:
                                        message = "A rare error that tells you that you are making API calls too quickly."
                                    case 503:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    case 500:
                                        message = "Our services are unavailable (maybe due to an ongoing maintenance)"
                                    default:
                                        message = "Something went wrong. Please try again."
                                    }
                                    
                                    
                                    let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                                    
                                    // add the actions (buttons)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                                        action in
                                        
                                        //self.callPostmanType(data: q)
                                        
                                    }))
                                    //alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
                                    
                                    // show the alert
                                    DispatchQueue.main.async {
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    
                                }
                                
                                
                                
                                
                            }else {
                                if let internetResponse:InternetsearchResponse = Mapper<InternetsearchResponse>().map(JSON: json) {
                                    
                                    self.searchDataResponse = internetResponse.data!
                                    DispatchQueue.main.async {
                                        self.searchTableView.reloadData()
                                    }
                                    
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                        
                    } catch {
                        NSLog("ERROR \(error.localizedDescription)")
                        
                    }
                }
                
                
                
                //            DispatchQueue.main.async {
                //                self.showSpinner(onView: self.view)
                //            }
            }
        })
        
        dataTask.resume()
    }
    
    
}
