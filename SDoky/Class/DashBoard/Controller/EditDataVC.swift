//
//  AddNewDataVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/20/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class EditDataVC: UIViewController, HomeAPIDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var tfTitle : UITextField!
    @IBOutlet weak var tfType : UITextField!
    @IBOutlet weak var tfData : UITextView!
    @IBOutlet weak var btnSelectFolder : UIButton!
    
    //var folder: FolderDocResponseData?
    var folders: [FolderDocResponseData]?
    var docId: Int?
    var adminPushed: Bool?
    var selectModelList: [SelectModel] = []
    var localDocs: [Document] = []
    var localFolders: [Folder] = []
    var folderId = 0
    var source = ""
    var docTitle = ""
    var desc = ""
    
    var isFromOccurence = false
    var sMap: SMap?
    var occurenceId: Int?
    var isFromData = false
    
    var selectedFolder = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        btnSelectFolder.layer.cornerRadius = 5.0
        btnSelectFolder.layer.borderWidth = 1.0
        btnSelectFolder.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        tfData.layer.cornerRadius = 5.0
        tfData.layer.borderWidth = 1.0
        tfData.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        
        self.tfTitle.attributedPlaceholder = NSAttributedString(string: "Type data title", attributes: attributes)
        self.tfType.attributedPlaceholder = NSAttributedString(string: "Type data source", attributes: attributes)
        self.endEditing()
        tfData.delegate = self
        tfTitle.delegate = self
        tfType.delegate = self
        
        //if AppUtility.sharedInstance.isConnectedToInternet() {
        if false {
            let homeAPI = HomeAPI()
            homeAPI.delegate = self
            homeAPI.getDoc(params: ["doc_id" : docId ?? -1])
            self.showSpinner(onView: self.view)
        }else {
            
            do {
                let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                if let doc = docs.filter({$0.docId == Int16(self.docId!)}).first {
                    
                    self.tfTitle.text = doc.title
                                   self.tfData.text = doc.desc
                                   self.folderId = Int(doc.folderId)
                                   self.tfType.text = doc.source
                                   self.source = doc.source ?? ""
                                   self.docTitle = doc.title ?? ""
                                   self.desc = doc.desc ?? ""
                                   
                                   self.btnSelectFolder.setTitle(doc.folder?.title ?? "", for: .normal)
                                   self.selectedFolder = SelectModel(id: self.folderId, title: doc.folder?.title ?? "")
                                   
                }
               
            }catch{
                print("Error Fetching")
            }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        //self.view.window?.windowLevel = .statusBar

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//         if tfType.text == "" {
//            self.tfType.text = self.source ?? ""
//        }
    }
    
    
    @IBAction func editingChanged(_ textField: UITextField){
//        if tfType.text == "" {
//            self.tfType.text = self.source ?? ""
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    
    @IBAction func btnSelectFolderAction(_ sender: UIButton) {
        
        self.selectModelList.removeAll()
        
        if let folders = self.folders {
            for folder in folders {
                if folder.folderId != self.folderId {
                    self.selectModelList.append(SelectModel(id: folder.folderId!, title: folder.title!))
                }
               
            }
            
            var array = self.selectModelList.filter({$0.title == "Help"})
            if array.count > 0 {
                self.selectModelList.remove(at: 0)
            }
            
            self.selectModelList = selectModelList.sorted { $0.title.lowercased() < $1.title.lowercased() }
            
            array.append(contentsOf: self.selectModelList)
            
            self.selectModelList = array
        }
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            
            self.selectedFolder.id = model.id
            self.selectedFolder.title = model.title
            self.btnSelectFolder.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: self.view.bounds.width - 40.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        controller.isFromData = true
        
        showPopup(controller, sourceView: sender as UIView)
        
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.removePhoneError(message: "", texrField: tfTitle)
        self.removePhoneError(message: "", texrField: tfType)
        
                if tfType.text == "" {
                    self.tfType.text = self.source ?? "Personal"
                }
        if tfTitle.text == "" {
            self.showError(message: "Required field", texrField: self.tfTitle)
//        }else  if tfType.text == "" {
//            self.showError(message: "Required field", texrField: self.tfType)
//        }
        }else {
            let params = ["doc_id": self.docId ?? -1,
                          "title": self.tfTitle.text ?? "",
                          "folder_id": self.selectedFolder.id,
                          "description": self.tfData.text ?? "" ,
                          "source": self.tfType.text ?? ""] as [String : Any]
            
            //if AppUtility.sharedInstance.isConnectedToInternet() {
            if false {
                
                let homeAPI = HomeAPI()
                homeAPI.delegate = self
                homeAPI.updateDoc(params: params)
                self.showSpinner(onView: self.view)
                
            }else {
                do {
                    self.localFolders = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                    
                }catch {
                    print("Fetch Error")
                }
                
                do {
                    self.localDocs = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                    
                }catch{
                    print("Error Fetching")
                }
                
                if self.folderId == self.selectedFolder.id {
                    
                    if var folder = self.localFolders.filter({$0.folderId == self.folderId}).first {
                        
                        var docs = NSMutableSet()
                        for doc in folder.docs! {
                            
                            let d = doc as! Document
                            if Int(d.docId) == self.docId {
                                d.title = self.tfTitle.text ?? ""
                                d.folderId = Int16(self.selectedFolder.id)
                                d.source = self.tfType.text ?? ""
                                d.desc = self.tfData.text ?? ""
                                d.offlineAction = OFFLINE_EDIT
                            }
                            docs.add(doc)
                        }
                        folder.docs = docs
                        folder.offlineAction = OFFLINE_EDIT
                    }
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                }else {
                    
                    let sourceFolder = self.localFolders.filter({$0.folderId == self.folderId}).first!
                    let destinationFolder = self.localFolders.filter({$0.folderId == self.selectedFolder.id}).first!
                    var docToEdit : Document?
                    var newDocsToSaveToSource = NSMutableSet()
                    for doc in sourceFolder.docs! {
                        newDocsToSaveToSource.add(doc)
                        let d = doc as! Document
                       if Int(d.docId) == self.docId {
                        docToEdit = d
                        newDocsToSaveToSource.remove(doc)
                       }
                        
                    }
                    sourceFolder.docs = newDocsToSaveToSource
                    sourceFolder.offlineAction = OFFLINE_EDIT
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                    
                    var docToSaveToDestination = NSMutableSet()
                    for doc in destinationFolder.docs! {
                        let d = doc as! Document
                        docToSaveToDestination.add(d)
                    }
                    docToEdit!.title = self.tfTitle.text ?? ""
                    docToEdit!.folderId = Int16(self.selectedFolder.id)
                    docToEdit!.source = self.tfType.text ?? ""
                    docToEdit!.desc = self.tfData.text ?? ""
                    docToEdit?.folderName = destinationFolder.title ?? ""
                    docToSaveToDestination.add(docToEdit)
                    docToEdit?.offlineAction = OFFLINE_EDIT
                    destinationFolder.offlineAction = OFFLINE_EDIT
                    destinationFolder.docs = docToSaveToDestination
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: true)
                    
                }
                
                
                
//                if var doc = (self.localDocs.filter{ $0.docId == docId!}).first {
//
//                    doc.title = self.tfTitle.text ?? ""
//                    doc.folderId = Int16(self.selectedFolder.id)
//                    doc.source = self.tfType.text ?? ""
//                    doc.desc = self.tfData.text ?? ""
//                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                }
                //self.navigationController?.popViewController(animated: true)
                
                
                if isFromOccurence == true {
                    
                    do {
                        let localSMaps : [SMapLocal] = try AppUtility.sharedInstance.getContext().fetch(SMapLocal.fetchRequest())
                        if let sMapToEdit = localSMaps.filter({Int($0.smapId) == sMap?.smapId}).first {
                            sMapToEdit.updatedAt = Date().stringDateSDoky()
                            sMapToEdit.offlineAction = OFFLINE_EDIT
                            var occurences = NSMutableSet()
                            for occurence in sMapToEdit.occurences! {
                                let o = occurence as! OccurrenceLocal
                                if Int(o.occurenceId) == self.occurenceId {
                                    o.docRefId = Int16(self.docId ?? 0)
                                    o.offlineAction = OFFLINE_EDIT
                                    o.updatedAt = Date().stringDateSDoky()
                                }
                                occurences.add(o)
                            }
                            
                            sMapToEdit.occurences = occurences
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                            AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: true)
                            if isFromData == true {
                                self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                            }else {
                                self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                            }
                        }
                        
                    }catch {
                        print("Fetch Error")
                    }
                    
                }else {
                    self.showHome()
                }
                
                
//                do {
//                    let docs:[Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
//                    let doc = docs.filter({$0.docId == Int16(self.docId!)}).first!
//                    self.tfTitle.text = doc.title
//                    self.tfData.text = doc.desc
//                    //self.adminPushed = doc.adminPushed
//                    self.tfType.text = doc.source
//
//
//                }catch{
//                    print("Error Fetching")
//                }
                
            }
            
            
            
        }
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        if tfTitle.text != self.docTitle || tfType.text != self.source || tfData.text != self.desc || self.selectedFolder.id != self.folderId  {
            showSaveAlert()
        }else {
            //self.navigationController?.popViewController(animated: true)
            if isFromOccurence == true {
                
                if isFromData == true {
                    self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                }else {
                    self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                }
                
            }else {
                self.showHome()
                
            }
        }
    }
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
         if tfTitle.text != self.docTitle || tfType.text != self.source || tfData.text != self.desc || self.selectedFolder.id != self.folderId  {
              showSaveAlert()
          }else {
              //self.navigationController?.popViewController(animated: true)
            
            
            if isFromOccurence == true {
                
                if isFromData == true {
                    self.navigationController?.backToViewController(viewController: EditSMapDataVC.self)
                }else {
                    self.navigationController?.backToViewController(viewController: EditSMapPersonVC.self)
                }
                
            }else {
                self.showHome()
                
            }
          }
    }
    
    func didUpdateDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    func didFailUpdateDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func didGetDocSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
            }else {
                
                if let docResponse: DocResponse = Mapper<DocResponse>().map(JSON: json) {
                    self.tfTitle.text = docResponse.data!.title ?? ""
                    self.tfType.text = docResponse.data!.source ?? ""
                    self.source = docResponse.data!.source ?? ""
                    self.tfData.text = docResponse.data!.description ?? ""
                    self.docTitle = docResponse.data!.title ?? ""
                    self.desc = docResponse.data!.source ?? ""
                    
                    let folder = self.folders?.filter {$0.folderId == docResponse.data?.folderId}
                    if let folderData = folder?.first {
                        self.btnSelectFolder.setTitle(folderData.title ?? "", for: .normal)
                        self.selectedFolder = SelectModel(id: folderData.folderId!, title: folderData.title!)
                        self.folderId = folderData.folderId!
                    }
                }
                
            }
        }
    }
    
    func didFailGetDocError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
    }
    
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in view.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    func showSaveAlert() {
           
           let alert = UIAlertController(title: "Proceed without saving?", message: "You are about to cancel your write process.\n\n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
           
           // add the actions (buttons)
           alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
               action in
//               if self.folder != nil {
//                  self.navigationController?.popViewController(animated: false)
//              }else {
//
//                  self.tabBarController?.selectedIndex = 0
//
//              }
            //self.navigationController?.popViewController(animated: false)
            self.showHome()
               //self.clear()
               //self.tabBarController?.selectedIndex = 0
           }))
           
           alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
               action in
               self.dismiss(animated: false, completion: nil)
           }))
           
           // show the alert
           self.present(alert, animated: true, completion: nil)
       }
    
    func showHome() {
        let tabBarController = UITabBarController()
                       let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                       let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                       let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                       let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                       let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                       tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                       tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                       self.show(tabBarController, sender: self)
    }
    
    
    
}

  

