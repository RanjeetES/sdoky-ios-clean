//
//  CreateSMapResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 05/12/2020.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class CreateSMapResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var statusCode: Int?
    var message:String?
    var status: Bool?
    var data: SMap?
    
    func mapping(map: Map) {
        status <- map["status"]
        statusCode <- map["status_code"]
        message <- map["message"]
        data <- map["data"]
        
    }
}
