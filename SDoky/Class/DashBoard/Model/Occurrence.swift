//
//  FolderResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class Occurrence: Mappable {
    required init?(map: Map) {
        
    }
    
    var occurenceId: Int?
    var smapId: Int?
    var title:String?
    var isFirst: Bool?
    var description: String?
    var createdAt : String?
    var updatedAt: String?
    var deletedAt: String?
    var offlineAction: String?
    var newTitle:String?
    var newDescription: String?
    var docRefId: Int?
    var docRefIdEdited: Int?
    var tempTitle: String?
    
    init(occurenceId : Int, smapId: Int, title: String, isFirst: Bool, description: String,  createdAt: String, updatedAt : String, deletedAt: String, offlineAction: String, newTitle: String, newDescription:String) {
        self.occurenceId = occurenceId
        self.smapId = smapId
        self.title = title
        self.isFirst = isFirst
        self.description = description
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.offlineAction = offlineAction
        self.newTitle = newTitle
        self.newDescription = newDescription
        
    }
    
    init(occurenceId : Int, smapId: Int, title: String, isFirst: Bool, description: String,  createdAt: String, updatedAt : String, deletedAt: String, offlineAction: String) {
        self.occurenceId = occurenceId
        self.smapId = smapId
        self.title = title
        self.isFirst = isFirst
        self.description = description
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.offlineAction = offlineAction
        
    }
    
    init(occurenceId : Int, smapId: Int, title: String, isFirst: Bool, description: String,  createdAt: String, updatedAt : String, deletedAt: String, offlineAction: String, tempTitle: String, docRefId: Int, docRefIdEdited: Int) {
        self.occurenceId = occurenceId
        self.smapId = smapId
        self.title = title
        self.isFirst = isFirst
        self.description = description
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.offlineAction = offlineAction
        self.tempTitle = tempTitle
        self.docRefId = docRefId
        self.docRefIdEdited = docRefIdEdited
        
    }
    
    func mapping(map: Map) {
        occurenceId <- map["occurence_id"]
        smapId <- map["smap_id"]
        title <- map["title"]
        isFirst <- map["is_first"]
        //description <- map["description"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        offlineAction <- map["offline_action"]
        docRefId <- map["doc_ref_id"]
        docRefIdEdited <- map["doc_ref_id_edited"]
        tempTitle <- map["temp_title"]
        
        
    }
}

