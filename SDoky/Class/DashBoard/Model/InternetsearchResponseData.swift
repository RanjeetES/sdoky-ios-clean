//
//  InternetsearchResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 9/8/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class InternetsearchResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    var title:String?
    var link: String?
    var snippet: String?
    
    
    
    func mapping(map: Map) {
        title <- map["title"]
        link <- map["url"]
        snippet <- map["snippet"]
        
    }
}
