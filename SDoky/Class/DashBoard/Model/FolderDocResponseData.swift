//
//  FolderResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class FolderDocResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    
    var folderId: Int?
    var title:String?
    var userId: Int?
    var status: Bool?
    var updatedAt: String?
    var docs: [Doc]?
    var offlineAction: String?
    var createdAt : String?
    
    init(folderId : Int, title: String, userId: Int, status: Bool, updatedAt : String, docs: [Doc], offlineAction: String, createdAt : String) {
        self.folderId = folderId
        self.title = title
        self.userId = userId
        self.status = status
        self.updatedAt = updatedAt
        self.docs = docs
        self.offlineAction = offlineAction
        self.createdAt = createdAt
    }
    
    init(folderId : Int, title: String, userId: Int, status: Bool, updatedAt : String) {
        self.folderId = folderId
        self.title = title
        self.userId = userId
        self.status = status
        self.updatedAt = updatedAt
    }
        
    func mapping(map: Map) {
        folderId <- map["folder_id"]
        title <- map["title"]
        userId <- map["user_id"]
        status <- map["status"]
      //  deletedAt <- map["deleted_at"]
        updatedAt <- map["updated_at"]
        docs <- map["docs"]
        offlineAction <- map["offline_action"]
        createdAt <- map["created_at"]
        
    }
}


 
 
