//
//  FolderResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class Doc: Mappable {
    required init?(map: Map) {
        
    }
    
    var docId: Int?
    var folderId: Int?
    var title:String?
    var userId: Int?
    var status: Bool?
    var description: String?
    var updatedAt: String?
    var source: String?
    var adminPushed: Bool?
    var offlineAction: String?
    var createdAt : String?
    
    
    init(docId : Int, folderId: Int, title: String, userId: Int, status : Bool, description: String, updatedAt : String, source: String, adminPushed: Bool, offlineAction: String,  createdAt: String) {
        self.docId = docId
        self.folderId = folderId
        self.title = title
        self.userId = userId
        self.status = status
        self.description = description
        self.updatedAt = updatedAt
        self.source = source
        self.adminPushed = adminPushed
        self.offlineAction = offlineAction
        self.createdAt = createdAt
    }
    
    
    func mapping(map: Map) {
        docId <- map["doc_id"]
        title <- map["title"]
        userId <- map["user_id"]
        folderId <- map["folder_id"]
        status <- map["status"]
        description <- map["description"]
        updatedAt <- map["updated_at"]
        source <- map["source"]
        adminPushed <- map["admin_pushed"]
        offlineAction <- map["offline_action"]
        createdAt <- map["created_at"]
        
    }
}

                                                                    

                                                                            		
  
