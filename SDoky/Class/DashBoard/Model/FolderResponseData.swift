//
//  FolderResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class FolderResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    var folderId: Int?
    var title:String?
    var userId: Int?
    var status: Bool?
    var updatedAt: String?
    
    
    
    func mapping(map: Map) {
        folderId <- map["folder_id"]
        title <- map["title"]
        userId <- map["user_id"]
        status <- map["status"]
        updatedAt <- map["updated_at"]
        
    }
}

