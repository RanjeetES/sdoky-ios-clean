//
//  FolderResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/14/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class SMap: Mappable {
    required init?(map: Map) {
        
    }
    
    var smapId: Int?
    var isData: Bool?
    var title:String?
    var firstEntry: String?
    var secondEntry: String?
    var isOmy: Bool?
    var status: Bool?
    var userId: Int?
    var createdAt : String?
    var updatedAt: String?
    var deletedAt: String?
    var occurrences : [Occurrence]?
    var offlineAction: String?
    var oldOccurrences : [Occurrence]?
    
    init(smapId : Int, isData: Bool, title: String, firstEntry: String, secondEntry: String, userId: Int, status : Bool, createdAt: String, updatedAt : String, deletedAt: String, occurrences: [Occurrence], offlineAction: String, isOmy: Bool, oldOccurrences: [Occurrence]) {
        self.smapId = smapId
        self.isData = isData
        self.title = title
        self.firstEntry = firstEntry
        self.secondEntry = secondEntry
        self.userId = userId
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.occurrences = occurrences
        self.offlineAction = offlineAction
        self.isOmy = isOmy
    }
    
    init(smapId : Int, isData: Bool, title: String, firstEntry: String, secondEntry: String, userId: Int, status : Bool, createdAt: String, updatedAt : String, deletedAt: String, occurrences: [Occurrence], offlineAction: String, isOmy: Bool) {
        self.smapId = smapId
        self.isData = isData
        self.title = title
        self.firstEntry = firstEntry
        self.secondEntry = secondEntry
        self.userId = userId
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.occurrences = occurrences
        self.offlineAction = offlineAction
        self.isOmy = isOmy
    }
    
    
    func mapping(map: Map) {
        smapId <- map["smap_id"]
        isData <- map["is_data"]
        title <- map["title"]
        firstEntry <- map["first_entry"]
        secondEntry <- map["second_entry"]
        status <- map["status"]
        userId <- map["user_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        occurrences <- map["occurrences"]
        offlineAction <- map["offline_action"]
        isOmy <- map["is_omy"]
        
        
    }
}





