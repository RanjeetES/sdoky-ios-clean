//
//  ConnectAllUserResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/30/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class ConnectAllUserResponseData: Object,Mappable {
    
    
    @objc dynamic var total_connect_user = ""
    @objc dynamic var total_low_risk_user = ""
    @objc dynamic var total_high_risk_user = ""
    @objc dynamic var total_medium_risk_user = ""
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        total_connect_user    <- map["total_connect_user"]
        total_low_risk_user         <- map["total_low_risk_user"]
        total_high_risk_user <- map ["total_high_risk_user"]
        total_medium_risk_user    <- map["total_medium_risk_user"]
        
    }
    
}
