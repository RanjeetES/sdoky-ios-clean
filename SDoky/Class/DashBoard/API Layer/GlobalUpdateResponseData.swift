//
//  GlobalUpdateResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/24/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class GlobalUpdateResponseData: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var confirmed_case = ""
    @objc dynamic var latest_case = ""
    @objc dynamic var total_recovered = ""
    @objc dynamic var total_death = ""
    @objc dynamic var date = ""
   
    required convenience init?(map: Map) {
      self.init()
    }

    
    
    func mapping(map: Map) {
            id    <- map["id"]
            confirmed_case         <- map["confirmed_case"]
            latest_case      <- map["new_case"]
            total_recovered       <- map["total_recovered"]
            total_death  <- map["total_death"]
            date  <- map["date"]
    }
    
}
