//
//  NepalUpdateResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/24/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class NepalUpdateResponseData: Object, Mappable {
    
    
    @objc dynamic var id = 0
    @objc dynamic var total_sample_test = ""
    @objc dynamic var total_covid_postive = ""
    @objc dynamic  var total_covid_negative = ""
    @objc dynamic  var total_covid_isolation = ""
    @objc dynamic  var total_covid_recovered = ""
    @objc dynamic  var total_covid_death = ""
    @objc dynamic var date = ""
     @objc dynamic var time = ""
    
    
   required convenience init?(map: Map) {
      self.init()
    }
    
    
    func mapping(map: Map) {
        
        id    <- map["id"]
        total_sample_test         <- map["total_sample_test"]
        total_covid_postive      <- map["total_covid_postive"]
        total_covid_negative       <- map["total_covid_negative"]
        total_covid_isolation  <- map["total_covid_isolation"]
        total_covid_recovered  <- map["total_covid_recovered"]
        date  <- map["date"]
        total_covid_death  <- map["total_covid_death"]
        time  <- map["time"]
     
    }
}
