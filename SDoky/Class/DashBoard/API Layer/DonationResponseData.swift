//
//  DonationResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/25/20.
//  Copyright © 2020 Nabin. All rights reserved.
//


import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class DonationResponseData: Object,Mappable {
    
    
     @objc dynamic var id = 0
    @objc dynamic var bank_name = ""
    @objc dynamic var account_no = ""
    @objc dynamic var account_name = ""
   // @objc dynamic var designation = ""
    @objc dynamic var organisation_name = ""
     @objc dynamic var description1 = ""
    
   required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        
        id    <- map["id"]
        bank_name         <- map["bank_name"]
      //  title      <- map["title"]
        account_name <- map ["account_name"]
        account_no    <- map["account_no"]
        organisation_name         <- map["organisation_name"]
         description1         <- map["description"]
    }
    
}
