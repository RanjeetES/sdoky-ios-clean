//
//  SettingsAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/5/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol SMapAPIDelegate {
    @objc optional func didCreateFolderSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWiCreateFolderError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetFolderSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailGetFolderError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didUpdateFolderSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailUpdateFolderError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didDeleteFolderSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailDeleteFolderError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetSMapsSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailGetSMapsError(_ error: NSError,resultStatus:Bool)
    
    //Docs
    @objc optional func didCreateDocSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWiCreateDocError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetDocSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailGetDocError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didUpdateDocSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailUpdateDocError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didDeleteDocSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailDeleteDocError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetDocPdfSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailGetDocPdfError(_ error: NSError,resultStatus:Bool)
    
    //Sync
    @objc optional func didPostSyncFolderSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailPostSyncFolderError(_ error: NSError,resultStatus:Bool)
    
    //Internet Search
    @objc optional func didGetInternetResultsSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithInternetResultsError(_ error: NSError,resultStatus:Bool)
    
    //Post File To Text
    @objc optional func didPostFileSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostFileError(_ error: NSError,resultStatus:Bool)
    
    //Create SMap
    @objc optional func didCreateSMapSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithCreateSMapError(_ error: NSError,resultStatus:Bool)
    
}

class SMapAPI {
    
    var delegate:SMapAPIDelegate?
    
    func createFolder(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+createFolderAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didCreateFolderSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWiCreateFolderError!(error, resultStatus: false)
        }
    }
    
    func updateFolder(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+updateFolderAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didUpdateFolderSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailUpdateFolderError!(error, resultStatus: false)
        }
    }
    
    func deleteFolder(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+deleteFolderAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didDeleteFolderSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailDeleteFolderError!(error, resultStatus: false)
        }
    }
    
    func getFolders() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+getFolderAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetFolderSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailGetFolderError!(error, resultStatus: false)
        }
    }
    
    func getSMaps() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+getSMapAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetSMapsSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailGetSMapsError!(error, resultStatus: false)
        }
    }
    
    // Docs
    func getDoc(params : [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+getDocAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didGetDocSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailGetDocError!(error, resultStatus: false)
        }
    }
    
    func createDoc(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+createDocAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didCreateDocSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWiCreateDocError!(error, resultStatus: false)
        }
    }
    
    func createSMap(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+createSMapAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didCreateSMapSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithCreateSMapError!(error, resultStatus: false)
        }
    }
    
    func updateDoc(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+updateDocAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didUpdateDocSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailUpdateDocError!(error, resultStatus: false)
        }
    }
    
    func deleteDoc(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+deleteDocAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didDeleteDocSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailDeleteDocError!(error, resultStatus: false)
        }
    }
    
    func getDocPdf() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+getDocPdfAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetDocPdfSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailGetDocPdfError!(error, resultStatus: false)
        }
    }
    
    func syncFolder(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+syncFolderAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostSyncFolderSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailPostSyncFolderError!(error, resultStatus: false)
        }
    }
    
    func getInternetSearchData(params: [String: Any])  {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+internetSearchAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didGetInternetResultsSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithInternetResultsError!(error, resultStatus: false)
        }
        
    }
    
    func postFileToText(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+fileToTextAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostFileSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithPostFileError!(error, resultStatus: false)
        }
        
    }
}
