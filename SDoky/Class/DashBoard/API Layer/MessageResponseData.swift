//
//  MessageResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/24/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class MessageResponseData: Object,Mappable {
    
    
     @objc dynamic var id = 0
    @objc dynamic var profile_pic = ""
    @objc dynamic var title = ""
    @objc dynamic var name = ""
    @objc dynamic var designation = ""
    @objc dynamic var message = ""
    
   required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        
        id    <- map["id"]
        profile_pic         <- map["profile_pic"]
        title      <- map["title"]
        name <- map ["name"]
        designation    <- map["designation"]
        message         <- map["message"]
    }
    
}
