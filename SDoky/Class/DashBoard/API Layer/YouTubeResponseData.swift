//
//  YouTubeResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/24/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class YoutubeResponseData: Object,Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var video_title = ""
    @objc dynamic var description1 = ""
    @objc dynamic var video_id = ""
    @objc dynamic var status = ""
    @objc dynamic var is_featured = 0
    @objc dynamic var featured_sort_order = 0
    @objc dynamic var normal_sort_order: String?
    @objc dynamic var lang_flag = ""
    
     required convenience init?(map: Map) {
         self.init()
       }
    
    func mapping(map: Map) {
        
        id    <- map["id"]
        video_title         <- map["video_title"]
        description1      <- map["description"]
        video_id <- map ["video_id"]
        status    <- map["status"]
        is_featured         <- map["is_featured"]
        featured_sort_order      <- map["featured_sort_order"]
        normal_sort_order <- map ["normal_sort_order"]
        lang_flag    <- map["lang_flag"]
    }
}
