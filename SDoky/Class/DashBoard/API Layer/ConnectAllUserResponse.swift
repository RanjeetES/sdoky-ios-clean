//
//  ConnectAllUserResponse.swift
//  Covid-19
//
//  Created by Nabin on 4/30/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper

class ConnectAllUserResponse: Mappable {
    
    var connectAllUserResponseData: ConnectAllUserResponseData?
    var error: String?
    var message: String?
    var statusCode: Int?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        error    <- map["error"]
        message         <- map["message"]
        statusCode      <- map["status_code"]
        connectAllUserResponseData <- map ["results"]
   
    }
    
}
