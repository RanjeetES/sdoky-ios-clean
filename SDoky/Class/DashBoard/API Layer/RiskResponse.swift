//
//  RiskResponse.swift
//  Covid-19
//
//  Created by Nabin on 4/29/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper

class RiskResponse: Mappable {
    
    var riskResponseData: RiskResponseData?
    var error: String?
    var message: String?
    var statusCode: Int?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        error    <- map["error"]
        message         <- map["message"]
        statusCode      <- map["status_code"]
        riskResponseData <- map ["results"]
   
    }
    
}
