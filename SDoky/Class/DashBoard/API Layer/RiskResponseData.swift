//
//  RiskResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/29/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class RiskResponseData: Object,Mappable {
    
    
    @objc dynamic var id = 0
    @objc dynamic var total_score = 0
    @objc dynamic var risk = ""
    @objc dynamic var risk_percent = ""

   
    
   required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        
        id            <- map["id"]
        total_score   <- map["total_score"]
        risk          <- map ["risk"]
        risk_percent  <- map["risk_percent"]
       
    }
    
}
