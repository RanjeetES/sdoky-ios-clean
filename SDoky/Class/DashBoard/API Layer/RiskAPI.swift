//
//  RiskAPI.swift
//  Covid-19
//
//  Created by Nabin on 4/29/20.
//  Copyright © 2020 Nabin. All rights reserved.
//

import UIKit


@objc protocol RiskAPIDelegate {
    @objc optional func didReceiveRiskDataSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithRiskDatError(_ error: NSError,resultStatus:Bool)
}


class RiskAPI {
    
    var delegate:RiskAPIDelegate?
    
    
    func getRiskList(language:String) {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+riskPercentageAPI+language as NSString),
            params: [:],
            success: { (response) in
              
                self.delegate?.didReceiveRiskDataSuccessfully!(resultDict: response, resultStatus: true)
                
        }) { (error, response) in
            self.delegate?.didFailWithRiskDatError!(error, resultStatus: false)
        }
    }
}


