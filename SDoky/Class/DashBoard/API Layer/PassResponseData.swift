//
//  PassResponseData.swift
//  Covid-19
//
//  Created by Nabin on 4/24/20.
//  Copyright © 2020 Nabin. All rights reserved.
//


import UIKit
import ObjectMapper
import UIKit
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class PassResponseData:Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var info = ""
    @objc dynamic var link = ""
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        
        id    <- map["total"]
        info  <- map["description"]
        link  <- map["link"]
        
        
    }
}
