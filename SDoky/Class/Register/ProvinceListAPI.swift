//
//  ProvinceListAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 5/8/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

@objc protocol ProvinceListAPIAPIDelegate {
    @objc optional func didReceiveProvinceListSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithProvinceListError(_ error: NSError,resultStatus:Bool)
}

class ProvinceListAPI {
    
    var delegate:ProvinceListAPIAPIDelegate?
    
    
    func getProvinceList(language:String) {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+""+language as NSString),
            params: [:],
            success: { (response) in
                
                self.delegate?.didReceiveProvinceListSuccessfully!(resultDict: response, resultStatus: true)
                
        }) { (error, response) in
            self.delegate?.didFailWithProvinceListError!(error, resultStatus: false)
        }
    }
    
}

