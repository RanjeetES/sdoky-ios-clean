////
////  ProvinceListResponse.swift
////  SDoky
////
////  Created by Ranjeet Sah on 5/8/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//class ProvinceListResponse: Mappable {
//    
//    var provinceListResponseData: [ProvinceListResponseData]?
//    var error: String?
//    var message: String?
//    var statusCode: Int?
//    
//    
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        
//        error    <- map["error"]
//        message         <- map["message"]
//        statusCode      <- map["status_code"]
//        provinceListResponseData <- map ["results"]
//   
//    }
//    
//}
