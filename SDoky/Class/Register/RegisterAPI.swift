//
//  RegisterAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/24/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper


//@objc protocol RegisterAPIDelegate {
//    @objc optional func didPostRegisterSuccessfully(resultDict:AnyObject,resultStatus:Bool)
//    @objc optional func didFailWithPostRegisterError(_ error: NSError,resultStatus:Bool)
//}
//
//class RegisterAPI {
//    
//    var delegate:RegisterAPIDelegate?
//    
//    func postRegisterRequest(params: [String: Any]) {
//        
//        APIHandler.sharedInstance.performPOSTRequest(
//            (BASE_URL+"" as NSString),
//            params: params as [String : AnyObject],
//            success: { (response) in
//                self.delegate?.didPostRegisterSuccessfully!(resultDict: response, resultStatus: true)
//        }) { (error, response) in
//            self.delegate?.didFailWithPostRegisterError!(error, resultStatus: false)
//        }
//    }
//    
//}


@objc protocol ProfessionUpdateAPIDelegate {
    @objc optional func didReceiveProfessionDataSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithProfessionDataError(_ error: NSError,resultStatus:Bool)
}

class ProfessionUpdateAPI {
    
    var delegate:ProfessionUpdateAPIDelegate?
    
    
    func getProfessionlUpdate() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+"" as NSString),
            params: [:],
            success: { (response) in
                
                self.delegate?.didReceiveProfessionDataSuccessfully!(resultDict: response, resultStatus: true)
                
        }) { (error, response) in
            self.delegate?.didFailWithProfessionDataError!(error, resultStatus: false)
        }
    }
    
}

@objc protocol ConditionUpdateAPIDelegate {
    @objc optional func didReceiveConditionDataSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithConditionDataError(_ error: NSError,resultStatus:Bool)
}

class ConditionUpdateAPI {
    
    var delegate:ConditionUpdateAPIDelegate?
    
    
    func getConditionUpdate() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+"" as NSString),
            params: [:],
            success: { (response) in
                
                self.delegate?.didReceiveConditionDataSuccessfully!(resultDict: response, resultStatus: true)
                
        }) { (error, response) in
            self.delegate?.didFailWithConditionDataError!(error, resultStatus: false)
        }
    }
    
}
