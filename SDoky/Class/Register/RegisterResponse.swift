//
//  RegisterResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/24/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
class RegisterResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var token:String?
   // var registerResponseData:registerResponseData?
    var statusCode: Int?
    var message:String?
    
    func mapping(map: Map) {
        token <- map["access_token"]
      //  registerResponseData <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }
}

