//
//  RegisterDeviceAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/21/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

@objc protocol RegisterDeviceAPIDelegate {
    @objc optional func didPostRegisterdeviceSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostlRegisterdeviceError(_ error: NSError,resultStatus:Bool)
}

class RegisterDeviceAPI {
    
    var delegate:RegisterDeviceAPIDelegate?
    
    func postRegisterdeviceRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+"" as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostRegisterdeviceSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error,response) in
            self.delegate?.didFailWithPostlRegisterdeviceError!(error, resultStatus: false)
        }
    }
    
}

