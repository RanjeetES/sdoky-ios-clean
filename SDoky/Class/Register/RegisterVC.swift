////
////  AssessmentVC.swift
////  SDoky
////
////  Created by Ranjeet Sah on 21/04/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//import Localize_Swift
//
//
//
//class ChooseTableViewCell: UITableViewCell {
//    @IBOutlet weak var chooseLabel: UILabel!
//}
//
//class RegisterVC: UIViewController,UITextFieldDelegate {
//
////    @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
////    var bottomInset: CGFloat {
////        return view.safeAreaInsets.bottom + 50
////    }
////    var provinceId: Int?
////    var mobileNo: String?
////    var otpCode: String?
////    var unique_id: String?
////    var register: Register!
////    //var messagesList = [Assess]()
////    var answerList = [String]()
////    let questionList: [String] = ["Full Name","What is your gender?","Age","Address","Profession","Which country did you travel last 30 days?", "Are you ready to volunteer","Are you feeling well?", "Current condition state"]
////    let genderList: [String] = ["Male","Female","Other"]
////    let yesNoList: [String] = ["Yes","No"]
////    //    let conditionList: [String] = ["Normal","weak"]
////    //    let professionList: [String] = ["Doctor","Nurse","Pilot"]
////    var conditionList = [ConditionUpdateResponseData]()
////    var professionList = [ProfessionUpdateResponseData]()
////    var questionNo = 0
////
////    @IBOutlet weak var tableView: UITableView!
////    @IBOutlet weak var answerTableView: UITableView!
////    @IBOutlet weak var answerTxtField: UITextField!
////    @IBOutlet weak var registerBtn: UIButton!
////    @IBOutlet weak var replyView: UIView!
////    @IBOutlet weak var sendBtn: UIButton!
////
////    @IBOutlet weak var signUp: UILabel!
////    @IBOutlet weak var signUpMessage: UILabel!
////    @IBOutlet weak var signupbutton: UILabel!
////
////
////
////    @IBOutlet weak var tfFullName: UITextField!
////
////    @IBOutlet weak var tfGender: UITextField!
////    @IBOutlet weak var tfAge: UITextField!
////    @IBOutlet weak var tfProvince: UITextField!
////    @IBOutlet weak var tfLocation: UITextField!
////
////
////    var currentLanguage : String?
////    let agePicker = UIPickerView()
////    let genderPicker = UIPickerView()
////    let provincePicker = UIPickerView()
////    let age = Array(10...80)
////    let gender = ["Male","Female","Others"]
////    var province = [ProvinceListResponseData]()
////
////
////    override func viewDidLoad() {
////        super.viewDidLoad()
////
////
////        if Localize.currentLanguage() == "ne" {
////            self.currentLanguage = "nep"
////        } else {
////            self.currentLanguage = "eng"
////        }
////
////        agePicker.dataSource = self
////        agePicker.delegate = self
////
////        genderPicker.dataSource = self
////        genderPicker.delegate = self
////        provincePicker.dataSource = self
////        provincePicker.delegate = self
////
////        genderPicker.tag = 1
////        agePicker.tag = 2
////
////        provincePicker.tag = 3
////
////
////
////        tfAge.inputView = agePicker
////        tfGender.inputView = genderPicker
////        tfProvince.inputView = provincePicker
////
////
////        let provinceDelegate = ProvinceListAPI()
////        provinceDelegate.delegate = self
////        provinceDelegate.getProvinceList(language: currentLanguage ?? "")
////
////
////
////
////
////
////        //
////        //                      let privacyDelegate = AboutUsAPI()
////        //                      privacyDelegate.delegate = self
////        //                      privacyDelegate.getAboutUdData(language: currentLanguage ?? "")
////        //
////        //                      fetchAboutUsData()
////
////
////
////        //profession Update APi
////
////        let professionlApiDelegate = ProfessionUpdateAPI()
////        professionlApiDelegate.delegate = self
////        professionlApiDelegate.getProfessionlUpdate()
////
////
////        //Nepal Update APi
////
////        let conditionApiDelegate = ConditionUpdateAPI()
////        conditionApiDelegate.delegate = self
////        conditionApiDelegate.getConditionUpdate()
////
////        //        tableView.dataSource = self
////        //        answerTableView.dataSource = self
////        //        answerTableView.delegate = self
////        //  setQuestion()
////        //answerTxtField.delegate = self
////        navigationController?.navigationBar.isHidden = true
////
////        setUIText()
////        hideKeyboardWhenTappedAround()
////    }
////
////
////    func hideKeyboardWhenTappedAround() {
////        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
////        tap.cancelsTouchesInView = false
////        view.addGestureRecognizer(tap)
////    }
////
////    @objc func dismissKeyboard() {
////        view.endEditing(true)
////    }
////
////
////    func setUIText(){
////        self.signUp.text = "text_signUp".localized()
////        self.signUpMessage.text = "text_signUpMessage".localized()
////        self.signupbutton.text = "text_signUp".localized()
////        self.tfAge.placeholder = "text_age".localized()
////        self.tfGender.placeholder = "text_gender".localized()
////        self.tfProvince.placeholder = "text_province".localized()
////        self.tfFullName.placeholder = "text_fullName".localized()
////        self.tfLocation.placeholder = "text_location".localized()
////
////    }
////
////
////
////    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
////        self.view.endEditing(true)
////    }
////    //    func setQuestion() {
////    //        if questionNo < questionList.count {
////    //            let id = 0
////    //            let message = questionList[questionNo]
////    //            let answer = Assess.init(id: id, message: message)
////    //            messagesList.append(answer)
////    //            questionNo += 1
////    ////            tableView.reloadData()
////    ////            answerTableView.reloadData()
////    ////            self.tableView.scroll(to: .bottom, animated: true)
////    //        } else {
////    //            tableView.reloadData()
////    //            answerTableView.reloadData()
////    //            self.tableView.scroll(to: .bottom, animated: true)
////    //            self.replyView.isHidden = false
////    //            self.answerTableView.isHidden = true
////    //            self.sendBtn.isHidden = true
////    //            self.answerTxtField.isHidden = true
////    //            self.registerBtn.isHidden = false
////    //        }
////    //
////    //    }
////
////
////    //    @IBAction func sendAnswer(_ sender: UIButton) {
////    //        if answerTxtField.text != "" {
////    //            let id = 1
////    //            let message = answerTxtField.text ?? ""
////    //            answerList.append(message)
////    //            let answer = Assess.init(id: id, message: message)
////    //            answerTxtField.text = ""
////    //            messagesList.append(answer)
////    //            tableView.reloadData()
////    //            self.tableView.scroll(to: .bottom, animated: true)
////    //            setQuestion()
////    //        }
////    //    }
////
////
////    @IBAction func btnRegister(_ sender: Any) {
////
////
////        var idTosend = ""
////        let id = UIDevice.current.identifierForVendor?.uuidString
////
////        if let idValue = id {
////            idTosend = idValue
////        }
////        let str = idTosend.prefix(23)
////        let dictionary = ["mobile_no": mobileNo ?? "",
////                          "otp_code": otpCode ?? "",
////                          "unique_id": str ,
////                          "full_name": tfFullName.text!,
////                          "gender": tfGender.text!,
////                          "age": tfAge.text!,
////                          "address": tfLocation.text!,
////                          "province_id": self.provinceId,
////                          // "address": tf.text!,
////            "country_travel_last_30_days": "",
////            "ready_to_volunteer": "",
////            "feeling_well": "",
////            "current_condition_state": ""
////            ] as [String : Any]
////
////        let regiserAPI = RegisterAPI()
////        regiserAPI.delegate = self
////        //regiserAPI.postRegisterRequest(params: dictionary)
////
////    }
////
////
////    func fetchProfessionData() {
////        //self.professionList = Array(realm.objects(ProfessionUpdateResponseData.self))
////    }
////
////    func fetchConditionData() {
////        //self.conditionList = Array(realm.objects(ConditionUpdateResponseData.self))
////    }
//
//
//}
//
//extension RegisterVC: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == answerTableView {
//            switch questionNo {
//            case 2:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return genderList.count
//            case 5:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return professionList.count
//            case 6:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return yesNoList.count
//            case 7:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return yesNoList.count
//            case 8:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return yesNoList.count
//            case 9:
//                self.view.endEditing(true)
//                self.replyView.isHidden = true
//                self.answerTableView.isHidden = false
//                return conditionList.count
//            default:
//                self.view.endEditing(true)
//                self.replyView.isHidden = false
//                self.answerTableView.isHidden = true
//                return conditionList.count
//            }
//        }
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == answerTableView {
//            let cell = tableView.dequeueReusableCell(withIdentifier:  "ChooseTableViewCell", for: indexPath) as! ChooseTableViewCell
//            switch questionNo {
//            case 2:
//                cell.chooseLabel.text = genderList[indexPath.row]
//            case 5:
//                cell.chooseLabel.text = professionList[indexPath.row].name
//            case 6:
//                cell.chooseLabel.text = yesNoList[indexPath.row]
//            case 7:
//                cell.chooseLabel.text = yesNoList[indexPath.row]
//            case 8:
//                cell.chooseLabel.text = yesNoList[indexPath.row]
//            case 9:
//                cell.chooseLabel.text = conditionList[indexPath.row].name
//            default:
//                cell.chooseLabel.text = yesNoList[indexPath.row]
//            }
//            return cell
//        }
//
//        return UITableViewCell()
//
//    }
//
//
//}
//
////extension RegisterVC: UITableViewDelegate {
////    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        let cell = self.answerTableView.cellForRow(at: indexPath) as! ChooseTableViewCell
////        let message = cell.chooseLabel.text
////        answerList.append(message ?? "")
////        let id = 1
////        let answer = Assess.init(id: id, message: message ?? "")
////        messagesList.append(answer)
////        tableView.reloadData()
////        answerTableView.reloadData()
////        self.tableView.scroll(to: .bottom, animated: true)
////        setQuestion()
////
////    }
////}
//
//
//
//extension RegisterVC: ProfessionUpdateAPIDelegate {
//    func didReceiveProfessionDataSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
//        if (resultDict["message"] as! String == "Success") {
//            if let json = resultDict as? [String: Any] {
//                if let professionResponse:ProfessionUpdateResponse = Mapper<ProfessionUpdateResponse>().map(JSON: json) {
//                    let professionData = professionResponse.professionUpdateResponseData
////                    try! realm.write {
////                        realm.delete(realm.objects(ProfessionUpdateResponseData.self))
////                        realm.add(professionData ?? [ProfessionUpdateResponseData]())
////                        print("Succes adding profession data")
////                        fetchProfessionData()
////                    }
//                }
//            }
//        }
//    }
//
//    func didFailWithProfessionDataError(_ error: NSError, resultStatus: Bool) {
//        print(error.description)
//    }
//
//}
//
//extension RegisterVC: ConditionUpdateAPIDelegate {
//    func didReceiveConditionDataSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
//        if (resultDict["message"] as! String == "Success") {
//            if let json = resultDict as? [String: Any] {
//                if let condtionResponse:ConditionUpdateResponse = Mapper<ConditionUpdateResponse>().map(JSON: json) {
//                    let conditionData = condtionResponse.conditionUpdateResponseData
//
////                    try! realm.write {
////                        realm.delete(realm.objects(ConditionUpdateResponseData.self))
////                        realm.add(conditionData ?? [ConditionUpdateResponseData]())
////                        print("Succes adding condition data")
////                        fetchConditionData()
////                    }
//                }
//            }
//        }
//    }
//
//    func didFailWithConditionDataError(_ error: NSError, resultStatus: Bool) {
//        print(error.description)
//    }
//}
//
//extension RegisterVC: RegisterAPIDelegate {
//    func didPostRegisterSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
//        if let json = resultDict as? [String: Any] {
//            if let token = json["access_token"] as? String {
//                let deviceToken = DeviceToken()
//                deviceToken.accessToken = token
////                try! realm.write {
////                    realm.delete(realm.objects(DeviceToken.self))
////                    realm.add(deviceToken)
////                    print(deviceToken)
////                    print("Succes adding token data")
////                }
//            }
//            if let userResponse:UserDetailResponse = Mapper<UserDetailResponse>().map(JSON: json) {
//                let userData = userResponse.userDetailData
////                try! realm.write {
////                    realm.delete(realm.objects(UserDetailData.self))
////                    realm.add(userData ?? UserDetailData())
////                    print("Succes adding user data")
////
////                }
//
//                let uniqueId = AppUtility.sharedInstance.getPlayerId()
//                let params = ["identifier": uniqueId,
//                              "device_type":  "IOS" ] as [String : String]
//                print(params)
//                let deviceRegisterApiDelegate = RegisterDeviceAPI()
//                deviceRegisterApiDelegate.delegate = self
//                deviceRegisterApiDelegate.postRegisterdeviceRequest(params: params)
//            }
//        }
//    }
//
//    func didFailWithPostRegisterError(_ error: NSError, resultStatus: Bool) {
//        print(error.localizedDescription)
//        Loader.sharedInstance.removeLoader()
//    }
//}
//
//
//extension RegisterVC: RegisterDeviceAPIDelegate {
//    func didPostRegisterdeviceSuccessfully(resultDict:AnyObject,resultStatus:Bool) {
//        let vc = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//    func didFailWithPostlRegisterdeviceError(_ error: NSError,resultStatus:Bool) {
//        print(error.description)
//    }
//}
//
//extension RegisterVC:UIPickerViewDataSource, UIPickerViewDelegate {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//        if pickerView == agePicker {
//            return age.count
//
//        } else if pickerView == genderPicker{
//            return gender.count
//        }else if pickerView == provincePicker{
//            return province.count
//        }
//
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//
//        if pickerView == agePicker {
//            return String(age[row])
//
//        } else if pickerView == genderPicker{
//            return gender[row]
//        }else if pickerView == provincePicker{
//            return "\(province[row].name)"
//        }
//        return ""
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if pickerView == agePicker {
//            tfAge.text = String(age[row])
//            self.view.endEditing(false)
//        } else if pickerView == genderPicker{
//            tfGender.text = gender[row]
//            self.view.endEditing(false)
//        }else if pickerView == provincePicker{
//            tfProvince.text = "\(province[row].name)"
//            self.provinceId = province[row].id
//            self.view.endEditing(false)
//
//        }
//    }
//}
//
//extension RegisterVC: ProvinceListAPIAPIDelegate {
//
//    func didReceiveProvinceListSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
//
//        if (resultDict["message"] as! String == "Success"){
//            if let json = resultDict as? [String: Any] {
//                if let provinceResponse:ProvinceListResponse = Mapper<ProvinceListResponse>().map(JSON: json) {
//                    self.province  = provinceResponse.provinceListResponseData ?? [ProvinceListResponseData]()
//
//
//                }
//            }
//        }
//    }
//    func didFailWithProvinceListError(_ error: NSError, resultStatus: Bool) {
//        print(error.localizedDescription)
//
//    }
//
//}
//
