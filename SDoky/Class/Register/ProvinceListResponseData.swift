////
////  ProvinceListResponseData.swift
////  SDoky
////
////  Created by Ranjeet Sah on 5/8/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
////import ObjectMapper_Realm
////import RealmSwift
//
//class ProvinceListResponseData: Object,Mappable {
//    
//    
//     @objc dynamic var id = 0
//    @objc dynamic var name = ""
//   
//   required convenience init?(map: Map) {
//      self.init()
//    }
//    
//    func mapping(map: Map) {
//        
//        id    <- map["id"]
//        name         <- map["name"]
//      
//    }
//    
//}
