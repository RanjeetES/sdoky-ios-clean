////
////  Assess.swift
////  SDoky
////
////  Created by Ranjeet Sah on 21/04/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import Foundation
//import UIKit
//import ObjectMapper
////import ObjectMapper_Realm
////import RealmSwift
//
//struct Register {
//    let mobileNo : String
//    let otpCode: String
//    let id: String
//    let full_name: String
//    let gender: String
//    let age: String
//    let profession: String
//    let address: String
//    let countryTraveled: String
//    let readyToVolunteer: String
//    let feelingWell: String
//    let currentCondition: String
//    
//}
//
//class ProfessionUpdateResponse: Mappable {
//    
//    var professionUpdateResponseData: [ProfessionUpdateResponseData]?
//    var error: String?
//    var message: String?
//    var statusCode: Int?
//    
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        
//        error    <- map["error"]
//        message         <- map["message"]
//        professionUpdateResponseData <- map ["results"]
//        statusCode      <- map["status_code"]
//        
//        
//    }
//    
//}
//
//class ConditionUpdateResponse: Mappable {
//    
//    var conditionUpdateResponseData: [ConditionUpdateResponseData]?
//    var error: String?
//    var message: String?
//    var statusCode: Int?
//    
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        
//        error    <- map["error"]
//        message         <- map["message"]
//        conditionUpdateResponseData <- map ["results"]
//        statusCode      <- map["status_code"]
//        
//        
//    }
//    
//}
//
//
//class ProfessionUpdateResponseData: Object, Mappable {
//    
//    @objc dynamic var id = 0
//    @objc dynamic var name = ""
//
//      
//    required convenience init?(map: Map) {
//         self.init()
//    }
//
//    func mapping(map: Map) {
//               id    <- map["id"]
//               name  <- map["name"]
//    }
//    
//}
//
//class ConditionUpdateResponseData: Object, Mappable {
//    
//    @objc dynamic var id = 0
//    @objc dynamic var name = ""
//
//   
//    required convenience init?(map: Map) {
//      self.init()
//    }
//
//    func mapping(map: Map) {
//            id    <- map["id"]
//            name  <- map["name"]
//
//    }
//    
//}
