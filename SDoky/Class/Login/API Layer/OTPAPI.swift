//
//  OTPAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/21/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//
//
//import UIKit
//
//class OTPAPI: UIViewController {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//    }
//
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
import UIKit
import ObjectMapper

@objc protocol OTPAPIDelegate {
    @objc optional func didPostOTPSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostOTPError(_ error: String,resultStatus:Bool)
    @objc optional func didPostCheckOTPSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostCheckOTPError(_ error: String,resultStatus:Bool)
    @objc optional func didPostChangePasswordSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostChangePasswordError(_ error: String,resultStatus:Bool)
}

class OTPAPI {
    
    var delegate:OTPAPIDelegate?
    
    func postOTPRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+forgotPassword as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                if (response["error"] as? Bool == true) {
                    let error = response["message"] as? String
                    self.delegate?.didFailWithPostOTPError!(error ?? "", resultStatus: false)
                } else {
                    self.delegate?.didPostOTPSuccessfully!(resultDict: response, resultStatus: true)
                }
                
        }) { (error, response) in
            self.delegate?.didFailWithPostOTPError!(error.description, resultStatus: false)
        }
    }
    
    func postOTPCheckRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+otpCheckAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                if (response["error"] as? Bool == true) {
                    let error = response["message"] as? String
                    self.delegate?.didFailWithPostCheckOTPError!(error ?? "", resultStatus: false)
                } else {
                    self.delegate?.didPostCheckOTPSuccessfully!(resultDict: response, resultStatus: true)
                }
                
        }) { (error, response) in
            self.delegate?.didFailWithPostCheckOTPError!(error.description, resultStatus: false)
        }
    }
    
    func postChangePasswordRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+changePasswordAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                if (response["error"] as? Bool == true) {
                    let error = response["message"] as? String
                    self.delegate?.didFailWithPostChangePasswordError!(error ?? "", resultStatus: false)
                } else {
                    self.delegate?.didPostChangePasswordSuccessfully!(resultDict: response, resultStatus: true)
                }
                
        }) { (error, response) in
            self.delegate?.didFailWithPostChangePasswordError!(error.description, resultStatus: false)
        }
    }
    
}
