//
//  LogInAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol LogInAPIDelegate {
    @objc optional func didPostLoginSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostloginError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didReceiveEmailVerificationSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithReceiveEmailVerificationError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didPostSubsriptionSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithSubscriptionError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didPostDeviceRegistrationSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithDeviceRegistrationError(_ error: NSError,resultStatus:Bool)
    
    
    
    
}

class LogInAPI {
    
    var delegate:LogInAPIDelegate?
    
    func postLoginRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+loginAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostLoginSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithPostloginError!(error, resultStatus: false)
        }
    }
    
    func sendEmailVerification() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+sendEmailVerificationAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didReceiveEmailVerificationSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithReceiveEmailVerificationError!(error, resultStatus: false)
        }
    }
    
    func postSubscriptionRequest(params: [String: Any], subCode:String?) {
        
        var url = ""
        if subCode != nil  {
            url = BASE_URL+subscriptionAPI+"?sub_code_id=\(subCode ?? "")"
        }else {
            url = BASE_URL+subscriptionAPI
        }
        APIHandler.sharedInstance.performPOSTRequest(
            (url as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostSubsriptionSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithSubscriptionError!(error, resultStatus: false)
        }
    }
    
    func postDeviceRegistrationRequest(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+registerDeviceAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostDeviceRegistrationSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithDeviceRegistrationError!(error, resultStatus: false)
        }
    }
    
}

