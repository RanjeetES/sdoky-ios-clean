//
//  LogInAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol RegisterAPIDelegate {
    @objc optional func didPostSignupStep1Successfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostSignupStep1Error(_ error: NSError,resultStatus:Bool)
    @objc optional func didPostSignupStep2Successfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPostSignupStep2Error(_ error: NSError,resultStatus:Bool)
    @objc optional func didGetCountryCodesSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithCountryCodesError(_ error: NSError,resultStatus:Bool)
}

class RegisterAPI {
    
    var delegate:RegisterAPIDelegate?
    
    func postSignupStep1Request(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+signupStep1 as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostSignupStep1Successfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithPostSignupStep1Error!(error, resultStatus: false)
        }
    }
    
    func postSignupStep2Request(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+signupStep2 as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didPostSignupStep2Successfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithPostSignupStep2Error!(error, resultStatus: false)
        }
    }
    
    func getCountryCodes() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+countryCodesAPI as NSString),
            params: [:],
            success: { (response) in
                
                self.delegate?.didGetCountryCodesSuccessfully!(resultDict: response, resultStatus: true)
                
        }) { (error, response) in
            self.delegate?.didFailWithCountryCodesError!(error, resultStatus: false)
        }
    }
    
}

