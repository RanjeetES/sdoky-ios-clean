//
//  OTPResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/21/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

import UIKit
import ObjectMapper

class OTPResponseData: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    class OTPResponseData: Mappable {
        
        var userOtpId:Int?
        var otp:Int?
        var otpFor:String?
        var userId:Int?

        
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            
            userOtpId <- map["user_otp_id"]
            otp <- map["otp"]
            otpFor <- map["otp_for"]
            userId <- map["user_id"]
            
        }
    }

    

    
}

