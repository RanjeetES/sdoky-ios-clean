//
//  LoginResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
class CountryCodeResponseData: Mappable {
    
    var countryCodeId:Int?
    var countryCode:String?
    var country:String?
    var alphaCode:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        countryCodeId <- map["country_code_id"]
        countryCode <- map["country_code"]
        country <- map["country"]
        alphaCode <- map["alpha_code"]
    }
}

