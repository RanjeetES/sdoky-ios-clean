//
//  LoginResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
class RegistrationCompleteResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var user: User?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        user <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }
}

