//
//  LoginResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper

class Subscription: Mappable {
    
    var subscriptionId:Int?
    var subscriptionName:String?
    var userId:Int?
    var validityPeriod:String?
    var price: String?
    var createdBy: String?
    var isActive: Bool?
    var expiresAt: String?
    var createdAt: String?
    var updatedAt: String?
    
    init(subscriptionId : Int, subscriptionName: String, userId: Int, validityPeriod: String, price : String, createdBy: String, isActive: Bool, expiresAt: String, createdAt: String, updatedAt: String) {
        self.subscriptionId = subscriptionId
        self.subscriptionName = subscriptionName
        self.userId = userId
        self.validityPeriod = validityPeriod
        self.price = price
        self.createdBy = createdBy
        self.isActive = isActive
        self.expiresAt = expiresAt
        self.createdAt = createdAt
        self.createdAt = createdAt
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        subscriptionId <- map["subscription_id"]
        subscriptionName <- map["subscription_name"]
        userId <- map["user_id"]
        validityPeriod <- map["validity_period"]
        price <- map["price"]
        createdBy <- map["created_by"]
        isActive <- map["is_active"]
        expiresAt <- map["expires_at"]
        createdAt <- map["created_at"]
        updatedAt <- map["updatedAt"]
        
       
        
    }
}





