//
//  LoginResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginResponseData: Mappable {
    
    
    var accessToken: String?
    var refreshToken: String?
    var tokenType: String?
    var expiresAt: String?
    var user: User?
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        accessToken <- map["access_token"]
        refreshToken <- map["refresh_token"]
        expiresAt <- map["expires_at"]
        tokenType <- map["token_type"]
        user <- map["user"]
        
    }
}





