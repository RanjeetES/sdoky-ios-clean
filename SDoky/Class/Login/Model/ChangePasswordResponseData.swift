//
//  LoginResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
class ChangePasswordResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    var token:String?
    
    func mapping(map: Map) {
        token <- map["token"]
    }
}

