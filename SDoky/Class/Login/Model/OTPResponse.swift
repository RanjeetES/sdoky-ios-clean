//
//  OTPResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/21/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//

import UIKit
import ObjectMapper

   class OTPResponse: Mappable {
        required init?(map: Map) {
    
        }
    
        var status:Bool?
        var otpResponseData:OTPResponseData?
        var statusCode: Int?
        var message:String?
        
        
        func mapping(map: Map) {
            status <- map["status"]
            otpResponseData <- map["data"]
            statusCode <- map["status_code"]
            message <- map["message"]
           
        }
    }

