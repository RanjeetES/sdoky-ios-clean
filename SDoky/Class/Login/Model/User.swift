//
//  LoginResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper

class User: Mappable {
    
    var userId:Int?
    var firstName:String?
    var lastName:String?
    var phone:String?
    var password: String?
    var dateOfBirth: String?
    var city: String?
    var country: String?
    var email: String?
    var emailStatus: Bool?
    var phoneStatus: Bool?
    var countryCodeId: Int?
    var dialCode: String?
    var active: Bool?
    var subscription : Subscription?
    
    init(userId : Int, firstName: String, lastName: String, phone: String, dateOfBirth : String, city: String, country: String, email: String, emailStatus: Bool, phoneStatus: Bool, countryCodeId: Int, dialCode: String, active: Bool, subscription: Subscription) {
        self.userId = userId
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.password = ""
        self.dateOfBirth = dateOfBirth
        self.city = city
        self.country = country
        self.email = email
        self.emailStatus = emailStatus
        self.phoneStatus = phoneStatus
        self.countryCodeId = countryCodeId
        self.dialCode = dialCode
        self.active = active
        self.subscription = subscription
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userId <- map["user_id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        phone <- map["phone"]
        password <- map["password"]
        dateOfBirth <- map["date_of_birth"]
        city <- map["city"]
        country <- map["country"]
        email <- map["email"]
        emailStatus <- map["email_status"]
        phoneStatus <- map["phone_status"]
        countryCodeId <- map["country_code_id"]
        dialCode <- map["dial_code"]
        active <- map["active"]
        subscription <- map["subscription"]
       
    }
}





