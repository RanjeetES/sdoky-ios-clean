//
//  LoginResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
class LoginResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var loginResponseData:[LoginResponseData]?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        loginResponseData <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }
}

