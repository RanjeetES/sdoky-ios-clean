//
//  SubscriptionCheckVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 06/01/2021.
//  Copyright © 2021 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
//import FolioReaderKit

//enum Epub: Int {
//    case bookOne = 0
//    case bookTwo = 1
//
//    var name: String {
//        switch self {
//        case .bookOne:      return "A Little Life by Hanya Yanagihara" // standard eBook
//        case .bookTwo:      return "The Adventures Of Sherlock Holmes - Adventure I" // audio-eBook
//        }
//    }
//
//    var shouldHideNavigationOnTap: Bool {
//        switch self {
//        case .bookOne:      return false
//        case .bookTwo:      return true
//        }
//    }
//
//    var scrollDirection: FolioReaderScrollDirection {
//        switch self {
//        case .bookOne:      return .vertical
//        case .bookTwo:      return .vertical
//        }
//    }
//
//    var bookPath: String? {
//        return Bundle.main.path(forResource: self.name, ofType: "epub")
//    }
//
//    var readerIdentifier: String {
//        switch self {
//        case .bookOne:      return "READER_ONE"
//        case .bookTwo:      return "READER_TWO"
//        }
//    }
//}


//class StoryboardFolioReaderContrainer: FolioReaderContainer {
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//        let config = FolioReaderConfig()
//        config.scrollDirection = .horizontalWithVerticalContent
//
//        guard let bookPath = Bundle.main.path(forResource: "A Little Life by Hanya Yanagihara", ofType: "epub") else { return }
//        setupConfig(config, epubPath: bookPath)
//    }
//}


class SubscriptionCheckVC: UIViewController, SettingsAPIDelegate {
    
    @IBOutlet weak var btnViewEBook : UIButton!
    @IBOutlet weak var btnSubscribe : UIButton!
    //let folioReader = FolioReader()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnViewEBook.layer.cornerRadius = 5.0
        btnSubscribe.layer.cornerRadius = 5.0

    }
    
    @IBAction func btnViewEBookAction(_ sender: Any) {
        
      //  let config = FolioReaderConfig()
     //   let bookPath = Bundle.main.path(forResource: "A Little Life by Hanya Yanagihara", ofType: "epub")
        
        //let folioReader = FolioReader()
        
      //  folioReader.presentReader(parentViewController: self, withEpubPath: bookPath!, andConfig: config)
        
//        let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//        self.show(epubReader, sender: self)
        
        
//        guard let epub = Epub(rawValue: 0) else {
//              return
//          }
//
//          self.open(epub: epub)
        
        if AppUtility.sharedInstance.isConnectedToInternet() == true {
            
            let settingsAPI = SettingsAPI()
                    settingsAPI.delegate = self
                    settingsAPI.getEbook()
                    DispatchQueue.main.async {
                        self.showSpinner(onView: self.view)
                   }
            
        }else {
            
            let resourceDocPath = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "oppo.epub"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            let bookPath = actualPath.absoluteString.replacingOccurrences(of: "file://", with: "")
            
            if FileManager.default.fileExists(atPath: bookPath) {
                
                let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
                self.show(epubReader, sender: self)
                
            } else {
                self.showAlertWithMessageWithTitle(title: "Alert", message: "Please connect to internet to download the book")
            }
            
        }
        
       
        
//        let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//        self.show(epubReader, sender: self)
        
        
        
    }
    
    
    func didGetEbookSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
            print(resultDict)
            
            if let json = resultDict as? [String: Any] {
                if let response:EbookResponse = Mapper<EbookResponse>().map(JSON: json) {
                    if response.status == true {
                        DispatchQueue.main.async {
                            self.removeSpinner()
                        }

                        let url = response.ebookResponseData?.link ?? ""
                        self.savePdf(urlString: url, fileName: "1610087270-en")
                        
    //                    let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
    //                                                   self.show(epubReader, sender: self)
                                            //self.showSavedPdf(url: urlString, fileName: fileName)

                        
                    }
                }
            }
        }
        
        func didFailWithEbookError(_ error: NSError, resultStatus: Bool) {
            print(error)
            DispatchQueue.main.async {
               self.removeSpinner()
            }
        }
    
//    fileprivate func open(epub: Epub) {
//        guard let bookPath = epub.bookPath else {
//            return
//        }
//
//        let readerConfiguration = self.readerConfiguration(forEpub: epub)
//        folioReader.presentReader(parentViewController: self, withEpubPath: bookPath, andConfig: readerConfiguration, shouldRemoveEpub: false)
//    }
//
 //   private func readerConfiguration(forEpub epub: Epub) -> FolioReaderConfig
//        {
//
//         let config = FolioReaderConfig(withIdentifier: epub.readerIdentifier)
//
//         config.shouldHideNavigationOnTap = epub.shouldHideNavigationOnTap
//
//         config.scrollDirection = epub.scrollDirection
//
//
//
//         // Custom sharing quote background
//         config.quoteCustomBackgrounds = []
//
//         if let image = UIImage(named: "pdf")
//         {
//             let customImageQuote = QuoteImage(withImage: image, alpha: 0.6, backgroundColor: UIColor.black)
//
//             config.quoteCustomBackgrounds.append(customImageQuote)
//
//         }
//
//         let textColor = UIColor(red:0.86, green:0.73, blue:0.70, alpha:1.0)
//
//         let customColor = UIColor(red:0.30, green:0.26, blue:0.20, alpha:1.0)
//
//         let customQuote = QuoteImage(withColor: customColor, alpha: 1.0, textColor: textColor)
//
//         config.quoteCustomBackgrounds.append(customQuote)
//
//         return config
//     }
    
    @IBAction func btnSubscribeAction(_ sender: Any) {
        
        let pricingVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
        pricingVC.isFromAccount = false
        pricingVC.isFromNotification = false
        self.show(pricingVC, sender: self)
        
    }
    
    func savePdf(urlString:String, fileName:String) {
                DispatchQueue.main.async {
                    let url = URL(string: urlString)
                    let pdfData = try? Data.init(contentsOf: url!)
                    let resourceDocPath = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)).last! as URL
                    let pdfNameFromUrl = "oppo.epub"
                    let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                    do {
                        print(actualPath)
                        try pdfData?.write(to: actualPath, options: .atomic)
                        print("pdf successfully saved!")
                        
    //                    let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
    //                               self.show(epubReader, sender: self)
                        //self.showSavedPdf(url: urlString, fileName: fileName)
                        let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
                                   self.show(epubReader, sender: self)
                        
                        
                        
                    } catch {
                        print("Pdf could not be saved")
                    }
                }
            }

}
