//
//  RegistrationSecondVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class RegistrationSecondVC: UIViewController {
    
    var firstName: String?
    var lastName: String?
    var phone: String?
    var countryCodeId: String?
    
    
    @IBOutlet weak var tfdateOfBirth: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var btnlanguage: UIButton!
    
    let datePicker = UIDatePicker()
    var selectModelList: [SelectModel] = []
       
       var selectedVehicleType = SelectModel() {
           didSet {
               self.view.setNeedsLayout()
           }
       }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnlanguage.setTitle(languages[AppUtility.sharedInstance.getLanguageSelected()], for: .normal)

        // Do any additional setup after loading the view.
        
        tfdateOfBirth.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "calendar", hasError: false, view: self.view, message: "Email cannot be empty")
        tfCountry.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "location", hasError: false, view: self.view, message: "Email cannot be empty")
        tfCity.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "location", hasError: false, view: self.view, message: "Email cannot be empty")
        
        tfdateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth (Optional)", attributes:attributes)
        tfCountry.attributedPlaceholder = NSAttributedString(string: "Country (Optional)", attributes:attributes)
        tfCity.attributedPlaceholder = NSAttributedString(string: "City (Optional)", attributes:attributes)
        showDatePicker()
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    

    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        tfdateOfBirth.inputAccessoryView = toolbar
        // add datepicker to textField
        tfdateOfBirth.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        tfdateOfBirth.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "RegistrationThirdVC") as! RegistrationThirdVC
        vc.firstName = firstName
        vc.lastName = lastName
        vc.phone = phone
        vc.countryCodeId = countryCodeId
        vc.dateOfBirth = tfdateOfBirth.text
        vc.country = tfCountry.text
        vc.city = tfCity.text
        self.show(vc, sender: self)
    }
    
    @IBAction func btnSigninClicked(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.show(vc, sender: self)
    }
    
    @IBAction func onLanguageChangeBtnClick(_ sender: Any) {
        self.selectModelList.removeAll()
        self.selectModelList.append(SelectModel(id: 0, title: "En"))
        self.selectModelList.append(SelectModel(id: 1, title: "Fr"))
        self.selectModelList.append(SelectModel(id: 2, title: "De"))
        self.selectModelList.append(SelectModel(id: 3, title: "Es"))
        self.selectModelList.append(SelectModel(id: 4, title: "Ru"))
        self.selectModelList.append(SelectModel(id: 5, title: "Jp"))
        self.selectModelList.append(SelectModel(id: 6, title: "Cn"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            self.btnlanguage.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: 50.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        showPopup(controller, sourceView: sender as! UIView)
    }
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }

    
    
}
