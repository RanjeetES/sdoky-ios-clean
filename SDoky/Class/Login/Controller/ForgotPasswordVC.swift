//
//  ForgotPasswordVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import ReCaptcha
import WebKit


class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var tfEmailOrPhone: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var vRecaptcha: UIView!
    
    var recaptcha : ReCaptcha?
//    let recaptcha = try? ReCaptcha(
//        apiKey: "6LfyqLMZAAAAAFl8bgc4Ntm15JjXw4XOWkxWKw2a",
//        baseURL: URL(string: "https://www.sdoky.com")!
//    )
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recaptcha = try? ReCaptcha(
            apiKey: "6LfyqLMZAAAAAFl8bgc4Ntm15JjXw4XOWkxWKw2a",
            baseURL: URL(string: "https://www.sdoky.com")!
        )
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
        recaptcha?.configureWebView { [weak self] webview in
            //webview.frame = self?.view.bounds ?? CGRect.zero
            let point = CGPoint(x: 35, y: 80)
            webview.frame = CGRect(origin: point, size: CGSize(width: self!.view.bounds.width - 40, height: self!.view.bounds.height - 160))
            
        }
        
        tfEmailOrPhone.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "phone", hasError: false, view: self.view, message: "")
        
        tfEmailOrPhone.attributedPlaceholder = NSAttributedString(string: "Enter email or phone", attributes:attributes)
    }
    
    
    
    
    
    @IBAction func btnContinueClick(_ sender: Any) {        
        
        self.removeErrorViews()
        //self.viewDidLoad()
        
        recaptcha = try? ReCaptcha(
            apiKey: "6LfyqLMZAAAAAFl8bgc4Ntm15JjXw4XOWkxWKw2a",
            baseURL: URL(string: "https://www.sdoky.com")!
        )
        
        recaptcha?.configureWebView { [weak self] webview in
            //webview.frame = self?.view.bounds ?? CGRect.zero
            
            let point = CGPoint(x: 35, y: 80)
            webview.frame = CGRect(origin: point, size: CGSize(width: self!.view.bounds.width - 40, height: self!.view.bounds.height - 160))
            
        }
        
        tfEmailOrPhone.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "phone", hasError: false, view: self.view, message: "")
        
        tfEmailOrPhone.attributedPlaceholder = NSAttributedString(string: "Enter email or phone", attributes:attributes)
        
        if tfEmailOrPhone.text == "" {
            tfEmailOrPhone.addLeftImage(imageTintColor: .systemBlue, imageName: "phone", hasError: true, view: self.view, message: REQUIRED_FIELD_TEXT)
        }else{
            self.validate()
            
        }
        
    }
    
    func validate() {
        
        
        recaptcha?.validate(on: view) { [weak self] (result: ReCaptchaResult) in
            
            for eachView in (self?.view.subviews)! {
                if eachView is WKWebView {
                    eachView.removeFromSuperview()
                }
            }
            
            print(try? result.dematerialize())
            let params = ["email_or_phone":self!.tfEmailOrPhone.text]
            let otpAPI = OTPAPI()
            otpAPI.delegate = self
            otpAPI.postOTPRequest(params: params)
            self?.showSpinner(onView: self!.view)
            
        }
        
    }
    
    
}

extension ForgotPasswordVC: OTPAPIDelegate {
    func didPostOTPSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)["email_or_phone"] as! String
                tfEmailOrPhone.addLeftImage(imageTintColor: .systemBlue, imageName: "phone", hasError: true, view: self.view, message: message)
                
                                
            }else {
                if let forgotPasswordResponse:ForgotPasswordResponse = Mapper<ForgotPasswordResponse>().map(JSON: json) {
                    self.removeSpinner()
                    let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                    vc.token = forgotPasswordResponse.forgotPasswordResponseData?.token
                    vc.emailOrPhone = tfEmailOrPhone.text
                    vc.message = forgotPasswordResponse.message!
                    vc.isFromMyAccount = false
                    self.show(vc, sender: self)
                    
                }
            }
            
            
        }
        self.removeSpinner()
    }
    
    func didFailWithPostOTPError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
}

extension UITextField {
    
    
    func addLeftImage(imageTintColor: UIColor, imageName: String, hasError: Bool, view: UIView, message: String) {
        
        leftViewMode = UITextField.ViewMode.always
        
        let imageView = UIImageView(frame: CGRect(x: 13, y: 11, width: 18, height: 18))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: imageName)
        
        // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
        imageView.tintColor = imageTintColor
        
        let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 54, height: self.frame.height))
        let imageRightView = UIView(frame: CGRect(x: 42, y: 11, width: 1.0, height: 18.0))
        imageRightView.backgroundColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR)
        
        imageContainerView.addSubview(imageView)
        imageContainerView.addSubview(imageRightView)
        leftView = imageContainerView
        
        if hasError == true {
            self.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
            self.layer.cornerRadius = 5.0
            imageView.tintColor = UIColor.red
            self.layer.borderWidth = 1.0
            
            
        }else {
            
            self.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
            //self.layer.borderColor = UIColor.lightGray.cgColor
            self.layer.cornerRadius = 5.0
            imageView.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
            self.layer.borderWidth = 0.5
            
        }
        
        self.addLabelbelowTextField(view: view, textField: self, message: message, error: hasError)
        
        
    }
    
    func addLabelbelowTextField(view: UIView, textField: UITextField, message: String, error: Bool) {
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 16.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: textField.bottomAnchor, multiplier: 1),
            textField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        if error == true {
            
            if message == "Email is not verified. Resend verification link" {
                
                
//                var myMutableString = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Light", size: 11.0)!,
//                NSAttributedString.Key.foregroundColor : ERROR_COLOR])
//                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: TEXTFIELD_ICON_COLOR, range: NSRange(location:5,length:10))
                
                let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 11), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)]

                let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Light", size: 11), NSAttributedString.Key.foregroundColor : AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)]

                let attributedString1 = NSMutableAttributedString(string:"Email is not verified. ", attributes:attrs1)

                let attributedString2 = NSMutableAttributedString(string:"Resend verification link", attributes:attrs2)

                attributedString1.append(attributedString2)
               // self.lblText.attributedText = attributedString1
                
                label.attributedText = attributedString1
                //label.font = UIFont(name: "Montserrat-Light", size: 11)
                //label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
                label.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(label)
                view.layoutIfNeeded()
                           
                           
            }else {
                
                label.text = message
                label.font = UIFont(name: "Montserrat-Light", size: 11)
                label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
                label.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(label)
                view.layoutIfNeeded()
                
            }
            
            
            
            NSLayoutConstraint.activate(constraints)
            
            for constraint in view.constraints {
                //                if constraint.identifier == "fpConstraint" {
                //                    constraint.constant = 40.0
                //                } else if constraint.identifier == "button" {
                //                    constraint.constant = 64.0
                //                }
                
                if constraint.identifier == "fpConstraint" {
                    constraint.constant = 40.0
                }
                
                if constraint.identifier == self.placeholder {
                    constraint.constant = 40.0
                } else if constraint.identifier == "button" {
                    constraint.constant = 64.0
                }
            }
            
           
            
        }else {
            NSLayoutConstraint.deactivate(constraints)
            //            for constraint in view.constraints {
            //                if constraint.identifier == "fpConstraint" {
            //                    constraint.constant = 16.0
            //                }else if constraint.identifier == "button" {
            //                    constraint.constant = 40.0
            //                }
            //            }
            
            
            
            for constraint in view.constraints {
                
                if constraint.identifier == "fpConstraint" {
                    constraint.constant = 40.0
                }
                
                if constraint.identifier == self.placeholder {
                    constraint.constant = 16.0
                }else if constraint.identifier == "button" {
                    constraint.constant = 40.0
                }
            }
        }
        
        
        
    }
}

extension UILabel {
    func myLabel(message: String) {
        textAlignment = .center
        textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        backgroundColor = .white
        font = UIFont.systemFont(ofSize: 14)
        numberOfLines = 0
        lineBreakMode = .byWordWrapping
        sizeToFit()
    }
    
    func addLeftImage() {
        //(imageTintColor: UIColor, imageName: String, hasError: Bool, view: UIView, message: String) {
        
        let image: UIImage = UIImage(named: "doc")!
        var bgImage: UIImageView?
        bgImage = UIImageView(image: image)
        bgImage!.frame = CGRect(x: 0,y: 12,width: 16 ,height: 16)
        self.addSubview(bgImage!)
        
        let constraints = [
           bgImage!.trailingAnchor.constraint(equalToSystemSpacingAfter: self.leadingAnchor, multiplier: 1)
       ]
       NSLayoutConstraint.activate(constraints)
    }
}
