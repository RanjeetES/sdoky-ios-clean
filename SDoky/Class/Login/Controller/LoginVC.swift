//
//  LoginVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 4/20/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import ObjectMapper
import CryptoSwift
import FirebaseMessaging
import CommonCrypto
import Alamofire

extension String {
    var md5: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.MD5)
    }
    
    var sha1: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA1)
    }
    
    var sha224: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA224)
    }
    
    var sha256: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA256)
    }
    
    var sha384: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA384)
    }
    
    var sha512n: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA512)
    }
}

public struct HMAC {
    
    static func hash(inp: String, algo: HMACAlgo) -> String {
        if let stringData = inp.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            return getBase64Data(input: digest(input: stringData as NSData, algo: algo))
        }
        return ""
    }
    
    private static func digest(input : NSData, algo: HMACAlgo) -> NSData {
        let digestLength = algo.digestLength()
        var hash = [UInt8](repeating: 0, count: digestLength)
        switch algo {
        case .MD5:
            CC_MD5(input.bytes, UInt32(input.length), &hash)
            break
        case .SHA1:
            CC_SHA1(input.bytes, UInt32(input.length), &hash)
            break
        case .SHA224:
            CC_SHA224(input.bytes, UInt32(input.length), &hash)
            break
        case .SHA256:
            CC_SHA256(input.bytes, UInt32(input.length), &hash)
            break
        case .SHA384:
            CC_SHA384(input.bytes, UInt32(input.length), &hash)
            break
        case .SHA512:
            CC_SHA512(input.bytes, UInt32(input.length), &hash)
            break
        }
        return NSData(bytes: hash, length: digestLength)
    }
    
    private static func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
            
            
        }
        
        return input.base64EncodedString(options: [])
    }
    
    private static func getBase64Data(input: NSData) -> String {
        
        input.base64EncodedString(options: .lineLength76Characters)
        return input.base64EncodedString()
    }
}

enum HMACAlgo {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    func digestLength() -> Int {
        var result: CInt = 0
        switch self {
        case .MD5:
            result = CC_MD5_DIGEST_LENGTH
        case .SHA1:
            result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:
            result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:
            result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:
            result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:
            result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}

struct KeychainConfiguration {
    static let serviceName = "SDokyAppService"
    static let accessGroup: String? = nil
}


class LoginVC: UIViewController , LogInAPIDelegate {
    
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btneye: UIButton!
    @IBOutlet weak var btnlanguage: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var myConstraint : NSLayoutConstraint!
    var x: Int?
    
    var isFromNotification = false
    var docId: Int?
    
    var isRemembered = false
    
    var keyboardAdjusted = false
    
    var showPassword: Bool = false
    
    var lastKeyboardOffset: CGFloat = 0.0
    
    var passwordItems: [KeychainPasswordItem] = []
    
    var selectModelList: [SelectModel] = []
    
    var user : User?
    
    var selectedVehicleType = SelectModel() {
        didSet {
            self.contentView.setNeedsLayout()
        }
    }
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupUI()
        tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: false, view: self.contentView, message: "Email or phone is required")
        tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.contentView, message: "Password cannot be empty")
        //        ivLogo.layer.cornerRadius = 6.0
        //        btnSubmit.layer.cornerRadius = 4.0
        //        tfMobileNumber.delegate = self
        //        tfMobileNumber.returnKeyType = .done
        self.navigationController?.navigationBar.isHidden = true
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        //print(x!)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.navigationController?.navigationBar.isHidden = true
        self.btnlanguage.setTitle(languages[AppUtility.sharedInstance.getLanguageSelected()], for: .normal)
        self.removeOtherErrors()
        navigationItem.hidesBackButton = true
        tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: false, view: self.contentView, message: "Email or phone is required")
        tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.contentView, message: "Password cannot be empty")
        self.removeErrorViews()
        
    }
    
    
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        //self.removeErrorViews()
        self.removeOtherErrors()
        tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: false, view: self.contentView, message: "Email or phone is required")
        tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.contentView, message: "Password cannot be empty")
        
        if tfEmail.text == "" && tfPassword.text == "" {
            
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email or phone is required")
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
            
            
            
            //            let alert = UIAlertController(title: "Alert", message: "Please enter valid credentials", preferredStyle: .alert)
            //
            //            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            //
            //            present(alert, animated: true, completion: nil)
            
        }else if tfEmail.text == ""{
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email or phone is required")
            
        }else if tfPassword.text == "" {
            
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
        }else{
            
            let loginAPIDelegate = LogInAPI()
            loginAPIDelegate.delegate = self
            loginAPIDelegate.postLoginRequest(params: ["email_or_phone":tfEmail.text,"password":tfPassword.text])
            self.showSpinner(onView: self.contentView)
            
            
        }
        
    }
    
    func removeOtherErrors() {
        for eachView in contentView.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func btnEyeClicked(_ sender: Any) {
        
        if (showPassword == false) {
            self.tfPassword.isSecureTextEntry = false
            self.btneye.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showPassword = true
            
        }else {
            
            self.tfPassword.isSecureTextEntry = true
            self.btneye.setImage(UIImage(named: "eye"), for: .normal)
            self.showPassword = false
        }
        
    }
    
    @IBAction func btnRememberMeClick(_ sender: Any) {
        
        if isRemembered == true {
            
            // This has to be done at API level
            self.isRemembered = false
            //AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: false)
            
            //self.btnRememberMe.setImage(UIImage(named: ""), for: .normal)
            self.btnRememberMe.setImage(nil, for: .normal)
            self.btnRememberMe.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
            
        }else {
            // This has to be done at API level
            self.isRemembered = true
            //AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: true)
            
            self.btnRememberMe.setImage(UIImage(named: "remember-me-click"), for: .normal)
            self.btnRememberMe.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        }
    }
    
    @IBAction func btnSignupClick(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "RegistrationFirstVC") as! RegistrationFirstVC
        self.show(vc, sender: self)
    }
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.show(vc, sender: self)
    }
    
    func setupUI() {
        
        self.btnRememberMe.layer.cornerRadius = 5.0
        self.btnRememberMe.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.btnRememberMe.layer.borderWidth = 1.0
        self.btnSignin.layer.cornerRadius = 5.0
        
        
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Enter email or phone", attributes:attributes)
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes:attributes)
        
        
        if AppUtility.sharedInstance.getIsRememberMe() == true {
            
            self.isRemembered = true
            
        }else {
            
            self.isRemembered = false
        }
        
    }
    
    
    
    
    func didPostLoginSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        //  if (resultDict["message"] as! String == "success") {
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                
                if let error = message["password"] as? String {
                    tfPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: error)
                }
                
                if let error = message["email_or_phone"] as? String {
                    tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: error)
                }
                
                if let error = message["error"] as? String {
                    tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: error)
                }
                
                //                if let error = message.keys.first {
                //                    if error == "password"{
                //                        let errorMessage = message["password"] as! String
                //                        tfPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: errorMessage)
                //                    }
                //                    if error == "email_or_phone" {
                //                        let errorMessage = message["email_or_phone"] as! String
                //                        tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: errorMessage)
                //                    }
                //
                //                    if error == "error"{
                //                        let errorMessage = message["error"] as! String
                //                        tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: errorMessage)
                //                        //tfPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: errorMessage)
                //                    }
                //                }
                
                
            }else {
                
                if let loginResponse:LoginResponse = Mapper<LoginResponse>().map(JSON: json) {
                    self.removeSpinner()
                    if loginResponse.status == true {
                        
                        if loginResponse.loginResponseData!.first!.user!.emailStatus == false {
                            
                            //AppUtility.sharedInstance.setToken(token: loginResponse.loginResponseData!.first!.accessToken!)
                            //AppUtility.sharedInstance.setToken(token: "Test")
                            
                            tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: "Email is not verified. Resend verification link")
                            
                            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
                            
                            for eachView in contentView.subviews {
                                if eachView is UILabel {
                                    if (eachView as! UILabel).text == "Email is not verified. Resend verification link" {
                                        eachView.isUserInteractionEnabled = true
                                        eachView.addGestureRecognizer(tap)
                                    }
                                    
                                }
                            }
                            
                        } else if loginResponse.loginResponseData!.first!.user!.active == false {
                            
                            tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: "Your account is suspended. Please contact Sdoky.")
                            
                        } else {
                            
                            AppUtility.sharedInstance.setToken(token: loginResponse.loginResponseData!.first!.accessToken!)
                            AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: self.isRemembered)
                            AppUtility.sharedInstance.setCustomerEmail(customerEmail: loginResponse.loginResponseData!.first!.user!.email!)
                            
                            if AppUtility.sharedInstance.getIsLogedInBefore() == false {
                                let loginAPI = LogInAPI()
                                loginAPI.delegate = self
                                loginAPI.postDeviceRegistrationRequest(params: ["device_token":  Messaging.messaging().fcmToken ?? ""])
                                
                            }
                            
                            
                            if AppUtility.sharedInstance.getCustomerId() != loginResponse.loginResponseData!.first!.user!.userId! {
                                
                                do {
                                    let folders: [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                                    if folders.count > 0 {
                                        for folder in folders {
                                            AppUtility.sharedInstance.getContext().delete(folder)
                                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                        }
                                        
                                    }
                                    
                                    let documents: [Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                                    if documents.count > 0 {
                                        for doc in documents {
                                            AppUtility.sharedInstance.getContext().delete(doc)
                                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                        }
                                        
                                    }
                                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: false)
                                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: false)
                                    
                                }catch{
                                    print("Error Fetching")
                                }
                                
                                
                            }else {
                                print("RK - Same uer")
                            }
                            AppUtility.sharedInstance.setCustomerId(customerId: loginResponse.loginResponseData!.first!.user!.userId!)
                            
                            do {
                                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: tfEmail.text!,accessGroup: KeychainConfiguration.accessGroup)
                                
                                try passwordItem.savePassword(AppUtility.sharedInstance.passwordHash(from: tfEmail.text!, password: tfPassword.text!))
                            } catch {
                                fatalError("Error updating keychain - \(error)")
                            }
                            
                            
                            do {
                                
                                let user : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
                                for u in user {
                                    AppUtility.sharedInstance.getContext().delete(u)
                                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                }
                                
                                let subscriptionLocal : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
                                for s in subscriptionLocal {
                                    AppUtility.sharedInstance.getContext().delete(s)
                                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                }
                                
                                let userToSave = UserInfo(context: AppUtility.sharedInstance.getContext())
                                self.user = loginResponse.loginResponseData?.first?.user
                                userToSave.userId = Int16(self.user?.userId ?? -1)
                                userToSave.firstName = self.user!.firstName ?? ""
                                userToSave.lastName = self.user!.lastName ?? ""
                                userToSave.phone = self.user!.phone ?? ""
                                userToSave.dateOfBirth = self.user!.dateOfBirth?.changeDateFormat() ?? ""
                                userToSave.city = self.user!.city ?? ""
                                userToSave.country = self.user!.country ?? ""
                                userToSave.email = self.user!.email ?? ""
                                userToSave.dialCode = self.user!.dialCode ?? ""
                                userToSave.emailStatus = self.user!.emailStatus ?? false
                                userToSave.phoneStatus = self.user!.phoneStatus ?? false
                                userToSave.active = self.user!.active ?? true
                                
                                let subscriptionToSave = SubscriptionLocal(context: AppUtility.sharedInstance.getContext())
                                
                                if self.user?.subscription != nil {
                                    
                                    subscriptionToSave.subscriptionId = Int16(self.user?.subscription?.subscriptionId ?? -1)
                                    subscriptionToSave.subscriptionName = self.user?.subscription?.subscriptionName ?? ""
                                    subscriptionToSave.userId = Int16(self.user?.subscription?.userId ?? -1)
                                    subscriptionToSave.validityPeriod = self.user?.subscription?.validityPeriod ?? ""
                                    subscriptionToSave.price = self.user?.subscription?.price ?? ""
                                    subscriptionToSave.createdBy = self.user?.subscription?.createdBy ?? ""
                                    subscriptionToSave.isActive = self.user?.subscription?.isActive ?? false
                                    subscriptionToSave.expiresAt = self.user?.subscription?.expiresAt ?? ""
                                    subscriptionToSave.createdAt = self.user?.subscription?.createdAt ?? ""
                                    subscriptionToSave.updatedAt = self.user?.subscription?.updatedAt ?? ""
                                    userToSave.subscription = subscriptionToSave
                                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                                    
                                    
                                }else {
                                    
                                    //                                    subscriptionToSave.subscriptionId = Int16(self.user?.userId ?? 0 + 1)
                                    //                                    subscriptionToSave.subscriptionName = "Trial"
                                    //                                    subscriptionToSave.userId = Int16(self.user?.userId ?? -1)
                                    //                                    subscriptionToSave.validityPeriod = "0"
                                    //                                    subscriptionToSave.price = "0"
                                    //                                    subscriptionToSave.createdBy = self.user?.firstName ?? ""
                                    //                                    subscriptionToSave.isActive = true
                                    //                                    subscriptionToSave.expiresAt = Calendar.current.date(byAdding: .weekOfYear, value: 1, to: Date())?.convertToSDokyDate() ?? ""
                                    //                                    subscriptionToSave.createdAt = Date().stringDateSDoky()
                                    //                                    subscriptionToSave.updatedAt = Date().stringDateSDoky()
                                    //
                                    //                                    let params = ["price":"0","validity_period":"0","is_active":true,"created_by":self.user?.firstName ?? "","subscription_name":"trial","expires_at": Calendar.current.date(byAdding: .weekOfYear, value: 1, to: Date())?.convertToSDokyDate() ?? ""] as [String : Any]
                                    //                                    let loginAPI = LogInAPI()
                                    //                                    loginAPI.delegate = self
                                    //                                    loginAPI.postSubscriptionRequest(params: params, subCode: nil)
                                    
                                    let subscriptionCheckVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "SubscriptionCheckVC") as! SubscriptionCheckVC
                                    self.show(subscriptionCheckVC, sender: self)
                                    return
                                    
                                }
                                
                                
                            }catch{
                                print("Error Fetching")
                            }
                            
                            
                            
                            
                            //                            if AppUtility.sharedInstance.getCustomerId() != loginResponse.loginResponseData!.first!.user!.userId! {
                            //
                            //                                do {
                            //                                    let folders: [Folder] = try AppUtility.sharedInstance.getContext().fetch(Folder.fetchRequest())
                            //                                    if folders.count > 0 {
                            //                                        for folder in folders {
                            //                                            AppUtility.sharedInstance.getContext().delete(folder)
                            //                                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                            //                                        }
                            //
                            //                                    }
                            //
                            //                                    let documents: [Document] = try AppUtility.sharedInstance.getContext().fetch(Document.fetchRequest())
                            //                                    if documents.count > 0 {
                            //                                        for doc in documents {
                            //                                            AppUtility.sharedInstance.getContext().delete(doc)
                            //                                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                            //                                        }
                            //
                            //                                    }
                            //                                    AppUtility.sharedInstance.setHasLocalData(hasLocalData: false)
                            //                                    AppUtility.sharedInstance.setHasLocalSMap(hasLocalSMap: false)
                            //
                            //                                }catch{
                            //                                    print("Error Fetching")
                            //                                }
                            //
                            //
                            //                            }else {
                            //                                print("RK - Same uer")
                            //                            }
                            //                            AppUtility.sharedInstance.setCustomerId(customerId: loginResponse.loginResponseData!.first!.user!.userId!)
                            //
                            //                            do {
                            //                                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: tfEmail.text!,accessGroup: KeychainConfiguration.accessGroup)
                            //
                            //                                try passwordItem.savePassword(AppUtility.sharedInstance.passwordHash(from: tfEmail.text!, password: tfPassword.text!))
                            //                            } catch {
                            //                                fatalError("Error updating keychain - \(error)")
                            //                            }
                            //
                            
                            
                            //                        let dashboardVC =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                            //                        dashboardVC.loginResponse = loginResponse
                            //
                            //                        self.show(dashboardVC, sender: self)
                            //
                            
                            do {
                                let userInfoList : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
                                if userInfoList.count > 0 {
                                    if userInfoList.first?.subscription?.expiresAt?.getDaysFromStringDate() ?? 0 < 0 {
                                        let pricingVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                        pricingVC.isFromAccount = false
                                        pricingVC.isFromNotification = false
                                        self.show(pricingVC, sender: self)
                                    }else {
                                        
                                        if isFromNotification {
                                            guard let window = UIApplication.shared.keyWindow else { return }
                                            
                                            
                                            if docId == -1 {
                                                let pricingVC = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                                pricingVC.isFromNotification = true
                                                let navController = UINavigationController(rootViewController: pricingVC)
                                                navController.modalPresentationStyle = .fullScreen
                                                
                                                // you can assign your vc directly or push it in navigation stack as follows:
                                                window.rootViewController = navController
                                                window.makeKeyAndVisible()
                                                
                                                
                                            }else {
                                                
                                                let viewDocVC = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ViewDocVC") as! ViewDocVC
                                                viewDocVC.isFromNotification = true
                                                viewDocVC.docId = docId
                                                
                                                let navController = UINavigationController(rootViewController: viewDocVC)
                                                navController.modalPresentationStyle = .fullScreen
                                                
                                                window.rootViewController = navController
                                                window.makeKeyAndVisible()
                                            }
                                            
                                            
                                            
                                        }else {
                                            
                                            let tabBarController = UITabBarController()
                                            let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                                            let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                                            let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                                            let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                                            let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                                            tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
                                            tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                                            tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                                            
                                            self.show(tabBarController, sender: self)
                                        }
                                        
                                    }
                                }
                            }catch {
                                print("Could not fetch")
                            }
                            
                            //                            if self.user?.subscription?.expiresAt?.getDaysFromStringDate() ?? 0 > 0 {
                            //
                            //                            }else {
                            //
                            //                                let pricingVC =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                            //                                self.show(pricingVC, sender: self)
                            //                            }
                            
                            
                        }
                    }
                    
                    //                let vc = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    //                vc.mobileNo = tfEmail.text
                    //                self.navigationController?.pushViewController(vc, animated: true)
                    //                let  deviceId  = UIDevice.current.identifierForVendor!.uuidString.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                    //                print("device Id",deviceId)
                    
                }
            }
            
        }
        
        //   }
    }
    
    func didFailWithPostloginError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        
        tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "User does not exist")
        //tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "User does not exists")
        
    }
    
    func didPostSubsriptionSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                print("Failure")
            }else {
                print("Success")
            }
            
        }
    }
    
    func didFailWithSubscriptionError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    func didPostDeviceRegistrationSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
            }else {
                print("Device Registration success.")
                AppUtility.sharedInstance.setIsLogedInBefore(isLogedInBefore: true)
            }
        }
    }
    
    func didFailWithDeviceRegistrationError(_ error: NSError, resultStatus: Bool) {
        print("Error")
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.contentView.endEditing(true)
    }
    
    @IBAction func onLanguageChangeBtnClick(_ sender: Any) {
        self.selectModelList.removeAll()
        self.selectModelList.append(SelectModel(id: 0, title: "En"))
        self.selectModelList.append(SelectModel(id: 1, title: "Fr"))
        self.selectModelList.append(SelectModel(id: 2, title: "De"))
        self.selectModelList.append(SelectModel(id: 3, title: "Es"))
        self.selectModelList.append(SelectModel(id: 4, title: "Ru"))
        self.selectModelList.append(SelectModel(id: 5, title: "Jp"))
        self.selectModelList.append(SelectModel(id: 6, title: "Cn"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            self.btnlanguage.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: 50.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        
        showPopup(controller, sourceView: sender as! UIView)
    }
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
}


extension LoginVC: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: Int) -> Bool {
        if textField == tfEmail{
            return true
        }
        else{
            return false
        }
    }
    
    @IBAction func tapFunction(sender: UITapGestureRecognizer) {
        print("tap working")
        let loginAPI = LogInAPI()
        loginAPI.delegate = self
        loginAPI.sendEmailVerification()
        self.showSpinner(onView: self.contentView)
    }
    
    func didReceiveEmailVerificationSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
            }else {
                self.showAlertWithMessageTitleAlert(message: "Verification link sent to your mail")
            }
        }
    }
    
    func didFailWithReceiveEmailVerificationError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
    }
    
    
}

enum CryptoAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    var HMACAlgorithm: CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:      result = kCCHmacAlgMD5
        case .SHA1:     result = kCCHmacAlgSHA1
        case .SHA224:   result = kCCHmacAlgSHA224
        case .SHA256:   result = kCCHmacAlgSHA256
        case .SHA384:   result = kCCHmacAlgSHA384
        case .SHA512:   result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    var digestLength: Int {
        var result: Int32 = 0
        switch self {
        case .MD5:      result = CC_MD5_DIGEST_LENGTH
        case .SHA1:     result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:   result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:   result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:   result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:   result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}
