//
//  NewPasswordVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper

class NewPasswordVC: UIViewController {
    
    @IBOutlet weak var tfPassword: DesignableUITextField!
    @IBOutlet weak var tfRetypePassword: DesignableUITextField!
    @IBOutlet weak var btnEyePassword: UIButton!
    @IBOutlet weak var btnEyeConfirmPassword: UIButton!
    
    var token: String?
    var showPassword = false
    var showConfirmPassword = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.view, message: "Required Field")
        tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.view, message: "Required Field")
        self.removeErrorViews()
        
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
        tfRetypePassword.attributedPlaceholder = NSAttributedString(string: "Retype password", attributes:attributes)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSavePasswordClick(_ sender: Any) {
        
        self.removeErrorViews()
        
        if tfPassword.text == "" && tfRetypePassword.text == "" {
            
            tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password cannot be empty")
            //tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "")
            
        }else if tfPassword.text == ""{
            tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password cannot be empty")
            
        }else if tfRetypePassword.text == "" {
            
            tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password confirmation failed")
        }else if tfPassword.text != tfRetypePassword.text {
            
            tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password confirmation failed")
        }else {
            let params = ["password":tfPassword.text]
            let otpAPI = OTPAPI()
            otpAPI.delegate = self
            otpAPI.postChangePasswordRequest(params: params)
            self.showSpinner(onView: self.view)
        }
        
    }
    
    
    
    @IBAction func btnEyePasswordClicked(_ sender: Any) {
        
        if (showPassword == false) {
            self.tfPassword.isSecureTextEntry = false
            self.btnEyePassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showPassword = true
            
        }else {
            
            self.tfPassword.isSecureTextEntry = true
            self.btnEyePassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showPassword = false
        }
        
    }
    
    @IBAction func btnEyeConfirmPasswordClicked(_ sender: Any) {
        
        if (showConfirmPassword == false) {
            self.tfRetypePassword.isSecureTextEntry = false
            self.btnEyeConfirmPassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showConfirmPassword = true
            
        }else {
            
            self.tfRetypePassword.isSecureTextEntry = true
            self.btnEyeConfirmPassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showConfirmPassword = false
        }
        
    }
    
    
}

extension NewPasswordVC: OTPAPIDelegate {
    func didPostChangePasswordSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                if let error = message.keys.first {
                    if error == "password"{
                        let errorMessage = message["password"] as! String
                        tfRetypePassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.view, message: errorMessage)
                    }
                }
                
            }else {
                
                if let changePasswordResponse:ChangePasswordResponse = Mapper<ChangePasswordResponse>().map(JSON: json) {
                    self.removeSpinner()
                    
                    if changePasswordResponse.status == true {
                        
                        
                        do {
                            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: AppUtility.sharedInstance.getCustomerEmail(),accessGroup: KeychainConfiguration.accessGroup)
                            
                            try passwordItem.savePassword(AppUtility.sharedInstance.passwordHash(from: AppUtility.sharedInstance.getCustomerEmail(), password: tfPassword.text!))
                        } catch {
                            fatalError("Error updating keychain - \(error)")
                        }
                        
                        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "PasswordChangeSuccessVC") as! PasswordChangeSuccessVC
                        vc.isFromRegister = false
                        self.show(vc, sender: self)
                        
                    }
                    
                }
            }
        }
        self.removeSpinner()
    }
    
    func didFailWithPostChangePasswordError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
    
}

