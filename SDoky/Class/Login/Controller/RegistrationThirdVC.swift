//
//  RegistrationThirdVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import ReCaptcha
import WebKit

class RegistrationThirdVC: UIViewController {
    
    var firstName: String?
    var lastName: String?
    var phone: String?
    var countryCodeId: String?
    var dateOfBirth: String? = ""
    var country: String? = ""
    var city: String? = ""
    var showPassword = false
    var showConfirmPassword = false
    var isChecked = false
    
    var selectModelList: [SelectModel] = []
    
    var selectedVehicleType = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPasswordConfirmation: UITextField!
    @IBOutlet weak var btnEyePassword: UIButton!
    @IBOutlet weak var btnEyeConfirmPassword: UIButton!
    @IBOutlet weak var btnCheckTermsConditions: UIButton!
    @IBOutlet weak var btnlanguage: UIButton!
    @IBOutlet weak var lblAgree: UILabel!
    @IBOutlet weak var contentView: UIView!
    var recaptcha : ReCaptcha?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recaptcha = try? ReCaptcha(
            apiKey: "6LfyqLMZAAAAAFl8bgc4Ntm15JjXw4XOWkxWKw2a",
            baseURL: URL(string: "https://www.sdoky.com")!
        )
        // Do any additional setup after loading the view.
        self.btnCheckTermsConditions.layer.cornerRadius = 5.0
        self.btnCheckTermsConditions.layer.borderColor = hexStringToUIColor(hex: "1873DC").cgColor
        self.btnCheckTermsConditions.layer.borderWidth = 1.0
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes:attributes)
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes:attributes)
        tfPasswordConfirmation.attributedPlaceholder = NSAttributedString(string: "Retype password", attributes:attributes)
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        recaptcha?.configureWebView { [weak self] webview in
            //webview.frame = self?.view.bounds ?? CGRect.zero
            let point = CGPoint(x: 35, y: 80)
            webview.frame = CGRect(origin: point, size: CGSize(width: self!.view.bounds.width - 40, height: self!.view.bounds.height - 160))
        }
        
        
        let tapL = UITapGestureRecognizer(target: self, action: #selector(lblTap))
        lblAgree.isUserInteractionEnabled = true
        lblAgree.addGestureRecognizer(tapL)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.removeOtherErrors()
        self.btnlanguage.setTitle(languages[AppUtility.sharedInstance.getLanguageSelected()], for: .normal)

        
        tfEmail.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "email", hasError: false, view: self.contentView, message: "Email cannot be empty")
        tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.contentView, message: "Password cannot be empty")
        tfPasswordConfirmation.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.contentView, message: "Password confirmaton failed")
        
    }
    
    func validate() {
        recaptcha?.validate(on: view) { [weak self] (result: ReCaptchaResult) in
            
            print(try? result.dematerialize() ?? "")
            
            for eachView in (self?.view.subviews)! {
                if eachView is WKWebView {
                    eachView.removeFromSuperview()
                }
            }
            
            self!.isChecked = true
            self!.btnCheckTermsConditions.setImage(UIImage(named: "remember-me-click"), for: .normal)
            
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCompleteClick(_ sender: Any) {
        
        //self.removeErrorViews()
        self.removeOtherErrors()
        tfEmail.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "email", hasError: false, view: self.contentView, message: "Email cannot be empty")
        tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.contentView, message: "Password cannot be empty")
        tfPasswordConfirmation.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: false, view: self.contentView, message: "Password confirmaton failed")
        
        if tfEmail.text == "" && tfPassword.text == "" && tfPasswordConfirmation.text == "" {
            
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email cannot be empty")
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
            //tfPasswordConfirmation.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password confirmation failed")
            
            
            
            
        }else  if tfEmail.text == "" && tfPassword.text == "" {
            
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email cannot be empty")
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
            
        } else  if tfEmail.text == "" && tfPasswordConfirmation.text == "" {
            
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email cannot be empty")
            
            tfPasswordConfirmation.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password confirmation failed")
            
            
        } else  if tfPassword.text == "" && tfPasswordConfirmation.text == "" {
            
            tfPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
            //tfPasswordConfirmation.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password confirmation failed")
            
        }
            
        else if tfEmail.text == ""{
            tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: true, view: self.contentView, message: "Email cannot be empty")
            
        }else if tfPassword.text == "" {
            
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password cannot be empty")
            
        }else if tfPasswordConfirmation.text == ""{
            tfPasswordConfirmation.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Password confirmation failed")
            
        } else if tfPassword.text != tfPasswordConfirmation.text {
            tfPasswordConfirmation.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: "Password confirmation failed")
        }else{
            
            if isChecked == true {
                let params = ["phone": self.phone ?? "",
                              "email": self.tfEmail.text ?? "",
                              "password": self.tfPassword.text ?? "" ,
                              "first_name": self.firstName,
                              "last_name": self.lastName,
                              "dial_code": self.countryCodeId,
                              "city": self.city,
                              "country": self.country,
                              "date_of_birth": self.dateOfBirth ?? ""] as [String : Any]
                
                let regiserAPI = RegisterAPI()
                regiserAPI.delegate = self
                regiserAPI.postSignupStep2Request(params: params)
                self.showSpinner(onView: self.contentView)
            }else {
                //showAlertWithMessage(message: "Please check terms and conditions")
                showAlertWithMessageWithTitle(title: "Alert", message: "Please check terms and conditions")
            }
            
            //            let params = ["phone": phone ?? "",
            //                          "email": tfEmail.text ?? "",
            //                          "password": tfPassword.text ,
            //                          "first_name": firstName,
            //                          "last_name": lastName,
            //                          "country_code_id": countryCodeId,
            //                          "city": city,
            //                          "country": country,
            //                          "date_of_birth": dateOfBirth,
            //                ] as [String : Any]
            //
            //            let regiserAPI = RegisterAPI()
            //            regiserAPI.delegate = self
            //            regiserAPI.postSignupStep2Request(params: params)
            //
            
        }
    }
    
    @IBAction func btnSigninClicked(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.show(vc, sender: self)
    }
    
    @IBAction func btnEyePasswordClicked(_ sender: Any) {
        
        if (showPassword == false) {
            self.tfPassword.isSecureTextEntry = false
            self.btnEyePassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showPassword = true
            
        }else {
            
            self.tfPassword.isSecureTextEntry = true
            self.btnEyePassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showPassword = false
        }
        
    }
    
    @IBAction func btnEyeConfirmPasswordClicked(_ sender: Any) {
        
        if (showConfirmPassword == false) {
            self.tfPasswordConfirmation.isSecureTextEntry = false
            self.btnEyeConfirmPassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showConfirmPassword = true
            
        }else {
            
            self.tfPasswordConfirmation.isSecureTextEntry = true
            self.btnEyeConfirmPassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showConfirmPassword = false
        }
        
    }
    
    @IBAction func btnCheckTermsConditionsClick(_ sender: Any) {
        self.checkTAC()
    }
    
    @objc func lblTap() {
        self.checkTAC()
    }
    
    func checkTAC() {
        
        if isChecked == true {
            
            // This has to be done at API level
            self.recaptcha = try? ReCaptcha(
                apiKey: "6LfyqLMZAAAAAFl8bgc4Ntm15JjXw4XOWkxWKw2a",
                baseURL: URL(string: "https://www.sdoky.com")!
            )
            
            
            recaptcha?.configureWebView { [weak self] webview in
                //webview.frame = self?.view.bounds ?? CGRect.zero
                
                let point = CGPoint(x: 35, y: 80)
                webview.frame = CGRect(origin: point, size: CGSize(width: self!.view.bounds.width - 40, height: self!.view.bounds.height - 160))
                
            }
            
            self.isChecked = false
            
            self.btnCheckTermsConditions.setImage(UIImage(named: ""), for: .normal)
            
            
        }else {
            // This has to be done at API level
            self.validate()
        }
    }
    
    func removeOtherErrors() {
        for eachView in contentView.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
            
            if eachView is UITextField {
                
                eachView.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
                eachView.layer.cornerRadius = 5.0
                //imageView.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
                eachView.layer.borderWidth = 0.5
            }
        }
        
        
        
        
    }
    
    @IBAction func onLanguageChangeBtnClick(_ sender: Any) {
        self.selectModelList.removeAll()
        self.selectModelList.append(SelectModel(id: 0, title: "En"))
        self.selectModelList.append(SelectModel(id: 1, title: "Fr"))
        self.selectModelList.append(SelectModel(id: 2, title: "De"))
        self.selectModelList.append(SelectModel(id: 3, title: "Es"))
        self.selectModelList.append(SelectModel(id: 4, title: "Ru"))
        self.selectModelList.append(SelectModel(id: 5, title: "Jp"))
        self.selectModelList.append(SelectModel(id: 6, title: "Cn"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            self.btnlanguage.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: 50.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        showPopup(controller, sourceView: sender as! UIView)
    }
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
}

extension RegistrationThirdVC: RegisterAPIDelegate {
    func didPostSignupStep2Successfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                                
                if let error = message["password"] as? String {
                    tfPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: error)
                }
                
                if let error = message["email"] as? String {
                    tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: error)
                }
                
                if let error = message["error"] as? String {
                    tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: error)
                }
                //                if let error = message.keys.first {
                //                    if error == "password"{
                //                        let errorMessage = message["password"] as! String
                //                        tfPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: "Password is not valid")
                //                        //tfPasswordConfirmation.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.contentView, message: "Password must be 8 characters")
                //                        if tfEmail.text?.contains("@") == false {
                //                            tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: "Email address is not valid")
                //                        }
                //                    }else if error == "email" {
                //                        let errorMessage = message["email"] as! String
                //                        tfEmail.addLeftImage(imageTintColor: .systemBlue, imageName: "email", hasError: true, view: self.contentView, message: errorMessage)
                //                    }
                //                }
                
            }else{
                
                
                if let loginResponse:RegistrationCompleteResponse = Mapper<RegistrationCompleteResponse>().map(JSON: json) {
                    self.removeSpinner()
                    if loginResponse.status == true {
                        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "PasswordChangeSuccessVC") as! PasswordChangeSuccessVC
                        vc.isFromRegister = true
                        self.show(vc, sender: self)
                    }else {
                        
                        
                        //DispatchQueue.main.async {
                        //let alert = UIAlertController(title: "Error", message: "Something went wrong. Please try again later", preferredStyle: .alert)
                        //alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        //  self.present(alert, animated: false, completion: nil)
                        
                        //}
                    }
                    
                    
                    
                }
            }
        }
        self.removeSpinner()
    }
    
    func didFailWithPostSignupStep2Error(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }

}
