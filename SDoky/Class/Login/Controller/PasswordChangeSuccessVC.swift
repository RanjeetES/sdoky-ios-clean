//
//  PasswordChangeSuccessVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class PasswordChangeSuccessVC: UIViewController {
    
    var isFromRegister = false
    
    @IBOutlet weak var btnContinuetoLogin: UIButton!
    @IBOutlet weak var lblInfoMessage: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        AppUtility.sharedInstance.setToken(token: "")
        btnContinuetoLogin.layer.borderWidth = 1.0
        btnContinuetoLogin.layer.borderColor = hexStringToUIColor(hex: "1873DC").cgColor
        
        if isFromRegister == true {
            lblInfoMessage.text = "We have sent a verification link to your email address, please verify to continue."
        }else {
            lblInfoMessage.text = "Your password has been successfully recovered."
        }
    }
    
    @IBAction func btnContinueClick(_ sender: Any) {
                 
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
             self.show(vc, sender: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
