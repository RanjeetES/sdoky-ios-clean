//
//  CountrySelectorVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/26/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

protocol CountrySelectorDelegate {
    func didSelectCountry(country:CountryModel)
}


struct CountryModelArray: Decodable {
    var data: [CountryModel]
}
struct CountryModel : Decodable {
    var name: String
    var flag: String
    var code: String
    var dial_code: String
    
}

class CountrySelectorTableViewCell : UITableViewCell {
    @IBOutlet weak var lblFlag: UILabel!
    @IBOutlet weak var lblCounyryName: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    
}


class CountrySelectorVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tvCountry: UITableView!
    var countriesArray: [CountryModel] = []
    var delegate: CountrySelectorDelegate?
    var selectedCountry: CountryModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let list = self.loadJson(filename: "country_dial_info") {
            countriesArray = list
        }
        tvCountry.delegate = self
        tvCountry.dataSource = self
        self.tvCountry.reloadData()
        
        
    }
    
    func loadJson(filename fileName: String) -> [CountryModel]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(CountryModelArray.self, from: data)
                return jsonData.data
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "country_cell") as! CountrySelectorTableViewCell
        cell.lblFlag.text = self.countriesArray[indexPath.row].flag
        cell.lblCounyryName.text = self.countriesArray[indexPath.row].name
        cell.lblCountryCode.text = self.countriesArray[indexPath.row].dial_code
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCountry = self.countriesArray[indexPath.row]
        self.delegate?.didSelectCountry(country: self.countriesArray[indexPath.row])
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
}
