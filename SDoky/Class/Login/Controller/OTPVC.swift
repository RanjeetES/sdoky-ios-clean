////
////  OTPVC.swift
////  SDoky
////
////  Created by Ranjeet Sah on 4/21/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//import Localize_Swift
//
//class OTPVC: UIViewController,OTPAPIDelegate,RegisterDeviceAPIDelegate {
//    
//    var loginResponse: LoginResponse?
//    var mobileNo: String?
//    var otpCode: String?
//    @IBOutlet weak var lblTimer: UILabel!
//    @IBOutlet weak var submitLabel: UILabel!
//    
//    
//    
//    @IBOutlet weak var otpTextField: UITextField!
//    @IBOutlet weak var messageLabel: UILabel!
//    @IBOutlet weak var otpLabel: UILabel!
//    @IBOutlet weak var otpMessageLabel: UILabel!
//    
//    var countdownTimer: Timer!
//    var totalTime = 60
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        //startTimer()
//        //        tfMobileNumber.delegate = self
//        //        tfMobileNumber.returnKeyType = .done
//        navigationItem.hidesBackButton = true
//
//
//     //   self.navigationController?.navigationBar.isHidden = false
//        //setUIText()
//        //messageLabel.text = "\(loginResponse?.loginResponseData?.first?.user?.userId)"
//        messageLabel.text = AppUtility.sharedInstance.getCustomerEmail()
//        
//    }
//    
//    func setUIText(){
//        messageLabel.text = "text_covid_Info".localized()
//        otpLabel.text = "text_otp".localized()
//        otpMessageLabel.text = "text_otp_message".localized()
//        self.submitLabel.text = "text_submit".localized()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = true
//    }
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
//    
//    @IBAction func btnLogout(_ sender: Any) {
//        
//        AppUtility.sharedInstance.setToken(token: "")
//        AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: false)
//        //AppUtility.sharedInstance.setCustomerEmail(customerEmail: "")
//        let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        
//        self.show(loginVC, sender: self)
//    }
//    
//    @IBAction func btnSettings(_ sender: Any) {
//        
//        let loginVC = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//        
//        self.show(loginVC, sender: self)
//    }
//    
//    
//    @IBAction func btnSubmiteOTP(_ sender: Any) {
//        let dictonary = ["mobile_no": mobileNo,"otp_code" : otpTextField.text]
//        let postotpRequest = OTPAPI()
//        postotpRequest.delegate = self
//       // postotpRequest.postotpRequest( params: dictonary)
//        
//        
//    }
//    
//    func startTimer() {
//        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
//    }
//    
//    @objc func updateTime() {
//        lblTimer.text = "RESEND OTP in \(timeFormatted(totalTime)) s"
//        
//        if totalTime != 0 {
//            totalTime -= 1
//        } else {
//            endTimer()
//        }
//    }
//    
//    func endTimer() {
//        countdownTimer.invalidate()
//        lblTimer.text = "RESEND OTP"
//    }
//    
//    func timeFormatted(_ totalSeconds: Int) -> String {
//        let seconds: Int = totalSeconds % 60
//        let minutes: Int = (totalSeconds / 60) % 60
//        //return String(format: "%02d:%02d", minutes, seconds)
//        return String(format: "%02d", seconds)
//    }
//    
//    
//    
//    func didPostOTPSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
//        if (resultDict["user_registered"] as! Bool == false) {
//            let vc = REGISTER_STORY_BOARD.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
//            vc.mobileNo = self.mobileNo
//            vc.otpCode = self.otpTextField.text
//            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            if let json = resultDict as? [String: Any] {
//                if let token = json["access_token"] as? String {
//                    let deviceToken = DeviceToken()
//                    deviceToken.accessToken = token
////                    try! realm.write {
////                        realm.add(deviceToken)
////                        print(deviceToken)
////                        print("Succes adding token data")
////                    }
//                }
//                if let userResponse:UserDetailResponse = Mapper<UserDetailResponse>().map(JSON: json) {
//                    let userData = userResponse.userDetailData                    
////                    try! realm.write {
////                        realm.delete(realm.objects(UserDetailData.self))
////                        realm.add(userData ?? UserDetailData())
////                        print("Succes adding user data")
////                        
////                    }
////                    let storyborad = DASHBOARD_STORY_BOARD
////                    let vc = storyborad.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
////                    vc.data = userData?.isRegistered as! Int
//                    //                        self.navigationController?.pushViewController(vc, animated: true)
//                    //self.show(vc, sender: self)
//                }
//                
//                
//            }
//        }
//        
//        
//    }
//    
//    
//    func didFailWithPostOTPError(_ error: String, resultStatus: Bool) {
//        let alert = UIAlertController.alertWithMessage(error)
//        self.present(alert, animated: true, completion: nil)
//    }
//    
//    
//}
