//
//  OTPVerificationVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper

enum Direction { case left, right }

class OTPVerificationVC: UIViewController, UITextFieldDelegate {
    
    var token: String?
    var emailOrPhone: String?
    var message : String?
    var verificationCode = ""
    var isFromMyAccount = false
    
    @IBOutlet var tfVerificationCollection:[UITextField]!
    @IBOutlet weak var myConstraint : NSLayoutConstraint!
    @IBOutlet weak var lblError : UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    
    
    var tfIndexes: [UITextField:Int] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblInfo.text = message ?? "Enter the verification code we sent you to phone number or email"
        
        AppUtility.sharedInstance.setToken(token: token ?? "")
        
        for index in 0 ..< tfVerificationCollection.count {
            tfIndexes[tfVerificationCollection[index]] = index
            tfVerificationCollection[index].delegate = self
        }
        //self.myConstraint.constant = 0.0
        
        for constraint in view.constraints {
            
            if constraint.identifier == "otp" {
                constraint.constant = 24.0
            }
        }
        
        if isFromMyAccount == true {
            self.btnVerify.setTitle("Verify phone", for: .normal)
        }else {
            self.btnVerify.setTitle("Reset password", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.removeError(message: "")
    }
    
    func setNextResponder(_ index:Int?, direction:Direction) {
        
        guard let index = index else { return }
        
        if direction == .left {
            index == 0 ?
                (_ = tfVerificationCollection.first?.resignFirstResponder()) :
                (_ = tfVerificationCollection[(index - 1)].becomeFirstResponder())
        } else {
            index == tfVerificationCollection.count - 1 ?
                (_ = tfVerificationCollection.last?.resignFirstResponder()) :
                (_ = tfVerificationCollection[(index + 1)].becomeFirstResponder())
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.length == 0 {
            textField.text = string
            setNextResponder(tfIndexes[textField], direction: .right)
            return true
        } else if range.length == 1 {
            textField.text = ""
            setNextResponder(tfIndexes[textField], direction: .left)
            return false
        }
        return false
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == self.tfVerificationCollection[self.tfVerificationCollection.count - 1]) {
            self.verificationCode = ""
            for singleField in tfVerificationCollection {
                self.verificationCode += singleField.text!
            }
            print(self.verificationCode)
        }
    }
    
    @IBAction func btnResetPasswordClick(_ sender: Any) {
        
        //self.removeErrorViews()
        
        if isFromMyAccount == false {
            
            self.removeError(message: "")
            if self.verificationCode.count == 4 {
                let params = ["otp":self.verificationCode, "otp_for":"password"]
                let otpAPI = OTPAPI()
                otpAPI.delegate = self
                otpAPI.postOTPCheckRequest(params: params)
                self.showSpinner(onView: self.view)
            }else {
                self.showError(message: "Verification code required")
                
                
                //            tfVerificationCollection[0].addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "")
                //            tfVerificationCollection[1].addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "")
                //            tfVerificationCollection[2].addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "")
                //            tfVerificationCollection[3].addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "Verification code required")
                //self.showAlertWithMessage(message: "Please enter the OTP")
            }
            
        }else {
            
            self.removeError(message: "")
            if self.verificationCode.count == 4 {
                let params = ["otp":self.verificationCode, "otp_for":"verification"]
                let otpAPI = OTPAPI()
                otpAPI.delegate = self
                otpAPI.postOTPCheckRequest(params: params)
                self.showSpinner(onView: self.view)
            }else {
                self.showError(message: "Verification code required")
                
            }
            
        }
        
        
    }
    
    func showError(message: String) {
        
        lblError.isHidden = false
        lblError.text = message
       // myConstraint.constant = 18.0
        for constraint in view.constraints {
                   
                   if constraint.identifier == "otp" {
                       constraint.constant = 40.0
                   }
               }
        for singleField in tfVerificationCollection {
            singleField.layer.borderColor = UIColor.red.cgColor
            singleField.layer.cornerRadius = 5.0
            singleField.layer.borderWidth = 1.0
        }
        
    }
    
    func removeError(message: String) {
        lblError.isHidden = true
       // myConstraint.constant = 0.0
        for constraint in view.constraints {
                   
                   if constraint.identifier == "otp" {
                    constraint.constant = 24.0
                   }
               }
        for singleField in tfVerificationCollection {
            singleField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
            singleField.layer.cornerRadius = 5.0
            singleField.layer.borderWidth = 1.0
        }
        
    }
    
    @IBAction func btnResendOTPCode(_ sender: Any) {
        
        let params = ["email_or_phone":emailOrPhone]
        let otpAPI = OTPAPI()
        otpAPI.delegate = self
        otpAPI.postOTPRequest(params: params)
        self.showSpinner(onView: self.view)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        if isFromMyAccount == true {
            self.dismiss(animated: true, completion: nil)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
}


extension OTPVerificationVC: OTPAPIDelegate, SettingsAPIDelegate {
    func didPostOTPSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            if let forgotPasswordResponse:ForgotPasswordResponse = Mapper<ForgotPasswordResponse>().map(JSON: json) {
                self.removeSpinner()
                self.showAlertWithMessageTitleAlert(message: "Verification code resent")
            }
        }
    }
    
    func didFailWithPostOTPError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
    
    func removeOtherErrors() {
        for eachView in view.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    func didPostCheckOTPSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            if let otpResponse:OTPResponse = Mapper<OTPResponse>().map(JSON: json) {
                self.removeSpinner()
                
                if otpResponse.status ==  true {
                    
                    if isFromMyAccount == false {
                        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "NewPasswordVC") as! NewPasswordVC
                        vc.token = token ?? ""
                        self.show(vc, sender: self)
                    }else {
                        
                        let alert = UIAlertController(title: "Alert", message: "Phone number verified successfully", preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                            action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }else {
                    self.showError(message: "Incorrect verification code")
                }
                
            }
        }
        self.removeSpinner()
    }
    
    func didFailWithPostCheckOTPError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
}
