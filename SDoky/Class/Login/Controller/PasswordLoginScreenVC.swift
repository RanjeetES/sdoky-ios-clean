//
//  PasswordLoginScreenVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/18/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class PasswordLoginScreenVC: UIViewController {
    
    var showPassword = false
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var tfPassword: DesignableUITextField!
    @IBOutlet weak var btneye: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnOK.layer.cornerRadius = 5.0
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.isHidden = true
        
        
        if AppUtility.sharedInstance.getCustomerEmail() != "" {
            self.lblEmail.text = "Enter password \n" + AppUtility.sharedInstance.getCustomerEmail()
        }
        
        self.removeOtherErrors()
        tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
    }
    
    @IBAction func btnOKClick( _ sender: Any) {
        self.removeOtherErrors()
        if tfPassword.text == "" {
            
            tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.view, message: "Required field")
            
        }else {
            
            do {
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: AppUtility.sharedInstance.getCustomerEmail(),
                                                        accessGroup: KeychainConfiguration.accessGroup)
                let keychainPassword = try passwordItem.readPassword()
                
                let saltedPassword = AppUtility.sharedInstance.passwordHash(from: AppUtility.sharedInstance.getCustomerEmail(), password: tfPassword.text!)
                if(saltedPassword == keychainPassword) {
                    
                    
                    
                    
                    let tabBarController = UITabBarController()
                    let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                    let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                    let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                    let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                    let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                     tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
                    tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                    tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                    
                    self.show(tabBarController, sender: self)
                    
                }else {
                    
                    tfPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.view, message: "Incorrect password")
                    
                }
            } catch {
                fatalError("Error reading password from keychain - \(error)")
            }
            
        }
        
        
        
    }
    
    func removeOtherErrors() {
        for eachView in view.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func btnEyeClicked(_ sender: Any) {
        
        if (showPassword == false) {
            self.tfPassword.isSecureTextEntry = false
            self.btneye.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showPassword = true
            
        }else {
            
            self.tfPassword.isSecureTextEntry = true
            self.btneye.setImage(UIImage(named: "eye"), for: .normal)
            self.showPassword = false
        }
        
    }
    
}
