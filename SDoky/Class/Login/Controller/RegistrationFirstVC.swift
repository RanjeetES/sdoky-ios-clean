//
//  RegistrationFirstVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/15/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer

class RegistrationFirstVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, CountrySelectorDelegate {
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var vPhone: UIView!
    @IBOutlet weak var pvCountry: UIPickerView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var ivPhone: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnlanguage: UIButton!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    var countryCodes:[CountryCodeResponseData] = []
    var selectedCountry: String = "+41"
    var selectedCountryCodeId: Int = 2
    var selectModelList: [SelectModel] = []
    
    var selectedVehicleType = SelectModel() {
        didSet {
            self.view.setNeedsLayout()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vPhone.layer.borderWidth = 1.0
        self.vPhone.layer.borderColor = UIColor.lightGray.cgColor
        self.vPhone.layer.cornerRadius = 5.0
        //pvCountry.dataSource = self
        //pvCountry.delegate = self
        
        tfFirstName.attributedPlaceholder = NSAttributedString(string: "Type first name", attributes:attributes)
        tfLastName.attributedPlaceholder = NSAttributedString(string: "Type last name", attributes:attributes)
        tfPhone.attributedPlaceholder = NSAttributedString(string: "Type phone", attributes:attributes)
        
        //addBottomSheetView()
        
        
        
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        //        let registerAPI = RegisterAPI()
        //        registerAPI.delegate = self
        //        registerAPI.getCountryCodes()
    }
    
    @IBAction func showCountryList(_ sender: Any) {
        
        // View controller the bottom sheet will hold
        let viewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "CountrySelectorVC") as! CountrySelectorVC
        viewController.delegate = self
        // Initialize the bottom sheet with the view controller just created
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height - 100.0)
        // Present the bottom sheet
         bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
               bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.btnlanguage.setTitle(languages[AppUtility.sharedInstance.getLanguageSelected()], for: .normal)

        self.removeOtherErrors()
        self.removePhoneError(message: "")
        tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.contentView, message: "Required Field")
        tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.contentView, message: "Required Field")
        
        ivPhone.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        
    }
    
    func didSelectCountry(country: CountryModel) {
        
        self.selectedCountry = country.dial_code
        self.btnCountryCode.setTitle(self.selectedCountry, for: .normal)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.countryCodes.count
    }
    
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        return self.countryCodes[row].countryCode
    //    }
    
    //    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    //
    //        let myAttribute = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8.0) ]
    //        let myString = NSAttributedString(string: self.countryCodes[row].countryCode!, attributes: myAttribute)
    //        return myString
    //    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont.systemFont(ofSize: 14.0)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = self.countryCodes[row].alphaCode!
        pickerLabel?.textColor = UIColor.lightGray
        
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCountry = self.countryCodes[row].countryCode!
        self.selectedCountryCodeId = self.countryCodes[row].countryCodeId!
        self.lblCountryCode.text = self.countryCodes[row].countryCode!
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        
        self.removeOtherErrors()
        self.removePhoneError(message: "")
        tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.contentView, message: "Required Field")
        tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.contentView, message: "Required Field")
        ivPhone.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        if tfFirstName.text == "" && tfLastName.text == "" && tfPhone.text == "" {
            
            tfFirstName.addLeftImage(imageTintColor:hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            //tfPhone.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "Required Field")
            self.phoneError(message: "Required Field")
            
            
            
        }else  if tfFirstName.text == "" && tfLastName.text == "" {
            
            tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            
        } else  if tfFirstName.text == "" && tfPhone.text == "" {
            
            tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            //tfPhone.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "Required Field")
            self.phoneError(message: "Required Field")
            
            
            
        } else  if tfLastName.text == "" && tfPhone.text == "" {
            
            tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            //tfPhone.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "Required Field")
            self.phoneError(message: "Required Field")
            
            
            
            
        }
            
        else if tfFirstName.text == ""{
            tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
            
        }else if tfLastName.text == "" {
            
            tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: true, view: self.contentView, message: "Required Field")
        }else if tfPhone.text == ""{
            // tfPhone.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "", hasError: true, view: self.view, message: "Required Field")
            self.phoneError(message: "Required Field")
        } else{
            
            let params = ["phone": ( tfPhone.text ?? "") , "dial_code": self.selectedCountry] as [String : Any]
            
            let regiserAPI = RegisterAPI()
            regiserAPI.delegate = self
            regiserAPI.postSignupStep1Request(params: params)
            self.showSpinner(onView: self.contentView)
        }
        
    }
    
    @IBAction func btnSigninClicked(_ sender: Any) {
        
        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.show(vc, sender: self)
    }
    
    func phoneError(message: String) {
        
        self.vPhone.layer.borderWidth = 1.0
        self.vPhone.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        self.vPhone.layer.cornerRadius = 5.0
        self.ivPhone.tintColor = .red
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: vPhone.bottomAnchor, multiplier: 0.5),
            vPhone.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            vPhone.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(label)
        contentView.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        //        for constraint in contentView.constraints {
        //            if constraint.identifier == "fpConstraint" {
        //                constraint.constant = 64.0
        //            }
        //        }
    }
    
    func removePhoneError(message: String) {
        
        self.vPhone.layer.borderWidth = 1.0
        self.vPhone.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.vPhone.layer.cornerRadius = 5.0
        self.ivPhone.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        //        for constraint in contentView.constraints {
        //            if constraint.identifier == "fpConstraint" {
        //                constraint.constant = 40.0
        //            }
        //        }
    }
    
    func removeOtherErrors() {
        for eachView in contentView.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    
    
    //    func addBottomSheetView() {
    //        let bottomSheetVC = BottomSheetViewController()
    //
    //        self.addChild(bottomSheetVC)
    //        self.view.addSubview(bottomSheetVC.view)
    //        bottomSheetVC.didMove(toParent: self)
    //
    //        let height = view.frame.height
    //        let width  = view.frame.width
    //        bottomSheetVC.view.frame = CGRect(x: 0, y: view.frame.midY, width: width, height: 200.0)
    //    }
    
    @IBAction func onLanguageChangeBtnClick(_ sender: Any) {
        
        
        self.selectModelList.removeAll()
        self.selectModelList.append(SelectModel(id: 0, title: "En"))
        self.selectModelList.append(SelectModel(id: 1, title: "Fr"))
        self.selectModelList.append(SelectModel(id: 2, title: "De"))
        self.selectModelList.append(SelectModel(id: 3, title: "Es"))
        self.selectModelList.append(SelectModel(id: 4, title: "Ru"))
        self.selectModelList.append(SelectModel(id: 5, title: "Jp"))
        self.selectModelList.append(SelectModel(id: 6, title: "Cn"))
        
        let controller = ArrayChoiceTableViewController(selectModelList) { (model) in
            self.selectedVehicleType.id = model.id
            self.selectedVehicleType.title = model.title
            self.btnlanguage.setTitle(model.title, for: .normal)
            
        }
        controller.preferredContentSize = CGSize(width: 50.0, height: setHeightForPopOverController(count: self.selectModelList.count))
        showPopup(controller, sourceView: sender as! UIView)
        
    }
    
    
    func setHeightForPopOverController(count: Int) -> CGFloat {
        
        let height = CGFloat(count * 40 + 24)
        
        if (height > (view.frame.size.height - 100)){
            return (view.frame.size.height - 100)
        }else{
            return height
        }
    }
    
    
}


extension RegistrationFirstVC: RegisterAPIDelegate {
    func didPostSignupStep1Successfully(resultDict: AnyObject, resultStatus: Bool) {
        //        if let json = resultDict as? [String: Any] {
        //            if let loginResponse:RegisterFirstResponse = Mapper<RegisterFirstResponse>().map(JSON: json) {
        //                self.removeSpinner()
        //                if loginResponse.status == true {
        //                    let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "RegistrationSecondVC") as! RegistrationSecondVC
        //                    vc.firstName = tfFirstName.text
        //                    vc.lastName = tfLastName.text
        //                    vc.phone = tfPhone.text
        //                    vc.countryCodeId = self.selectedCountry
        //                    self.show(vc, sender: self)
        //                }else {
        //                    self.showAlert()
        //                }
        //
        //
        //
        //            }
        //        }
        //        self.removeSpinner()
        
        
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                if let error = message.keys.first {
                    if error == "phone"{
                        let errorMessage = message["phone"] as! String
                        self.phoneError(message: errorMessage)
                    }
                }
                
            }else {
                
                if let loginResponse:RegisterFirstResponse = Mapper<RegisterFirstResponse>().map(JSON: json) {
                    self.removeSpinner()
                    if loginResponse.status == true {
                        let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "RegistrationSecondVC") as! RegistrationSecondVC
                        vc.firstName = tfFirstName.text
                        vc.lastName = tfLastName.text
                        vc.phone = tfPhone.text
                        vc.countryCodeId = self.selectedCountry
                        self.show(vc, sender: self)
                    }
                    
                }
            }
            
        }
    }
    
    func didFailWithPostSignupStep1Error(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
    
    func didGetCountryCodesSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if let json = resultDict as? [String: Any] {
            if let countryCodeResponse:CountryCodeResponse = Mapper<CountryCodeResponse>().map(JSON: json) {
                self.removeSpinner()
                if countryCodeResponse.status == true {
                    self.countryCodes = countryCodeResponse.data!
                    //self.pvCountry.reloadAllComponents()
                }
                
            }
        }
    }
    
    func didFailWithCountryCodesError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        self.removeSpinner()
    }
    
}
