////
////  LocalizationVC.swift
////  SDoky
////
////  Created by Ranjeet Sah on 20/04/20.
////  Copyright © 2020 Ranjeet Sah. All rights reserved.
////
//
//import UIKit
//import Localize_Swift
//
//class LocalizationVC: UIViewController {
//   // var accessToken: DeviceToken?
//    
//    @IBOutlet weak var englishChoosenImgView: UIImageView!
//    @IBOutlet weak var nepaliChoosenImgView: UIImageView!
//    @IBOutlet weak var chooseLanguageLabel: UILabel!
//    @IBOutlet weak var selectLanguagelabel: UILabel!
//    
//    @IBOutlet weak var submitLabel: UILabel!
//    @IBOutlet weak var nepaliButton: UIButton!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        checkCurrentLanguage()
//        fetchToken()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
//        
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        if accessToken?.accessToken == nil {
//            self.navigationController?.navigationBar.isHidden = true
//        } else {
//            self.navigationController?.navigationBar.isHidden = false
//        }
//    }
//    
//    func fetchToken() {
//       // self.accessToken = realm.objects(DeviceToken.self).first
//    }
//    
//    func checkCurrentLanguage() {
//        if Localize.currentLanguage() == "eng" {
//            setEngLanguage()
//        } else {
//            setNepLanguage()
//        }
//    }
//    
//    func setEngLanguage() {
//        Localize.setCurrentLanguage("eng")
//        self.englishChoosenImgView.isHidden = false
//        self.nepaliChoosenImgView.isHidden = true
//        self.submitLabel.text = "text_submit".localized()
//    }
//    
//    func setNepLanguage() {
//        Localize.setCurrentLanguage("ne")
//        self.englishChoosenImgView.isHidden = true
//        self.nepaliChoosenImgView.isHidden = false
//        self.submitLabel.text = "text_submit".localized()
//        self.nepaliButton.titleLabel?.text = "text_nepali_language".localized()
//    }
//    
//    
//    @objc func setText() {
//        chooseLanguageLabel.text = "text_choose_language".localized()
//        selectLanguagelabel.text = "text_select_language".localized()
//    }
//    
//    @IBAction func englishBtnPressed(_ sender: UIButton) {
//        Localize.setCurrentLanguage("eng")
//        self.englishChoosenImgView.isHidden = false
//        self.nepaliChoosenImgView.isHidden = true
//        self.submitLabel.text = "text_submit".localized()
//    }
//    
//    @IBAction func nepaliBtnPressed(_ sender: UIButton) {
//        Localize.setCurrentLanguage("ne")
//        self.englishChoosenImgView.isHidden = true
//        self.nepaliChoosenImgView.isHidden = false
//        self.submitLabel.text = "text_submit".localized()
//    }
//    
//    @IBAction func languageSubmitBtnPressed(_ sender: UIButton) {
////        if accessToken?.accessToken == nil {
////           
////        } else {
////            let vc = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
////            
////            self.navigationController?.pushViewController(vc, animated: true)
////        }
//    }
//    
//    
//}
