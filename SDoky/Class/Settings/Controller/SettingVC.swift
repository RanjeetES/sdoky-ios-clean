//
//  SettingVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/21/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import StoreKit
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer
import FolioReaderKit
import ObjectMapper


class StoryboardFolioReaderContrainer: FolioReaderContainer {
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        let config = FolioReaderConfig()
        config.scrollDirection = .horizontal
        config.allowSharing = false
        config.enableTTS = false
        config.hideBars = false
        config.displayTitle = true
        config.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        //config.menuBackgroundColor = hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR)
        config.shouldHideNavigationOnTap = false
        config.useReaderMenuController = false
        config.canChangeScrollDirection = true
        config.hidePageIndicator = true
        
        
//        let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
//        let pdfNameFromUrl = "1610087270-en.epub"
//        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
//        print(file)
        
        
        let resourceDocPath = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)).last! as URL
        let pdfNameFromUrl = "oppo.epub"
        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
        
        //guard let bookPath = Bundle.main.path(forResource: "hello", ofType: "epub") else { return }
        
        let bookPath = actualPath.absoluteString.replacingOccurrences(of: "file://", with: "")
        setupConfig(config, epubPath: bookPath)
        
        
        //let p = Bundle.main.resourcePath
        //let bookPath = actualPath.absoluteString
        
        
//        if #available(iOS 10.0, *) {
//            do {
//                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
//                for url in contents {
//                       // its your file! do what you want with it!
//                    if url.description.contains("1610087270-en.epub") {
//                        print(url)
//                      //  setupConfig(config, epubPath: url.absoluteString)
//
//
//                    }
//
//            }
//        } catch {
//            print("could not locate pdf file !!!!!!!")
//        }
            
//            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//            let filePath = "\(documentsPath)/1610087270-en.epub"
//            print(filePath)
        

    }
}
//}

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var lblMenuExtra: UILabel!
    
}

class SettingVC: UIViewController, ChangeLanguageDelegate, SettingsAPIDelegate {
    
    
    @IBOutlet weak var tvSetting: UITableView!
    let languages = ["English","French","German","Spanish","Russian","Japanese", "Chinese (Simplified)"]
    var selectedLanguage = 0
    
    let menuList = ["My account","Change password","Language","About SDoky","Share SDoky","Rate SDoky","Terms & Conditions","SDoky Ebook","Logout"]
    let menuImageList = ["user","lock_settings","language","about","share","rate","terms","ebook","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvSetting.delegate = self
        tvSetting.dataSource = self
        
        tvSetting.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
        
        if let languageSelected = AppUtility.sharedInstance.getLanguageSelected() as? Int {
            self.selectedLanguage = languageSelected
        }
        
    }
    
    func didSelectLanguage(language: Int) {
        self.selectedLanguage = language
        self.tvSetting.reloadData()
        AppUtility.sharedInstance.setLangugeSelected(id: language)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
}


extension SettingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settings_cell", for: indexPath) as! SettingTableViewCell
        cell.ivIcon.image = UIImage(named: menuImageList[indexPath.row])
        cell.lblMenu.text = menuList[indexPath.row]
        
        if menuList[indexPath.row] == "Language" {
            cell.lblMenuExtra.isHidden = false
            cell.lblMenuExtra.text = self.languages[selectedLanguage]
        }else {
            cell.lblMenuExtra.isHidden = true
        }
        
        if menuList[indexPath.row] == "Logout" {
            cell.accessoryView = .none
        }else {
            let disclosureImageView = UIImageView.init(image: UIImage(named: "disclosure"))
            cell.accessoryView = disclosureImageView
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if menuList[indexPath.row] == "Logout" {
            
            // create the alert
            //            let alert = UIAlertController(title: "", message: "Are you sure to logout?", preferredStyle: UIAlertController.Style.alert)
            //
            //            // add the actions (buttons)
            //            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            //                action in
            //                self.logout()
            //            }))
            //            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            //
            //            // show the alert
            //            self.present(alert, animated: true, completion: nil)
            
            
            
            
        }
        
        switch indexPath.row {
        case 0:
            print("0")
            self.showAccount()
        case 1:
            print("1")
            self.showChangePassword()
        case 2:
            print("2")
            self.showLanguageSelector()
        case 3:
            print("3")
            self.showAboutUs()
        case 4:
            print("4")
            self.shareApp()
        case 5:
            print("5")
            self.rateApp()
        case 6:
            print("6")
            self.openLink(link: "https://sdoky.com/terms-and-conditions")
        case 7:
            self.open()
        case 8:
            print("8")
            print("7")
            let alert = UIAlertController(title: "", message: "Are you sure to logout?", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
                action in
                self.logout()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        default:
            print("0")
        }
    }
    
    func open() {
    
//      let config = FolioReaderConfig()
//          let bookPath = Bundle.main.path(forResource: "A Little Life by Hanya Yanagihara", ofType: "epub")
//
//          let folioReader = FolioReader()
//
//          folioReader.presentReader(parentViewController: self, withEpubPath: bookPath!, andConfig: config)
        
       if AppUtility.sharedInstance.isConnectedToInternet() == true {
                   
                   let settingsAPI = SettingsAPI()
                           settingsAPI.delegate = self
                           settingsAPI.getEbook()
                           DispatchQueue.main.async {
                               self.showSpinner(onView: self.view)
                          }
                   
               }else {
                   
                   let resourceDocPath = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)).last! as URL
                   let pdfNameFromUrl = "oppo.epub"
                   let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                   let bookPath = actualPath.absoluteString.replacingOccurrences(of: "file://", with: "")
                   
                   if FileManager.default.fileExists(atPath: bookPath) {
                       
                       let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
                       self.show(epubReader, sender: self)
                       
                   } else {
                       self.showAlertWithMessageWithTitle(title: "Alert", message: "Please connect to internet to download the book")
                   }
                   
               }
        
//            let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//            self.show(epubReader, sender: self)
    }
    
    func didGetEbookSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        print(resultDict)
        
        if let json = resultDict as? [String: Any] {
            if let response:EbookResponse = Mapper<EbookResponse>().map(JSON: json) {
                if response.status == true {
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }

                    let url = response.ebookResponseData?.link ?? ""
                    self.savePdf(urlString: url, fileName: "1610087270-en")
                    
//                    let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//                                                   self.show(epubReader, sender: self)
                                        //self.showSavedPdf(url: urlString, fileName: fileName)

                    
                }
            }
        }
    }
    
    func didFailWithEbookError(_ error: NSError, resultStatus: Bool) {
        print(error)
        DispatchQueue.main.async {
           self.removeSpinner()
        }
    }
    
    
    func savePdf(urlString:String, fileName:String) {
            DispatchQueue.main.async {
                let url = URL(string: urlString)
                let pdfData = try? Data.init(contentsOf: url!)
                let resourceDocPath = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)).last! as URL
                let pdfNameFromUrl = "oppo.epub"
                let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                do {
                    print(actualPath)
                    try pdfData?.write(to: actualPath, options: .atomic)
                    print("pdf successfully saved!")
                    
//                    let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//                               self.show(epubReader, sender: self)
                    //self.showSavedPdf(url: urlString, fileName: fileName)
                    let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
                               self.show(epubReader, sender: self)
                    
                    
                    
                } catch {
                    print("Pdf could not be saved")
                }
            }
        }

        func showSavedPdf(url:String, fileName:String) {
            if #available(iOS 10.0, *) {
                do {
                    let docURL = try FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                    for url in contents {
                        if url.description.contains("\(fileName).epub") {
                           // its your file! do what you want with it!
                            print(url)
//                            let epubReader =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "StoryboardFolioReaderContrainer") as! StoryboardFolioReaderContrainer
//                            epubReader.file = url.path
//                                                          self.show(epubReader, sender: self)
                            
                            

                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }

    // check to avoid saving a file multiple times
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("YourAppName-\(fileName).pdf") {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    func logout() {
        
        AppUtility.sharedInstance.setToken(token: "")
        AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: false)
        AppUtility.sharedInstance.setCustomerId(customerId: 0)
        AppUtility.sharedInstance.setCustomerEmail(customerEmail: "")
        let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        
        self.show(loginVC, sender: self)
        
    }
    
    func showAboutUs() {
        
        let vc =  SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        self.show(vc, sender: self)
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func rateApp() {
        
        let url  = NSURL(string: "itms://itunes.apple.com/us/app/xxxxxxxxxxx/id10769823725299?ls=1&mt=8")
        if UIApplication.shared.canOpenURL(url! as URL) == true  {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as! URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as! URL)
            }
        }
        
    }
    
    func shareApp() {
        
        if let name = URL(string: "https://www.apple.com/ios/app-store/"), !name.absoluteString.isEmpty {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }else  {
            // show alert for not available
        }
    }
    
    func openLink(link: String) {
        
        guard let url = URL(string: link) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func showLanguageSelector() {
        
        let viewController = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "ChangeLanguageVC") as! ChangeLanguageVC
        viewController.delegate = self
        // Initialize the bottom sheet with the view controller just created
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 490.0)
        // Present the bottom sheet
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func showChangePassword() {
        
        let viewController = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        //viewController.delegate = self
        // Initialize the bottom sheet with the view controller just created
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 390.0)
        // Present the bottom sheet
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    func showAccount() {
        
        let viewController = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
        //viewController.delegate = self
        // Initialize the bottom sheet with the view controller just created
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height - 50.0)
        // Present the bottom sheet
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    
    
}
