//
//  ChangePasswordVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/4/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController, SettingsAPIDelegate {
    
    @IBOutlet weak var tfCurrentPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfRetypePassword: UITextField!
    @IBOutlet weak var btnSavePassword: UIButton!
    @IBOutlet weak var btnCancelPassword: UIButton!
    
    @IBOutlet weak var btnEyeCurrentPassword: UIButton!
    @IBOutlet weak var btnEyeNewPassword: UIButton!
    @IBOutlet weak var btnEyeRetypePassword: UIButton!
    
    var showCurrentPassword = false
    var showNewPassword = false
    var showRetypePassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        // Do any additional setup after loading the view.
        tfCurrentPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        tfNewPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        tfRetypePassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        
        btnSavePassword.layer.cornerRadius = 5.0
        btnCancelPassword.layer.cornerRadius = 5.0
        btnCancelPassword.layer.borderWidth = 1.0
        btnCancelPassword.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    @IBAction func btnSavePasswordPressed(_ sender: Any) {
        
        self.removeErrorViews()
        
        tfCurrentPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        tfNewPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        tfRetypePassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.view, message: "")
        
        if tfCurrentPassword.text == "" && tfNewPassword.text == "" {
            
            tfCurrentPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Required field")
            tfNewPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password cannot be empty")
            
        }else if tfNewPassword.text == ""{
            tfNewPassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password cannot be empty")
            
        }else if tfRetypePassword.text == "" {
            
            tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password confirmation failed")
        }else if tfRetypePassword.text != tfNewPassword.text {
            
            tfRetypePassword.addLeftImage(imageTintColor: UIColor.systemBlue, imageName: "lock", hasError: true, view: self.view, message: "Password confirmation failed")
        }else {
            let params = ["password":tfNewPassword.text, "current_password":tfCurrentPassword.text]
            let settingAPI = SettingsAPI()
            settingAPI.delegate = self
            settingAPI.changeAccountPassword(params: params)
            self.showSpinner(onView: self.view)
        }
        
    }
    
    
    @IBAction func btnEyeCurrentPasswordAction(_ sender: Any) {
        
        
        if (showCurrentPassword == false) {
            self.tfCurrentPassword.isSecureTextEntry = false
            self.btnEyeCurrentPassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showCurrentPassword = true
            
        }else {
            
            self.tfCurrentPassword.isSecureTextEntry = true
            self.btnEyeCurrentPassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showCurrentPassword = false
        }
        
    }
    
    @IBAction func btnEyeNewPasswordAction(_ sender: Any) {
        
        
        if (showNewPassword == false) {
            self.tfNewPassword.isSecureTextEntry = false
            self.btnEyeNewPassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showNewPassword = true
            
        }else {
            
            self.tfNewPassword.isSecureTextEntry = true
            self.btnEyeNewPassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showNewPassword = false
        }
        
    }
    
    @IBAction func btnEyeRetypePasswordAction(_ sender: Any) {
        
        
        if (showRetypePassword == false) {
            self.tfRetypePassword.isSecureTextEntry = false
            self.btnEyeRetypePassword.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showRetypePassword = true
            
        }else {
            
            self.tfRetypePassword.isSecureTextEntry = true
            self.btnEyeRetypePassword.setImage(UIImage(named: "eye"), for: .normal)
            self.showRetypePassword = false
        }
        
    }
    
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didChangeAccountPasswordSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                
                if let error = message["current_password"] as? String {
                    tfCurrentPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.view, message: error)
                }
                
                if let error = message["password"] as? String {
                    tfNewPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.view, message: error)
                }
                
                if let error = message["error"] as? String {
                    tfCurrentPassword.addLeftImage(imageTintColor: .systemBlue, imageName: "lock", hasError: true, view: self.view, message: error)
                }
                //                if let error = message.keys.first {
                //                    if error == "password"{
                //                        let errorMessage = message["password"] as! String
                //
                //                    }else if error == "email_or_phone" {
                //                        let errorMessage = message["email_or_phone"] as! String
                //
                //                    }else if error == "error"{
                //                        let errorMessage = message["error"] as! String
                //                        tfCurrentPassword.addLeftImage(imageTintColor: self.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.view, message: errorMessage)
                //                    }
                //                }
                
            }else {
                
                let alert = UIAlertController(title: "Alert", message: "Password changed successfully", preferredStyle: UIAlertController.Style.alert)
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                    action in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
    
    func didFailChangeAccountPasswordError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
        if error.code == 401 {
            self.checkUnAuthorized()
        }
        
    }
}
