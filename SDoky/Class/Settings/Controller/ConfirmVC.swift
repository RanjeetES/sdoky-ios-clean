//
//  ConfirmVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/10/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

class ConfirmVC: UIViewController, SettingsAPIDelegate {
    
    @IBOutlet weak var tfCurrentPassword: UITextField!
    @IBOutlet weak var tfReason: UITextField!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var contentView: UIView!
    var showPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        tfCurrentPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.contentView, message: "")
        
        tfCurrentPassword.attributedPlaceholder = NSAttributedString(string: "Current password", attributes:attributes)
        
        tfReason.attributedPlaceholder = NSAttributedString(string: "Type reason (Optional)", attributes:attributes)
        
        btnDelete.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        
        self.removeOtherErrors()
        tfCurrentPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: false, view: self.contentView, message: "Required field")
        
        
        if tfCurrentPassword.text == "" {
            
            tfCurrentPassword.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Required field")
        }else{
            
            let settingsAPI = SettingsAPI()
            settingsAPI.delegate = self
            settingsAPI.deleteAccount(params: ["language":"en", "reason":tfReason.text ?? "","password":tfCurrentPassword.text])
            
            self.showSpinner(onView: self.contentView)
            
            let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
            let window = UIApplication.shared.windows.first

            // Embed loginVC in Navigation Controller and assign the Navigation Controller as windows root
            //let nav = UINavigationController(rootViewController: loginVC)
            
            let nvc: UINavigationController = UINavigationController(rootViewController: loginVC!)
            nvc.navigationBar.isHidden = true
            window?.rootViewController = nvc
            
        }
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnEyeAction(_ sender: Any) {
        
        
        if (showPassword == false) {
            self.tfCurrentPassword.isSecureTextEntry = false
            self.btnEye.setImage(UIImage(named: "eye-seen"), for: .normal)
            self.showPassword = true
            
        }else {
            
            self.tfCurrentPassword.isSecureTextEntry = true
            self.btnEye.setImage(UIImage(named: "eye"), for: .normal)
            self.showPassword = false
        }
        
    }
    
    func removeOtherErrors() {
        for eachView in contentView.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    func didDeleteUserSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
                let message = (json["message"] as! Dictionary<String, Any>)
                if let error = message.keys.first {
                    if error == "password"{
                        let errorMessage = message["password"] as! String
                         tfCurrentPassword.addLeftImage(imageTintColor: self.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: errorMessage)
                       
                    }else if error == "email_or_phone" {
                        let errorMessage = message["email_or_phone"] as! String
                       
                    }else if error == "error"{
                        let errorMessage = message["error"] as! String
                        tfCurrentPassword.addLeftImage(imageTintColor: self.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: errorMessage)
                    }
                }
                
            }else {
                
                //self.showAlertWithMessage(message: "Password changed successfully")
                //self.dismiss(animated: true, completion: nil)
                self.checkUnAuthorized()
                
            }
            
        }
    }
    
    func didFailWithDeleteUserError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
        self.removeSpinner()
        if error.code == 401 {
            self.checkUnAuthorized()
        }
        tfCurrentPassword.addLeftImage(imageTintColor: self.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "lock", hasError: true, view: self.contentView, message: "Your password do not match")
        
    }
    
    
}
 
 
