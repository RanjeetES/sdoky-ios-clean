//
//  PricingVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/4/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import PassKit
import ObjectMapper
import IQKeyboardManagerSwift

class GradientProgressView: UIProgressView {
    
    @IBInspectable var firstColor: UIColor = UIColor.white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var secondColor: UIColor = UIColor.black {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        
        if let gradientImage = UIImage(bounds: self.bounds, colors: [firstColor, secondColor]) {
            self.progressImage = gradientImage
        }
    }
}

public enum GradientOrientation {
    case vertical
    case horizontal
}

class PricingTableViewCell : UITableViewCell {
    
    @IBOutlet weak var btnPricing: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
}

class PricingVC: UIViewController, UITableViewDelegate, UITableViewDataSource, LogInAPIDelegate, SettingsAPIDelegate {
    
    
    @IBOutlet weak var tvPricing: UITableView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    //    var priceLabel = ["Payment for 1 year","Payment for 2 year","Payment for 3 year","Payment for 4 year","Payment for 5 year"]
    //    var price = ["$ 100.00","$ 80.00","$ 60.00","$ 40.00","$ 20.00"]
    //
    //
    //    var subscriptionName = ["One year", "Two year", "Three year", "Four year", "Five year"]
    //    var validity = ["1","2","3","4","5"]
    //    var priceInt = [100.00,80.00,60.00,40.00,20.00]
    
    var pricings: [PricingResponseData] = []
    
    var selectedPrice = -1.0
    var selectedSubscriptionName = ""
    var selectedValidity = ""
    var isFromAccount = false
    var selectedRow = -1
    var selectedPriceId = -1
    var isFromNotification = false
    var subCode = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tfCode.addLeftImage(imageTintColor: .systemBlue, imageName: "", hasError: false, view: self.view, message: "")
        self.navigationController?.navigationBar.isHidden = true
        tfCode.attributedPlaceholder = NSAttributedString(string: "Type code", attributes:attributes)
        // Do any additional setup after loading the view.
        tvPricing.delegate = self
        tvPricing.dataSource = self
        tvPricing.separatorStyle = .none
        tvPricing.allowsMultipleSelection = false
        
        btnPayNow.layer.cornerRadius = 5.0
        btnPayNow.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        btnPayNow.setTitleColor(hexStringToUIColor(hex: "909AB1"), for: .normal)
        
        //0B75D2
        
        self.endEditing()
        
        let settingAPI = SettingsAPI()
        settingAPI.delegate = self
        settingAPI.getPricingDetails()
        DispatchQueue.main.async {
            self.showSpinner(onView: self.view)
        }
        
        if isFromAccount == false {
            self.btnBack.isHidden = true
        }else {
            self.btnBack.isHidden = false
        }
        
        
    }
    
    func didGetPricingSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        
        if let json = resultDict as? [String: Any] {
            if let response:PricingResponse = Mapper<PricingResponse>().map(JSON: json) {
                if response.status == true {
                    self.pricings = response.pricingData!
                    
                    DispatchQueue.main.async {
                        self.tvPricing.reloadData()
                        self.removeSpinner()
                    }
                    
                    
                }
            }
        }
    }
    
    func didFailWithPricingError(_ error: NSError, resultStatus: Bool) {
        DispatchQueue.main.async {
            self.removeSpinner()
        }
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    @IBAction func btnBackClick(_ sender: Any) {
        
        if isFromNotification {
            self.showHome()
        }else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return pricings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pricing_cell", for: indexPath) as! PricingTableViewCell
        cell.btnPricing.setTitle(pricings[indexPath.section].planName ?? "", for: .normal)
        cell.lblPrice.text = "\(pricings[indexPath.section].price ?? 0)"
        cell.lblPrice.layer.cornerRadius = 5.0
        cell.layer.cornerRadius = 5.0
        cell.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        cell.layer.borderWidth = 1.0
        
        
        if indexPath.section == selectedRow {
            cell.layer.backgroundColor = self.hexStringToUIColor(hex: "2F93EB").cgColor
            cell.btnPricing.setTitleColor(.white, for: .normal)
            cell.btnPricing.backgroundColor = self.hexStringToUIColor(hex: "2F93EB")
            cell.lblPrice.textColor = .white
            cell.lblPrice.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
            self.selectedPrice = pricings[indexPath.section].price ?? 0
            self.selectedSubscriptionName = pricings[indexPath.section].planName ?? ""
            self.selectedValidity = pricings[indexPath.section].years ?? ""
            self.selectedPriceId = pricings[indexPath.section].pricingId ?? 0
        }else{
            
            cell.layer.backgroundColor = UIColor.white.cgColor
            cell.btnPricing.setTitleColor(hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR), for: .normal)
            cell.btnPricing.backgroundColor = .white
            cell.lblPrice.textColor = hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR)
            cell.lblPrice.backgroundColor = hexStringToUIColor(hex: "F2F4FA")
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRow = indexPath.section
        let cell = tableView.cellForRow(at: indexPath) as? PricingTableViewCell
        cell?.layer.backgroundColor = self.hexStringToUIColor(hex: "2F93EB").cgColor
        cell?.btnPricing.setTitleColor(.white, for: .normal)
        cell?.btnPricing.backgroundColor = self.hexStringToUIColor(hex: "2F93EB")
        cell?.lblPrice.textColor = .white
        cell?.lblPrice.backgroundColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        self.selectedPrice = pricings[indexPath.section].price ?? 0
        self.selectedSubscriptionName = pricings[indexPath.section].planName ?? ""
        self.selectedValidity = pricings[indexPath.section].years ?? ""
        self.selectedPriceId = pricings[indexPath.section].pricingId ?? 0
        cell?.layer.cornerRadius = 5.0
        
        btnPayNow.backgroundColor = hexStringToUIColor(hex: "0B75D2")
        btnPayNow.setTitleColor(hexStringToUIColor(hex: "FFFFFF"), for: .normal)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedRow = -1
        let cell = tableView.cellForRow(at: indexPath) as? PricingTableViewCell
        cell?.layer.backgroundColor = UIColor.white.cgColor
        cell?.btnPricing.setTitleColor(hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR), for: .normal)
        cell?.btnPricing.backgroundColor = .white
        cell?.lblPrice.textColor = hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR)
        cell?.lblPrice.backgroundColor = hexStringToUIColor(hex: "F2F4FA")
        self.selectedPrice = -1.0
        self.selectedSubscriptionName = ""
        self.selectedValidity = ""
        self.selectedPriceId = -1
        cell?.layer.cornerRadius = 5.0
    }
    
    
    
    @IBAction func payAction(_ sender: Any) {
        self.removeErrorViews()
        self.removePhoneError(message: "", texrField: self.tfCode)
        if selectedPrice == -1.0 {
            self.showAlertWithMessageWithTitle(title: "Alert", message: "Please select a plan")
            return
        }
        
        if tfCode.text != "" {
            let settingsAPI = SettingsAPI()
            settingsAPI.delegate = self
            settingsAPI.checkSubCode(params: ["sub_code": tfCode.text ?? "", "pricing_id":"\(self.selectedPriceId)"])
            print("params")
        }else {
            
            let request = PKPaymentRequest()
            
            request.merchantIdentifier = "merchant.test.sdoky"
            
            
            request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.discover]
            
            request.merchantCapabilities = [PKMerchantCapability.capability3DS, .capabilityCredit, .capabilityDebit]
            
            request.countryCode = "US"
            
            request.currencyCode = "USD"
            request.paymentSummaryItems = [PKPaymentSummaryItem(label: self.selectedSubscriptionName, amount: NSDecimalNumber(value: self.selectedPrice))]
            
            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayController?.delegate = self
            
            self.present(applePayController!, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    
}

extension PricingVC: PKPaymentAuthorizationViewControllerDelegate {
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        
        
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
        print("Success")
        var expiryDate = Date()
        do {
            let subscription : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
            
            if subscription.count > 0 {
                expiryDate = subscription.first?.expiresAt?.getDateFromSdokyStringDate() ?? Date()
            }
        }catch {
            print("Error fetching")
        }
        
        
        
        let params = ["price":self.selectedPrice,"validity_period":self.selectedValidity,"is_active":true,"created_by":"\(AppUtility.sharedInstance.getCustomerId())","subscription_name":self.selectedSubscriptionName.lowercased(),"expires_at": Calendar.current.date(byAdding: .year, value: Int(self.selectedValidity) ?? 0, to: expiryDate)?.convertToSDokyDate() ?? ""] as [String : Any]
        let loginAPI = LogInAPI()
        loginAPI.delegate = self
        loginAPI.postSubscriptionRequest(params: params, subCode: self.subCode)
    }
    
    func didPostSubsriptionSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                print("Failure")
                self.dismiss(animated: false) {
                 self.dismiss(animated: false) {
                     self.showAlertWithMessageWithTitle(title: "Error", message: "Purchase could not be completed.")
                 }
                }
                
                
            }else {
                print("Success")
                if let data = json["data"] as? [String: Any] {
                    
                    do {
                        let user : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
                        
                        if user.count > 0 {
                            user.first?.subscription?.subscriptionId = Int16(data["subscription_id"] as? Int ?? 0)
                            user.first?.subscription?.subscriptionName = data["subscription_name"] as? String ?? ""
                            user.first?.subscription?.userId = Int16(data["user_id"] as? Int ?? AppUtility.sharedInstance.getCustomerId())
                            user.first?.subscription?.validityPeriod = data["validity_period"] as? String ?? ""
                            user.first?.subscription?.price = data["price"] as? String ?? ""
                            user.first?.subscription?.createdBy = data["created_by"] as? String ?? ""
                            user.first?.subscription?.isActive = data["is_active"] as? Bool ?? true
                            user.first?.subscription?.expiresAt = data["expires_at"] as? String ?? ""
                            user.first?.subscription?.createdAt = data["created_at"] as? String ?? ""
                            user.first?.subscription?.updatedAt = data["updated_at"] as? String ?? ""
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        } else {
                            
                            let subscriptionToSave = SubscriptionLocal(context: AppUtility.sharedInstance.getContext())
                            subscriptionToSave.subscriptionId = Int16(data["subscription_id"] as? Int ?? 0)
                            subscriptionToSave.subscriptionName = data["subscription_name"] as? String ?? ""
                            subscriptionToSave.userId = Int16(data["user_id"] as? Int ?? AppUtility.sharedInstance.getCustomerId())
                            subscriptionToSave.validityPeriod = data["validity_period"] as? String ?? ""
                            subscriptionToSave.price = data["price"] as? String ?? ""
                            subscriptionToSave.createdBy = data["created_by"] as? String ?? ""
                            subscriptionToSave.isActive = data["is_active"] as? Bool ?? true
                            subscriptionToSave.expiresAt = data["expires_at"] as? String ?? ""
                            subscriptionToSave.createdAt = data["created_at"] as? String ?? ""
                            subscriptionToSave.updatedAt = data["updated_at"] as? String ?? ""
                            user.first?.subscription = subscriptionToSave
                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        }
                        
                    }catch{
                        print("Error Fetching")
                    }
                    
                    if isFromAccount == false {
                        
                        let tabBarController = UITabBarController()
                        let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                        let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                        let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                        let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                        let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                        tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
                        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                        tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                        
                        self.show(tabBarController, sender: self)
                        
                    }else {
                        self.dismiss(animated: true, completion: nil)
                        self.dismiss(animated: true, completion: nil)
                        
                        
                    }
                    
                    
                    
                    
                }
            }
            
        }
    }
    
    func didFailWithSubscriptionError(_ error: NSError, resultStatus: Bool) {
        print(error.description)
       self.dismiss(animated: false) {
        self.dismiss(animated: false) {
            self.showAlertWithMessageWithTitle(title: "Error", message: "Purchase could not be completed.")
        }
       }
        
        if error.code == 401 {
            self.checkUnAuthorized()
        }
        
    }
    
    
    func didGetSubCodeSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                if let m = json["message"] as? [String: Any] {
                    self.showError(message: m["sub_code"] as? String ?? "Subscription code is invalid", texrField: self.tfCode)
                }else {
                    self.showError(message: "Subscription code is invalid", texrField: self.tfCode)
                }
                
            }else {
                if let response:CheckSubCodeResponse = Mapper<CheckSubCodeResponse>().map(JSON: json) {
                    
                    
                        //let fallsBetween = Date().isBetween(response.data?.discountStartDate?.getDateFromStringDate() ?? Date(), and: response.data?.discountEndDate?.getDateFromStringDate() ?? Date())
                        
                        let expired = response.data?.expiresAt?.getDateFromSdokyStringDate() ?? Date() < Date()
                        
                        if response.data?.isApplied == true {
                            self.showError(message: "Code already used", texrField: self.tfCode)
                        }else if expired == true {
                            self.showError(message: "Code expired", texrField: self.tfCode)
                            //                    }else if fallsBetween != true {
                            //                        self.showError(message: "Code is not valid", texrField: self.tfCode)
                        }else {
                            
                            let discountFractionA = Double(response.data?.percentage ?? 0)/100
                            let discountA = discountFractionA * self.selectedPrice
                            let priceA = self.selectedPrice - discountA
                            
                            
                            let alert = UIAlertController(title: self.selectedSubscriptionName, message: "Subscription price: $\(self.selectedPrice) \n Discount: $\(discountA) \n Total to pay: $\(priceA)", preferredStyle: UIAlertController.Style.alert)
                                   
                                   // add the actions (buttons)
                                   alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: {
                                       action in
                                   
                                    if priceA == 0.0 {
                                        
                                        var expiryDate = Date()
                                        do {
                                            let subscription : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
                                            
                                            if subscription.count > 0 {
                                                expiryDate = subscription.first?.expiresAt?.getDateFromSdokyStringDate() ?? Date()
                                            }
                                        }catch {
                                            print("Error fetching")
                                        }
                                        
                                        let params = ["price":self.selectedPrice,"validity_period":self.selectedValidity,"is_active":true,"created_by":"\(AppUtility.sharedInstance.getCustomerId())","subscription_name":self.selectedSubscriptionName.lowercased(),"expires_at": Calendar.current.date(byAdding: .year, value: Int(self.selectedValidity) ?? 0, to: expiryDate)?.convertToSDokyDate() ?? ""] as [String : Any]
                                        let loginAPI = LogInAPI()
                                        loginAPI.delegate = self
                                        loginAPI.postSubscriptionRequest(params: params, subCode: self.subCode)
                                        
                                   } else {
                                   
                                       
                                    self.subCode = response.data?.couponCode ?? ""
                                    
                                    let request = PKPaymentRequest()
                                    
                                    request.merchantIdentifier = "merchant.test.sdoky"
                                    
                                    request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.discover]
                                    
                                    request.merchantCapabilities = [PKMerchantCapability.capability3DS, .capabilityCredit, .capabilityDebit]
                                    
                                    request.countryCode = "US"
                                    
                                    request.currencyCode = "USD"
                                    
                                    
                                    let discountFraction = Double(response.data?.percentage ?? 0)/100
                                    let discount = discountFraction * self.selectedPrice
                                    self.selectedPrice = self.selectedPrice - discount
                                    request.paymentSummaryItems = [PKPaymentSummaryItem(label: self.selectedSubscriptionName, amount: NSDecimalNumber(value: self.selectedPrice))]
                                    
                                    let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
                                    applePayController?.delegate = self
                                    
                                    self.present(applePayController!, animated: true, completion: nil)
                                    
                            }
                                    
                                       
                                   }))
                                   alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                                   
                                   // show the alert
                                   self.present(alert, animated: true, completion: nil)
                            
                            
                            
                            
                    }
                }
            }
        }
        
//        if let json = resultDict as? [String: Any] {
//
//            if let response:CheckSubCodeResponse = Mapper<CheckSubCodeResponse>().map(JSON: json) {
//                if response.status == true {
//                    //let fallsBetween = Date().isBetween(response.data?.discountStartDate?.getDateFromStringDate() ?? Date(), and: response.data?.discountEndDate?.getDateFromStringDate() ?? Date())
//
//                    let expired = response.data?.expiresAt?.getDateFromStringDate() ?? Date() < Date()
//
//                    if response.data?.isApplied == true {
//                        self.showError(message: "Code already used", texrField: self.tfCode)
//                    }else if expired == true {
//                        self.showError(message: "Code expired", texrField: self.tfCode)
//                        //                    }else if fallsBetween != true {
//                        //                        self.showError(message: "Code is not valid", texrField: self.tfCode)
//                    }else {
//
//                        let request = PKPaymentRequest()
//
//                        request.merchantIdentifier = "merchant.test.sdoky"
//
//                        request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.discover]
//
//                        request.merchantCapabilities = [PKMerchantCapability.capability3DS, .capabilityCredit, .capabilityDebit]
//
//                        request.countryCode = "US"
//
//                        request.currencyCode = "USD"
//
//                        let discountFraction = Double(response.data?.percentage ?? 0)/100
//                        let discount = discountFraction * self.selectedPrice
//                        self.selectedPrice = self.selectedPrice - discount
//                        request.paymentSummaryItems = [PKPaymentSummaryItem(label: self.selectedSubscriptionName, amount: NSDecimalNumber(value: self.selectedPrice))]
//
//                        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
//                        applePayController?.delegate = self
//
//                        self.present(applePayController!, animated: true, completion: nil)
//
//                    }
//                }else {
//                    if let m = json["message"] as? [String: Any] {
//                        self.showError(message: m["sub_code"] as? String ?? "Subscription code is invalid", texrField: self.tfCode)
//                    }else {
//                        self.showError(message: "Subscription code is invalid", texrField: self.tfCode)
//                    }
//
//
//                }
//            }
//        }
        
    }
    
    func didFailWithSubCodeError(_ error: NSError, resultStatus: Bool) {
        print("Error")
        if error.code == 401 {
            self.checkUnAuthorized()
        }
        self.showAlertWithMessageWithTitle(title: "Error", message: "Something went wrong. Please try again.")
    }
    
    func showError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: texrField.bottomAnchor, multiplier: 0.5),
            texrField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            texrField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label)
        view.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for v in tvPricing.subviews {
            for constraint in v.constraints {
                if constraint.identifier == texrField.placeholder {
                    constraint.constant = 40.0
                }
                if constraint.identifier == "code"{
                    constraint.constant = 20.0
                }
            }
        }
        
    }
    
    func removePhoneError(message: String, texrField: UITextField) {
        
        texrField.layer.borderWidth = 1.0
        texrField.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        texrField.layer.cornerRadius = 5.0
        texrField.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for v in tvPricing.subviews {
            for constraint in v.constraints {
                if constraint.identifier == texrField.placeholder {
                    constraint.constant = 16.0
                }
                
                if constraint.identifier == "code"{
                    constraint.constant = 44.0
                }
            }
        }
    }
    
    func showHome() {
        let tabBarController = UITabBarController()
                       let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
                       let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
                       let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
                       let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
                       let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                       tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
                       tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
        tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
                       self.show(tabBarController, sender: self)
    }
}



