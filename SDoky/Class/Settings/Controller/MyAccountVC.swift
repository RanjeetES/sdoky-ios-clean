//
//  MyAccountVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/2/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
import MaterialComponents.MaterialBottomSheet
import MaterialComponents.MaterialBottomSheet_ShapeThemer
import CoreData


class MyAccountVC: UIViewController, SettingsAPIDelegate, CountrySelectorDelegate {
    
    
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var lblPaymentStatus: UILabel!
    
    @IBOutlet weak var lblDaysPending: UILabel!
    
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfDateOfBirth: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var ivPhone: UIImageView!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet weak var btnSaveHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCancelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vPhone: UIView!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var maskProgressView: UIProgressView!
    @IBOutlet weak var progressViewImage: UIImageView!
    
    let datePicker = UIDatePicker()
    
    
    var selectedCountry: String = ""
    var phone: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var dob: String = ""
    var city: String = ""
    var country: String = ""
    
    var user : User?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        showDatePicker()
        progressView.layer.borderWidth = 0.5
        progressView.layer.borderColor = UIColor.lightGray.cgColor
        progressView.layer.cornerRadius = 8.0
        
        maskProgressView.layer.borderWidth = 0.5
        maskProgressView.layer.borderColor = UIColor.lightGray.cgColor
        maskProgressView.layer.cornerRadius = 8.0
        
        var totalDays = 0
        var remainingDays = 0
        
        
        do {
            let subscription : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
            
            if subscription.count > 0 {
                
                
                var x = subscription.first?.expiresAt?.getDaysFromStringDate() ?? 0
                var y = subscription.first?.createdAt?.getDaysFromStringDate() ?? 0
                
                if y < 0 {
                    y = -y
                }
                if y == 0 {
                    y=1
                }
                
                let total = x+y
                let escapedDays = y
                
                self.maskProgressView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
                self.maskProgressView.progress = 1.0 - Float(escapedDays)/Float(total)
                
                //self.progressView.progress = Float(escapedDays)/Float(total)
                self.progressView.progress = 1.0
                
            }
        }catch {
            print("Error fetching")
        }
        
        
        self.enableEditing(state: false)
        
        self.btnSave.isHidden = true
        self.btnCancel.isHidden = true
        
        self.btnSaveHeight.constant = 0.0
        self.btnCancelHeight.constant = 0.0
        
        tfEmail.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "email", hasError: false, view: self.view, message: "")
        tfFirstName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.view, message: "")
        tfLastName.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "user", hasError: false, view: self.view, message: "")
        tfDateOfBirth.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "calendar", hasError: false, view: self.view, message: "")
        tfCountry.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "location", hasError: false, view: self.view, message: "")
        tfCity.addLeftImage(imageTintColor: hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR), imageName: "location", hasError: false, view: self.view, message: "")
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        vPhone.layer.borderWidth = 1.0
        vPhone.layer.cornerRadius = 5.0
        vPhone.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        
        //        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(tapFunction))
        //        swipe.direction = .down
        //        lblName.isUserInteractionEnabled = true
        //        lblName.addGestureRecognizer(swipe)
        
        
    }
    
    //    @IBAction func tapFunction(sender: UISwipeGestureRecognizer) {
    //        print("Swipe")
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //if AppUtility.sharedInstance.isConnectedToInternet() {
        self.checkExpiry()
        if false {
            
            let settingsAPI = SettingsAPI()
            settingsAPI.delegate = self
            settingsAPI.getUserDetails()
            self.showSpinner(onView: self.view)
            self.btnEdit.isEnabled = true
            
        }else {
            self.getLocalAccountData()
            //self.btnEdit.isEnabled = false
        }
        
    }
    
    @IBAction func showPricingMenu( _sender: Any) {
        
        if AppUtility.sharedInstance.isConnectedToInternet() {
            let viewController = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
            viewController.isFromAccount = true
            viewController.isFromNotification = false
            self.show(viewController, sender: self)
        }else {
            self.showAlertWithMessageWithTitle(title: "Alert", message: "Connection required.")
        }
        
        
        
    }
    
    
    @IBAction func editProfile(_ sender: Any) {
        
        self.btnSave.isHidden = false
        self.btnCancel.isHidden = false
        
        self.btnSaveHeight.constant = 40.0
        self.btnCancelHeight.constant = 40.0
        
        self.enableEditing(state: true)
    }
    
    @IBAction func showCountrySelector(_ sender: Any) {
        
        let viewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "CountrySelectorVC") as! CountrySelectorVC
        viewController.delegate = self
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
        //bottomSheet.trackingScrollView?.scrollsToTop = true
        bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height - 100.0)
        // Present the bottom sheet
        bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
        bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        present(bottomSheet, animated: true, completion: nil)
    }
    
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        var d = ""
        let dobConverted = AppUtility.sharedInstance.convertDateString(dateString: self.dob, fromFormat: "d.MM.y", toFormat: "yyyy/MM/dd")
        
        if self.dob == self.tfDateOfBirth.text {
            d = AppUtility.sharedInstance.convertDateString(dateString: self.tfDateOfBirth.text, fromFormat: "d.MM.y", toFormat: "yyyy/MM/dd")
        }
        else {
            d = self.tfDateOfBirth.text ?? dobConverted
        }

        
        
        let params = ["phone": self.tfPhone.text ?? self.phone,
                      "first_name": self.tfFirstName.text ?? self.firstName,
                      "last_name": self.tfLastName.text ?? self.lastName,
                      "dial_code": self.selectedCountry,
                      "city": self.tfCity.text ?? self.city,
                      "country": self.tfCountry.text ?? self.country,
                      "date_of_birth": d]
        
        let settingAPI = SettingsAPI()
        settingAPI.delegate = self
        settingAPI.updateProfile(params: params)
        self.showSpinner(onView: self.view)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.btnSave.isHidden = true
        self.btnCancel.isHidden = true
        
        self.btnSaveHeight.constant = 0.0
        self.btnCancelHeight.constant = 0.0
        
        self.enableEditing(state: false)
    }
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Confirm to delete account?", message: "You are about to delete your account information with us along with all your folders, data, SMap records and other records if any. You will be permanently deleted from SDoky system and please be informed that this action is unrecoverable. \n \n Are you sure to proceed?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
            action in
            
            let viewController = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "ConfirmVC") as! ConfirmVC
            //viewController.delegate = self
            let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
            //bottomSheet.trackingScrollView?.scrollsToTop = true
            bottomSheet.preferredContentSize = CGSize(width: self.view.frame.width, height: 500.0)
            // Present the bottom sheet
            bottomSheet.contentViewController.view.layer.cornerRadius = 20.0
            bottomSheet.contentViewController.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            self.present(bottomSheet, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func verifyPhone(_ sender: Any) {
        
        
        self.removeOtherErrors()
        self.removePhoneError(message: "")
        let settingAPI = SettingsAPI()
        settingAPI.delegate = self
        settingAPI.verifyPhoneNumber()
        self.showSpinner(onView: self.view)
        
    }
    
    func didSelectCountry(country: CountryModel) {
        self.selectedCountry = country.dial_code
        self.btnCountryCode.setTitle(self.selectedCountry, for: .normal)
    }
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        tfDateOfBirth.inputAccessoryView = toolbar
        // add datepicker to textField
        tfDateOfBirth.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        tfDateOfBirth.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    func didGetUserSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if let response:UserResponse = Mapper<UserResponse>().map(JSON: json) {
                self.removeSpinner()
                if response.status == true {
                    let user = response.loginResponseData
                    
                    self.lblName.text = "\(user?.firstName ?? "") \(user?.lastName ?? "")"
                    self.tfEmail.attributedPlaceholder = NSAttributedString(string: user?.email ?? "", attributes: attributes)
                    self.user = user
                    self.showPlaceholders()
                    
                    if user?.phoneStatus == true {
                        self.btnVerify.isHidden = true
                    }else {
                        self.btnVerify.isHidden = false
                    }
                    
//                    do {
//                        
//                        let user : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
//                        for u in user {
//                            AppUtility.sharedInstance.getContext().delete(u)
//                            (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        }
//                    
//                        let userToSave = UserInfo(context: AppUtility.sharedInstance.getContext())
//                        userToSave.userId = Int16(self.user?.userId ?? -1)
//                        userToSave.firstName = self.user!.firstName ?? ""
//                        userToSave.lastName = self.user!.lastName ?? ""
//                        userToSave.phone = self.user!.phone ?? ""
//                        userToSave.dateOfBirth = self.user!.dateOfBirth ?? ""
//                        userToSave.city = self.user!.city ?? ""
//                        userToSave.country = self.user!.country ?? ""
//                        userToSave.email = self.user!.email ?? ""
//                        userToSave.dialCode = self.user!.dialCode ?? ""
//                        userToSave.emailStatus = self.user!.emailStatus ?? false
//                        userToSave.phoneStatus = self.user!.phoneStatus ?? false
//                        userToSave.active = self.user!.active ?? true
//                        
//                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        
//                    }catch{
//                        print("Error Fetching")
//                    }
                    
                }else {
                    self.showAlert()
                }
                
                
                
            }
        }
        
    }
    
    func didFailWithGetUserError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    func enableEditing(state: Bool) {
        //self.btnVerify.isEnabled = state
        self.tfPhone.isEnabled = state
        self.btnCountryCode.isEnabled = state
        self.tfEmail.isEnabled = false
        self.tfFirstName.isEnabled = state
        self.tfLastName.isEnabled = state
        self.tfDateOfBirth.isEnabled = state
        self.tfCountry.isEnabled = state
        self.tfCity.isEnabled = state
        self.btnEdit.isHidden = state
        self.btnVerify.isHidden = state
        
        if state == true {
            self.showText()
        }else {
            self.showPlaceholders()
        }
    }
    
    func showText() {
        
        self.tfFirstName.attributedPlaceholder = NSAttributedString(string: user?.firstName ?? "", attributes: attributes)
        self.tfFirstName.attributedText = NSAttributedString(string: user?.firstName ?? "", attributes: textFieldattributes)
        self.firstName = user?.firstName ?? ""
        
        self.tfLastName.attributedText = NSAttributedString(string: user?.lastName ?? "", attributes: textFieldattributes)
        self.lastName = user?.lastName ?? ""
        
        if user?.dateOfBirth == "" {
            self.tfDateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth (Optional)", attributes: attributes)
        }else {
            self.tfDateOfBirth.attributedText = NSAttributedString(string: user?.dateOfBirth ?? "Date of birth (Optional)", attributes: textFieldattributes)
            self.dob = user?.dateOfBirth ?? ""
        }
        
        if  user?.country == "" {
            self.tfCountry.attributedPlaceholder = NSAttributedString(string: "Country (Optional)", attributes: attributes)
        }else {
            self.tfCountry.attributedText = NSAttributedString(string: user?.country ?? "Country (Optional)", attributes: textFieldattributes)
            self.country =  user?.country ?? ""
        }
        
        if user?.city == "" {
            self.tfCity.attributedPlaceholder = NSAttributedString(string: "City (Optional)", attributes: attributes)
        }else {
            self.tfCity.attributedText = NSAttributedString(string: user?.city ?? "City (Optional)", attributes: textFieldattributes)
            self.city = user?.city ?? ""
        }
        
        self.tfPhone.attributedText = NSAttributedString(string: user?.phone ?? "", attributes: textFieldattributes)
        self.phone = user?.phone ?? ""
        self.selectedCountry = user?.dialCode ?? ""
        
    }
    
    func showPlaceholders() {
        
        self.tfFirstName.attributedPlaceholder = NSAttributedString(string: user?.firstName ?? "", attributes: attributes)
        self.tfFirstName.attributedPlaceholder = NSAttributedString(string: user?.firstName ?? "", attributes: attributes)
        self.firstName = user?.firstName ?? ""
        
        self.tfLastName.attributedPlaceholder = NSAttributedString(string: user?.lastName ?? "", attributes: attributes)
        self.lastName = user?.lastName ?? ""
        
        if user?.dateOfBirth == "" {
            self.tfDateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth (Optional)", attributes: attributes)
        }else {
            self.tfDateOfBirth.attributedPlaceholder = NSAttributedString(string: user?.dateOfBirth ?? "Date of birth (Optional)", attributes: attributes)
            self.dob = user?.dateOfBirth ?? ""
        }
        
        if  user?.country == "" {
            self.tfCountry.attributedPlaceholder = NSAttributedString(string: "Country (Optional)", attributes: attributes)
        }else {
            self.tfCountry.attributedPlaceholder = NSAttributedString(string: user?.country ?? "Country (Optional)", attributes: attributes)
            self.country =  user?.country ?? ""
        }
        
        if user?.city == "" {
            self.tfCity.attributedPlaceholder = NSAttributedString(string: "City (Optional)", attributes: attributes)
        }else {
            self.tfCity.attributedPlaceholder = NSAttributedString(string: user?.city ?? "City (Optional)", attributes: attributes)
            self.city = user?.city ?? ""
        }
        
        self.tfPhone.attributedPlaceholder = NSAttributedString(string: user?.phone ?? "", attributes: attributes)
        self.phone = user?.phone ?? ""
        self.selectedCountry = user?.dialCode ?? ""
        self.btnCountryCode.setTitle(user?.dialCode ?? "", for: .normal)
        
        
        
        
        self.tfFirstName.attributedPlaceholder = NSAttributedString(string: user?.firstName ?? "", attributes: attributes)
        self.tfFirstName.attributedText = NSAttributedString(string: "", attributes: textFieldattributes)
        
        self.tfLastName.attributedText = NSAttributedString(string:  "", attributes: textFieldattributes)
        
        if user?.dateOfBirth == "" {
            self.tfDateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth (Optional)", attributes: attributes)
        }else {
            self.tfDateOfBirth.attributedText = NSAttributedString(string: "", attributes: textFieldattributes)
        }
        
        if  user?.country == "" {
            self.tfCountry.attributedPlaceholder = NSAttributedString(string: "Country (Optional)", attributes: attributes)
        }else {
            self.tfCountry.attributedText = NSAttributedString(string: "", attributes: textFieldattributes)
        }
        
        if user?.city == "" {
            self.tfCity.attributedPlaceholder = NSAttributedString(string: "City (Optional)", attributes: attributes)
        }else {
            self.tfCity.attributedText = NSAttributedString(string: "", attributes: textFieldattributes)
        }
        
        self.tfPhone.attributedText = NSAttributedString(string: "", attributes: textFieldattributes)
        
    }
    
    func didUpdateUserSuccessfully(resultDict: AnyObject, resultStatus: Bool) {        
        self.removeSpinner()
        
        if let json = resultDict as? [String: Any] {
            
            if json["status"] as! Bool == false {
                
            }else {
                
                if let response:UserResponse = Mapper<UserResponse>().map(JSON: json) {
                self.removeSpinner()
                if response.status == true {
                    let userUpdated = response.loginResponseData
                        
                        do {
                        
                                            let user : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
                        
                        
                        //                    let subscriptionLocal : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
                        //                    for s in subscriptionLocal {
                        //                        AppUtility.sharedInstance.getContext().delete(s)
                        //                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        //                    }
                        
                                            if let userToSave = user.first {
                        
                                                userToSave.userId = Int16(userUpdated?.userId ?? -1)
                                                userToSave.firstName = userUpdated?.firstName ?? ""
                                                userToSave.lastName = userUpdated?.lastName ?? ""
                                                userToSave.phone = userUpdated?.phone ?? ""
                                                userToSave.dateOfBirth = userUpdated?.dateOfBirth?.changeDateFormat() ?? ""
                                                userToSave.city = userUpdated?.city ?? ""
                                                userToSave.country = userUpdated?.country ?? ""
                                                userToSave.email = userUpdated?.email ?? ""
                                                userToSave.dialCode = userUpdated?.dialCode ?? ""
                                                userToSave.emailStatus = userUpdated?.emailStatus ?? false
                                                userToSave.phoneStatus = userUpdated?.phoneStatus ?? false
                                                userToSave.active = userUpdated?.active ?? true
                        
                                                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                        
                                            }
                        
                        
                                        }
                                        catch {
                                            print("Error fetching..")
                    }
                }
                }
                
//                do {
//                    
//                    let user : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
//            
//                    
////                    let subscriptionLocal : [SubscriptionLocal] = try AppUtility.sharedInstance.getContext().fetch(SubscriptionLocal.fetchRequest())
////                    for s in subscriptionLocal {
////                        AppUtility.sharedInstance.getContext().delete(s)
////                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
////                    }
//                    
//                    if let userToSave = user.first {
//                        
//                        userToSave.userId = Int16(self.user?.userId ?? -1)
//                        userToSave.firstName = self.user!.firstName ?? ""
//                        userToSave.lastName = self.user!.lastName ?? ""
//                        userToSave.phone = self.user!.phone ?? ""
//                        userToSave.dateOfBirth = self.user!.dateOfBirth?.changeDateFormat() ?? ""
//                        userToSave.city = self.user!.city ?? ""
//                        userToSave.country = self.user!.country ?? ""
//                        userToSave.email = self.user!.email ?? ""
//                        userToSave.dialCode = self.user!.dialCode ?? ""
//                        userToSave.emailStatus = self.user!.emailStatus ?? false
//                        userToSave.phoneStatus = self.user!.phoneStatus ?? false
//                        userToSave.active = self.user!.active ?? true
//                                                 
//                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//                        
//                    }
//                
//                    
//                }
//                catch {
//                    print("Error fetching..")
//                }
                let alert = UIAlertController(title: "Alert", message: "Profile updated successfully", preferredStyle: UIAlertController.Style.alert)
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                    action in
                    self.dismiss(animated: true, completion: nil)
                    self.dismiss(animated: false) {
                        DispatchQueue.main.async {
                            self.getLocalAccountData()
                        }
                    }
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    func didUpdateUserError(_ error: NSError, resultStatus: Bool) {
        self.removeSpinner()
        print(error.debugDescription)
        print(error.code)
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    func didVerifyPhoneNumberSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        self.removeSpinner()
        if let json = resultDict as? [String: Any] {
            if json["status"] as! Bool == false {
                
                //                let message = (json["message"] as! Dictionary<String, Any>)
                //                if let error = message.keys.first {
                //                    if error == "phone"{
                //                        let errorMessage = message["phone"] as! String
                //                        //self.phoneError(message: errorMessage)
                //                    }
                //                }
                
                self.phoneError(message: "Invalid phone number")
                
            }else {
                
                let vc =  LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                vc.token = AppUtility.sharedInstance.getToken()
                vc.emailOrPhone = self.phone
                vc.message = "Enter the verification code we sent you to phone number " + self.selectedCountry + self.phone
                vc.isFromMyAccount = true
                self.show(vc, sender: self)
                
            }
        }
    }
    
    func didFailWithVerifyPhoneNumberError(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
        self.removeSpinner()
        if error.code == 401 {
            self.checkUnAuthorized()
        }
    }
    
    func phoneError(message: String) {
        
        self.vPhone.layer.borderWidth = 1.0
        self.vPhone.layer.borderColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR).cgColor
        self.vPhone.layer.cornerRadius = 5.0
        self.ivPhone.tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        
        let x: CGFloat = 0
        let y: CGFloat = 0
        let height: CGFloat = 24.0
        
        let label = UILabel(frame: CGRect(x: x, y: y, width: UIScreen.main.bounds.width, height: height))
        
        let constraints = [
            label.heightAnchor.constraint(equalToConstant: height),
            label.topAnchor.constraint(equalToSystemSpacingBelow: vPhone.bottomAnchor, multiplier: 0.5),
            vPhone.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            vPhone.trailingAnchor.constraint(equalTo: label.trailingAnchor),
        ]
        
        label.text = message
        label.font = UIFont(name: "Montserrat-Light", size: 11)
        label.textColor = AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(label)
        contentView.layoutIfNeeded()
        
        NSLayoutConstraint.activate(constraints)
        
        for constraint in contentView.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 40.0
            }
        }
    }
    
    func removePhoneError(message: String) {
        
        self.vPhone.layer.borderWidth = 1.0
        self.vPhone.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        self.vPhone.layer.cornerRadius = 5.0
        self.ivPhone.tintColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        for constraint in contentView.constraints {
            if constraint.identifier == "fpConstraint" {
                constraint.constant = 16.0
            }
        }
    }
    
    func removeOtherErrors() {
        for eachView in contentView.subviews {
            if eachView is UILabel {
                if (eachView as! UILabel).textColor == AppUtility.sharedInstance.hexStringToUIColor(hex: ERROR_COLOR) {
                    eachView.removeFromSuperview()
                }
            }
        }
    }
    
    func getLocalAccountData() {
        
        do {
            let userInfoList : [UserInfo] = try AppUtility.sharedInstance.getContext().fetch(UserInfo.fetchRequest())
            
            for u in userInfoList {
                
                let subscription = Subscription(subscriptionId: Int(u.subscription?.subscriptionId ?? 0), subscriptionName: u.subscription?.subscriptionName ?? "", userId: Int(u.subscription?.userId ?? 0), validityPeriod: u.subscription?.validityPeriod ?? "", price: u.subscription?.price ?? "", createdBy: u.subscription?.createdBy ?? "", isActive: u.subscription?.isActive ?? false, expiresAt: u.subscription?.expiresAt ?? "", createdAt: u.subscription?.createdAt ?? "", updatedAt: u.subscription?.updatedAt ?? "")
                
                let userInfo = User(userId: Int(u.userId), firstName: u.firstName ?? "", lastName: u.lastName ?? "", phone: u.phone ?? "", dateOfBirth: u.dateOfBirth ?? "", city: u.city ?? "", country: u.country ?? "", email: u.email ?? "", emailStatus: u.emailStatus, phoneStatus: u.phoneStatus, countryCodeId: Int(u.countryCodeId), dialCode: u.dialCode ?? "", active: u.active, subscription: subscription)
                self.user = userInfo
                
            }
            self.lblName.text = "\(user?.firstName ?? "") \(user?.lastName ?? "")"
            self.tfEmail.attributedPlaceholder = NSAttributedString(string: user?.email ?? "", attributes: attributes)
            self.lblPaymentStatus.text = user?.subscription?.subscriptionName ?? ""
            self.lblDaysPending.text = "\(user?.subscription?.expiresAt?.getDaysFromStringDate() ?? 0)" ?? ""
            self.showPlaceholders()
            
        }catch{
            print("Error Fetching")
        }
    
}
    
    func changeDateFormat(date: String) -> String {
        if date != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let d = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "d.MM.y"
            let goodDate = dateFormatter.string(from: d!)
            return goodDate
        }else {
            return ""
        }
        
    }

}

                                    
