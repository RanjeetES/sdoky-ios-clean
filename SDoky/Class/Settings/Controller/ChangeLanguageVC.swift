//
//  ChangeLanguageVC.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/4/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit

protocol ChangeLanguageDelegate {
    func didSelectLanguage(language:Int)
}

class LanguageTableViewCell: UITableViewCell {
    @IBOutlet weak var lblLanguage: UIButton!
}

class ChangeLanguageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let languages = ["English","French","German","Spanish","Russian","Japanese", "Chinese (Simplified)"]
    var selectedLanguage :  Int = 0
    var delegate:ChangeLanguageDelegate?
    
    @IBOutlet weak var tvLanguage: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvLanguage.delegate = self
        tvLanguage.dataSource = self
        
        if let languageSelected = AppUtility.sharedInstance.getLanguageSelected() as? Int {
            self.selectedLanguage = languageSelected
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.checkExpiry()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return languages.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "language_cell", for: indexPath) as! LanguageTableViewCell
        cell.lblLanguage.setTitle(languages[indexPath.row], for: .normal)
        
        cell.lblLanguage.tag = indexPath.row
        cell.lblLanguage.addTarget(self, action: #selector(languageSelected(_:)), for: .touchUpInside)
        
        cell.lblLanguage.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_BORDER_COLOR).cgColor
        cell.lblLanguage.layer.borderWidth = 1.0
        
        if indexPath.row == selectedLanguage {
            cell.lblLanguage.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
            cell.lblLanguage.layer.borderWidth = 1.0
        }
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//        let selectedCell = tableView.cellForRow(at: indexPath) as! LanguageTableViewCell
//
//        selectedCell.lblLanguage.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
//        selectedCell.lblLanguage.layer.borderWidth = 1.0
//
//        // selectedCell.lblLanguage.addTarget(self, action: #selector(self.customize(_:)), for: .touchUpInside)
//    }
    
    @objc func customize(_ sender: UIButton) {
        
        sender.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        sender.layer.borderWidth = 1.0
    }
    
    @objc func languageSelected(_ sender: UIButton) {
        
        sender.layer.borderColor = hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR).cgColor
        sender.layer.borderWidth = 1.0
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didSelectLanguage(language: sender.tag)
        
    }
    
}
