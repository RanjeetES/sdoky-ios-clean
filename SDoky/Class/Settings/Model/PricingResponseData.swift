//
//  PricingResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 11/21/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class PricingResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    var pricingId:Int?
    var planName:String?
    var price: Double?
    var years: String?
    var description: String?
    var createdAt:String?
    var updatedAt: String?
    
    func mapping(map: Map) {
        
        pricingId <- map["pricing_id"]
        planName <- map["plan_name"]
        price <- map["price"]
        years <- map["years"]
        description <- map["description"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        
    }
    
}
