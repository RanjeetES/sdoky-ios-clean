//
//  EbookResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 19/01/2021.
//  Copyright © 2021 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class EbookResponseData: Mappable {
    required init?(map: Map) {
        
    }
    var link:String?
    
    func mapping(map: Map) {
        link <- map["link"]
        
    }

}
