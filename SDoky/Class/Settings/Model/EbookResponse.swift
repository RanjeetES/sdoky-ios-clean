//
//  EbookResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 19/01/2021.
//  Copyright © 2021 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class EbookResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var ebookResponseData:EbookResponseData?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        ebookResponseData <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }

}

