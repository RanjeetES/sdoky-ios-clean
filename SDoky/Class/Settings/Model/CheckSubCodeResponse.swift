//
//  CheckSubCodeRespinse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 11/9/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class CheckSubCodeResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var data:CheckSubCodeResponseData?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }

}
