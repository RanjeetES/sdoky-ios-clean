//
//  CheckSubCodeResponseData.swift
//  SDoky
//
//  Created by Ranjeet Sah on 11/9/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class CheckSubCodeResponseData: Mappable {
    required init?(map: Map) {
        
    }
    
    var couponId: Int?
    var pricingId: Int?
    var adminId: Int?
    var userId: Int?
    var couponCode: String?
    var validityPeriod: String?
    var percentage: Int?
    var isApplied: Bool?
    var expiresAt: String?
    var createdAt:String?
    var updatedAt: String?
    
    
    //var discountEndDate:String?
    //var discountPercent:Int?
    //var discountStartDate:String?
    
    
    //var subCode: String?
    //var subscriptionCodeId: Int?
    
   
    
    func mapping(map: Map) {
        
        couponId <- map["coupon_id"]
        pricingId <- map["pricing_id"]
        adminId <- map["admin_id"]
        userId <- map["user_id"]
        couponCode <- map["coupon_code"]
        validityPeriod <- map["validity_period"]
        percentage <- map["percentage"]
        isApplied <- map["is_applied"]
        expiresAt <- map["expires_at"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        
        
    }
    
}

