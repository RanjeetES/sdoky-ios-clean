//
//  UserResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/5/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class UserResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var loginResponseData:User?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        loginResponseData <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }

}

                                                                    									

    																																																																															
