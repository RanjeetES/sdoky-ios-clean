//
//  PricingResponse.swift
//  SDoky
//
//  Created by Ranjeet Sah on 11/21/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper
class PricingResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    var pricingData:[PricingResponseData]?
    var statusCode: Int?
    var message:String?
    var status: Bool?
    
    func mapping(map: Map) {
        status <- map["status"]
        pricingData <- map["data"]
        statusCode <- map["status_code"]
        message <- map["message"]
        
    }

}
