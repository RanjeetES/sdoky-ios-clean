//
//  SettingsAPI.swift
//  SDoky
//
//  Created by Ranjeet Sah on 8/5/20.
//  Copyright © 2020 E-Signature. All rights reserved.
//

import UIKit
import ObjectMapper

@objc protocol SettingsAPIDelegate {
    @objc optional func didGetUserSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithGetUserError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didDeleteUserSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithDeleteUserError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didChangeAccountPasswordSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailChangeAccountPasswordError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didUpdateUserSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didUpdateUserError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didVerifyPhoneNumberSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithVerifyPhoneNumberError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetSubCodeSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithSubCodeError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetPricingSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithPricingError(_ error: NSError,resultStatus:Bool)
    
    @objc optional func didGetEbookSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithEbookError(_ error: NSError,resultStatus:Bool)
}

class SettingsAPI {
    
    var delegate:SettingsAPIDelegate?
    
    func getUserDetails() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+userDetailsAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetUserSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithGetUserError!(error, resultStatus: false)
        }
    }
    
    func deleteAccount(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+deleteUserAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didDeleteUserSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithDeleteUserError!(error, resultStatus: false)
        }
    }
    
    func changeAccountPassword(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+changeAccountPasswordAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didChangeAccountPasswordSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailChangeAccountPasswordError!(error, resultStatus: false)
        }
        
    }
    
    func updateProfile(params: [String: Any]) {
        
        APIHandler.sharedInstance.performPOSTRequest(
            (BASE_URL+updateUserAPI as NSString),
            params: params as [String : AnyObject],
            success: { (response) in
                self.delegate?.didUpdateUserSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didUpdateUserError!(error, resultStatus: false)
        }
        
    }
    
    func verifyPhoneNumber() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+verifyPhoneNumberAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didVerifyPhoneNumberSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithVerifyPhoneNumberError!(error, resultStatus: false)
        }
    }
    
    func checkSubCode(params: [String: Any]) {
           
           APIHandler.sharedInstance.performPOSTRequest(
               (BASE_URL+checkSubCodeAPI as NSString),
               params: params as [String : AnyObject],
               success: { (response) in
                   self.delegate?.didGetSubCodeSuccessfully!(resultDict: response, resultStatus: true)
           }) { (error, response) in
               self.delegate?.didFailWithSubCodeError!(error, resultStatus: false)
           }
       }
    
    func getPricingDetails() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+pricingAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetPricingSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithPricingError!(error, resultStatus: false)
        }
    }
    
    func getEbook() {
        
        APIHandler.sharedInstance.performGETRequest(
            (BASE_URL+getEbookAPI as NSString),
            params: [:] as [String : Any] as NSDictionary,
            success: { (response) in
                self.delegate?.didGetEbookSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error, response) in
            self.delegate?.didFailWithEbookError!(error, resultStatus: false)
        }
    }
    
}
