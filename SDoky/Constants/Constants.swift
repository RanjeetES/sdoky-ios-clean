//
//  Constants.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

//Placeholder Attributes
let attributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_PLACEHOLDER_COLOR),
    //NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14, weight: .light)
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 13)
]

let textattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 13)
]

let textattributesHead = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 13)
]

let textFieldattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 13)
]

let tableOptionsattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_INPUT_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 12)
]

let languageTextattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 12)
]

let tabaAttributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6"),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 10)
]

let tabSelectedaAttributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 10)
]

let occurenceTextNormalattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXT_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 13)
]

let occurenceTextHighlightattributes = [
    NSAttributedString.Key.foregroundColor: AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR),
    NSAttributedString.Key.font:UIFont(name: "Montserrat-Light", size: 13)
]


let HOME_STORY_BOARD: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
let LOCALIZATION_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Localization", bundle: nil)
let DASHBOARD_STORY_BOARD: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
let SMAP_STORY_BOARD: UIStoryboard = UIStoryboard(name: "SMap", bundle: nil)
let REGISTER_USER_STORY_BOARD: UIStoryboard = UIStoryboard(name: "RegisterUser", bundle: nil)
let LOG_IN_STORY_BOARD: UIStoryboard = UIStoryboard(name: "LogIn", bundle: nil)
let REGISTER_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Register", bundle: nil)
let SETTING_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)


let languages = ["En","Fr","De","Es","Ru","Jp", "Cn"]

let BASE_URL = "https://api.sdoky.com"
let loginAPI = "/api/v1/sign-in"
let signupStep1 = "/api/v1/sign-up-step1"
let signupStep2 = "/api/v1/sign-up-step2"
let forgotPassword = "/api/v1/forgot-password"
let otpCheckAPI = "/api/v1/otp-check"
let changePasswordAPI = "/api/v1/change-password"
let countryCodesAPI = "/api/v1/country-codes"
let userDetailsAPI = "/api/v1/get-user"
let deleteUserAPI = "/api/v1/delete-user"
let changeAccountPasswordAPI = "/api/v1/change-account-password"
let updateUserAPI = "/api/v1/update-user"
let verifyPhoneNumberAPI = "/api/v1/phone-verify"
let createFolderAPI = "/api/v1/create-folder"
let updateFolderAPI = "/api/v1/update-folder"
let deleteFolderAPI = "/api/v1/delete-folder"
let getFolderAPI = "/api/v1/get-folders"
let getFolderDocsAPI = "/api/v1/get-folders-docs"
let sendEmailVerificationAPI = "/api/v1/resend-email-confirmation"
let getDocAPI = "/api/v1/get-doc"
let createDocAPI = "/api/v1/create-doc"
let updateDocAPI = "/api/v1/update-doc"
let deleteDocAPI = "/api/v1/delete-doc"
let getDocPdfAPI = "/api/v1/get-doc-pdf"
let getSMapPdfAPI = "/api/v1/get-smap-pdf"
let getFolderPdfAPI = "/api/v1/get-folder-docs-pdf"
let syncFolderAPI = "​/api​/v1​/folder-doc-sync"
let internetSearchAPI = "​/api/v1/internet-search"
let fileToTextAPI = "/api/v1/file-to-text"
let subscriptionAPI = "/api/v1/add-subscription"
let checkSubCodeAPI = "/api/v1/check-sub-code"
let pricingAPI = "/api/v1/pricings"
let registerDeviceAPI = "/api/v1/register-device"
let getEbookAPI = "/api/v1/get-ebook"

//SMap
let getSMapAPI = "/api/v1/get-smaps"
let createSMapAPI = "/api/v1/create-smap"



//CONSTANTS
let IS_LOGGED_IN_KEY = "IS_LOGGED_IN_KEY"
let HAS_LOCAL_DATA = "HAS_LOCAL_DATA"
let HAS_LOCAL_SMAP = "HAS_LOCAL_SMAP"
let TOKEN_KEY = "TOKEN_KEY"
let PLAYER_KEY = "PLAYER_KEY"
let CUSTOMER_ID_KEY = "CUSTOMER_ID_KEY"
let CUSTOMER_EMAIL = "CUSTOMER_EMAIL"
let IS_REMEMBER_ME_CLICKED = "IS_REMEMBER_ME_CLICKED"
let LANGUAGE_SELECTED = "LANGUAGE_SELECTED"
let IS_LOGGED_IN_BEFORE = "IS_LOGGED_IN_BEFORE"

//Color Codes
let BLUE_COLOR = "#0064b2"
let GREEN_COLOR = "#008000"
let PRIMARY_COLOR = "1873DC"
let ERROR_COLOR = "E40E0E"
let TEXTFIELD_PLACEHOLDER_COLOR = "B6BFD5"
let TEXTFIELD_INPUT_COLOR = "0B1223"
let TEXT_COLOR = "091128"



let TEXTFIELD_BORDER_COLOR = "B6BFD5"
let TEXTFIELD_ICON_COLOR = "0E80E4"
// Error Messages
let REQUIRED_FIELD_TEXT = "Required Field"


//Data List Menu
let NEW_DATA = "New data"
let RENAME = "Rename"
let DOWNLOAD_AS_PDF = "Download as pdf"
let DELETE = "Delete"
let EDIT = "Edit"
let MOVE = "Move"
let SAVE_AS_PDF = "Download as pdf"

//Offline Action
let OFFLINE_DELETE = "delete"
let OFFLINE_EDIT = "edit"
let OFFLINE_ADD = "new"

//let hashtagRegex = "(([#])((([a-zA-Z\\-\'\"\\s])\\w+){1,5})([,]))"
//let hashtagRegex = "(([\\#])(([\\w;\\-\'\"]\\s*){1,25})([,]))"
let hashtagRegex = "(([\\#])(((?!\\#|\\,|\\%|\\^|\\&|\\*|\\(|\\)|\\=|\\+).\\s*){1,25})([,]))"


  
//Proj
let SIGNATURESERVER = "http://5b246d42799f.ngrok.io"
let DATASERVER = "http://291776d8e503.ngrok.io"
