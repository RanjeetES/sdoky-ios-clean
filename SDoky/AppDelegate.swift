//
//  AppDelegate.swift
//  SDoky
//
//  Created by Ranjeet Sah on 7/14/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Alamofire
import IQKeyboardManagerSwift
import Firebase
import FirebaseCrashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate, LogInAPIDelegate {
    //    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    //
    //
    //    var backgroundTaskTimer:Timer! = Timer()
    
    
    var window: UIWindow?
    
    func setUpNavBar(){
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.isTranslucent = false
        navBarAppearance.tintColor = UIColor.black
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setUpNavBar()
        
        // IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        print("FCM registration token: \(Messaging.messaging().fcmToken)")
        IQKeyboardManager.shared.enableAutoToolbar = false
        //OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        UITabBarItem.appearance().setTitleTextAttributes(tabaAttributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(tabSelectedaAttributes, for: .selected)
        UITabBar.appearance().tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        
        //        if let selectedLang = AppUtility.sharedInstance.getLanguageSelected() as? Int {
        //
        //        }else {
        //            AppUtility.sharedInstance.setLangugeSelected(id: 0)
        //        }
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        
        AppUtility.sharedInstance.setLangugeSelected(id: AppUtility.sharedInstance.getLanguageSelected())
        
        var mainViewController: UIViewController?
        
        if AppUtility.sharedInstance.getIsRememberMe() == true  {
            
            let tabBarController = UITabBarController()
            let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
            let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
            let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
            let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
            let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
            tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
            tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
            mainViewController = tabBarController
            
        }else {
            if AppUtility.sharedInstance.isConnectedToInternet() == true {
                mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            }else {
                mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "PasswordLoginScreenVC") as! PasswordLoginScreenVC
            }
            
        }
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController!)
        nvc.navigationBar.isHidden = true
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
        
        
        //        var mainViewController: UIViewController?
        //
        //        if AppUtility.sharedInstance.getIsRememberMe() == true  {
        //            mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        //        }else {
        //            mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        //        }
        //
        //
        //        //let leftMenuViewController = SLIDER_STORY_BOARD.instantiateViewController(withIdentifier: "SliderVC") as! SliderVC
        //
        //        //let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController!)
        //        //let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftMenuViewController)
        //        //      slideMenuController.automaticallyAdjustsScrollViewInsets = true
        //        //slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        //        self.window?.rootViewController = mainViewController
        //        self.window?.makeKeyAndVisible()
        
        //START OneSignal initialization code
        //        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
        //
        //        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
        //        OneSignal.initWithLaunchOptions(launchOptions,
        //                                        appId: ONE_SIGNAl_KEY,
        //                                        handleNotificationAction: nil,
        //                                        settings: onesignalInitSettings)
        //        OneSignal.add(self as? OSSubscriptionObserver)
        //
        //        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        //
        //        // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
        //        OneSignal.promptForPushNotifications(userResponse: { accepted in
        //            print("User accepted notifications: \(accepted)")
        //        })
        //
        //        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        //        if let playerId = status.subscriptionStatus.userId  {
        //            AppUtility.sharedInstance.setPlayerId(playerId: playerId)
        //        }
        
        
        
        //self.SetupPushNotification(application: application)
        //END OneSignal initializataion code
        return true
        
    }
    
    
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        print(dataDict)
        
//        Messaging.messaging().subscribe(toTopic: "testnotify") { error in
//            print("Subscribed to testnotify topic")
//        }
        
    }
    

    
    
    
    
    
    
    // Setup appdelegate for push notifications
    func SetupPushNotification(application: UIApplication) -> () {
        
        // UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge])
        //{(granted,error) in
        //   if granted{
        //     DispatchQueue.main.async {
        //       application.registerForRemoteNotifications()
        // }
        //} else {
        //     print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
        //}
        //}
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("Successful registration. Token is:")
        //print(tokenString(deviceToken)) // this method will convert token "Data" to string formate
    }
    
    
    // Method: 2 - Failed registration. Explain why.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    
    
    // Method: 3 - In this method app will receive notifications in [userInfo]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        print(userInfo.description)
        print(userInfo.debugDescription)
//        if let targetValue = userInfo["aps"] as? [String:Any]
//        {
//            if let alert = targetValue["alert"] as? [String:Any]
//            {
//                if let titleN = alert["title"] as? String
//               {
//                if titleN == "Trial expiring" {
//                    self.coordinateToPricingVC()
//                }else if titleN == "Admin pushed" {
//                    if let docId = alert["doc_id"] as? Int {
//                        self.coordinateToViewDocVC(docId: docId)
//                    }
//                }
//               }
//            }
//        }
        
        if let titleN = (userInfo["aps"] as? NSDictionary)?["category"] as? String {
            if titleN.lowercased() == "pricing".lowercased() {
                self.coordinateToPricingVC()
            }else if titleN.lowercased() == "new_admin_doc".lowercased() {
                if let docId = userInfo["doc_id"] as? String {
                    self.coordinateToViewDocVC(docId: Int(docId) ?? 0)
                }
            }
            
        }
        
        completionHandler()
    }
    
    private func coordinateToPricingVC()
    {
        guard let window = UIApplication.shared.keyWindow else { return }
//        let pricingVC = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
//        pricingVC.isFromNotification = true
//        let navController = UINavigationController(rootViewController: pricingVC)
//        navController.modalPresentationStyle = .fullScreen
//
//        // you can assign your vc directly or push it in navigation stack as follows:
//        window.rootViewController = navController
//        window.makeKeyAndVisible()
        
        
        
        if AppUtility.sharedInstance.getToken() == "" {
            
            let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVC.isFromNotification = true
            loginVC.docId = -1
            
            let navController = UINavigationController(rootViewController: loginVC)
            navController.modalPresentationStyle = .fullScreen

            window.rootViewController = navController
            window.makeKeyAndVisible()
            
        }else {
            
            let pricingVC = SETTING_STORY_BOARD.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
            pricingVC.isFromNotification = true
            let navController = UINavigationController(rootViewController: pricingVC)
            navController.modalPresentationStyle = .fullScreen

            // you can assign your vc directly or push it in navigation stack as follows:
            window.rootViewController = navController
            window.makeKeyAndVisible()
            
        }
        
    }
    
    private func coordinateToViewDocVC(docId: Int)
    {
        guard let window = UIApplication.shared.keyWindow else { return }
        
        if AppUtility.sharedInstance.getToken() == "" {
            
            let loginVC = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVC.isFromNotification = true
            loginVC.docId = docId
            
            let navController = UINavigationController(rootViewController: loginVC)
            navController.modalPresentationStyle = .fullScreen

            window.rootViewController = navController
            window.makeKeyAndVisible()
            
        }else {
            
            let viewDocVC = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ViewDocVC") as! ViewDocVC
            viewDocVC.isFromNotification = true
            viewDocVC.docId = docId
            
            let navController = UINavigationController(rootViewController: viewDocVC)
            navController.modalPresentationStyle = .fullScreen

            window.rootViewController = navController
            window.makeKeyAndVisible()
            
        }
        
        
    }
    
    
    //code to make a token string
    func tokenString(_ deviceToken:Data) -> String{
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes{
            token += String(format: "%02x",byte)
            print(token)
        }
        return token //  this token will be passed to your backend that can be written in php, js, .net etc.
    }
    
    
    
    
//    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
//        // if !stateChanges.from.subscribed && stateChanges.to.subscribed {
//        //   print("Subscribed for OneSignal push notifications!")
//        //}
//        //print("SubscriptionStateChange: \n\(stateChanges)")
//        
//        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
//    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        if AppUtility.sharedInstance.getIsRememberMe() == false {
            
            AppUtility.sharedInstance.setToken(token: "")
            AppUtility.sharedInstance.setIsRememberMe(rememberMeFlag: false)
            AppUtility.sharedInstance.setCustomerId(customerId: 0)
            AppUtility.sharedInstance.setCustomerEmail(customerEmail: "")
        }
        
    }
    
    
    //    func SetupPushNotification(application: UIApplication) -> () {
    //
    //        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge])
    //        {(granted,error) in
    //            if granted{
    //                DispatchQueue.main.async {
    //                    application.registerForRemoteNotifications()
    //                }
    //            } else {
    //                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
    //            }
    //        }
    //    }
    
    
    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
    //
    //        print("Device Token",deviceTokenString)
    //
    //        let userPushTokenDefaults = UserDefaults.standard
    //        userPushTokenDefaults.removeObject(forKey: "deviceTokenString")
    //        userPushTokenDefaults.set(deviceTokenString, forKey: "deviceTokenString")
    //    }
    
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //doBackgroundTask()
    }
    
    
    //    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    //        print("DEVICE TOKEN = \(deviceToken)")
    //    }
    
    // MARK: UISceneSession Lifecycle
    
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "SDoky")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
