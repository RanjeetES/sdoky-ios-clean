//
//  SceneDelegate.swift
//  Covid-19
//
//  Created by Ranjeet Sah on 4/19/20.
//  Copyright © 2020 Ranjeet Sah. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var time = 60
    
    var window: UIWindow?
    
    var timerBack  = Timer()
    func doBackgroundTask() {
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            self.beginBackgroundUpdateTask()
            
            print("Background time remaining = \(UIApplication.shared.backgroundTimeRemaining) seconds")
            
            self.timerBack = Timer.scheduledTimer(timeInterval: TimeInterval(self.time), target: self, selector: #selector(self.displayAlert), userInfo: nil, repeats: true)
            RunLoop.current.add(self.timerBack, forMode: RunLoop.Mode.default)
            RunLoop.current.run()
            
            self.endBackgroundUpdateTask()
        }
    }
    
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    
    func beginBackgroundUpdateTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskIdentifier.invalid
    }
    
    @objc func displayAlert(){
        print("Running on background")
        
    }
    
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        UITabBarItem.appearance().setTitleTextAttributes(tabaAttributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(tabSelectedaAttributes, for: .selected)
        UITabBar.appearance().tintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: TEXTFIELD_ICON_COLOR)
        
        
        
        if #available(iOS 13.0, *) {
            guard let _ = (scene as? UIWindowScene) else { return }
        } else {
            // Fallback on earlier versions
        }
        
        var mainViewController: UIViewController?
        
        if AppUtility.sharedInstance.getIsRememberMe() == true  {
            // mainViewController = DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            
            
            let tabBarController = UITabBarController()
            let dataVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "DataVC") as! DataVC
            let scanVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
            let writeVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "WriteVC") as! WriteVC
            let smapVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SMapVC") as! SMapVC
            let settingsVC =  DASHBOARD_STORY_BOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            tabBarController.viewControllers = [dataVC, scanVC, writeVC, smapVC, settingsVC]
            tabBarController.tabBar.unselectedItemTintColor = AppUtility.sharedInstance.hexStringToUIColor(hex: "A2A2B6")
            tabBarController.tabBar.roundCorners(corners: [.topLeft, .topRight], radius: 15.0)
            tabBarController.navigationController?.navigationBar.isHidden = true
            mainViewController = tabBarController
        }else {
            if AppUtility.sharedInstance.isConnectedToInternet() == true {
                mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            }else {
                mainViewController = LOG_IN_STORY_BOARD.instantiateViewController(withIdentifier: "PasswordLoginScreenVC") as! PasswordLoginScreenVC
            }
            
        }
        
        
        
        
        //let leftMenuViewController = SLIDER_STORY_BOARD.instantiateViewController(withIdentifier: "SliderVC") as! SliderVC
        
        //let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController!)
        //let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftMenuViewController)
        //      slideMenuController.automaticallyAdjustsScrollViewInsets = true
        //slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController!)
        nvc.navigationBar.isHidden = true
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
        
        
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        //        doBackgroundTask()
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        
        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
    
    
}
